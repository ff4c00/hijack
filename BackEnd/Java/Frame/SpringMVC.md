
<!-- TOC -->

- [简介](#简介)
  - [什么是MVC](#什么是mvc)
  - [基本概念](#基本概念)
  - [DAO层](#dao层)
  - [Service层](#service层)
    - [Service逻辑层设计](#service逻辑层设计)
  - [Controller层](#controller层)
  - [View层](#view层)
- [参考资料](#参考资料)

<!-- /TOC -->

# 简介

## 什么是MVC

MVC是web开发通用的架构方式,MVC 模式代表 Model-View-Controller(模型-视图-控制器) 模式.这种模式用于应用程序的分层开发.

MVC的核心思想是业务数据抽取和业务数据呈现相分离.

* Model(模型) - 模型代表一个存取数据的对象或 JAVA POJO.它也可以带有逻辑,在数据变化时更新控制器.
* View(视图) - 视图代表模型包含的数据的可视化.
* Controller(控制器) - 控制器作用于模型和视图上.它控制数据流向模型对象,并在数据变化时更新视图.它使视图与模型分离开.

![](http://www.yiibai.com/uploads/tutorial/20160116/1-1601161F914292.png)

## 基本概念

用户请求->DispatcherService->controller->model->DispatcherService->view->DispatcherService->用户呈现

名称|含义
-|-
DispatcherService|前端控制器
HandlerAdapter|DispatcherService内部的一个类,controller的一种表现形式.
HandlerInterceptor|拦截器
HandleerMapping|告诉DispatcherServlet如何到正确的controller

## DAO层

> data access object,即数据访问对象.<br>
数据可能保存到各种数据库,或者各种文件,或者内存.<br>
dao层隐藏了这种实现细节,更换数据库或者文件或者内存,对调用dao的更高层来说不受影响.


> DAO层主要是做数据持久层的工作,<br>
负责与数据库进行联络的一些任务都封装在此,<br>
DAO层的设计首先是设计DAO的接口,<br>
然后在Spring的配置文件中定义此接口的实现类,<br>
然后就可在模块中调用此接口来进行数据业务的处理,<br>
而不用关心此接口的具体实现类是哪个类,显得结构非常清晰,<br>
DAO层的数据源配置,以及有关数据库连接的参数都在Spring的配置文件中进行配置.

DAO设计的总体规划需要和设计的表,和实现类之间一一对应.

DAO层所定义的接口里的方法都大同小异,<br>
这是由在DAO层对数据库访问的操作来决定的,<br>
对数据库的操作,<br>
基本要用到的就是新增,更新,删除,查询等方法.<br>
因而DAO层里面基本上都应该要涵盖这些方法对应的操作.<br>
除此之外,可以定义一些自定义的特殊的对数据库访问的方法.   

在DAO层定义的一些方法,在Service层并没有使用,那为什么还要在DAO层进行定义呢？这是由我们定义的需求逻辑所决定的.DAO层的操作 经过抽象后基本上都是通用的,因而我们在定义DAO层的时候可以将相关的方法定义完毕,这样的好处是在对Service进行扩展的时候不需要再对DAO层进行修改,提高了程序的可扩展性. 

## Service层

> Model层中分离出了service层.

> Service层主要负责业务模块的逻辑应用设计.<br>
同样是首先设计接口,再设计其实现的类,接着再Spring的配置文件中配置其实现的关联.<br>
这样就可以在应用中调用Service接口来进行业务处理.<br>
Service层的业务实现,具体要调用到已定义的DAO层的接口,<br>
封装Service层的业务逻辑有利于通用的业务逻辑的独立性和重复利用性,程序显得非常简洁.

### Service逻辑层设计 

Service层是建立在DAO层之上的,<br>
建立了DAO层后才可以建立Service层,而Service层又是在Controller层之下的,<br>
因而Service层应该既调用DAO层的接口,又要提供接口给Controller层的类来进行调用,它刚好处于一个中间层的位置.<br>
每个模型都有一个Service接口,每个接口分别封装各自的业务处理方法.

## Controller层

> Controller层负责具体的业务模块流程的控制,<br>
在此层里面要调用Serice层的接口来控制业务流程,<br>
控制的配置也同样是在Spring的配置文件里面进行,<br>
针对具体的业务流程,会有不同的控制器,<br>
具体的设计过程中可以将流程进行抽象归纳,<br>
设计出可以重复利用的子单元流程模块,<br>
这样不仅使程序结构变得清晰,<br>
也大大减少了代码量.

## View层

> View层 此层与控制层结合比较紧密,主要负责前台页面内容的渲染.

DAO层,Service层这两个层次都可以单独开发,<br>
互相的耦合度很低,完全可以独立进行,<br>
这样的一种模式在开发大项目的过程中尤其有优势,<br>
Controller,View层因为耦合度比较高,因而要结合在一起开发,<br>
但是也可以看作一个整体独立于前两个层进行开发.<br>
这样,在层与层之前只需要知道接口的定义,<br>
调用接口即可完成所需要的逻辑单元应用,<br>
一切显得非常清晰简单.  


# 参考资料

> [易百教程 | Spring MVC教程](https://www.yiibai.com/spring_mvc/)

> [CSDN | DAO层,Service层,Controller层、View层](https://blog.csdn.net/zdwzzu2006/article/details/6053006)
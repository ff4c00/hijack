
<!-- TOC -->

- [注释](#注释)
  - [从文档注释生成API文档](#从文档注释生成api文档)
  - [注释](#注释-1)
- [变量和常量](#变量和常量)
  - [关键字](#关键字)
  - [标识符](#标识符)
    - [命名规则](#命名规则)
  - [变量](#变量)
  - [数据类型](#数据类型)
    - [基本数据类型](#基本数据类型)
      - [数值型](#数值型)
        - [整数型](#整数型)
          - [byte](#byte)
          - [short](#short)
          - [int](#int)
          - [long](#long)
        - [浮点类型](#浮点类型)
          - [float](#float)
          - [double](#double)
      - [字符型(char)](#字符型char)
      - [布尔型(boolean)](#布尔型boolean)
    - [引用数据类型](#引用数据类型)
      - [类(class)](#类class)
      - [接口(interface)](#接口interface)
      - [数组](#数组)
  - [变量的使用规则](#变量的使用规则)
  - [自动类型转换](#自动类型转换)
  - [强制类型转换](#强制类型转换)
- [字符串](#字符串)
  - [字符串的不变性](#字符串的不变性)
  - [常用方法](#常用方法)
  - [StringBuilder 类](#stringbuilder-类)
    - [常用方法](#常用方法-1)
  - [常量](#常量)
- [运算符](#运算符)
  - [算术运算符](#算术运算符)
    - [关于++出现位置](#关于出现位置)
  - [赋值运算符](#赋值运算符)
  - [比较运算符](#比较运算符)
  - [逻辑运算符](#逻辑运算符)
    - [关于||与^的区别](#关于与^的区别)
    - [跳过执行](#跳过执行)
  - [条件运算符](#条件运算符)
  - [运算符的优先级](#运算符的优先级)
- [流程控制语句](#流程控制语句)
  - [条件语句](#条件语句)
  - [if](#if)
  - [if...else](#ifelse)
  - [多重 if](#多重-if)
  - [嵌套 if](#嵌套-if)
  - [switch](#switch)
  - [循环语句](#循环语句)
  - [while](#while)
  - [do...while](#dowhile)
  - [for](#for)
    - [省略循环变量初始化](#省略循环变量初始化)
    - [省略循环条件](#省略循环条件)
    - [省略循环变量变化](#省略循环变量变化)
    - [多个循环变量初始化或循环变量变化部分](#多个循环变量初始化或循环变量变化部分)
  - [循环跳转语句](#循环跳转语句)
  - [break](#break)
  - [continue](#continue)
- [数组](#数组-1)
  - [声明数组](#声明数组)
  - [分配空间](#分配空间)
  - [赋值](#赋值)
  - [处理数组中数据](#处理数组中数据)
  - [常用方法](#常用方法-2)
  - [二维数组](#二维数组)
    - [声明数组并分配空间](#声明数组并分配空间)
    - [赋值](#赋值-1)
- [方法](#方法)
  - [如何定义方法](#如何定义方法)
  - [无参无返回值方法](#无参无返回值方法)
  - [无参带返回值方法](#无参带返回值方法)
  - [带参无返回值方法](#带参无返回值方法)
  - [带参带返回值方法](#带参带返回值方法)
  - [方法的重载](#方法的重载)
    - [判断方法重载的依据](#判断方法重载的依据)
- [类和对象](#类和对象)
  - [什么是类和对象](#什么是类和对象)
  - [如何定义类](#如何定义类)
  - [如何使用对象](#如何使用对象)
    - [创建对象](#创建对象)
    - [使用对象](#使用对象)
  - [成员变量和局部变量](#成员变量和局部变量)
  - [构造方法](#构造方法)
    - [构造方法的重载](#构造方法的重载)
  - [static](#static)
    - [静态变量](#静态变量)
    - [静态方法](#静态方法)
    - [静态初始化块](#静态初始化块)
- [封装](#封装)
  - [实现步骤](#实现步骤)
  - [使用包管理类](#使用包管理类)
    - [定义包](#定义包)
    - [系统中的包](#系统中的包)
    - [使用包](#使用包)
  - [访问修饰符](#访问修饰符)
  - [this关键字](#this关键字)
  - [内部类](#内部类)
    - [成员内部类](#成员内部类)
    - [静态内部类](#静态内部类)
    - [方法内部类](#方法内部类)
    - [匿名内部类](#匿名内部类)
- [继承](#继承)
  - [方法重写](#方法重写)
  - [继承初始化顺序](#继承初始化顺序)
  - [final关键字](#final关键字)
  - [super的使用](#super的使用)
  - [Object类](#object类)
- [多态](#多态)
  - [引用多态](#引用多态)
  - [方法多态](#方法多态)
  - [引用类型转换](#引用类型转换)
    - [instanceof运算符](#instanceof运算符)
  - [抽象类](#抽象类)
  - [接口](#接口)
    - [定义接口](#定义接口)
    - [使用接口](#使用接口)
- [异常与异常处理](#异常与异常处理)
  - [Throwable](#throwable)
    - [Error](#error)
    - [Exception](#exception)
      - [非检查异常(RuntimeException)](#非检查异常runtimeexception)
        - [空指针异常(NullPointerException)](#空指针异常nullpointerexception)
        - [数组下标越界异常(ArrayIntexOutOfBoundsException)](#数组下标越界异常arrayintexoutofboundsexception)
        - [类型转换异常(ClassCastException)](#类型转换异常classcastexception)
        - [算术异常(ArithmeticException)](#算术异常arithmeticexception)
        - [异常(Exception)](#异常exception)
  - [使用try..catch..finally实现异常处理](#使用trycatchfinally实现异常处理)
  - [异常抛出以及自定义异常](#异常抛出以及自定义异常)
    - [自定义异常](#自定义异常)
  - [异常链](#异常链)
- [常用类](#常用类)
  - [包装类](#包装类)
    - [Integer常用方法](#integer常用方法)
  - [基本类型和包装类之间的转换](#基本类型和包装类之间的转换)
  - [基本类型和字符串之间的转换](#基本类型和字符串之间的转换)
  - [使用 Date 和 SimpleDateFormat 类表示时间](#使用-date-和-simpledateformat-类表示时间)
  - [Calendar类](#calendar类)
  - [使用 Math 类操作数据](#使用-math-类操作数据)
- [参考资料](#参考资料)

<!-- /TOC -->

# 注释

> 对于一份规范的程序源代码而言,注释应该占到源代码的1/3以上.

```java  
// 单行注释

/*
 * 多行注释
 * 多行注释
 */

/**
 * 文档注释
 * 和多行注释的区别在于开头
 * 文档注释时可以使用的javadoc标记
 * @author 标明开发该类模块的作者
 * @version 标明该类模块的版本
 * @see 参考转向,也就是相关主题
 * @param 对方法中某参数的说明
 * @return 对方法返回值的说明
 * @exception 对方法可能抛出的异常进行说明
 */

```

## 从文档注释生成API文档

```java 
javadoc -d doc class_name.java
```

## 注释




# 变量和常量

## 关键字

&emsp;|&emsp;|&emsp;|&emsp;|&emsp;|&emsp;|&emsp;|&emsp;|
-|-|-|-|-|-|-|-|
abstract|class|extends|implements|null|strictfp|true
assert|const|false|import|package|super|try
boolean|continue|final|instanceof|private|switch|void
break|default|finally|int|protected|synchronized|volatile
byte|do|float|interface|public|this|while
case|double|for|long|return|throw
catch|else|goto|native|short|throws
char|enum|if|new|static|transient

## 标识符

> 标识符是用于给变量、类、方法等命名的符号.

### 命名规则

> 变量名由多单词组成时,第一个单词的首字母小写,其后单词的首字母大写,俗称骆驼式命名法(也称驼峰命名法).

0. 标识符<br>
**可以包含:** 字母、数字、下划线(_)、美元符($)组成,<br>
**不能包含:** @、%、空格等其它特殊字符,<br>
**不能** 以数字开头.
0. 标识符不能是Java关键字和保留字.
0. 标识符**严格区分大小写**.

## 变量

> 三个元素描述变量:变量类型、变量名以及变量值.

![](http://img.mukewang.com/535a6c6000015cda02910139.jpg)

## 数据类型

> Java 语言是一种强类型语言.<br> 就是在 存储的数据都是有类型的,而且必须在编译时就确定其类型.

> 在 Java 的领域里,基本数据类型变量存的是数据本身,而引用类型变量存的是保存数据的空间地址.<br>基本数据类型变量里存储的是直接放在抽屉里的东西,而引用数据类型变量里存储的是这个抽屉的钥匙,钥匙和抽屉一一对应.

### 基本数据类型



#### 数值型



##### 整数型



###### byte 



###### short 



###### int 



###### long 



##### 浮点类型



###### float 



###### double 



#### 字符型(char)



#### 布尔型(boolean)



### 引用数据类型



#### 类(class) 



#### 接口(interface)



#### 数组



## 变量的使用规则

0. 需要先声明后使用.
0. 变量使用时,可以声明变量的同时进行初始化.
0. 变量中每次只能赋一个值,但可以修改多次.
0. main 方法中定义的变量必须先赋值,然后才能输出.    

## 自动类型转换

> int型变量可以直接为double型变量完成赋值操作,这种转换称为自动类型转换.

自动类型转换需要满足的条件:

0. 目标类型能与源类型兼容,如double型兼容int型,但是char型不能兼容int型.
0. 目标类型大于源类型,如double类型长度为8字节,int类型为4字节,因此double类型的变量里直接可以存放int类型的数据,但反过来就不可以了.

## 强制类型转换

> 语法:(数据类型) 数值

**强制类型转换可能会造成数据的丢失.**

```java
double avr1 = 66.3;
int avg2 = (int) avg1;
System.out.println(avg2); // => 66
```

# 字符串

> 字符串被作为 String 类型的对象处理.<br>
String 类位于 java.lang 包中.<br>
默认情况下,该包被自动导入所有的程序.

## 字符串的不变性

> String 对象创建后则不能被修改,是不可变的,<br>
所谓的修改其实是创建了新的对象,所指向的内存空间不同.

通过 String s1="Ruby"; 声明了一个字符串对象, s1 存放了到字符串对象的引用.

一旦一个字符串在内存中创建,则这个字符串将不可改变.<br>
如果需要一个可以改变的字符串,我们可以使用StringBuffer或者StringBuilder.

每次 new 一个字符串就是产生一个新的对象,即便两个字符串的内容相同,使用 ”==” 比较时也为 ”false” ,如果只需比较内容是否相同,应使用 ”equals()” 方法.

## 常用方法

方法名|作用
-|-
length()|返回当前字符串的长度
indexOf('f')|查找f字符在该字符中首次出现的位置
lastIndexOf('f')|查找f字符在该字符中最后一次出现的位置
substring(2)|查找下标2开始到结束的子字符串
substring(2, 5)|查找下标2开始到下标5的子字符串
trim()|除去字符串前后空格
equals(foo)|与指定对象foo进行比较是否相同
toLowerCase()|转小写
toUpperCase()|转大写
charAt(1)|字符串在指定位置的字符
spllit()|分割字符串,并返回数组
getBytes()|该字符串的byte数组

## StringBuilder 类

> 除了可以使用 String 类来存储字符串,还可以使用 StringBuilder 类或 StringBuffer 类存储字符串.

当频繁操作字符串时,String会额外产生很多临时变量.<br>
使用 StringBuilder 或 StringBuffer 就可以避免这个问题.<br>
StringBuilder 和StringBuffer ,它们基本相似,不同之处,<br>
StringBuffer 是线程安全的,而 StringBuilder 则没有实现线程安全功能,所以性能略高.<br>
因此一般情况下,如果需要创建一个内容可变的字符串对象,应优先考虑使用 StringBuilder 类.

### 常用方法

方法名|作用
-|-
append()|追加内容到StringBuilder对象末尾.
insert(位置, 参数)|将内容插入到指定位置.
length()|返回当前字符串的长度

## 常量

>  一种特殊的变量,它的值被设定后,在程序运行过程中**不允许改变**.

> 语法:final 数据类型 常量名 = 值;

> 常量名一般使用大写字符.

```java
final String LOVE = "Ruby";
```

# 运算符

## 算术运算符

算数运算符|名称
-|-
+|加法
-|减法
*|乘法
/|除法
%|求余
++|自增1
--|自减1

### 关于++出现位置

```java 
int a = 5;
int b = a++; // 将a的值赋给b,然后执行自增
int b = ++a; // a先执行自增,然后赋值给b 
```

## 赋值运算符

运算符|名称
-|- 
=|赋值
+=|加等于c+=a等价于c=c+a
-=|
*=|
/=|
%=|

## 比较运算符

比较运算符|名称
-|-
\>|大于
<|小于
\>=|大于等于
<=|小于等于
==|等等于
!=|不等于

0. \> 、 < 、 >= 、 <= 只支持左右两边操作数是数值类型
0. == 、 != 两边的操作数既可以是数值类型,也可以是引用类型


## 逻辑运算符

逻辑运算符|名称|解释
-|-|-
&&|与|要求所有人都投票同意,才能通过某议题
\|\||或|只要求一个人投票同意就可以通过某议题
!|非|某人原本投票同意,通过非运算符,可以使其投票无效
^|异或|有且只能有一个人投票同意,才可以通过某议题

### 关于||与^的区别

```java
boolean a = true;
boolean b = true;
boolean c = false;

a || b // => true
a ^ b // => false

a || c // => true
a ^ c // => true
```

### 跳过执行

0. a && b 若a为假,则跳过执行b
0. a ||b 若a为真,则跳过执行b

## 条件运算符

> 条件运算符( ? : )也称为 “三元运算符”.

> 语法形式:布尔表达式 ? 表达式1 :表达式2

## 运算符的优先级

级别为 1 的优先级最高,级别 11 的优先级最低.

![](http://img.mukewang.com/5360ffb90001b4f002620224.jpg)


# 流程控制语句

## 条件语句 



## if

```java 
if (条件的布尔表达式) {
  // 条件成立时执行代码
}
```

如果条件成立时执行代码只有一行或者很短,可以写作:

```java 
if (a > b) System.out.println(a);
```

##  if...else

```java 
if (条件的布尔表达式) {
  // 条件成立时执行代码
}else {
  // 条件不成立时执行代码  
}
```


## 多重 if

```java 
if (条件1的布尔表达式) {
  // 条件1成立时执行代码
}else if (条件2的布尔表达式) {
  // 条件2成立时执行代码
}else {
  // 所有条件都条件不成立时执行代码  
}
```

## 嵌套 if

```java 
if (条件1的布尔表达式) {
  if (条件2的布尔表达式) {
    // 条件2成立时执行代码
  }else {
    // 条件2不成立时执行代码  
  }
}else {
  // 条件1不成立时执行代码  
}
```

##  switch

> 在switch中直到遇到break语句或者switch语句块结束才会结束.

```java 
switch (表达式) {
  case 值1 :
    // 执行代码块1 
    break;
  case 值2 :
    // 执行代码块2 
    break;  
  case 值3 :
    // 执行代码块3
    break;
  default :
    // 默认执行的代码
}
```

## 循环语句

## while

> 特点:**先判断,后执行**.

执行过程:

1. 判断 while 后面的条件是否成立( true / false )
2. 当条件成立时,执行循环内的操作代码 ,然后重复执行1->2->1 直到循环条件不成立为止.

```java 
while (判断条件) {
  // 循环操作
}
```

## do...while

> 特点: **先执行,后判断**.

执行过程:

1. 先执行一遍循环操作,然后判断循环条件是否成立
2. 如果条件成立,继续执行 1->2->1直到循环条件不成立为止

由此可见,do...while 语句保证循环至少被执行一次！

```java 
do {
  // 循环操作
}while (判断条件);
```

## for

执行过程:

1. 执行循环变量初始化部分,设置循环的初始状态,此部分在整个循环中只执行一次.
2. 进行循环条件的判断,如果条件为 true ,则执行循环体内代码;如果为 false ,则直接退出循环.
3. 执行循环变量变化部分,改变循环变量的值,以便进行下一次条件判断.
4. 依次重新执行2->3->4,直到退出循环.

```java 
//  for 关键字后面括号中的三个表达式必须用 “;” 隔开,三个表达式都可以省略,但 “;” 不能省略.
for (循环变量初始化; 循环条件; 循环变量变化) {
  // 循环操作
}
```

### 省略循环变量初始化

可以在for语句之前由赋值语句进行变量初始化操作:

```java 
int i = 0; 
for (; i <10; i++) {
// ...
}
```

### 省略循环条件

可以在循环体中使用 break 强制跳出循环:

```java 
for (int i = 0;; i++) {
  if (i > 10) break;
}
```

### 省略循环变量变化

可以在循环体中进行循环变量的变化:

```java 
for (int i = 0; i <10;) {
  i++;
}
```

### 多个循环变量初始化或循环变量变化部分

可以是使用 “,” 同时初始化或改变多个循环变量的值:

```java 
for (int i = 1, j=5; j > i; i++, j--) {
  
}
```

## 循环跳转语句

## break

> 使用break语句退出指定的循环,直接执行循环后面的代码.

```java 
// 使用循环输出1--10的数值,其中,如果数值大于2,并且为3的倍数则停止输出
for (int i = 0; i <= 10; i++) {
  if ((i > 2) && (i %3 == 0)) break;
}
```

## continue

> continue 的作用是跳过循环体中剩余的语句执行下一次循环.

```java 
// 打印 1--10 之间所有的偶数
for (int i = 0; i <= 10; i++) {
  if (i%2 != 0) continue;
  System.out.println(i)
}
```

# 数组

操作数组只需要四个步骤:

## 声明数组

数据类型[ ] 数组名;

数据类型 数组名[ ];

```java 
int[] array;
```

## 分配空间

数组名 = new  数据类型 [ 数组长度 ];

```java
array = new int[5];
```

也可以将上面的两个步骤合并,在声明数组的同时为它分配空间,如:

```java 
int[] array = new int[5];
```

## 赋值

```java
array[0] = 99;
```

上面三步可用另外一种直接创建数组的方式,它将声明数组、分配空间和赋值合并完成,如:

```java
int[] array={1, 2, 3, 4, 5}
```

等价于:

```java
int[] array= new int[]{1, 2, 3, 4, 5}
```

等价于:

```java
int array[]= new int[]{1, 2, 3, 4, 5}
```

## 处理数组中数据

```java
// 取值
array[0] // => 99
```

## 常用方法

方法名|作用|描述
-|-|-
length|获取数组长度|array.length
sort|排序|Arrays.sort(array);
toString|将数组转换为字符串|Arrays.toString(array);
foreach|是for语句的特殊简化版本|foreach (元素类型 元素变量:遍历对象){<br>&emsp;// 执行代码<br>}

## 二维数组

### 声明数组并分配空间

数据类型[][] 数组名=new 数据类型[行的个数][列的个数];

数据类型[][] 数组名;
数组名 = new 数据类型 [行的个数][列的个数];

### 赋值 

数组名[行索引][列索引] = 值;

也可以在声明数组的同时为其赋值:

数据类型[][] 数组名 = {{行1列1值, 行1列2值, 行1列3值}, {行2列1值, 行2列2值}, }

# 方法

> 方法,就是用来解决一类问题的代码的有序组合,是一个功能模块.


## 如何定义方法

```java
访问修饰符 返回值类型 方法名(参数列表){
  // 方法主体
}
```

名词|解释
-|-
访问修饰符|方法允许被访问的权限范围,<br>可以是public、protected、private甚至可以省略,<br>其中public表示该方法可以被其他任何代码调用
返回值类型|方法返回值的类型,如果方法不返回任何值,则返回值类型指定为void;<br>如果方法具有返回值,则需要指定返回值的类型,并且在方法体中使用**return语句返回值**
方法名|定义的方法的名字,必须使用合法的标识符
参数列表|传递给方法的参数列表,参数可以有多个,多个参数间以逗号隔开,每个参数由参数类型和参数名组成,以空格隔开.

根据方法是否带参、是否带返回值,可将方法分为四类.

## 无参无返回值方法

```java
public class HelloWorld {
  public static void main (String[] args) {
    HelloWorld hello = new HelloWorld();
    // 调用方法
    hello.foo();
  }
  public void foo(){
    System.out.println('Hello World')
  }  
}
```

## 无参带返回值方法

```java
public class HelloWorld {
  public static void main (String[] args) {
    HelloWorld hello = new HelloWorld();
    // 调用方法
    hello.bar();
  }
  public int bar(){
    int a = 5;
    int b = 2;
    int sum = a +b;
    return sum;
  }  
}
```

0. 如果方法的返回类型为void,则方法中不能使用return返回值
0. 方法的返回值最多只能有一个,不能返回多个值.
0. 方法返回值的类型必须兼容,例如,如果返回值类型为int,则不能返回String型值.

## 带参无返回值方法

```java
public class HelloWorld {
  public static void main (String[] args) {
    HelloWorld hello = new HelloWorld();
    // 调用方法
    hello.foo('bar');
  }
  public void foo(String name){
    System.out.println(name);
  }
}

```

0. 调用带参方法时,必须保证实参的数量、类型、顺序与形参一一对应.
0. 调用方法时,实参不需要指定数据类型.
0. 方法的参数可以是基本数据类型,如 int、double 等,也可以是引用数据类型,如 String、数组等(***包括接口***).
0. 当方法参数有多个时,多个参数间以逗号分隔.

## 带参带返回值方法

```java
public class HelloWorld {
  public static void main (String[] args) {
    HelloWorld hello = new HelloWorld();
    // 调用方法
    hello.bar();
  }
  public int bar(int a, int b){
    int sum = a +b;
    return sum;
  }  
}
```

## 方法的重载

> 如果同一个类中包含了两个或两个以上方法名相同、方法参数的个数、顺序或类型不同的方法,<br>则称为方法的重载,也可称该方法被重载了.

当调用被重载的方法时,Java 会根据参数的个数和类型来判断应该调用哪个重载方法,参数完全匹配的方法将被执行.
 
### 判断方法重载的依据

0. 必须是在同一个类中
0. 方法名相同
0. 方法参数的个数、顺序或类型不同
0. 与方法的修饰符或返回值没有关系

# 类和对象

面向对象是指在程序开发过程中更多的关注于事物相关的信息.

面向对象的三大特征: 封装,继承和多态.

## 什么是类和对象

> 类确定对象将会拥有的特征(属性)和行为(方法).<br>是对象的类型,具有相同属性和方法的一组对象的集合.

> 对象的属性是指对象具有的各种特征,每个对象的每个属性都拥有特定值.

> 类是抽象的概念,仅仅是模板,对象是一个具体实体.

## 如何定义类

```java
public class class_name {

  // 定义属性(成员变量或实例变量) 有什么
  数据类型 变量名;
  
  // 定义方法 能干什么
  访问修饰符 返回值类型 方法名(参数列表){
    // 方法主体
  }
  
}
```

## 如何使用对象

### 创建对象

```java
类名 对象名 = new 构造方法();
```

### 使用对象

```java
// 引用对象属性
对象名.属性;

// 引用对象方法
对象名.方法名();
```

## 成员变量和局部变量

&emsp;|成员变量|局部变量
-|-|-
定义|在*类中定义*,用来描述对象将要有什么.|在*类的方法中定义*,在方法中临时保存数据
作用域|本类及其子类中的方法可以用.|只能在方法中使用.
初始值|有初始值|无初始值
同名变量|在一个类中不允许有同名成员变量|在一个方法中不允许有同名局部变量<br>在不同方法中,可以有同名局部变量.
优先级||两类变量同名时,局部变量具有更高的优先级.

## 构造方法

> 构造方法是定义在类中用于初始化对象的方法.<br>构造方法与类同名且没有返回值.

如果没有指定无参构造方法,系统会默认生成一个.<br>
但如果手动定义了有参构造方法,则不会默认生成无参构造方法.

```java
public 构造方法名(类名)(){
  // 初始化代码
}
```

### 构造方法的重载

构造方法名相同但参数不同的多个方法,调用时会自动根据不同的参数选择相应的方法.

## static

> 被static修饰的成员称为静态成员或类成员.<br>
它属于整个类所有,而不是某个对象所有,即被类的所有对象所共享.<br>
静态成员可以使用类名直接访问,也可以使用对象名进行访问.<br>
当然,鉴于他作用的特殊性更推荐用类名访问.

使用static可以修饰变量、方法和代码块.

### 静态变量

```java
public class HelloWorld {
  static String foo = 'foo';
  public static void main(String[] args) {
    // 通过类名访问
    HelloWorld.foo;
    
    // 通过对象访问 
    HelloWorld bar = new HelloWorld();
    bar.foo;

    // 通过对象修改静态变量
    bar.foo = 'bar';
  }
}
```

### 静态方法

> main方法就是静态方法.

相当于Ruby中的类方法.

```java
public class HelloWorld {
  // 声明静态方法
  public static void foo() {
    
  }
  
  public static void main (String[] args) {
    // 使用类名调用
    HelloWorld.foo();

    // 直接调用
    foo();
  }
}
```

0. 静态方法中可以直接调用同类中的静态成员,但不能直接调用非静态成员.
0. 如果希望在静态方法中调用非静态变量,可以通过创建类的对象,然后通过对象来访问非静态变量.

### 静态初始化块

> 通过初始化块进行数据赋值.

在类的声明中,可以包含多个初始化块,当创建类的实例时,就会依次执行这些代码块:

```java
public class HelloWorld {
  // 定义成员变量
  String bar;
  // 通过初始化块为成员变量赋值
  {
    bar = "bar";
  }
}
```

如果使用 static 修饰初始化块,就称为静态初始化块.

静态初始化块只在类加载时执行,且只会执行一次,同时静态初始化块只能给静态变量赋值,不能初始化普通的成员变量.

# 封装

> 将类的某些信息隐藏在类的内部,不允许外部程序直接访问,<br>
而是通过该类提供的方法来实现对隐藏信息的操作和访问.

标准的面向对象程序是不能直接操作属性,威尔士通过方法进行操作.

好处:

0. 只能通过规定的方法访问数据.
0. 隐藏类的实例细节,方便修改和实现.

## 实现步骤

0. 修改属性的可见性 -> 设为private
0. 创建getter和setter方法 -> 用于读写属性
0. 在getter和setter方法中加入属性控制语句 -> 对属性的合法性进行判断.


```java
public class HelloWorld {
  private String foo;

  // get方法
  public float getFoo() {
    return foo;
  }

  // set方法
  public void setFoo(float foo){
    foo = foo;
  }
  
}
``` 

## 使用包管理类

### 定义包
 
> package 包名;<br>
包名间用'.'进行分隔.

包的命名规范是全小写字母拼写.

### 系统中的包

> 格式: java.(功能).(类)

包名|作用
-|-
java.lang.(类)|包含java语言基础的类
java.util.(类)|包含java语言中各种工具类
java.io.(类)|包含输出,输入相关功能的类

### 使用包

> import 包名.类名;

加载包下所有的类:

```java
import 包名.*;
```

## 访问修饰符

> 可以修饰属性和方法的访问范围.

访问修饰符|本类|本包|子类|其他
-|-|-|-|-
private|√||||
默认|√|√|||
protected|√|√|√||
public|√|√|√|√|√

## this关键字

> this关键字代表当前对象.

## 内部类

> 内部类(Inner Class)就是定义在另外一个类里面的类.<br>
与之对应,包含内部类的类被称为外部类.

内部类的主要作用如下:

0. 内部类提供了更好的封装,可以把内部类隐藏在外部类之内,不允许同一个包中的其他类访问该类.
0. 内部类的方法可以直接访问外部类的所有数据,包括私有的数据.
0. 内部类所实现的功能使用外部类同样可以实现,只是有时使用内部类更方便.

### 成员内部类

> 内部类中最常见的就是成员内部类,也称为普通内部类.

```java
public class Outer {
  private int foo = 99; // 外部私有属性
  private int bar = 27; // 外部私有属性
  public class Inner {
    int bar = 66; //内部类的成员属性
    public void test() {
      System.out.println("访问外部类中的foo:"+foo);
      System.out.println("访问内部类中的bar:"+bar);
      System.out.println("访问外部类中的bar:"+Outer.this.bar);
    }
  }

  public static void main (String[] args) {
    Outer outer = new Outer();
    Inner inner = outer.new Inner();
    inner.test();
  }
  
}
```

0. Inner 类定义在 Outer 类的内部,相当于 Outer 类的一个成员变量的位置,Inner 类可以使用任意访问控制符,如 public 、 protected 、 private 等.
0. Inner 类中定义的 test() 方法可以直接访问 Outer 类中的数据,而不受访问控制符的影响
0. 定义了成员内部类后,必须使用外部类对象来创建内部类对象,而不能直接去 new 一个内部类对象,即:内部类 对象名 = 外部类对象.new 内部类( );
0. 如果外部类和内部类具有相同的成员变量或方法,内部类默认访问自己的成员变量或方法,如果要访问外部类的成员变量,可以使用 this 关键字.

### 静态内部类

> 静态内部类是 static 修饰的内部类.

0. 静态内部类不能直接访问外部类的非静态成员,但可以通过 new 外部类().成员 的方式访问. 
0. 如果外部类的静态成员与内部类的成员名称相同,可通过“类名.静态成员”访问外部类的静态成员;如果外部类的静态成员与内部类的成员名称不相同,则可通过“成员名”直接调用外部类的静态成员.
0. 创建静态内部类的对象时,不需要外部类的对象,可以直接创建 内部类 对象名= new 内部类();

```java
public class Outer {
  private int foo = 39;
  static int bar = 83;
  
  public static class Inner {
    int foo = 58;
    int bar = 27;
    public void test() {
      System.out.println("外部类中的foo:"+ new Outer().foo);
      System.out.println("内部类中的foo:"+ foo);
      System.out.println("外部类中的bar:"+ Outer.bar);
      System.out.println("内部类中的bar:"+ bar);
    }
  }
  
  public static void main (String[] args) {
    Inner inner = new Inner();
    inner.test();
  }

}
```

### 方法内部类

> 方法内部类就是内部类定义在外部类的方法中,方法内部类只在该方法的内部可见,即只在该方法内可以使用.<br>
由于方法内部类不能在外部类的方法以外的地方使用,因此方法内部类不能使用访问控制符和 static 修饰符.

```java
public class Outer {
  public void test () {
    final int bar = 90;
    int foo = 33;
    
    class Inner {
      int foo = 29;
      public void print(){
        System.out.println("访问外部常量 bar:"+bar);
        System.out.println("访问外部常量 bar:"+bar);        
      }      
    }
    
    Inner inner = new Inner();
    inner.print();
  }

  public static void main (String[] args) {
    Output output = new Output();
    output.test();
  }
}
```

### 匿名内部类

> 匿名内部类就是没有名字的内部类, 多用于实现而不关注实现类的名称.

```java

```

# 继承

Java是单继承.

继承的好处:

0. 子类拥有父类所有属性和方法.
0. 实现代码复用.

子类无法继承父类private修饰符的方法和属性.

```java
class 类名 extends 父类名 {
  
}
```

## 方法重写

> 子类重写继承父类的方法,当调用时会优先调用子类的方法.

只有当返回值类型,方法名,参数类型及个数与继承父类方法完全一致时才构成重写.

## 继承初始化顺序

0. 先初始化父类再初始化子类.
0. 先执行初始化对象中属性,再执行构造方法中的初始化.

即:

父类对象 -> 属性初始化 -> 构造方法 -> 子类对象 -> 属性初始化 -> 构造方法

## final关键字

> final关键字可以修饰类,方法,属性和变量

0. 修饰类时该类则不允许被继承.
0. 修饰方法则该方法不允许被重写.
0. 修饰属性则不会进行隐式初始化,即: 类的初始化必须有值或在构造方法中赋值,只能选其一.
0. 修饰变量则该变量的值只能赋一次值,即为常量.

## super的使用

> 在对象内部使用,可以代表父类对象.

0. 子类的构造过程中必须调用其父类的构造方法,相当于在子类的构造方法中隐式的调用了super();
0. 如果子类的构造方法中没有显式调用父类的构造方法,则系统默认调用父类的无参构造方法.
0. 如果显示的调用了父类构造方法,必须在子类的构造方法的第一行.
0. 如果子类构造方法中既没有显示调用父类的构造方法,而父类又没有无参构造方法,则编译报错.

```java
public className {
  super(); // 显示调用
  // ...
}
```

## Object类

0. Object类是所有类的父类,如果一个类没有使用extends关键字明确标识继承另一个类,则这个类默认继承Object类.
0. Object类中的方法,适合所有子类.


常用方法|作用
-|-
equals()|比较对象的引用是否指向同一块内存地址. 

# 多态

继承是实现多态的基础.

## 引用多态

0. 父类的引用可以指向本类的对象.
0. 父类的引用可以指向子类的对象.

```java
public class Animal {
  
}

public class Dog extends Animal {
  
}

public class NoBody {
  public static void main (String[] args) {
    Animal animal_1 = new Animal();
    Animal animal_1 = new Dog();
    Dog animal_3 = new Animal(); // 子类引用不能指向父类
  }
}

```

## 方法多态

0. 创建本类对象时,调用的方法为本类方法.
0. 创建子类对象时,调用的方法为子类重写的方法或者继承的方法.

## 引用类型转换

类型|含义
-|-
向上类型转换/自动类型转换|小类型转换到大类型的转换
向下类型转换/强制类型转换|大类型转小类型

```java 
Dog dog = new Dog();
// 向上类型转换
Animal animal = dog;
// 强制类型转换
Dog dog_2 = (Dog) animal;
```

### instanceof运算符

> instanceof用于判断某一对象是否是某一类型或其子类,<br>
从而解决引用对象的类型,避免类型转换的安全性问题.

```java
Dog dog = new Dog();

if (dog instanceof Dog){
  Cat cat = (Cat)dog;
}
```

## 抽象类

> 抽象类使用abstract关键字修饰.<br>
抽象类的作用在于限制规定子类必须实现某些方法,但不关注实现细节.

应用场景:

0. 某个父类只是知道其子类应该包含怎样的方法,但无法准确知道这些子类该如何实现这些方法.
0. 从多个具有相同特征的类中抽象出一个抽象类,以这个抽象类作为子类的模板,从而避免子类设计的随意性.
 
使用规则:

0. abstract定义抽象类 
0. abstract定义抽象方法,只需要声明,不需要实现.
0. 包含抽象方法的类是抽象类. 
0. 抽象类中可以包含普通方法,也可以没有抽象方法.
0. 抽象类不能直接创建,可以定义引用变量.

```java
public abstract class Telphone {
  public abstract void call(); // 抽象方法没有方法体,以分号结束
  public abstract void message();
}
```

## 接口

接口可以理解为一种特殊的类,有全局常量和公共的抽象方法组成.

如果说类一种具体实现体,而接口定义了某一批类所需要遵守的规范,<br>
接口不关心这些类的内部数据, 它只规定这些类里面必须提供某些方法.

接口可以继承多个父接口.

### 定义接口

接口定义使用interface关键字:

```java
// 接口就是用来被继承和被实现的,修饰符一般用public,不能使用private和protected修饰接口
修饰符 abstract interface 接口名 [extends 父接口1, 父接口2...]{
  // 零到多个常量定义
  // 接口中的属性是常量,即使定义时不添加 public static final修饰符,系统也会自动加上
  
  // 零到多个抽象方法的定义
  // 接口中的方法只能是抽象方法, 即使定义时没有添加public abstract修饰符,系统也会自动加上.
}
```

### 使用接口

一个类可以实现一个或多个接口, 实现接口使用implements关键字.<br>
一个类只能继承一个父类, 可以通过实现多个父类作为补充.

```java
[修饰符] class 类名 extends 父类 implements 接口1, 接口2.. {
  
}
```

# 异常与异常处理

> 阻止当前方法或作用域继续执行的方法,称之为异常.

## Throwable

### Error

常用于系统异常,内存溢出,线程死锁等.

### Exception

#### 非检查异常(RuntimeException)

##### 空指针异常(NullPointerException)

##### 数组下标越界异常(ArrayIntexOutOfBoundsException)

##### 类型转换异常(ClassCastException)

##### 算术异常(ArithmeticException)

##### 异常(Exception)

## 使用try..catch..finally实现异常处理

多重catch块要注意顺序问题:先子类后父类.


```java
try{
  // 可能会抛出异常的代码块
} catch (NullPointerException e) {
  // 处理该异常的代码块
} catch (ArithmeticException e) {
  // 处理该异常的代码块  
} catch (Exception e) {
  // 处理该异常的代码块    
} finally {
  // 最终要执行的代码块
}
```

## 异常抛出以及自定义异常

> throw:将产生的异常抛出(动作)

> throws:声明将要抛出哪些类型的异常(声明)

```java
public void divide(int foo, int bar) throws Exception {
  if (foo == 0){
    throw new Exception("bar 不能为0");
  }

}
```

### 自定义异常

> 自定义异常要继承于 Exception 类或者其子类.

```java
class 自定义异常类 extends 异常类型 {
  
}
```


## 异常链

> 当调用方法有声明异常时,调用者必须处理异常或向上抛出.

```java
public void foo() throws Exception {
  throw new Exception("hello");
}

public void bar(){
  try{
    foo();  
  } catch (Exception e) {
    RuntimeException runError = new RuntimeException("world");
    runError.initCause(e);
    throw runError;
  }
}
```


# 常用类

## 包装类

> 为了让基本数据类型也具备对象的特性, <br>
Java为每个基本数据类型都提供了一个包装类,<br>
这样就可以像操作对象那样来操作基本数据类型.

基本类型|对应包装类
-|-
byte|Byte
short|Short
int|Integer
long|Long
float|Float
double|Double
char|Char
boolean|Boolean

### Integer常用方法

返回值|方法名|作用
-|-|-
byte|byteValue()|将Interger转换为byte类型
double|doubleValue()|转为double类型
int|intValue()|转为int类型
long|longValue()|转为long类型
float|floatValue()|转为float类型
static int|parseInt(str)|将字符串转为int类型
String|toString()|转为字符串类型
static Integer|valueOf(str)|将字符串转为Integer类型

## 基本类型和包装类之间的转换

> 装箱:把基本类型转换成包装类,使其具有对象的性质,又可分为手动装箱和自动装箱.

```java
int i = 10;
Integer x =new Integer(i); // 手动装箱
Integer y = i; // 自动装箱
```

> 拆箱:和装箱相反,把包装类对象转换成基本类型的值,又可分为手动拆箱和自动拆箱.

```java
Integer j = new Integer(90);
int m = j.intValue(); // 手动拆箱为int类型
int n = j; // 自动拆箱为int类型
```

## 基本类型和字符串之间的转换

基本类型转换为字符串有三种方法:

0. 使用包装类的 toString() 方法
0. 使用String类的 valueOf() 方法
0. 用一个空字符串加上基本类型,得到的就是基本类型数据对应的字符串

将字符串转换成基本类型有两种方法:

0. 调用包装类的 parseXxx 静态方法
0. 调用包装类的 valueOf() 方法转换为基本类型的包装类,会自动拆箱

## 使用 Date 和 SimpleDateFormat 类表示时间

使用 Date 类的默认无参构造方法创建出的对象就代表当前时间:

```java
Date timeNow = new Date();
// 可以使用 SimpleDateFormat 来对日期时间进行格式化,如可以将日期转换为指定格式的文本,也可将文本转换为日期
SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
// 转为文本
String date = sdf.format(timeNow);
// 文本转日期
Date timeNow = sdf.parse(date);
```

## Calendar类

> Date 类最主要的作用就是获得当前时间,<br>
同时这个类里面也具有设置时间以及一些其他的功能,<br>
但是由于本身设计的问题,这些方法却遭到众多批评,不建议使用,<br>
更推荐使用 Calendar 类进行时间和日期的处理.

java.util.Calendar 类是一个抽象类,可以通过调用 getInstance() 静态方法获取一个 Calendar 对象,<br>
此对象已由当前日期时间初始化,即默认代表当前时间,如: 

```java
Calendar c = Calendar.getInstance();
Date date = c.getTime(); // 转换为Date类型
```

## 使用 Math 类操作数据

> Math 类位于 java.lang 包中,包含用于执行基本数学运算的方法,<br>
 Math 类的所有方法都是静态方法,所以使用该类中的方法时,<br>
 可以直接使用类名.方法名,如: Math.round();

方法名|作用
-|-
round()|四舍五入后整数
floor()|小于参数的最大整数
ceil()|大于参数的最小整数
random()|返回[0,1)之间的随机数浮点数

# 参考资料

> [慕课网 | Java入门第一季](https://www.imooc.com/learn/85)
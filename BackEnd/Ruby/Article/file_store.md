> 关于文件存储的相关数据库结构设计总结

<!-- TOC -->

- [目的](#目的)
- [生命周期](#生命周期)
  - [存储文件](#存储文件)
  - [文件加密](#文件加密)
  - [读取文件](#读取文件)
- [常用文件类型](#常用文件类型)
- [权限设计](#权限设计)
  - [数据命名规则](#数据命名规则)
  - [伪码](#伪码)
- [表结构设计](#表结构设计)
  - [FileStore](#filestore)
  - [Code](#code)
  - [Authorize](#authorize)
- [参考资料](#参考资料)

<!-- /TOC -->

# 目的

通过一张数据表实现应用所有文件相关文件夹的管理及存储地址的记录.

# 生命周期

## 存储文件

```ruby
=begin
                                                     |-> 可根据用户角色等校验是否允许访问该文件夹内文件
                                                     |                          |-> 验证存储地址是否存在相关文件
                                                     |                          |-> 标记全局唯一标识
                                                     |                          |-> 设置标签: 必填的(是否公开,权限规则等)/非必填的
                                                     |                          |-> 表结构采用树状根据文件层级进行记录
                                                     |                          |-> 
                |-> 根据不同对象设置允许上传的文件后缀     |                          |
获取文件 -> 验证文件合法性 -> 计算存储文件夹 -> 文件夹权限/合法性验证 -> 存储文件 -> 文件存储地址数据库记录
                                |-> 在存储根路径下自定义存储文件夹
                                |-> 根据对象名称,id等得到存储地址
=end
```

## 文件加密

## 读取文件

```ruby
=begin
              |-> 文件是否设置权限,当前用户是否符合
              |-> 路径下是否存在指定文件
文件路径 -> 验证合法性 -> 读取文件
=end
```

# 常用文件类型

> 浏览器通常使用MIME类型(而不是文件扩展名)来确定如何处理URL,<br>因此Web服务器在响应头中添加正确的MIME类型非常重要.如果配置不正确,<br>浏览器可能会曲解文件内容,<br>网站将无法正常工作,<br>并且下载的文件也会被错误处理.<br>MIME类型对大小写不敏感,但是传统写法都是小写.

MIME的组成结构非常简单,由类型与子类型两个字符串中间用'/'分隔而组成.**不允许空格存在**:

```bash
# type 表示可以被分多个子类的独立类别.
# subtype 表示细分后的每个类型.
type/subtype
```

类型|描述|典型示例
-|-|-
text|表明文件是普通文本,理论上是人类可读|text/plain<br>text/html<br>text/css<br>text/javascript
image|表明是某种图像.不包括视频,但是动态图(比如动态gif)也使用image类型|image/gif<br>image/png<br>image/jpeg<br>image/bmp<br>image/webp<br>image/x-icon<br>image/vnd.microsoft.icon
audio|表明是某种音频文件|audio/midi<br>audio/mpeg<br>audio/webm<br>audio/ogg<br>audio/wav
video|表明是某种视频文件|video/webm<br>video/ogg
application|表明是某种二进制数据|application/octet-stream<br>application/pkcs12<br>application/vnd.mspowerpoint<br>application/xhtml+xml<br>application/xml<br> application/pdf

对于text文件类型若没有特定的subtype,就使用 text/plain.<br>
类似的,二进制文件没有特定或已知的 subtype,即使用 application/octet-stream.

# 权限设计

权限分为增删改查(a,d,e,s)四级.以记录为单位,针对拥有者,同组,其他组进行权限设置.<br>
其中admin组对全部数据进行ades操作(默认情况下).

若用户同属多个群组, 则所属多个群组下的用户与该用户属同组.

数据创建时默认拥有者及admin组用户可进行ades操作,同组及其他组无任何权限.

```
{owner: ['a', 'd', 'e', 's'], s_group: [], o_group: []}
```

## 数据命名规则

```ruby
# 权限键:权限值-权限键1:权限值1,某一权限为空则不在名称内展示:

{owner: ['a', 'd', 'e', 's'], s_group: [], o_group: []}
#=> owner:ades

{owner: ['a', 'd', 'e', 's'], s_group: ['s'], o_group: []}
#=> owner:ades-s_group:s
```

## 伪码

```ruby

# 查询当前用户主要角色
def find_user_main_role(object:)
  return [true, 'admin'] if current_user.is_admin?
  return [true, 'owner'] if current_user == object.user
  [true, 'other']
end

def judge_current_user_power(object:, authorize:, role:)
  return [false, '未发现当前登录用户'] unless current_user
    
  case authorize
  when 'admin'
  return [true, ['a', 'd', 'e', 's']]
  when 'owner'
    return [true, authorize[:ower]]
  else 
    if (object.groups || current_user.groups).present?
      return [true, authorize[:s_group]]
    else
      return [true, authorize[:o_group]]
    end 
  end
  [false, '权限处理异常']
end 

# 判断当前用户具体权限
def current_user_power
  authorize = self.authorize || Authorize.default_data_authorize
  res = find_user_main_role(object: self)
  return res unless res[0]
  judge_current_user_power(object: self, authorize: authorize, role: res[1])
end 

# 判断当前用户具体权限
# 是否加入类方法, 如果加入 多态的xxxable_id非空非去掉,而且限制只能有一个为空
def self.current_user_power
  authorize = self.authorize || Authorize.default_model_authorize
  res = find_user_main_role(object: self)
  return res unless res[0]
  judge_current_user_power(object: self, authorize: res[1])
end 
```

# 表结构设计

## FileStore

> 存储文件路径等相关信息, 以树结构数据库存储, 权限在文件夹记录上设置.<br> 
文件夹内文件的权限取自父级目录, 若父级目录未设置权限递归向上查找, 直到最终取默认权限.

```ruby
def change
  create_table :file_stores do |t|
    t.string :store_path, null: false, comment: '文件存储路径' 
    t.integer :used_count, default: 0, comment: '使用次数记录'
    
    # 多态相关
    t.integer :fileable_id, null: false
    t.string :fileable_type, null: false
    t.index :fileable_type
    t.index :fileable_id

    # 树状结构相关
    t.string :ancestry
    t.integer :ancestry_depth, default: 0
    t.index [:ancestry]
    t.index :ancestry_depth
    
    t.datetime :deleted_at
    t.timestamps
  end
end 
```

## Code

> 提供全局唯一识别码

```ruby
def change
  create_table :codes do |t|
    t.string :code, null: false, unique: true
    t.index :code, unique: true
    
    # 多态相关
    t.integer :codeable_id, null: false
    t.string :codeable_type, null: false
    t.index :codeable_type
    t.index :codeable_id
    
    t.datetime :deleted_at
    t.timestamps
  end
end
```

## Authorize

> 设置记录的增删改查权限

```ruby
def change
  create_table :authorizes do |t|
    t.string :name, null: false, unique: true
    t.index :name, unique: true
    t.string :detail, null: false, unique: true, comment: '权限具体规则'
    t.index :detail, unique: true
    t.string :synopsis, comment: '说明'
    t.datetime :deleted_at
    t.timestamps
  end
  
  # 权限多态关联中间表
  create_table :middle_authorizes do |t|
    t.integer :authorize_id, null: false
    t.integer :level, null: false, comment: '权限级别'
    
    # 多态相关
    t.integer :middleable_id, null: false
    t.string :middleable_type, null: false
    t.index :middleable_type
    t.index :middleable_id
    
    t.datetime :deleted_at
    t.timestamps
  end 

end 
```


# 参考资料

> [MDN | MIME 类型](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Basics_of_HTTP/MIME_types)
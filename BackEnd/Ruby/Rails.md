<!-- TOC -->

- [ActionDispatch](#actiondispatch)
  - [Request](#request)
    - [query_parameters](#query_parameters)
- [ActionController](#actioncontroller)
  - [model中调用helper方法](#model中调用helper方法)
- [ActionView](#actionview)
  - [capture [使用块渲染部分]](#capture-使用块渲染部分)
- [问题处理](#问题处理)
  - [ActionView](#actionview-1)
    - [ActionView::Template](#actionviewtemplate)
      - [ActionView::Template::Error (missing attribute: receipt)](#actionviewtemplateerror-missing-attribute-receipt)
- [参考资料](#参考资料)

<!-- /TOC -->

# ActionDispatch

## Request

### query_parameters

> 获取请求连接中的所有参数

```ruby
request.query_parameters

request.query_parameters.merge({:per_page => 20})
```

# ActionController

## model中调用helper方法

```ruby
# model 中

ActionController::Base.helpers.helper_method

# 例如:
ActionController::Base.helpers.number_to_capital_zh(contract_total)
end
```

# ActionView

## capture [使用块渲染部分]

```erb
<!-- shared/_modal.html.erb -->
<div class="ui modal form">
  <i class="close icon"></i>
  <div class="header">
    <%= heading %>
  </div>
  <div class="content">
    <%= capture(&block) %>
  </div>
  <div class="actions">
    <div class="ui negative button">Cancel</div>
    <div class="ui positive button">Ok</div>
  </div>
</div>
```

```ruby
# application_helper.rb
def modal_for(heading, &block)
  render(
    partial: 'shared/modal',
    locals: { heading: heading, block: block }
  )
end
```

```erb
<%= modal_for('My Title') do |t| %>
  <p>Here is some content to be rendered inside the partial</p>
<% end %>
```

# 问题处理

## ActionView

### ActionView::Template

#### ActionView::Template::Error (missing attribute: receipt)

&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;|&emsp;
-|-
数据库| Oracle11g
ruby-oci8| 2.2.6.1
存在问题| ActionView::Template::Error (missing attribute: receipt)
可能原因| 可能与该字段在模型中定义序列化有关: serialize :receipt, Hash
问题详述| 数据库orders表存在receipt字段,<br> order.receipt<br> order.try(:receipt)<br> order.send('receipt')<br> order&.receipt <br>等查询方式railsc中查询没有任何问题,<br>但是在页面调用到该字段时报上面错误. <br>服务 nginx全部重启无法解决.
解决办法|最后通过order.read_attribute("receipt")得以解决.
问题原因|控制器查询中存在select特定字段 没有包含这个字段导致查询出现问题 read_attribute不会报错但是也读取不到值.

# 参考资料

> [stackoverflow | Rails render partial with block](https://stackoverflow.com/questions/2951105/rails-render-partial-with-block)

> [apidock | capture](https://apidock.com/rails/ActionView/Helpers/CaptureHelper/capture)

> [stackoverflow | Rails: Preserving GET query string parameters in link_to](https://stackoverflow.com/questions/3762430/rails-preserving-get-query-string-parameters-in-link-to)
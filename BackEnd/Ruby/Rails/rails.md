<!-- TOC -->

- [路由](#路由)
  - [Rails.application.routes.url_helpers](#railsapplicationroutesurl_helpers)
  - [link_to](#link_to)
  - [data](#data)
    - [disable_with](#disable_with)
  - [where.not](#wherenot)
  - [pluck](#pluck)
  - [模型](#模型)
    - [校验](#校验)
      - [The provided regular expression is using multiline anchors (^ or $), which may present a security risk. Did you mean to use \A and \z, or forgot to add the :multiline => true option? (ArgumentError)](#the-provided-regular-expression-is-using-multiline-anchors-^-or--which-may-present-a-security-risk-did-you-mean-to-use-\a-and-\z-or-forgot-to-add-the-multiline--true-option-argumenterror)
- [参考资料](#参考资料)

<!-- /TOC -->

# 路由
## Rails.application.routes.url_helpers

用于在后台获取路由链接

```ruby
def get_product_url
  SpkInterface.config[:url] + Rails.application.routes.url_helpers.commodity_path(id: commodity_id, p_id: id) if commodity_id.present?
end
```


## link_to

方法|常用示例
-|-
link_to|link_to '名称', 路由, class: '样式类', back: 返回路径, data:{confirm: '再次确认内容', disable_with: '链接的禁用版本的名称(提交过程中)'}, method: 提交方式
link_to_if|与link_to相比在 *名称* 前多了一个判断条件:<br>link_to_if order.can_make_up_invoice?[0], '补开发票',...<br>如果判断条件为false,链接标签还会渲染,只是 *禁用状态* 展示.
link_to_unless|没看,参考_if

## data
### disable_with

> 按钮点击后禁用提示语(防止重复点击) 

```ruby
<%= link_to '补开发票',  make_up_invoice_order_path(order, back: request.fullpath), data: {:disable_with => "提交中..."} %>
```


## where.not

```ruby
User.where.not(status: -1).ransack(params[:q])
```

## pluck

```ruby
Department.pluck(:name, :id)
# 等价于
Department.map{|department|[department.name, department.id]}
```

## 模型

### 校验

#### The provided regular expression is using multiline anchors (^ or $), which may present a security risk. Did you mean to use \A and \z, or forgot to add the :multiline => true option? (ArgumentError)

> 问题代码

```ruby
validates :password, format: { with: /^(?![0-9a-z]+$)(?![a-zA-Z]+$)(?![0-9A-Z]+$)[0-9a-zA-Z_&@$*]{8,20}$/, message: "密码要求包含数字,小写,大写英文及部分特殊符号且长度要求大于8位小于20位" }
```

> 原因

正则中表示开始(^)和结尾($)的正则需要改为:\a和\
z,如:

```ruby
  validates :password, format: { with: /\a(?![0-9a-z]+$)(?![a-zA-Z]+$)(?![0-9A-Z]+$)[0-9a-zA-Z_&@$*]{8,20}\z/, message: "密码要求包含数字,小写,大写英文及部分特殊符号且长度要求大于8位小于20位" }
```


# 参考资料

> [参考链接 | stackoverflow](https://stackoverflow.com/questions/17759735/regular-expressions-with-validations-in-ror-4)
> 验证码工具

<!-- TOC -->

- [依赖](#依赖)
  - [ImageMagick,ghostscript](#imagemagickghostscript)
- [使用](#使用)
  - [Gemfile](#gemfile)
  - [generate](#generate)
  - [application.rb](#applicationrb)
  - [view](#view)
  - [zh-CN.yml](#zh-cnyml)
  - [app/views/simple_captcha/_simple_captcha.erb](#appviewssimple_captcha_simple_captchaerb)
  - [action](#action)
    - [健壮参数](#健壮参数)
- [进阶](#进阶)
  - [config/initializers/simple_captcha.rb](#configinitializerssimple_captcharb)
- [附件](#附件)
  - [点击图片刷新验证码](#点击图片刷新验证码)

<!-- /TOC -->

# 依赖

## ImageMagick,ghostscript

```
sudo apt update;sudo apt-get install -fy imagemagick ghostscript
```

# 使用

## Gemfile

```
gem 'simple_captcha2', require: 'simple_captcha'
bundle install
```

## generate

```
rails generate simple_captcha [可选项:可选erb或haml,默认erb]
rails db:migrate
```

## application.rb

```ruby
ApplicationController < ActionController::Base
  include SimpleCaptcha::ControllerHelpers
end
```

## view

```erb
<%= show_simple_captcha %>
```

## zh-CN.yml

```yml
  simple_captcha:
    placeholder: "验证码"
    label: "请输入验证码"
    refresh_button_text: "刷新"
    message:
      default: "验证码错误"
      user: "验证码不一样"
```

## app/views/simple_captcha/_simple_captcha.erb

验证码页面内容文件,可在此修改样式.

## action

```ruby
if simple_captcha_valid?
  do this
else
  do that
end
```
### 健壮参数

添加:

```ruby
:captcha, :captcha_key
```


# 进阶

## config/initializers/simple_captcha.rb

新建config/initializers/simple_captcha.rb文件.
在这个文件中可以针对验证码的格式进行配置:

```ruby
SimpleCaptcha.setup do |sc|
  
  # 图片大小
  sc.image_size = '100x28'

  # 验证码长度
  sc.length = 6

  # 图片背景
  # 可选值:'embosed_silver','simply_red','simply_green','simply_blue','distorted_black','all_black','charcoal_grey','almost_invisible''random'
  sc.image_style = 'simply_blue'

  # 扭曲程度
  # 可选值: 'low', 'medium', 'high', 'random'
  sc.distortion = 'high'

  # 字体变形程度
  # 可选值: 'none', 'low', 'medium', 'high'
  sc.implode = 'high'
end
```


# 附件

## 点击图片刷新验证码

```js
<script type="text/javascript">
  $(".simple_captcha_refresh_button").remove();
  $(".simple_captcha_image").click(function(){     
    var image_id = $(".simple_captcha_image img").attr("id");
    var url = "/simple_captcha?id=" + image_id;
    $.ajax({
      type: "get",
      dataType: "script",
      cache : false,
      url: url,
      beforeSend: function(XMLHttpRequest) {
      },
      success: function(data, textStatus) {
      },
      complete: function(XMLHttpRequest, textStatus) {
      },
      error: function(data, textStatus) {
        alert("系统繁忙中,请稍后再试！");
      }
    });
  })
</script>

```



<hr>

> [GitHub | simple-captcha](https://github.com/pludoni/simple-captcha)

<!-- TOC -->

- [生成公私钥](#生成公私钥)
- [公钥加密](#公钥加密)
- [私钥解密](#私钥解密)
- [网站登录](#网站登录)
  - [后台生成公钥](#后台生成公钥)
  - [前台根据公钥对密码进行加密](#前台根据公钥对密码进行加密)
  - [后台根据公钥解析密文](#后台根据公钥解析密文)
- [参考资料](#参考资料)

<!-- /TOC -->

# 生成公私钥 

```ruby
# 生成一个2048位的RSA key(私钥)
private_key = OpenSSL::PKey::RSA.new 2048

# 公钥
public_key = key.public_key

# 将 RSA Key(私钥)加密
cipher = OpenSSL::Cipher.new 'AES-128-CBC'
# pass_phrase为解密密钥
pass_phrase = 'my secure pass phrase goes here'

# 加密后的key
key_secure = key.export cipher, pass_phrase

# 对加密后的RSA key(私钥)进行解密
private_key_new = OpenSSL::PKey::RSA.new key_secure, pass_phrase

private_key_new.to_pem === key.to_pem
# => true
```

# 公钥加密

> 加密解密pading默认为OpenSSL::PKey::RSA::PKCS1_PADDING

```ruby
public_encrypt_message = private_key.public_encrypt("password: 123456")
``` 

# 私钥解密

```ruby
private_key.private_decrypt(public_encrypt_message)
```

# 网站登录

## 后台生成公钥

```ruby
key = OpenSSL::PKey::RSA.new(1024)
@public_modulus  = key.public_key.n.to_s(16)
@public_exponent = key.public_key.e.to_s(16)
session[:key] = key.to_pem
```

## 前台根据公钥对密码进行加密

```erb
<input type="password" id='tmp_password' name="user[tmp_password]" value="" placeholder="请输入密码" class="form-control">
<%= hidden_field_tag "user[password]", '' %>
<%= hidden_field_tag :public_modulus, @public_modulus %>
<%= hidden_field_tag :public_exponent, @public_exponent %>
```

```js
$(document).ready(function() {
  $("#user_login").on('submit', function() {
    var rsa = new RSAKey();
    rsa.setPublic($('#public_modulus').val(), $('#public_exponent').val());
    var res = rsa.encrypt($('#tmp_password').val());
    if (res) {
      $('#user_password').val(hex2b64(res));
      $('#tmp_password').val('');
      return true;
    }
    return false;
  });
});
```

## 后台根据公钥解析密文

```ruby
key = OpenSSL::PKey::RSA.new(session[:key])
params[:user][:password] = key.private_decrypt(Base64.decode64(params[:user][:password]))
```

# 参考资料

> [Github | OpenSSL](https://ruby.github.io/openssl/OpenSSL/PKey/RSA.html)

> [简书 | ruby加密](https://www.jianshu.com/p/bec3dd4cceda)

> [JSEncrypt](http://travistidwell.com/jsencrypt/)

> [CSDN | javascript RSA公钥加密](https://blog.csdn.net/k21325/article/details/54409381)
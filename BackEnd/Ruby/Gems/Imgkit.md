> Html转图片

<!-- TOC -->

- [相关依赖](#相关依赖)
  - [libpng12-0](#libpng12-0)
  - [wkhtmltopdf](#wkhtmltopdf)
- [Gemfile](#gemfile)
  - [关于wkhtmltox-binaries和wkhtmltoimage-binary](#关于wkhtmltox-binaries和wkhtmltoimage-binary)
- [使用](#使用)
- [常见问题](#常见问题)
  - [error while loading shared libraries: libpng12.so.0](#error-while-loading-shared-libraries-libpng12so0)
  - [Package 'libpng12-0' has no installation candidate](#package-libpng12-0-has-no-installation-candidate)
- [参考资料](#参考资料)

<!-- /TOC -->

# 相关依赖

## libpng12-0

依赖Gem选用wkhtmltoimage-binary时需要安装:

```
sudo apt-get install libpng12-0
```

## wkhtmltopdf

```bash
bash +x install_package.sh wkhtmltopdf https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox_0.12.5-1.bionic_amd64.deb
```

# Gemfile

```ruby
# gem 'wkhtmltoimage-binary'
gem 'wkhtmltox-binaries'
gem 'imgkit'
```

## 关于wkhtmltox-binaries和wkhtmltoimage-binary

wkhtmltoimage-binary要求系统装有libpng12-0库,对于Debian用户没有困难,但如果是部署在centos上没有对应可用安装包.

如果选用wkhtmltox-binaries对上述系统环境没有相关依赖.

# 使用

```ruby
kit = IMGKit.new('http://www.baidu.com')
kit.to_png

kit = IMGKit.new('http://www.baidu.com')
send_data(kit.to_png, :type => "image/png", :disposition => 'inline')

kit = IMGKit.new("<h1>Hello World</h1><br><h2>Hello World</h2>")
send_data(kit.to_png, :type => "image/png", :disposition => 'inline')

```

# 常见问题

## error while loading shared libraries: libpng12.so.0

```ruby
[1] pry(main)> kit = IMGKit.new('http://www.baidu.com')
=> #<IMGKit:0x000056229ba326e0
 @javascripts=[],
 @options={:height=>0},
 @source=#<IMGKit::Source:0x000056229ba32690 @source="http://www.baidu.com">,
 @stylesheets=[]>
[2] pry(main)> kit.to_img
IMGKit::CommandFailedError: Command failed: /home/web/.rvm/gems/ruby-2.3.8/bin/wkhtmltoimage --height 0 --format jpg http://www.baidu.com -: /home/web/.rvm/gems/ruby-2.3.8/gems/wkhtmltoimage-binary-0.12.5/libexec/wkhtmltoimage-amd64: error while loading shared libraries: libpng12.so.0: cannot open shared object file: No such file or directory
from /home/web/.rvm/gems/ruby-2.3.8/gems/imgkit-1.6.1/lib/imgkit/imgkit.rb:117:in `to_img'
```

```bash
sudo apt-get update
sudo apt-get install libpng12-0
```


## Package 'libpng12-0' has no installation candidate

```bash
web@e314e997b122:~/space/code/yggc/nanjingnongda$ sudo apt-get install libpng12-0
Reading package lists... Done
Building dependency tree
Reading state information... Done
Package libpng12-0 is not available, but is referred to by another package.
This may mean that the package is missing, has been obsoleted, or
is only available from another source

E: Package 'libpng12-0' has no installation candidate
```

/etc/apt/sources.list中添加:

```
deb http://security.ubuntu.com/ubuntu xenial-security main
```

# 参考资料

> [askubuntu | E: Package 'libpng12-0' has no installation candidate \[ubuntu 16.10 Gnome\]
](https://askubuntu.com/questions/840257/e-package-libpng12-0-has-no-installation-candidate-ubuntu-16-10-gnome)

> [Ubuntu | 用在 AMD64 上libpng12-0_1.2.54-1ubuntu1.1_amd64.deb](https://packages.ubuntu.com/xenial/amd64/libpng12-0/download)

> [GitHub | IMGKit NOT WORK ON CENTOS7 #100](https://github.com/csquared/IMGKit/issues/100)
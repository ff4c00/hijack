<!-- TOC -->

- [常见问题](#常见问题)
  - [415 Unsupported Media Type](#415-unsupported-media-type)
- [参考资料](#参考资料)

<!-- /TOC -->


# 常见问题

## 415 Unsupported Media Type

> RestClient::UnsupportedMediaType: 415 Unsupported Media Type


The payload hash needed to be converted to JSON.

```ruby
# ...
payload:
{
  # ...
}.to_json
# ...
```

# 参考资料

> [stackoverflow | Ruby Gem Rest-Client vs cURL, HTTP 415](https://stackoverflow.com/questions/40046357/ruby-gem-rest-client-vs-curl-http-415)
> Markdown 解析器

<!-- TOC -->

- [安装](#安装)
  - [Gemfile](#gemfile)
- [用法](#用法)
  - [转化为HTML](#转化为html)
  - [可用选项](#可用选项)
    - [解析选项](#解析选项)
    - [渲染选项](#渲染选项)
    - [扩展选项](#扩展选项)
- [示例](#示例)
  - [表格展示](#表格展示)
- [参考资料](#参考资料)

<!-- /TOC -->

# 安装

## Gemfile

```ruby
gem 'commonmarker'
```

# 用法

## 转化为HTML

```ruby
require 'commonmarker'
CommonMarker.render_html('Hi *there*', :DEFAULT)
#=> <p>Hi <em>there</em></p>\n
```
## 可用选项

### 解析选项

名称|描述
---|---
:DEFAULT|默认解析系统.
:UNSAFE|允许原始/自定义HTML和不安全的链接.
:FOOTNOTES|解析脚注.
:LIBERAL_HTML_TAG|支持自由解析内联HTML标记.
:SMART|使用智能标点符号(弯引号等).
:STRIKETHROUGH_DOUBLE_TILDE|通过双重倾斜解析删除线(与redcarpet的兼容性)
:VALIDATE_UTF8|用替换字符替换非法序列U+FFFD.

### 渲染选项

名称|描述
-|-
:DEFAULT|默认渲染系统.
:UNSAFE|允许原始/自定义HTML和不安全的链接.
:GITHUB_PRE_LANG|\<pre lang>对于隔离代码块使用GitHub样式.
:HARDBREAKS|治疗\n如hardbreaks(通过添加<br/>).
:NOBREAKS|翻译\n源到一个空白.
:SOURCEPOS|在呈现的HTML中包含源位置.
:TABLE_PREFER_STYLE_ATTRIBUTES|使用表格单元格的styleinstedalign
:FULL_INFO_STRING|在单独的属性中包含完整的代码块信息字符串

### 扩展选项

> render_html和render_doc方法支持第三个参数, 提供额外扩展

名称|描述
-|-
:table|这为表格提供支持.
:tasklist|这为任务列表项提供支持.
:strikethrough|这为删除线提供了支持.
:autolink|这为自动将URL转换为锚标签提供了支持.
:tagfilter|几个[“不安全”的HTML标记](https://github.github.com/gfm/#disallowed-raw-html-extension-),将导致它们没有任何效果.

# 示例

## 表格展示

```ruby
def markdown(content:, parse_options: [], render_options: [:table])
  CommonMarker.render_html(content, parse_options, render_options)
end
```

# 参考资料

> [GitLab | GitLab Markdown](https://docs.gitlab.com/ee/user/markdown.html)

> [Github | commonmarker](https://github.com/gjtorikian/commonmarker)

> [Github | extension tasklist not found](https://github.com/gjtorikian/commonmarker/issues/64)


> 将数据库内记录组织为树结构(或层次结构)

<!-- TOC -->

- [安装](#安装)
  - [Gemfile](#gemfile)
  - [将ancestry字段添加到表中](#将ancestry字段添加到表中)
  - [添加到模型中](#添加到模型中)
- [使用](#使用)
  - [新增子节点](#新增子节点)
  - [更新所属父节点](#更新所属父节点)
  - [常用方法](#常用方法)
  - [arrange](#arrange)
    - [用于作用域](#用于作用域)
    - [用于排序](#用于排序)
    - [用于序列化](#用于序列化)
      - [使用块自定义序列化逻辑](#使用块自定义序列化逻辑)
  - [has_ancestry 选项](#has_ancestry-选项)
- [常见问题](#常见问题)
  - [undefined local variable](#undefined-local-variable)
- [参考资料](#参考资料)

<!-- /TOC -->

# 安装

## Gemfile

```ruby
# Gemfile
gem 'ancestry'
```

## 将ancestry字段添加到表中

```ruby
rails g migration add_ancestry_to_[table] ancestry:string:index
```

## 添加到模型中

```ruby
# app/models/model_name.rb
class ModelName < ActiveRecord::Base
   ...
   has_ancestry
   ...
end
```

# 使用

## 新增子节点

```ruby
node.children.create(name: 'Stinky')
```

## 更新所属父节点

更新parent_id字段即可:

```ruby
node.update(parent_id: parent_id)
```

## 常用方法

方法名|作用
-|-
parent|记录的父级,根节点为nil
parent_id|记录的父ID,根节点的nil
root|记录树的根,自定义为根节点
root_id|记录树的root id,root节点的self
root?<br>is_root?|如果记录是根节点,则为true,否则为false
ancestors|记录的祖先,从根开始,以父节点结束
ancestors?|如果记录具有祖先(也就是根节点),则为true
ancestor_ids|记录的祖先ID
path|记录的路径,从根开始,以self结束
path_ids|列出路径ID,以root id开头,以节点自己的id结束
children|记录的直接孩子
child_ids|直接儿童的ids
has_parent?<br>ancestors?|如果记录具有父项,则为true,否则为false
has_children?<br>children?|如果记录有任何子节点,则为true,否则为false
is_childless?<br>childless?|true是记录没有子节点,否则为false
siblings|记录的兄弟姐妹,记录本身包括*
sibling_ids|兄弟ids
has_siblings?<br>siblings?|如果记录的父项有多个子项,则为true
is_only_child?<br>only_child?|如果记录是其父项的唯一子项,则为true
descendants|直接和间接的儿童记录
descendant_ids|直接和间接儿童的记录ID
indirects|记录的间接子女
indirect_ids|间接儿童的记录ID
subtree|关于后代和自身的模型
subtree_ids|记录子树中所有ID的列表
depth|节点的深度,根节点的深度为0
&nbsp;|下面方法为确定2个节点之间的关系的方法:
parent_of?(node)|节点的父节点是此记录
root_of?(node)|节点的根是此记录
ancestor_of?(node)|节点的祖先包括此记录
child_of?(node)|节点是记录的父节点
descendant_of?(node)|节点是这个记录的祖先之一
indirect_of?(node)|node是此记录的祖先之一,但不是父级
***如果记录是根,则其他根记录被视为兄弟姐妹***

## arrange

Ancestry可以将整个子树排列成嵌套的哈希:

```ruby
TreeNode.arrange
# =>
{
  <TreeNode id: 100018, name: "Stinky", ancestry: nil> => {
    <TreeNode id: 100019, name: "Crunchy", ancestry: "100018"> => {
      <TreeNode id: 100020, name: "Squeeky", ancestry: "100018/100019"> => {}
    },
    <TreeNode id: 100021, name: "Squishy", ancestry: "100018"> => {}
  }
}
```

### 用于作用域

```ruby
TreeNode.find_by_name('Crunchy').subtree.arrange
```

### 用于排序

```ruby
TreeNode.find_by_name('Crunchy').subtree.arrange(:order => :name)
```

### 用于序列化

```ruby
TreeNode.arrange_serializable
# =>
[
  {
    "ancestry" => nil, "id" => 1, "children" => [
      { "ancestry" => "1", "id" => 2, "children" => [] }
    ]
  }
]
```

#### 使用块自定义序列化逻辑

```ruby
TreeNode.arrange_serializable do |parent, children|
  {
     my_id: parent.id,
     my_children: children
  }
end
```

## has_ancestry 选项

选项|作用
-|-
:ancestry_column|传递一个符号以将祖先存储在不同的列中
:orphan_strategy|指示祖先如何处理被破坏节点的子节点：<br>:destroy 同时销毁所有子项(默认)<br>:rootify 被破坏节点的子节点变为根节点<br>:restrict 如果存在任何一个孩子,则限制一个祖先异常.<br>:adopt 采用将孤儿子树添加到已删除节点的父节点.
:cache_depth|缓存“祖先深度”列中每个节点的深度(默认值：false)<br>迁移: add_column [table], :ancestry_depth, :integer, :default => 0<br>创建缓存: TreeNode.rebuild_depth_cache!
:depth_cache_column|传入符号以将深度缓存存储在其他列中
:primary_key_format|提供与主键格式匹配的正则表达式.<br>默认情况下,主键只匹配整数([0-9]+).
:touch|指示祖先在节点改变时触摸其祖先,以使嵌套的基于键的缓存失效.(默认值：false)

# 常见问题

## undefined local variable

>  undefined local variable or method `has_ancestry' 

If you are using an engine you should put ancestry in your Gemfile and require it in lib/\<your_engine>/engine.rb file.

```ruby
# lib/your_engine/engine.rb
require 'ancestry'
```

# 参考资料

> [GIthub | ancestry](https://github.com/stefankroes/ancestry)

> [stackoverflow | 'method_missing': undefined local variable or method 'has_ancestry'](https://stackoverflow.com/questions/19406343/method-missing-undefined-local-variable-or-method-has-ancestry)
> 图表展示

<!-- TOC -->

- [安装](#%E5%AE%89%E8%A3%85)
  - [Gemfile](#gemfile)
  - [application.js](#applicationjs)
- [使用](#%E4%BD%BF%E7%94%A8)
  - [示例](#%E7%A4%BA%E4%BE%8B)
    - [controller](#controller)
    - [view](#view)
- [参考资料](#%E5%8F%82%E8%80%83%E8%B5%84%E6%96%99)

<!-- /TOC -->

# 安装

## Gemfile

```ruby
gem 'lazy_high_charts'
```

## application.js

```js
//= require jquery
//= require jquery_ujs
//= require_tree .
//= require turbolinks

//= require highcharts/highcharts
//= require highcharts/highcharts-more
//= require highcharts/highstock
```

# 使用

## 示例

### controller

```ruby
@chart = LazyHighCharts::HighChart.new('graph') do |f|
  f.title(text: "Population vs GDP For 5 Big Countries [2009]")
  f.xAxis(categories: ["United States", "Japan", "China", "Germany", "France"])
  f.series(name: "GDP in Billions", yAxis: 0, data: [14119, 5068, 4985, 3339, 2656])
  f.series(name: "Population in Millions", yAxis: 1, data: [310, 127, 1340, 81, 65])

  f.yAxis [
    {title: {text: "GDP in Billions", margin: 70} },
    {title: {text: "Population in Millions"}, opposite: true},
  ]

  f.legend(align: 'right', verticalAlign: 'top', y: 75, x: -50, layout: 'vertical')
  f.chart({defaultSeriesType: "column"})
end

@chart_globals = LazyHighCharts::HighChartGlobals.new do |f|
  f.global(useUTC: false)
  f.chart(
    backgroundColor: {
      linearGradient: [0, 0, 500, 500],
      stops: [
        [0, "rgb(255, 255, 255)"],
        [1, "rgb(240, 240, 255)"]
      ]
    },
    borderWidth: 2,
    plotBackgroundColor: "rgba(255, 255, 255, .9)",
    plotShadow: true,
    plotBorderWidth: 1
  )
  f.lang(thousandsSep: ",")
  f.colors(["#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354"])
end
```

### view

```erb
<%= high_chart_globals(@chart_globals) %>
<%= high_chart("some_id", @chart) %>
```

# 参考资料

> [Github | lazy_high_charts](https://github.com/michelson/lazy_high_charts)

> [Github | dummy_rails(example)](https://github.com/michelson/lazy_high_charts/tree/master/spec/dummy_rails)
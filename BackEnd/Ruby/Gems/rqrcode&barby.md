> rqrcode用于二维码生成.

> barby用于生成条形码,该Gem支持多种常见如HTML,SVG,PDF,PNG等格式条形码,<br>
只需加载不同输出器(Outputters)即可.

<!-- TOC -->

- [二维码](#二维码)
  - [生成二维码](#生成二维码)
  - [二维码展示](#二维码展示)
  - [关于打印失真问题](#关于打印失真问题)
- [条形码](#条形码)
  - [生成条形码](#生成条形码)
  - [展示](#展示)
- [关于打印失真问题](#关于打印失真问题-1)
- [参考资料](#参考资料)

<!-- /TOC -->

# 二维码


## 生成二维码

```ruby
@qr = RQRCode::QRCode.new("二维码内容")
```

## 二维码展示

```html
<style>
  table {
    border-width: 0;
    border-style: none;
    border-color: #0000ff;
    border-collapse: collapse;
  }

  td {
    border-left: solid 10px #000;
    padding: 0;
    margin: 0;
    width: 0px;
    height: 10px;
  }

  td.black { border-color: #000; }
  td.white { border-color: #fff; }

  .qr_code table {width: 10px;}
  .qr_code table tr {height: auto;}
  .qr_code table td{height: 0;}
  .qr_code table td.black{border: 2px solid black;}
  .qr_code table td.white{border: 2px solid white;}
</style>
<%= raw @qr.as_html %>
```

## 关于打印失真问题

页面生成png或pdf文件后,二维码若出现模糊,失真等情况.<br>
可以将二维码以svg格式展示

```ruby
qr_code = RQRCode::QRCode.new(qr_link)
@qr_code_svg = qr_code.as_svg(offset: 0,color: '000',shape_rendering: 'crispEdges',module_size: 1.3, standalone: true )
```

```erb
<%= @qr_code_svg.html_safe %>
```

# 条形码

> 这里选用了`SVG`格式输出,其他格式如HTML有遇到页面打印保存为PDF后条形码失真等情况.<br>

## 生成条形码

```ruby
require 'barby/barcode/code_128'
require 'barby/outputter/svg_outputter'

barcode = Barby::Code128B.new('需要展示的内容')
@outputter = Barby::SvgOutputter.new(barcode).to_svg
```

## 展示

```erb
<%= @outputter.html_safe %>
```

# 关于打印失真问题

SVG格式可有效解决打印失真问题.

# 参考资料

> [GitHub | rqrcode](https://github.com/whomwah/rqrcode)

> [GitHub | Barby | Outputters](https://github.com/toretore/barby/wiki/Outputters)

> [GitHub | rqrcode\#as-svg](https://github.com/whomwah/rqrcode#as-svg)



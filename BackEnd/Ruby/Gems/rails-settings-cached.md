> 存储在数据库中的全局Hash

<!-- TOC -->

- [安装](#安装)
  - [Gemfile](#gemfile)
  - [Generate](#generate)
  - [rake db:migrate](#rake-dbmigrate)
- [使用](#使用)
  - [赋值](#赋值)
  - [读取](#读取)
  - [删除](#删除)
  - [获取所有setting](#获取所有setting)
  - [命名空间](#命名空间)
    - [获取所有setting](#获取所有setting-1)
  - [默认设置](#默认设置)
    - [缓存流程](#缓存流程)
    - [更改缓存密钥](#更改缓存密钥)
  - [前台表单设置Settings](#前台表单设置settings)
    - [config/routes.rb](#configroutesrb)
    - [app/controllers/admin/settings_controller.rb](#appcontrollersadminsettings_controllerrb)
  - [扩展模型](#扩展模型)
    - [读取设置](#读取设置)
    - [查找具有或不具有某些设置的用户](#查找具有或不具有某些设置的用户)
- [参考资料](#参考资料)

<!-- /TOC -->

# 安装

## Gemfile

```ruby
gem "rails-settings-cached"
```

## Generate

```ruby
# 默认生成名为Setting的模型,可以自定义模型名称
rails g settings:install [model_name]
```

执行后会生成下面文件:

```ruby
# app/models/setting.rb
class Setting < RailsSettings::Base
  source Rails.root.join("config/app.yml")
  # cache_prefix { "v1" }
end
```

## rake db:migrate 

# 使用

## 赋值

```ruby
Setting.admin_password = 'supersecret'
Setting.date_format    = '%m %d, %Y'
Setting.cocktails      = ['Martini', 'Screwdriver', 'White Russian']
Setting.credentials    = { :username => 'tom', :password => 'secret' }
```

## 读取

```ruby
Setting.admin_password #=> 'supersecret'
```

## 删除

```ruby
Setting.destroy :admin_password
Setting.admin_password #=> nil
```

## 获取所有setting

```ruby
Setting.get_all
```

## 命名空间

```ruby
Setting['preferences.color'] = :blue
Setting['preferences.size'] = :large
```

### 获取所有setting

```ruby
Setting.get_all('preferences.')
```

## 默认设置

RailsS​​ettings生成了一个配置YAML文件,在这里可以设置默认配置:

```yml
# config/app.yml
defaults: &defaults
  github_token: "123456"
  twitter_token: "<%= ENV["TWITTER_TOKEN"] %>"
  foo:
    bar: "Foo bar"

development:
  <<: *defaults

test:
  <<: *defaults

production:
  <<: *defaults
```

### 缓存流程

```
Setting.foo -> Check Cache -> Exist - Write Cache -> Return
                   |
               Check DB -> Exist -> Write Cache -> Return
                   |
               Check Default -> Exist -> Write Cache -> Return
                   |
               Return nil
```

### 更改缓存密钥

当config/app.yml发生更改时,需要更改缓存前缀以使缓存过期:

```ruby
class Setting < RailsSettings::Base
  ...
  cache_prefix { 'app.yml_change_at: 2019-01-29' }
  ...
end
```

## 前台表单设置Settings

### config/routes.rb

```ruby
namespace :admin do
  resources :settings
end
```

### app/controllers/admin/settings_controller.rb

```ruby
module Admin
  class SettingsController < ApplicationController
    before_action :get_setting, only: [:edit, :update]

    def index
      @settings = Setting.get_all
    end

    def edit
    end

    def update
      if @setting.value != params[:setting][:value]
        @setting.value = params[:setting][:value]
        @setting.save
        redirect_to admin_settings_path, notice: 'Setting has updated.'
      else
        redirect_to admin_settings_path
      end
    end

    def get_setting
      @setting = Setting.find_by(var: params[:id]) || Setting.new(var: params[:id])
    end
  end
end
```

## 扩展模型

***对模型进行扩展的内容没有设置缓存***

```ruby
class User < ActiveRecord::Base
  include RailsSettings::Extend
end
```

### 读取设置

```ruby
user.settings.color = :red
user.settings.color #=> :red
user.settings.get_all
# { "color" => :red }
```

### 查找具有或不具有某些设置的用户

```ruby
User.with_settings 
#=> 返回具有任何设置的用户

User.without_settings 
#=> 返回完全没有设置的用户(等价于 user.settings.get_all == {})

User.with_settings_for('color') 
#=> 返回具有"颜色"设置的用户范围

User.without_settings('color') 
#=> 返回没有'color'设置的用户范围(等价于 user.settings.color == nil)
```

# 参考资料

> [Github | rails-settings-cached](https://github.com/huacnlee/rails-settings-cached)
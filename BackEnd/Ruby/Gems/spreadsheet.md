> Excel表格的读取和导出

<!-- TOC -->

- [常见问题](#%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98)
  - [Ole::Storage::FormatError: OLE2 signature is invalid](#olestorageformaterror-ole2-signature-is-invalid)

<!-- /TOC -->

# 常见问题

## Ole::Storage::FormatError: OLE2 signature is invalid

> excel文件保存格式问题,保存为 `97-2003(*xls)`格式即可.
<!-- TOC -->

- [增](#增)
  - [表单请求](#表单请求)
    - [[simple_form](./simple_form.md)](#simple_formsimple_formmd)
  - [文件上传](#文件上传)
    - [[carrierwave](./carrierwave.md)](#carrierwavecarrierwavemd)
    - [paperclip](#paperclip)
  - [批量导入数据库](#批量导入数据库)
    - [activerecord-import](#activerecord-import)
- [删](#删)
  - [软删除](#软删除)
    - [[paranoia](./paranoia.md)](#paranoiaparanoiamd)
- [改](#改)
  - [富文本编辑器](#富文本编辑器)
    - [kindeditor](#kindeditor)
    - [ckeditor](#ckeditor)
    - [ueditor](#ueditor)
  - [定时任务](#定时任务)
    - [whenever](#whenever)
    - [resque](#resque)
    - [clock](#clock)
  - [异步任务](#异步任务)
    - [sidekiq+sidetip](#sidekiqsidetip)
- [查](#查)
  - [模糊查询](#模糊查询)
    - [[ransack](./ransack.md)](#ransackransackmd)
  - [权限控制](#权限控制)
    - [[cancancan](./cancancan.md)](#cancancancancancanmd)
    - [pundit](#pundit)
    - [Policy](#policy)
  - [接口对接处理](#接口对接处理)
    - [rest-client](#rest-client)
    - [httparty](#httparty)
  - [树形结构数据库](#树形结构数据库)
    - [[ancestry](./ancestry.md)](#ancestryancestrymd)
  - [静态配置](#静态配置)
    - [[rails-settings-cached](./rails-settings-cached.md)](#rails-settings-cachedrails-settings-cachedmd)
  - [内存数据库](#内存数据库)
    - [redis/redis-namespace](#redisredis-namespace)
- [展](#展)
  - [分页](#分页)
    - [Kaminari](#kaminari)
    - [will_paginate](#will_paginate)
  - [图表](#图表)
    - [[lazy_high_charts](./lazy_high_charts.md)](#lazy_high_chartslazy_high_chartsmd)
  - [表格展示](#表格展示)
    - [jqgird](#jqgird)
    - [excelc处理](#excelc处理)
    - [[spreadsheet](./spreadsheet.md)](#spreadsheetspreadsheetmd)
  - [二维码&条形码](#二维码条形码)
    - [[rqrcode&barby](./Gems/rqrcode&barby.md)](#rqrcodebarbygemsrqrcodebarbymd)
  - [验证码](#验证码)
    - [[simple_captcha2](./simple_captcha2.md)](#simple_captcha2simple_captcha2md)
  - [PDF](#pdf)
    - [wicked_pdf(./wicked_pdf.md)](#wicked_pdfwicked_pdfmd)
    - [wkhtmltopdf-binary](#wkhtmltopdf-binary)
    - [pdfjs_viewer-rails](#pdfjs_viewer-rails)
    - [prawn/prawn-table](#prawnprawn-table)
    - [word转pdf](#word转pdf)
      - [libreconv](#libreconv)
  - [快照](#快照)
    - [wkhtmltoimage-binary](#wkhtmltoimage-binary)
  - [中文转拼音](#中文转拼音)
    - [ruby-pinyin](#ruby-pinyin)
  - [视频处理](#视频处理)
    - [streamio-ffmpeg](#streamio-ffmpeg)
  - [大写金额](#大写金额)
    - [number_to_capital_zh](#number_to_capital_zh)
- [调](#调)
  - [测试](#测试)
    - [shoulda-matchers](#shoulda-matchers)
    - [rspec-rails](#rspec-rails)
    - [guard-rspec/factory_gril](#guard-rspecfactory_gril)
    - [minitest](#minitest)
  - [bug调试](#bug调试)
    - [pry-rails](#pry-rails)
    - [pry-byebug](#pry-byebug)
- [备](#备)
  - [数据库备份](#数据库备份)
    - [backup](#backup)
  - [打包](#打包)
    - [rubyzip](#rubyzip)
- [其](#其)
  - [服务](#服务)
    - [thin](#thin)
    - [puma](#puma)
    - [passenger](#passenger)
  - [xml解析](#xml解析)
    - [nokorigi](#nokorigi)
    - [settingslogic](#settingslogic)
  - [状态机](#状态机)
    - [aasm](#aasm)
    - [workflow](#workflow)
  - [webservice](#webservice)
    - [服务端](#服务端)
      - [wash_out](#wash_out)
    - [客户端](#客户端)
      - [savon](#savon)
  - [用户管理](#用户管理)
    - [[devise](.devise.md)](#devisedevisemd)
  - [异常记录](#异常记录)
    - [exception_notification](#exception_notification)
  - [审核记录](#审核记录)
    - [audited](#audited)

<!-- /TOC -->

# 增

## 表单请求
### [simple_form](./simple_form.md)

## 文件上传
### [carrierwave](./carrierwave.md)
### paperclip

## 批量导入数据库
### activerecord-import

# 删

## 软删除
### [paranoia](./paranoia.md)

# 改

## 富文本编辑器
### kindeditor
### ckeditor
### ueditor

## 定时任务
### whenever
### resque
### clock

## 异步任务
### sidekiq+sidetip


# 查

## 模糊查询
### [ransack](./ransack.md)

## 权限控制
### [cancancan](./cancancan.md)
### pundit
### Policy

## 接口对接处理
### rest-client
### httparty

## 树形结构数据库
### [ancestry](./ancestry.md)

## 静态配置
### [rails-settings-cached](./rails-settings-cached.md)

## 内存数据库
### redis/redis-namespace

# 展

## 分页
### Kaminari
### will_paginate

## 图表
### [lazy_high_charts](./lazy_high_charts.md)

## 表格展示
### jqgird

### excelc处理
### [spreadsheet](./spreadsheet.md)

## 二维码&条形码
### [rqrcode&barby](./Gems/rqrcode&barby.md)

## 验证码
### [simple_captcha2](./simple_captcha2.md)

## PDF 
### wicked_pdf(./wicked_pdf.md)
### wkhtmltopdf-binary
### pdfjs_viewer-rails
### prawn/prawn-table
### word转pdf
#### libreconv

## 快照
### wkhtmltoimage-binary

## 中文转拼音
### ruby-pinyin

## 视频处理
### streamio-ffmpeg

## 大写金额
### number_to_capital_zh

# 调

## 测试
### shoulda-matchers
### rspec-rails
### guard-rspec/factory_gril
### minitest

## bug调试
### pry-rails
### pry-byebug

# 备

## 数据库备份
### backup

## 打包
### rubyzip


# 其

## 服务
### thin  
### puma
### passenger

## xml解析
### nokorigi
### settingslogic

## 状态机
### aasm
### workflow

## webservice
### 服务端
#### wash_out
### 客户端
#### savon

## 用户管理
### [devise](.devise.md)

## 异常记录
### exception_notification

## 审核记录
### audited
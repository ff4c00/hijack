
<!-- TOC -->

- [相关依赖](#相关依赖)
- [Gemfile](#gemfile)
- [初始化](#初始化)
- [相关命令](#相关命令)
- [参考资料](#参考资料)

<!-- /TOC -->

# 相关依赖

* crontab

# Gemfile

```Gemfile
gem 'whenever', require: false
```

# 初始化

```bash
bundle exec wheneverize .
```

# 相关命令

命令|作用
-|-
whenever --update-crontab|更新定时任务
whenever --user app|设置执行定时任务的用户
whenever --load-file config/my_schedule.rb|设置schedule文件路径
whenever --crontab-command 'sudo crontab'|重写crontab命令

# 参考资料

> [Github | whenever](https://github.com/javan/whenever)

<!-- TOC -->

- [安装](#安装)
  - [Gemfile](#gemfile)
  - [generate](#generate)
  - [mime_types.rb](#mime_typesrb)
- [使用](#使用)
  - [基本用法](#基本用法)
  - [wicked_pdf_*_tag](#wicked_pdf__tag)
  - [wicked_pdf_asset_base64](#wicked_pdf_asset_base64)
  - [可用选项](#可用选项)
  - [高级用法](#高级用法)
  - [分页](#分页)
  - [页码](#页码)
  - [WickedPdf::Middleware](#wickedpdfmiddleware)
  - [包含在电子邮件中作为附件](#包含在电子邮件中作为附件)
  - [调试](#调试)
  - [model中调用render_to_string](#model中调用render_to_string)
- [参考资料](#参考资料)

<!-- /TOC -->

# 安装 

## Gemfile

```ruby
# 依赖
gem 'wkhtmltopdf-binary'
gem 'wicked_pdf'
```


## generate

```bash
rails generate wicked_pdf
```

## mime_types.rb

添加:

```ruby
# config/initializers/mime_types.rb
Mime::Type.register "application/pdf", :pdf
```

# 使用

## 基本用法

```ruby
class ThingsController < ApplicationController
  def show
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "file_name"   # 不包括 ".pdf" 扩展名
      end
    end
  end
end
```

## wicked_pdf_*_tag

wkhtmltopdf程序在Rails应用程序之外运行.<br> 
因此,布局将无法正常工作.<br>
如果计划使用任何CSS,JavaScript或图像文件,<br>
则必须修改布局,以便提供对这些文件的绝对引用.<br>
最佳选择是使用wicked_pdf_stylesheet_link_tag,wicked_pdf_image_tag和wicked_pdf_javascript_include_tag或直接引用一个CDN.

```erb
<!doctype html>
<html>
  <head>
    <meta charset='utf-8' />
    <%= wicked_pdf_stylesheet_link_tag "pdf" -%>
    <%= wicked_pdf_javascript_include_tag "number_pages" %>
  </head>
  <body onload='number_pages'>
    <div id="header">
      <%= wicked_pdf_image_tag 'mysite.jpg' %>
    </div>
    <div id="content">
      <%= yield %>
    </div>
  </body>
</html>
```

## wicked_pdf_asset_base64

wicked_pdf_*_tag和普通资源引用标签一起使用会报出:

Asset names passed to helpers should not include the "/assets/" prefix.

要解决此问题,可以使用wicked_pdf_asset_base64.

```erb
<!doctype html>
<html>
  <head>
    <meta charset='utf-8' />
    <%= stylesheet_link_tag wicked_pdf_asset_base64("pdf") %>
    <%= javascript_include_tag wicked_pdf_asset_base64("number_pages") %>

  </head>
  <body onload='number_pages'>
    <div id="header">
      <%= image_tag wicked_pdf_asset_base64('mysite.jpg') %>
    </div>
    <div id="content">
      <%= yield %>
    </div>
  </body>
</html>
```

## 可用选项

```ruby
class ThingsController < ApplicationController
  def show
    respond_to do |format|
      format.html
      format.pdf do
        render pdf:                            'file_name',
               disposition:                    'attachment',                 # 默认 'inline'
               template:                       'things/show',
               file:                           "#{Rails.root}/files/foo.erb",
               inline:                         '<!doctype html><html><head></head><body>INLINE HTML</body></html>',
               layout:                         'pdf',                        # 对于pdf.pdf.erb文件
               wkhtmltopdf:                    '/usr/local/bin/wkhtmltopdf', # wkhtmltopdf程序路径
               show_as_html:                   params.key?('debug'),         # 允许基于url参数进行调试
               orientation:                    'Landscape',                  # 默认 纵向
               page_size:                      'A4, Letter, ...',            # 默认 A4
               page_height:                    NUMBER,
               page_width:                     NUMBER,
               save_to_file:                   Rails.root.join('pdfs', "#{filename}.pdf"),
               save_only:                      false,                        # 先保存到正在设置的文件
               default_protocol:               'http',
               proxy:                          'TEXT',
               basic_auth:                     false                         # 当真正的用户名和密码从会话自动发送时
               username:                       'TEXT',
               password:                       'TEXT',
               title:                          'Alternate Title',            # 否则使用第一页标题
               cover:                          'URL, Pathname, or raw HTML string',
               dpi:                            'dpi',
               encoding:                       'TEXT',
               user_style_sheet:               'URL',
               cookie:                         ['_session_id SESSION_ID'], # 可以是数组或单个字符串,格式为"name-value"
               post:                           ['query QUERY_PARAM'],      # 可以是数组或单个字符串,格式为"name-value"
               redirect_delay:                 NUMBER,
               javascript_delay:               NUMBER,
               window_status:                  'TEXT',                     # 等待呈现,直到某些JS将window.status设置为给定的字符串
               image_quality:                  NUMBER,
               no_pdf_compression:             true,
               zoom:                           FLOAT,
               page_offset:                    NUMBER,
               book:                           true,
               default_header:                 true,
               disable_javascript:             false,
               grayscale:                      true,
               lowquality:                     true,
               enable_plugins:                 true,
               disable_internal_links:         true,
               disable_external_links:         true,
               print_media_type:               true,
               disable_smart_shrinking:        true,
               use_xserver:                    true,
               background:                     false,                     # background必须为真才能使背景色呈现
               no_background:                  true,
               viewport_size:                  'TEXT',                    # 仅适用于使用 use_xserver 或 patched QT
               extra:                          '',                        # 直接插入到wkhtmltopdf命令中
               raise_on_all_errors:            nil,                       # 对任何stderr输出引发错误,例如丢失的媒体、图像资源
               outline: {   outline:           true,
                            outline_depth:     LEVEL },
               margin:  {   top:               SIZE,                     # 默认 10 (mm)
                            bottom:            SIZE,
                            left:              SIZE,
                            right:             SIZE },
               header:  {   html: {            template: 'users/header',          # 使用:模板或:url
                                               layout:   'pdf_plain',             # 可选,将"pdf-plain"用于pdf-plain.html.pdf.erb文件,默认为主布局
                                               url:      'www.example.com',
                                               locals:   { foo: @bar }},
                            center:            'TEXT',
                            font_name:         'NAME',
                            font_size:         SIZE,
                            left:              'TEXT',
                            right:             'TEXT',
                            spacing:           REAL,
                            line:              true,
                            content:           'HTML CONTENT ALREADY RENDERED'}, # 或者,可以传递已经呈现的纯HTML(如果使用pdf-from-u字符串,则很有用)
               footer:  {   html: {   template:'shared/footer',         # 使用:模板或:url
                                      layout:  'pdf_plain.html',        # 可选,将"pdf-plain"用于pdf-plain.html.pdf.erb文件,默认为主布局
                                      url:     'www.example.com',
                                      locals:  { foo: @bar }},
                            center:            'TEXT',
                            font_name:         'NAME',
                            font_size:         SIZE,
                            left:              'TEXT',
                            right:             'TEXT',
                            spacing:           REAL,
                            line:              true,
                            content:           'HTML CONTENT ALREADY RENDERED'}, # 可以传递已经呈现的纯HTML(如果使用pdf-from-u字符串,则很有用)
               toc:     {   font_name:         "NAME",
                            depth:             LEVEL,
                            header_text:       "TEXT",
                            header_fs:         SIZE,
                            text_size_shrink:  0.8,
                            l1_font_size:      SIZE,
                            l2_font_size:      SIZE,
                            l3_font_size:      SIZE,
                            l4_font_size:      SIZE,
                            l5_font_size:      SIZE,
                            l6_font_size:      SIZE,
                            l7_font_size:      SIZE,
                            level_indentation: NUM,
                            l1_indentation:    NUM,
                            l2_indentation:    NUM,
                            l3_indentation:    NUM,
                            l4_indentation:    NUM,
                            l5_indentation:    NUM,
                            l6_indentation:    NUM,
                            l7_indentation:    NUM,
                            no_dots:           true,
                            disable_dotted_lines:  true,
                            disable_links:     true,
                            disable_toc_links: true,
                            disable_back_links:true,
                            xsl_style_sheet:   'file.xsl'}, # 用于设置目录样式的可选XSLT样式表
               progress: proc { |output| puts output } # 控制台输出更改时调用的过程
      end
    end
  end
end
```

## 高级用法

```ruby
# 从字符串创建PDF
pdf = WickedPdf.new.pdf_from_string('<h1>Hello There!</h1>')

# 从HTML文件创建PDF文件,而不将其转换为字符串
# 路径必须是绝对路径
pdf = WickedPdf.new.pdf_from_html_file('/your/absolute/path/here')

# 从URL创建PDF
pdf = WickedPdf.new.pdf_from_url('https://github.com/mileszs/wicked_pdf')

# 使用页眉或页脚的模板、布局和内容选项从字符串创建PDF
pdf = WickedPdf.new.pdf_from_string(
  render_to_string('templates/pdf', layout: 'pdfs/layout_pdf.html'),
  footer: {
    content: render_to_string(
  		'templates/footer',
  		layout: 'pdfs/layout_pdf.html'
  	)
  }
)

# 可以在没有布局的情况下使用页脚/页眉模板,在这种情况下,需要提供有效的HTML文档
pdf = WickedPdf.new.pdf_from_string(
  render_to_string('templates/full_pdf_template'),
  header: {
    content: render_to_string('templates/full_header_template')
  }
)

# 或者从你的控制器,使用视图和模板以及所有邪恶的PDF选项
pdf = render_to_string pdf: "some_file_name", template: "templates/pdf", encoding: "UTF-8"

# 然后保存到文件
save_path = Rails.root.join('pdfs','filename.pdf')
File.open(save_path, 'wb') do |file|
  file << pdf
end

# 还可以跟踪生成PDF的进度,例如在重新排序作业中使用它时
class PdfJob
  def perform
    blk = proc { |output|
      match = output.match(/\[.+\] Page (?<current_page>\d+) of (?<total_pages>\d+)/)
      if match
        current_page = match[:current_page].to_i
        total_pages = match[:total_pages].to_i
        message = "Generated #{current_page} of #{total_pages} pages"
        at current_page, total_pages, message
      end
    }
    WickedPdf.new.pdf_from_string(html, progress: blk)
  end
end
```

如果需要显示utf编码字符,请将其添加到pdf视图或布局中:

```html
<meta charset="utf-8" />
```

如果需要在API模式下使用Rails在控制器中返回PDF:

```ruby
pdf_html = ActionController::Base.new.render_to_string(template: 'controller_name/action_name', layout: 'pdf')
pdf = WickedPdf.new.pdf_from_string(pdf_html)
send_data pdf, filename: 'file.pdf'
```

## 分页

可以使用CSS控制分页符,在样式表或页面中添加以下几种样式:

```ruby
div.alwaysbreak { page-break-before: always; }
div.nobreak:before { clear:both; }
div.nobreak { page-break-inside: avoid; }
```

## 页码

可以使用以下命令创建模板或页眉/页脚文件:

```html
<html>
  <head>
    <script>
      function number_pages() {
        var vars={};
        var x=document.location.search.substring(1).split('&');
        for(var i in x) {var z=x[i].split('=',2);vars[z[0]] = decodeURIComponent(z[1]);}
        var x=['frompage','topage','page','webpage','section','subsection','subsubsection'];
        for(var i in x) {
          var y = document.getElementsByClassName(x[i]);
          for(var j=0; j<y.length; ++j) y[j].textContent = vars[x[i]];
        }
      }
    </script>
  </head>
  <body onload="number_pages()">
    Page <span class="page"></span> of <span class="topage"></span>
  </body>
</html>
```

上面"var x"中列出的任何类都将在渲染时自动填充.<br>


如果没有明确的分页符也可以通过将其中一个标题设置为"[page]"来使用wkhtmltopdf的内置页码生成:

```ruby
render pdf: 'filename', header: { right: '[page] of [topage]' }
```

## WickedPdf::Middleware

如果希望WickedPdf通过将.pdf附加到URL自动为所有(或几乎所有)页面生成PDF视图,请将以下内容添加到Rails应用程序中:

```ruby
# application.rb
require 'wicked_pdf'
config.middleware.use WickedPdf::Middleware

# 如果想打开或关闭的中间件某些URL,使用:only或:except条件
# conditions can be plain strings or regular expressions, and you can supply only one or an array
config.middleware.use WickedPdf::Middleware, {}, only: '/invoice'
config.middleware.use WickedPdf::Middleware, {}, except: [ %r[^/admin], '/secret', %r[^/people/\d] ]
```

## 包含在电子邮件中作为附件

```ruby
attachments['attachment.pdf'] = WickedPdf.new.pdf_from_string(
  render_to_string('link_to_view.pdf.erb', layout: 'pdf')
)
```

这会将pdf呈现为字符串并将其包含在电子邮件中,这非常慢.

## 调试

可以在URL上使用调试参数,以简单的html格式显示pdf的内容:

```ruby
# 参数设置
show_as_html: params.key?('debug')
# 访问 http://localhost:XXXX/XXXXXX/X.pdf?debug
```

## model中调用render_to_string

model中调用render_to_string然后生成pdf,然后保存

```ruby
# locals 起到变量传递作用
html = ActionController::Base.new.render_to_string(template: 'rest_url/show.html.erb', :layout => false, locals:{pdf_supplier: supplier, pdf_quote: quote})
pdf = WickedPdf.new.pdf_from_string(html,orientation: 'Landscape')
save_path = Rails.root.join('save_path', "#{pdf_name}.pdf")
File.open(save_path, 'wb') do |file|
  file << pdf
end
```

# 参考资料

> [GitHub | wicked_pdf](https://github.com/mileszs/wicked_pdf)

> [代码日志 | ruby-on-rails 如何将变量传递给render_to_string?](https://codeday.me/bug/20180921/257113.html)

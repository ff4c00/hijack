> 支持排序的模糊搜索插件.

<!-- TOC -->

- [常用搜索条件](#常用搜索条件)
- [实例](#实例)
  - [cont_any](#cont_any)
- [参考资料](#参考资料)

<!-- /TOC -->

# 常用搜索条件

条件|含义
-|-
eq|等于
eq_any|等于任意值
eq_all|等于所有值
not_eq|不等于
not_eq_any|不等于任意值
not_eq_all|不等于所有值
matches|符合
matches_any|符合任意条件
matches_all|符合所有条件
does_not_match|不符合
does_not_match_any|符合任意条件
does_not_match_all|不符合所有条件
lt|小于
lt_any|小于任意一个值
lt_all|小于所有值
lteq|小于等于
lteq_any|小于等于任意一个值
lteq_all|小于等于所有值
gt|大于
gt_any|大于任意一个值
gt_all|大于所有值
gteq|大于等于
gteq_any|大于等于任意一个值
gteq_all|大于等于所有值
in|被包含
in_any|被任意值包含
in_all|被所有值包含
not_in|不被包含
not_in_any|不被任意值包含
not_in_all|不被所有值包含
cont|包含
cont_any|包含任意一个值
cont_all|包含所有值
not_cont|不包含
not_cont_any|不包含任意一个值
not_cont_all|不包含所有值
start|以改值开始
start_any|以任意一个值开始
start_all|以所有值开始
not_start|不以改值开始
not_start_any|不以任意一个值开始
not_start_all|不以所有值开始
end|以改值结尾
end_any|以任意一个值结尾
end_all|以所有值结尾
not_end|不以改值结尾
not_end_any|不以任意一个值结尾
not_end_all|不以所有值结尾
'true'|等于true
'false'|等于false
present|有值
blank|为空
'null'|是null
not_null|不是null

# 实例

## cont_any

```erb
<div class="form-group">
  <label class="control-label col-xs-1">供应商名称:</label>
  <div class="col-xs-2">
    <%= f.text_field :supplier_name_cont_any, class: 'form-control' %>
  </div>  
</div>
```

同一字段多个候选参数的需要在后台把参数格式化为数组:

```ruby
# "a b  c,d,e;f;g|handi.j.k、l\tm".split(/ |,|,|;|;|\||and|\.|.|、|\t/).delete_if{|a| !a.present?}
if params[:q][:supplier_name_cont_any].present?
  params[:q][:supplier_name_cont_any] = params[:q][:supplier_name_cont_any].split(/ |,|,|;|;|\||and|\.|.|、|\t/).delete_if{|a| !a.present?}
end
```

# 参考资料
[RubyChina-开源项目 ransack 搜索条件](https://ruby-china.org/topics/29556)


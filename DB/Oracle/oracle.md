<!-- TOC -->

- [环境说明](#环境说明)
  - [环境搭建](#环境搭建)
- [删](#删)
  - [清除表结构](#清除表结构)
    - [关于purge](#关于purge)
  - [删除触发器](#删除触发器)
  - [删除序列](#删除序列)
- [触发器](#触发器)
  - [查询触发器具体内容](#查询触发器具体内容)
  - [常见问题](#常见问题)
    - [关联id保存失真](#关联id保存失真)
      - [问题描述](#问题描述)
      - [表结构](#表结构)
      - [问题特征](#问题特征)
      - [问题源](#问题源)
      - [解决措施](#解决措施)
      - [创建序列及触发器示例](#创建序列及触发器示例)
- [常见报错](#常见报错)
  - [ORA-28001](#ora-28001)
- [创建只读用户](#创建只读用户)
  - [创建用户](#创建用户)
  - [赋连接权限](#赋连接权限)
  - [赋表权限](#赋表权限)
  - [创建同义词](#创建同义词)
- [参考资料](#参考资料)

<!-- /TOC -->
# 环境说明

名称|版本
-|-
oracle| 11g+
ruby|2.3.7
rails|4.1.7
ruby-oci8|2.2.5.1
activerecord-oracle_enhanced-adapter|1.5.6

## 环境搭建

# 删

## 清除表结构

### 关于purge

purge用于清除oracle 回收站(recyclebin)中的表和索引并释放与其相关的空间,还可清空回收站,或者清除表空间中记录的已删除的部分表空间.<br>
注意:purge后不能回滚和恢复.

如果不使用purge参数,重新建表提示表名已存在,通过下面语句查询确实存在记录:

```sql 
select count(*) from user_tables where table_name =upper('表名')
```
      
```sql 
-- 删除表
drop table 表名 purge;

-- 删除序列
DROP SEQUENCE 表名_SEQ; 
```

## 删除触发器

```sql
DROP TRIGGER trigger_name;
```

## 删除序列

```sql
DROP SEQUENCE 序列名;
```

# 触发器

## 查询触发器具体内容

```sql
select dbms_metadata.get_ddl('TRIGGER','大写触发器名称','用户名') from dual;
```

## 常见问题

### 关联id保存失真

#### 问题描述

现有两张表,orders和order_items,关系如下表所示.<br>
订单保存后问题在于订单保存的产品并非下单时所购买的产品.<br>
通过断点发现了比较关键的特征.

#### 表结构

&nbsp;|orders|order_items
-|-|-
关联关系|一|多
关联字段||order_id

#### 问题特征

时间点|orders|order_items
-|-|-
前|id为503|order_id为503
后|id为504|order_id为503

#### 问题源

和触发器有关,保存时id已存在,但在触发器的作用下id又发生了变化

#### 解决措施

创建触发器时判断应自增序列是否null,如果为null再触发.

#### 创建序列及触发器示例

```sql
DROP SEQUENCE #{table_name}_ID_SEQ;

CREATE SEQUENCE #{table_name}_ID_SEQ START WITH #{last_id + num};

CREATE OR REPLACE TRIGGER #{table_name}_TRG
BEFORE INSERT ON #{table_name}
FOR EACH ROW
BEGIN
  IF :new.id IS NULL THEN
    :new.id := #{table_name}_ID_SEQ.NEXTVAL;
  END IF;
END;
/

```
`/` 为必须项

触发器执行成功示例:

![触发器执行成功示例](./assets/images/oracle-触发器执行成功示例.png)

> 还有另外一个可能就是一张表上作用了多个触发器,删掉多余的即可.


# 常见报错

## ORA-28001

> Oracle11G创建用户时缺省密码过期限制是180天(即6个月),<br> 
如果超过180天用户密码未做修改则该用户无法登录.

> 解决措施

1. 将密码有效期由默认的180天修改成"无限制":

```sql
ALTER PROFILE DEFAULT LIMIT PASSWORD_LIFE_TIME UNLIMITED;
```

修改之后不需要重启动数据库,会立即生效.

2. 修改后,再改一次密码:

```bash
sqlplus / as sysdba
sql>alter user 用户名 identified by <原来的密码> account unlock; ----不用换新密码
```

# 创建只读用户

## 创建用户

```sql
create user 用户名 identified by 密码 default tablespace 表空间;
```

## 赋连接权限 

```sql
grant connect to 用户名;
```

## 赋表权限

```sql
grant select on owner.表名 to 用户名;

--- 如果有多表,可以用selece转换批量执行语句:
select 'grant select on '||owner||'.'||object_name||' to 用户名;'
from dba_objects
where owner in ('owner')
and object_type='TABLE';
```

## 创建同义词

```sql
create or replace SYNONYM 用户名.表名 FOR owner.表名;
 
--- 如果有多表,可以用selece转换批量执行语句:
SELECT 'create or replace SYNONYM  用户名.'||object_name||' FOR '||owner||'.'||object_name||';'  from dba_objects 
where owner in ('owner')
and object_type='TABLE';
```


# 参考资料

> [博客园 | Oracle报错,ORA-28001: 口令已经失效](https://www.cnblogs.com/luckly-hf/p/3828573.html)

> [CSDN | oracle创建只读权限的用户](https://blog.csdn.net/antma/article/details/53435704)

> [CSDN | oracle数据库判断某表是否存在](https://blog.csdn.net/wohaqiyi/article/details/79358338)

> [CSDN | Oracle purge 用法介绍](https://blog.csdn.net/indexman/article/details/27379597)

> An error occurred while installing ruby-oci8 (2.1.8), and  
Bundler cannot continue.  
Make sure that `gem install ruby-oci8 -v '2.1.8'` succeeds  
before bundling

> 如果Rails项目中使用了Oracle数据库,在bundle时遇到了上面错误,下面的内容就是为了解决这个问题而存在的.

> Rails 应用连接Oracle数据库Bundle前准备事项

<!-- TOC -->

- [下载Oracle依赖](#下载oracle依赖)
- [/opt/oracle/instantclient_12_1](#optoracleinstantclient_12_1)
- [~/.bashrc](#bashrc)
- [关于macOs系统](#关于macos系统)
- [参考资料](#参考资料)

<!-- /TOC -->
 
# 下载Oracle依赖

官网下载下面三个文件:

* [instantclient-basic](http://download.oracle.com/otn/linux/instantclient/121020/instantclient-basic-linux.x64-12.1.0.2.0.zip) 
* [instantclient-sqlplus](http://download.oracle.com/otn/linux/instantclient/121010/instantclient-sqlplus-linux.x64-12.1.0.1.0.zip) 
* [instantclient-sdk](http://download.oracle.com/otn/linux/instantclient/121010/instantclient-sdk-linux.x64-12.1.0.1.0.zip) 

`下面创建的instantclient_12_1目录,其中版本号要以下载的具体版本为准.`

# /opt/oracle/instantclient_12_1

0. 将`instantclient-basic` 解压得到 `instantclient_12_1`文件夹.
0. 将 *instantclient-sqlplus* 和 *instantclient-sdk* 解压后的内容 `合并替换` 到上一步骤解压的目录.
0. mkdir -p /opt/oracle
0. cp -rf instantclient_12_1/ /opt/oracle/instantclient_12_1
0. sudo apt-get install libaio1 
0. sudo cd instantclient_12_1 
1. sudo ln -s libclntsh.so.12.1 libclntsh.so<sup>A1<sup> 

# ~/.bashrc 

添加如下环境变量:

```bash
export LD_LIBRARY_PATH=/opt/oracle/instantclient_12_1 
export NLS_LANG=AMERICAN_AMERICA.UTF8 # 汉字显示乱码
```


# 关于macOs系统

该文档某些步骤只要稍加改动即可适用于macOs系统:

A1改为:

```bash
ln -s libclntsh.dylib.12.1 libclntsh.dylib
```
<hr>
macOs没有mnt目录,需要自己弄,设置如下:

```bash
export OCI_DIR=/目录/oracle/instantclient_12_1 
```
<hr>

`gem安装好`之后,连接数据库报错:

```bash
oci8.c:601:in oci8lib_230.bundle: ORA-21561: OID generation failed (OCIError)
```

解决措施:

```bash
echo "127.0.0.1 $(hostname)" | sudo tee -a /etc/hosts
```

# 参考资料

> [Ubuntu下rails程序链接oracle数据库 | ChinaCheng](https://chinacheng.iteye.com/)

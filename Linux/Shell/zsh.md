> Zsh 是一款功能强大的终端(shell)软件,既可以作为一个交互式终端,也可以作为一个脚本解释器.

它在兼容 Bash 的同时 (默认不兼容,除非设置成 emulate sh) 还有提供了很多改进,例如：

0. 更高效
0. 更好的自动补全
0. 更好的文件名展开(通配符展开)
0. 更好的数组处理
0. 可定制性高

# 配置文件

## 手动打开新用户向导

```
zsh /usr/share/zsh/functions/Newuser/zsh-newuser-install -f
```

## 初始化加载顺序

当 Zsh 启动时,它会按照顺序依次读取下面的配置文件：

文件路径|说明
-|-
/etc/zsh/zshenv|该文件应该包含用来设置PATH 环境变量[broken link: invalid section]以及其他一些环境变量的命令.<br>不应该包含那些可以产生输出结果或者假设终端已经附着到 tty 上的命令.
~/.zshenv|该文件和 /etc/zsh/zshenv 相似,但是它是针对每个用户而言的.<br>一般来说是用来设置一些有用的环境变量.
/etc/zsh/zprofile|这是一个全局的配置文件,在用户登录的时候加载.<br>一般是用来在登录的时候执行一些命令.
/etc/profile|在登录时,该文件应该被所有和伯克利(Bourne)终端相兼容的终端加载：它在登录的的时候会加载应用相关的配置(/etc/profile.d/*.sh).
~/.zprofile|该文件一般用来在登录的时候自动执行一些用户脚本.
/etc/zsh/zshrc|当 Zsh 被作为交互式终端的时候,会加载这样一个全局配置文件.
~/.zshrc|当 Zsh 被作为交互式终端的时候,会加载这样一个用户配置文件.<br>
/etc/zsh/zlogin|在登录完毕后加载的一个全局配置文件.
~/.zlogin|和 /etc/zsh/zlogin 相似,但是它是针对每个用户而言的.<br>
/etc/zsh/zlogout|在注销的时候被加载的一个全局配置文件.
~/.zlogout|和 /etc/zsh/zlogout 相似,但是它是针对每个用户而言的.

## 常用配置

```zsh
tee ~/.zshrc <<-'EOF'
# 开启命令补全
autoload -U compinit promptinit
compinit
promptinit

# 设置 walters 主题的默认命令行提示符
prompt walters

# 配置 $PATH
typeset -U path
path=(~/bin /other/things/in/path $path[@])

# 使用方向键控制的自动补全(按两次 tab 键启动菜单)
zstyle ':completion:*' menu select

# 启动命令行别名的自动补全
setopt completealiases

# 消除历史记录中的重复条目
# 假如目前的历史记录中已经有重复条目,可以运行下面的命令清除
# $ sort -t ";" -k 2 -u ~/.zsh_history | sort -o ~/.zsh_history
setopt HIST_IGNORE_DUPS

# 刷新自动补全
# 一般来说,compinit 不会自动在 $PATH 里面查找新的可执行文件.例如当你安装了一个新的软件包,/usr/bin 里的新文件不会立即自动添加到自动补全当中.所以你需要执行下面的命令来将它们添加进自动补全：
# $ rehash
# 这个 'rehash' 可以被放到你的 zshrc 来自动执行
zstyle ':completion:*' rehash true

# Zsh 可以配置 DIRSTACK 相关变量来加速 cd 访问常用目录.在配置文件中添加下面的配置
DIRSTACKFILE="$HOME/.cache/zsh/dirs"
if [[ -f $DIRSTACKFILE ]] && [[ $#dirstack -eq 0 ]]; then
  dirstack=( ${(f)"$(< $DIRSTACKFILE)"} )
  [[ -d $dirstack[1] ]] && cd $dirstack[1]
fi
chpwd() {
  print -l $PWD ${(u)dirstack} >$DIRSTACKFILE
}
DIRSTACKSIZE=20
setopt autopushd pushdsilent pushdtohome
## Remove duplicate entries
setopt pushdignoredups
## This reverts the +/- operators.
setopt pushdminus
# 现在可以使用
# $ dirs -v
# 打印目录栈(dirstack).使用 cd -<NUM> 来跳转到以前访问过的目录

# 仿 Fish 命令高亮
# 参考资料: https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/INSTALL.md
# 安装
## $ (macos) brew install zsh-syntax-highlighting
## $ git clone https://github.com/zsh-users/zsh-syntax-highlighting.git
## $ echo "source ${(q-)PWD}/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" >> ${ZDOTDIR:-$HOME}/.zshrc
# 下面语句必须在配置文件的最尾部
source /Users/ff4c00/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
EOF

cat ~/.zshrc && source ~/.zshrc
```

# 参考资料

> [ArchLinux | Zsh (简体中文)](https://wiki.archlinux.org/index.php/Zsh_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87))
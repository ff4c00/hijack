<!-- TOC -->

- [基础](#基础)
  - [新建shell](#新建shell)
  - [注释](#注释)
  - [多行注释](#多行注释)
- [变量](#变量)
  - [定义变量](#定义变量)
    - [命名规范](#命名规范)
  - [使用变量](#使用变量)
  - [变量类型](#变量类型)
  - [默认值](#默认值)
  - [数据类型](#数据类型)
    - [字符串](#字符串)
      - [单引号](#单引号)
      - [双引号](#双引号)
        - [字符串拼接](#字符串拼接)
      - [常用方法](#常用方法)
      - [正则匹配](#正则匹配)
      - [切割为数组](#切割为数组)
    - [数组](#数组)
      - [定义数组](#定义数组)
      - [读取数组](#读取数组)
      - [获取数组长度](#获取数组长度)
      - [数组遍历](#数组遍历)
- [管道符&重定向](#管道符重定向)
  - [输入输出重定向](#输入输出重定向)
  - [管道命令符](#管道命令符)
  - [命令行的通配符](#命令行的通配符)
- [传递参数](#传递参数)
- [基本运算符](#基本运算符)
  - [算术运算符](#算术运算符)
  - [关系运算符](#关系运算符)
  - [布尔运算符](#布尔运算符)
  - [逻辑运算符](#逻辑运算符)
  - [字符串运算符](#字符串运算符)
- [文件测试运算符](#文件测试运算符)
- [printf 命令](#printf-命令)
  - [转义序列](#转义序列)
- [流程控制](#流程控制)
  - [if](#if)
  - [for 循环](#for-循环)
  - [while 语句](#while-语句)
    - [无限循环](#无限循环)
- [until 循环](#until-循环)
- [case](#case)
  - [跳出循环](#跳出循环)
- [函数](#函数)
- [输入/输出重定向](#输入输出重定向)
  - [重定向深入讲解](#重定向深入讲解)
  - [/dev/null 文件](#devnull-文件)
- [文件包含](#文件包含)
- [脚本中调用另外一个脚本的方法](#脚本中调用另外一个脚本的方法)
- [read 命令](#read-命令)
  - [倒计时功能](#倒计时功能)
- [模拟键盘输入](#模拟键盘输入)
  - [yes](#yes)
- [参考资料](#参考资料)

<!-- /TOC -->

# 基础

## 新建shell

```bash
echo '#!/bin/bash' > "${file_name}.sh＂
chmod +x "${file_name}.sh"

# 如果一定要让执行shell脚本的子shell读取.bashrc的话,可以给shell脚本第一行的解释器加上参数:

#!/bin/bash --login
```

## 注释

以 # 开头的行就是注释,会被解释器忽略.

## 多行注释

```bash
:<<EOF
注释内容...
注释内容...
注释内容...
EOF

# EOF 也可以使用其他符号

:<<'
注释内容...
注释内容...
注释内容...
'

:<<!
注释内容...
注释内容...
注释内容...
!

```
# 变量

## 定义变量

定义变量时,变量名不加美元符号($):

```bash
variable='variable'
```

除了显式地直接赋值,还可以用语句给变量赋值,如:

```bash
for file in `ls /etc`
# 或
for file in $(ls /etc)
```

### 命名规范

0. ***变量名和等号之间不能有空格***
0. 命名只能使用英文字母,数字和下划线,首个字符不能以数字开头
0. 中间不能有空格,可以使用下划线(_)
0. 不能使用标点符号
0. 关键字可以用help命令查看

## 使用变量

使用一个定义过的变量,只要在变量名前面加美元符号即可,如:

```bash
echo $variable
echo ${variable}
```

## 变量类型 

类型|作用 
-|- 
局部变量|局部变量在脚本或命令中定义,仅在当前shell实例中有效,其他shell启动的程序不能访问局部变量. 
环境变量|所有的程序,包括shell启动的程序,都能访问环境变量,有些程序需要环境变量来保证其正常运行.<br>
必要的时候shell脚本也可以定义环境变量.<br>

shell变量|shell变量是由shell程序设置的特殊变量.<br>
shell变量中有一部分是环境变量,有一部分是局部变量,这些变量保证了shell的正常运行.

## 默认值

```bash
# 设置默认遍历文件夹
default_path="$pangu_path/tilix"
foo=$1
# 如果foo没有被声明或为null或为空字符串时each_path=$default_path
each_path=${foo:-$default_path}
```

## 数据类型

### 字符串

字符串可以用单引号,也可以用双引号,也可以不用引号.

#### 单引号

单引号字符串的限制:

0. 单引号里的任何字符都会原样输出,单引号字符串中的变量是无效的
0. 单引号字串中不能出现单独一个的单引号(对单引号使用转义符后也不行),但可成对出现,作为字符串拼接使用.

#### 双引号

双引号的优点:

0. 双引号里可以有变量.
0. 双引号里可以出现转义字符.

##### 字符串拼接

```bash
greeting="hello, "$your_name" !"
greeting_1="hello, ${your_name} !"
```


#### 常用方法

```bash

string="runoob is a great site"
# 获取字符串长度
echo ${#string} # => 22 

# 提取子字符串
# 以下实例从字符串第 2 个字符开始截取 4 个字符:
echo ${string:1:4} # => unoo
# 查找子字符串
# 查找字符 i 或 o 的位置(哪个字母先出现就计算哪个)
echo `expr index "$string" io`  # => 4
```

#### 正则匹配

```bash
file='/dir1/dir2/dir3/my.file.txt'
```

操作|作用|返回值
-|-|-
${file#*/}|拿掉第一条 / 及其左边的字符串|# => dir1/dir2/dir3/my.file.txt
${file##*/}|拿掉最后一条 / 及其左边的字符串|# => my.file.txt
${file#*.}|拿掉第一个 . 及其左边的字符串|# => file.txt
${file##*.}|拿掉最后一个 . 及其左边的字符串|# => txt
${file%/*}|拿掉最后条 / 及其右边的字符串|# => /dir1/dir2/dir3
${file%%/*}|拿掉第一条 / 及其右边的字符串|# => (空值)
${file%.*}|拿掉最后一个 . 及其右边的字符串|# => /dir1/dir2/dir3/my.file
${file%%.*}|拿掉第一个 . 及其右边的字符串|# => /dir1/dir2/dir3/my


#### 切割为数组

```bash
str="git￥https://gitlab.com/ff4c00/software.git￥software/oracle/instantclient_12_1.tar.gz"  
arr=(${str//￥/ })
get_method=${arr[0]}  
get_url=${arr[1]}
target_file_path=${arr[2]}
```

### 数组

#### 定义数组

括号来表示数组,数组元素用"空格"符号分割开:

```bash
array_name=(值1 值2 ... 值n)

array_name=(
值1 
值2 
... 
值n
)

array_name[0]=value0
array_name[1]=value1
array_name[n]=value
```

#### 读取数组


```bash
array_name[n]

# 使用 @ 符号可以获取数组中的所有元素
array_name[@]
```

#### 获取数组长度


```bash
# 取得数组单个元素的长度
length=${#array_name[n]}

# 取得数组元素的个数
length=${#array_name[@]}
# 或者
length=${#array_name[*]}
```

#### 数组遍历

```bash
# 遍历(带数组下标):
for i in "${!arr[@]}";
do
    printf "%s\t%s\n" "$i" "${arr[$i]}"  
done
```

# 管道符&重定向

## 输入输出重定向

## 管道命令符

## 命令行的通配符

# 传递参数

在执行 Shell 脚本时,向脚本传递参数,脚本内获取参数的格式为:$n.<br>
n 代表一个数字,1 为执行脚本的第一个参数,以此类推……

参数处理|说明
-|-
$#|传递到脚本的参数个数
$*|以一个单字符串显示所有向脚本传递的参数.<br>
<br>如"$*"用「"」括起来的情况、以"$1 $2 … $n"的形式输出所有参数.
$$|脚本运行的当前进程ID号
$!|后台运行的最后一个进程的ID号
$@|与$*相同,但是使用时加引号,并在引号中返回每个参数.<br>
<br>如"$@"用「"」括起来的情况、以"$1" "$2" … "$n" 的形式输出所有参数.
$-|显示Shell使用的当前选项,与set命令功能相同.
$?|显示最后命令的退出状态.<br>
0表示没有错误,其他任何值表明有错误.

$* 与 $@ 区别:

特点|区别
相同点|都是引用所有参数.
不同点|只有在双引号中体现出来.<br>
假设在脚本运行时写了三个参数 1、2、3,,则 " * " 等价于 "1 2 3"(传递了一个参数),而 "@" 等价于 "1" "2" "3"(传递了三个参数).

# 基本运算符

种类|
-|
算数运算符|
关系运算符|
布尔运算符|
字符串运算符|
文件测试运算符|

## 算术运算符

原生bash不支持简单的数学运算,但是可以通过其他命令来实现,例如 awk 和 expr,expr 最常用.

expr 是一款表达式计算工具,使用它能完成表达式的求值操作.

例如,两个数相加(注意使用的是反引号 \` 而不是单引号 '):

```bash
val=`expr 2 + 2`
echo "两数之和为 : $val"
```

注意:

表达式和运算符之间要有空格,例如 2+2 是不对的,必须写成 2 + 2,这与大多数编程语言不一样.

完整的表达式要被 \` \` 包含,注意这个字符不是常用的单引号,在 Esc 键下边.

下表列出了常用的算术运算符,假定变量 a 为 10,变量 b 为 20:

运算符|说明|举例
-|-|-
+|加法|\`expr $a + $b\` 结果为 30
-|减法|\`expr $a - $b\` 结果为 -10
*|乘法|\`expr $a \* $b\` 结果为  200
/|除法|\`expr $b / $a\` 结果为 2
%|取余|\`expr $b % $a\` 结果为 0
=|赋值|a=$b 将把变量 b 的值赋给 a
==|相等|用于比较两个数字,相同则返回 true.<br>[ $a == $b ] 返回 false
!=|不相等|用于比较两个数字,不相同则返回 true.<br>[ $a != $b ] 返回 true

## 关系运算符

关系运算符只支持数字,不支持字符串,除非字符串的值是数字.

[ 注意空格 ]

假定变量 a 为 10,变量 b 为 20:

运算符|说明|举例
-|-|-
-eq|检测两个数是否相等,相等返回 true|[ $a -eq $b ] 返回 false
-ne|检测两个数是否不相等,不相等返回 true|[ $a -ne $b ] 返回 true
-gt|检测左边的数是否大于右边的,如果是,则返回 true|[ $a -gt $b ] 返回 false
-lt|检测左边的数是否小于右边的,如果是,则返回 true|[ $a -lt $b ] 返回 true
-ge|检测左边的数是否大于等于右边的,如果是,则返回 true|[ $a -ge $b ] 返回 false
-le|检测左边的数是否小于等于右边的,如果是,则返回 true|[ $a -le $b ] 返回 true

## 布尔运算符

假定变量 a 为 10,变量 b 为 20:

运算符|说明|举例
-|-|-
!|非运算|表达式为 true 则返回 false,否则返回 true<br>[ ! false ] 返回 true
-o|或运算|有一个表达式为 true 则返回 true<br>[ $a -lt 20 -o $b -gt 100 ] 返回 true
-a|与运算|两个表达式都为 true 才返回 true<br>[ $a -lt 20 -a $b -gt 100 ] 返回 false

## 逻辑运算符

假定变量 a 为 10,变量 b 为 20:

运算符|说明|举例
-|-|-
&&|逻辑的 AND|[[ $a -lt 100 && $b -gt 100 ]] 返回 false
\|\||逻辑的 OR|[[ $a -lt 100 || $b -gt 100 ]]返回 true

## 字符串运算符

假定变量 a 为 "abc",变量 b 为 "efg":

运算符|说明|举例
-|-|-
=|检测两个字符串是否相等,相等返回 true|[ $a = $b ] 返回 false
!=|检测两个字符串是否相等,不相等返回 true|[ $a != $b ] 返回 true
-z|检测字符串长度是否为0,为0返回 true|[ -z $a ] 返回 false
-n|检测字符串长度是否为0,不为0返回 true|[ -n "$a" ] 返回 true
$|检测字符串是否为空,不为空返回 true|[ $a ] 返回 true

# 文件测试运算符

文件测试运算符用于检测 Unix 文件的各种属性.


属性检测描述如下:

操作符|说明|举例
-|-|-
-b file|检测文件是否是块设备文件,如果是,则返回 true|[ -b $file ] 返回 false
-c file|检测文件是否是字符设备文件,如果是,则返回 true|[ -c $file ] 返回 false
-d file|检测文件是否是目录,如果是,则返回 true|[ -d $file ] 返回 false
-f file|检测文件是否是普通文件(既不是目录,也不是设备文件),如果是,则返回 true.|	[ -f $file ] 返回 true
-g file|检测文件是否设置了 SGID 位,如果是,则返回 true|[ -g $file ] 返回 false
-k file|检测文件是否设置了粘着位(Sticky Bit),如果是,则返回 true|[ -k $file ] 返回 false
-p file|检测文件是否是有名管道,如果是,则返回 true|[ -p $file ] 返回 false
-u file|检测文件是否设置了 SUID 位,如果是,则返回 true|[ -u $file ] 返回 false
-r file|检测文件是否可读,如果是,则返回 true|[ -r $file ] 返回 true
-w file|检测文件是否可写,如果是,则返回 true|[ -w $file ] 返回 true
-x file|检测文件是否可执行,如果是,则返回 true|[ -x $file ] 返回 true
-s file|检测文件是否为空(文件大小是否大于0),不为空返回 true.<br>	[ -s $file ] 返回 true
-e file|检测文件(包括目录)是否存在,如果是,则返回 true|[ -e $file ] 返回 true

# printf 命令

printf 命令模仿 C 程序库(library)里的 printf() 程序<br>
printf 由 POSIX 标准所定义,因此使用 printf 的脚本比使用 echo 移植性好<br>
printf 使用引用文本或空格分隔的参数,外面可以在 printf 中使用格式化字符串,还可以制定字符串的宽度、左右对齐方式等.<br>
默认 printf 不会像 echo 自动添加换行符,可以手动添加 \n

语法:

```bash
printf 格式控制字符串 参数列表

printf "%-10s %-8s %-4s\n" 姓名 性别 体重kg  
printf "%-10s %-8s %-4.2f\n" 郭靖 男 66.1234 
printf "%-10s %-8s %-4.2f\n" 杨过 男 48.6543 
printf "%-10s %-8s %-4.2f\n" 郭芙 女 47.9876 
# => 
姓名     性别   体重kg
郭靖     男      66.12
杨过     男      48.65
郭芙     女      47.99

```

%s %c %d %f都是格式替代符<br>
%-10s 指一个宽度为10个字符(-表示左对齐,没有则表示右对齐),任何字符都会被显示在10个字符宽的字符内,如果不足则自动以空格填充,超过也会将内容全部显示出来.<br>
%-4.2f 指格式化为小数,其中.2指保留2位小数.

## 转义序列

序列|说明
-|-
\a|警告字符,通常为ASCII的BEL字符
\b|后退
\c|抑制(不显示)输出结果中任何结尾的换行字符(只在%b格式指示符控制下的参数字符串中有效)<br>而且任何留在参数里的字符、任何接下来的参数以及任何留在格式字符串中的字符,都被忽略
\f|换页(formfeed)
\n|换行
\r|回车(Carriage return)
\t|水平制表符
\v|垂直制表符
\\|一个字面上的反斜杠字符
\ddd|表示1到3位数八进制值的字符.<br>
仅在格式字符串中有效
\0ddd|表示1到3位的八进制值字符

# 流程控制

## if

*sh的流程控制不可为空,如果else分支没有语句执行,就不要写这个else.*

语法格式:

```bash
if condition
then
    command1 
elif condition2 
then 
    command2 
else
    command3
fi
```

## for 循环

```bash
for value in item1 item2 ... itemN
do
    command1
    command2
    ...
    commandN
done
```

当变量值在列表里,for循环即执行一次所有命令,使用变量名获取列表中的当前取值.<br>
命令可为任何有效的shell命令和语句.<br>
in列表可以包含替换、字符串和文件名.

in列表是可选的,如果不用它,for循环使用命令行的位置参数.<br>


## while 语句

```bash
while condition
do
    command
done
```

while循环可用于读取键盘信息.<br>
下面的例子中,输入信息被设置为变量FILM,按<Ctrl-D>结束循环.

```bash
echo '按下 <CTRL-D> 退出'
echo -n '输入最喜欢的网站名: '
while read FILM
do
    echo "是的！$FILM 是一个好网站"
done
```

### 无限循环

```bash
while :
do
  command
done

# 或者

while true
do
  command
done

# 或者

for (( ; ; ))
```

# until 循环

until 循环执行一系列命令直至条件为 true 时停止.

until 循环与 while 循环在处理方式上刚好相反.

一般 while 循环优于 until 循环,但在某些时候—也只是极少数情况下,until 循环更加有用.


# case

```bash
case 值 in
模式1)
    command1
    command2
    ...
    commandN
    ;;
模式2)
    command1
    command2
    ...
    commandN
    ;;
esac
```

case工作方式如上所示.<br>
取值后面必须为单词in,每一模式必须以右括号结束.<br>
取值可以为变量或常数.<br>
匹配发现取值符合某一模式后,其间所有命令开始执行直至 ;;

取值将检测匹配的每一个模式.<br>
一旦模式匹配,则执行完匹配模式相应命令后不再继续其他模式.<br>
如果无一匹配模式,使用星号 * 捕获该值,再执行后面的命令.

## 跳出循环

命令|作用
-|-
break|跳出所有循环(终止执行后面的所有循环)
continue|不会跳出所有循环,仅仅跳出当前循环

# 函数

函数调用必须在函数声明之后

```bash
[ function ] funname [()]

{
  action;
  [return int;]
}
```

0. 可以带function fun() 定义,也可以直接fun() 定义,不带任何参数.
0. 参数返回,可以显示加:return 返回,如果不加,将以最后一条命令运行结果,作为返回值.<br>
 return后跟数值n(0-255)

# 输入/输出重定向

大多数 UNIX 系统命令从终端接受输入并将所产生的输出发送回到终端.<br>
一个命令通常从一个叫标准输入的地方读取输入,默认情况下,这恰好是的终端.<br>
同样,一个命令通常将其输出写入到标准输出,默认情况下,这也是的终端.

重定向命令列表如下:


命令|说明
-|-
command > file|将输出重定向到 file
command < file|将输入重定向到 file
command >> file|将输出以追加的方式重定向到 file
n > file|将文件描述符为 n 的文件重定向到 file
n >> file|将文件描述符为 n 的文件以追加的方式重定向到 file
n >& m|将输出文件 m 和 n 合并
n <& m|将输入文件 m 和 n 合并
<< tag|将开始标记 tag 和结束标记 tag 之间的内容作为输入

需要注意的是文件描述符 0 通常是标准输入(STDIN),1 是标准输出(STDOUT),2 是标准错误输出(STDERR).

## 重定向深入讲解

一般情况下,每个 Unix/Linux 命令运行时都会打开三个文件:

0. 标准输入文件(stdin):stdin的文件描述符为0,Unix程序默认从stdin读取数据0. 标准输出文件(stdout):stdout 的文件描述符为1,Unix程序默认向stdout输出数据
0. 标准错误文件(stderr):stderr的文件描述符为2,Unix程序会向stderr流中写入错误信息
默认情况下,command > file 将 stdout 重定向到 file,command < file 将stdin 重定向到 file 

## /dev/null 文件

如果希望执行某个命令,但又不希望在屏幕上显示输出结果,那么可以将输出重定向到 /dev/null:

```bash
command > /dev/null
```

/dev/null 是一个特殊的文件,写入到它的内容都会被丢弃;如果尝试从该文件读取内容,那么什么也读不到.<br>
但是 /dev/null 文件非常有用,将命令的输出重定向到它,会起到"禁止输出"的效果

# 文件包含

> 和其他语言一样,Shell 也可以包含外部脚本.<br>这样可以很方便的封装一些公用的代码作为一个独立的文件.

语法格式如下:

```bash
. filename # 注意点号(.)和文件名中间有一空格

# 或
source filename
```

# 脚本中调用另外一个脚本的方法

方法名|作用|示例
-|-|-
fork|shell中包含执行命令,子命令并不影响父级的命令,<br> 运行的时候开一个sub-shell执行调用的脚本,sub-shell执行的时候,parent-shell还在.<br>在子命令执行完后再执行父级命令.<br>子级的环境变量不会影响到父级.sub-shell执行完毕后返回parent-shell.<br> sub-shell从parent-shell继承环境变量.<br>但是sub-shell中的环境变量不会带回parent-shell|fork /directory/script.sh
exec|exec与fork不同,不需要新开一个sub-shell来执行被调用的脚本.<br>被调用的脚本与父脚本在同一个shell内执行.<br>但是使用exec调用一个新脚本以后, 父脚本中exec行之后的内容就不会再执行了.执行子级的命令后,不再执行父级命令.|exec /directory/script.sh
source|执行子级命令后继续执行父级命令,<br>同时子级设置的环境变量会影响到父级的环境变量.<br>与fork的区别是不新开一个sub-shell来执行被调用的脚本,而是在同一个shell中执行.<br>所以被调用的脚本中声明的变量和环境变量, <br>都可以在主脚本中得到和使用.|source /directory/script.sh

# read 命令

## 倒计时功能

```bash
if read -t 5 -p "please enter your name:" name 
then 
  echo "hello $name ,welcome to my script"
else
  echo "sorry,too slow"
fi
```

# 模拟键盘输入

## yes 

```bash
# 其中 yes 命令可以无限重复产生其后面的字符”y”,head 命令只选择其中两次重复输入,也就是两个字符”y”
yes y | head -2 | yum install tomcat -y
```

# 参考资料

> [runoob | Shell教程](http://www.runoob.com/linux/linux-shell.html)

> [简书 | linux资料总章](https://www.jianshu.com/p/8157b5e5c6b0)

> [CSDN | linux shell 字符串操作(长度,查找,替换,匹配)详解](https://www.cnblogs.com/itcomputer/p/4884436.html)

> [CSDN | Shell脚本中调用另外一个脚本的方法](https://blog.csdn.net/sayyy/article/details/80519307)

> [CSDN | Linux Shell—— read命令](https://blog.csdn.net/chen_zhipeng/article/details/8435049)

> [CSDN | shell 分隔字符串成数组](https://blog.csdn.net/zhang_Red/article/details/8443944)

> [CSDN | 如何在linux下shell编写脚本中模拟键盘输入](https://blog.csdn.net/quicmous/article/details/77602810)
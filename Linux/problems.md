> Linux 使用过程中遇到问题处理

# nfs

## 读写速度慢

### 描述
文件服务器文件读写速度缓慢.

### 处理思路

#### 本地文件的读写速度是多少?

#### 本地往服务器读写的速度是多少?

#### 网络情况怎么样?

#### 解决方案有哪些?

## E: Could not get lock /var/lib/dpkg/lock - open (11 Resource temporarily unavailable)

出现这个问题的原因是:<br>
还有一个线程在使用apt-get进行下载的操作.

解决措施:

```bash
# 搜索所有运行着的线程
ps -A | grep apt-get
kill -9 pid
```

# 参考资料

Could not get lock /var/lib/dpkg/lock - open 解决方法](https://blog.csdn.net/github_35160620/article/details/51933605)



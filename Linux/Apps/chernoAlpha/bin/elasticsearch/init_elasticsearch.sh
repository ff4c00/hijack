#!/bin/bash

if [ ! $PACKAGE_NAME ];then  echo '未定义安装包名称';exit 1;fi
if [ ! $PACKAGE_PATH ];then  echo '未定义安装包存放地址';exit 1;fi
if [ ! $OPERATOR_USER ];then echo '未定义用户名';exit 1;fi
if [ ! $GROUP_NAME ];then    echo '未定义用户所属组名';exit 1;fi
if [ ! $SCRIPT_PATH ];then   echo '未定义脚本位置';exit 1;fi

file_path="$PACKAGE_PATH/$PACKAGE_NAME"

dir_name=${PACKAGE_NAME%.*} # elasticsearch-5.6.3.zip => elasticsearch-5.6.3

echo '检查安装包是否存在'
if [ ! -f "$file_path" ]; then
  echo "$PACKAGE_PATH/位置并未发现安装包$PACKAGE_NAME"
  exit 1
fi

function tar_target_file () {
  case ${PACKAGE_NAME##*.} in 
  'zip')
    # 检查是否存在已解压文件
    extract_path="/home/$OPERATOR_USER/"

    if [ ! -d "$extract_path/$dir_name" ]; then
      # 如果软件存放地包含已解压文件则进行移动
      if [ -d "$PACKAGE_PATH/$dir_name" ]; then
        echo '复制软件包到指定位置'
        cp -r "$PACKAGE_PATH/$dir_name" $extract_path
      else
        echo -e "解压文件: \n$file_path \n=> \n$extract_path"
        unzip file_path -d extract_path
      fi
    fi

  ;;
  *)
    echo '未找到相关解压方式！'
    exit 1
  ;;
  esac
}

tar_target_file
if [[ ! $? -eq 0 ]];then 
  echo '安装包解压失败'
  exit 1
fi

echo '更改文件夹所属用户'
sudo chown -R $OPERATOR_USER:$GROUP_NAME "$extract_path/$dir_name"

source $SCRIPT_PATH/iso/init_jdk.sh open_jdk;


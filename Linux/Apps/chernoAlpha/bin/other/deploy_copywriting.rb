#!/usr/bin/env ruby

# @北京-运维-赵菁煜 assignee
# 项目名称: project_name
# 站点类型: 测试站默认 site_type
# 部署内容: 部署内容 content
# 数据迁移: 无 migrate
# 配置文件: 无 config_file
# 静态文件: 无 static_file
# 执行内容: 无 mark

require 'optparse'

options = Hash.new(false)
option_parser = OptionParser.new do |opts|

  opts.banner = '自动生成部署信息脚本帮助内容:'

  options[:assignee] = '@北京-运维-赵菁煜'
  opts.on('-a 执行人', '--assignee 执行人', '填写执行人(默认赵菁煜)') do |value|
    options[:assignee] = value
  end

  opts.on('-p 项目名称', '--project_name 项目名称', '填写项目名称(必填)') do |value|
    options[:project_name] = value
  end

  opts.on('-s', '--site_type', '站点类型(默认测试站)') do |value|
    options[:site_type] = true
  end

  opts.on('-c 部署内容', '--content 部署内容', '填写部署内容(必填)') do |value|
    options[:content] = value
  end
  # puts '部署内容不能为空' if options[:content].nil?


  opts.on('', '--migrate', '是否包含数据迁移(默认无)') do
    options[:migrate] = true
  end

  opts.on('', '--config_file', '是否包含配置文件(默认无)') do
    options[:config_file] = true
  end

  opts.on('', '--static_file', '是否包含静态文件(默认无)') do
    options[:static_file] = true
  end

  opts.on('-m 执行内容', '--mark 执行内容', '填写执行内容(默认无)') do |value|
    options[:mark] = value
  end

end.parse!

puts %Q|
请 #{options[:assignee]} 部署下
项目名称: #{options[:project_name]}
站点类型: #{options[:site_type] ? '正式站' : '测试站'}
部署内容: #{options[:content]}
数据迁移: #{options[:migrate] ? '有' : '无'}
配置文件: #{options[:config_file] ? '有' : '无'}
静态文件: #{options[:static_file] ? '有' : '无'}
执行内容: #{options[:mark] ? options[:mark] : '无'}
|
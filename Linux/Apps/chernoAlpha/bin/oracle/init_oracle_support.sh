#!/bin/bash

echo '检查环境变量是否存在'
if [ ! $ORACLE_SUPPORT_DOWN_URL ]; then
  echo '相关环境变量不存在,请检查.'
  exit 1
fi 

echo '解析相关环境变量'
arr=(${ORACLE_SUPPORT_DOWN_URL//￥/ })
get_method=${arr[0]}  
get_url=${arr[1]}
target_file_path=${arr[2]}
target_file_name=${target_file_path##*/}
dir_name=${target_file_name%%.*}

function tar_target_file () {
  echo '解压文件'
  case ${target_file_name#*.} in 
  'tar.gz')
    tar -xvf $PWD/$target_file_path -C $PWD/${target_file_path%/*}
  ;;
  *)
    echo '未找到相关解压方式！'
    exit 1
  ;;
  esac
}

function oracle_support_file_down() {
  echo '下载文件'
  case $get_method in 
  'git'|'Git'|'GIT')
    echo "正在克隆:$get_url"
    git clone $get_url
  ;;
  *)
    echo '未找到相关文件下载方式！'
    exit 1
  ;;
  esac
}

find $SCRIPT_PATH/oracle/$target_file_path
if [[ ! $? -eq 0 ]];then 
  oracle_support_file_down
fi
if [[ ! $? -eq 0 ]];then exit 1;fi

find $SCRIPT_PATH/oracle/${target_file_path%/*}/$dir_name
if [[ ! $? -eq 0 ]];then 
  tar_target_file
fi
if [[ ! $? -eq 0 ]];then echo '文件解压失败';exit 1;fi

echo '创建目录/opt/oracle'
sudo mkdir -p /opt/oracle

echo '复制文件'
sudo cp -rfv $SCRIPT_PATH/oracle/${target_file_path%/*}/$dir_name/ /opt/oracle/$dir_name

if [[ ! $? -eq 0 ]];then exit 1;fi

find /opt/oracle/$dir_name/libclntsh.so

if [[ ! $? -eq 0 ]];then
  echo '创建链接'
  # TODO这里的libclntsh.so.12.1可能导致链接错误
  sudo ln -s /opt/oracle/$dir_name/libclntsh.so.12.1 /opt/oracle/$dir_name/libclntsh.so
fi 

if [[ ! $? -eq 0 ]];then exit 1;fi

echo '写入环境变量'
# tee -a /home/${OPERATOR_USER}/.bashrc <<-'EOF'
# export LD_LIBRARY_PATH=/opt/oracle/${dir_name}
# # 修复汉字显示乱码
# export NLS_LANG=AMERICAN_AMERICA.UTF8
# EOF

echo -e "\
export LD_LIBRARY_PATH=/opt/oracle/${dir_name}\n\
# 修复汉字显示乱码 \n\
export NLS_LANG=AMERICAN_AMERICA.UTF8\
" >> /home/${OPERATOR_USER}/.bashrc


echo "127.0.0.1 $(hostname)" | sudo tee -a /etc/hosts

echo '安装libaio1'
sudo apt-get install -fy libaio1

# export INITIALIZED_DOCKER_ENV=1

if [ ! $SCRIPT_PATH ]; then
  read -p "请输入脚本文件所在目录路径:" path
  if [ ! $path ]
  then
    echo '参数不能为空'
    exit 1
  else
    echo "export SCRIPT_PATH=$path" >> /home/$USER/.bashrc
  fi
fi

if [ -n "$INITIALIZED_DOCKER_ENV" ]; then
  echo "Docker相关环境变量已写入,无需重复操作,尝试执行: source /home/$USER/.bashrc"
  exit 0
else
  echo -e "# 标记是否已写入环境变量\n export INITIALIZED_DOCKER_ENV=1 \n source $SCRIPT_PATH/../cnf/docker/secret_docker_env.sh" >> /home/$USER/.bashrc
  echo "已写入请执行: source /home/$USER/.bashrc"
fi 

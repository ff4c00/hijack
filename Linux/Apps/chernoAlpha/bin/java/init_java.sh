#!/bin/bash

su - ${OPERATOR_USER} <<EOFA

echo "安装: openjdk-8-jdk"
sudo apt-get install openjdk-8-jdk -y
echo "jdk相关信息: ";java -version

echo "下载Maven"
wget -t 5 -c "http://mirrors.tuna.tsinghua.edu.cn/apache/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz"

echo "下载内容解压缩"
sudo tar zxvf "apache-maven-$MAVEN_VERSION-bin.tar.gz" -C $MAVEN_INSTALL_PATH

echo 'Maven写入环境变量';
tee -a /home/${OPERATOR_USER}/.bashrc <<-'EOF'
  export MARVEN_HOME=$MAVEN_INSTALL_PATH/apache-maven-$MAVEN_VERSION
  export PATH=$PATH:$MARVEN_HOME/bin
EOF

source /home/${OPERATOR_USER}/.bashrc

echo '验证环境变量'
echo "MARVEN_HOME: $MARVEN_HOME"
echo "PATH: $PATH"

EOFA







#!/bin/bash

# 示例
## bash +x install_package.sh wkhtmltopdf https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox_0.12.5-1.bionic_amd64.deb

# 检查是否传入相关参数
software_name=$1
if [ ! $software_name ];then  echo '未传入软件名称';exit 1;fi
down_url=$2
if [ ! $down_url ];then  echo '未传入下载路径';exit 1;fi

# 针对下载链接进行解析
## 下载完成后保存文件名称
down_file_name=${down_url##*/}
## 文件保存路径
file_save_path=$SCRIPT_PATH/install_packages/software/$down_file_name

# 检查software文件夹下是否存在已下载文件
if [ ! -f $file_save_path ]; then
  # 失败尝试5次 断点续传 下载文件保存路径
  wget -t 5 -c -P "$SCRIPT_PATH/install_packages/software/" $down_url 
  if [[ ! $? -eq 0 ]];then 
    echo "安装包($down_file_name)下载异常"
    # 下载失败文件清除
    rm -rf $file_save_path
    exit 1
  fi
fi

# 相关依赖安装
function rely_no_install () {
  sudo bash -x $SCRIPT_PATH/iso/debian/update_system.sh

  case $software_name in
  'wkhtmltopdf'|'wkhtmltox')
    rely_no_install_cmd="sudo apt-get install xfonts-encodings xfonts-utils xfonts-base xfonts-75dpi"
    $rely_no_install_cmd
  ;;
  *)
    echo '跳过, 未定义相关依赖项.'
  ;;
  esac
  
  if [[ ! $? -eq 0 ]];then 
    echo '相关依赖安装失败,执行fix安装'
    sudo apt install -fy 
    $rely_no_install_cmd
  fi
}

## 针对文件后缀进行解析确定调用方法
case ${down_file_name##*.} in 
### deb => 调用 dpkg -i jxvf 进行安装
'deb')
  # dpkg -i jxvf $file_save_path
  rely_no_install
  if [[ ! $? -eq 0 ]];then echo '相关依赖安装失败';exit 1;fi
  sudo dpkg -i $file_save_path
  if [[ ! $? -eq 0 ]];then echo '安装包安装失败';exit 1;fi
;;
*)
  echo '未找到相关安装包后缀安装方式！'
  exit 1
;;
esac
### .tar.gz =是否传入文件夹移动位置=Y=> 解压移动
  

#!/bin/bash

dir_name=${PWD##*/}
relevant_alias=$(cat /home/$USER/.bashrc | grep "alias $dir_name")

# 判断bash.rc里面是否已存在别名
if [ -n "$relevant_alias" ]; then
  used_alias=$(alias | grep $dir_name)
  # 判断正在使用的别名中是否存在
  if [ -n "$used_alias"]; then
    echo "已存在现目录别名,无需再次创建."
    exit 0
  fi
else
  echo "alias ${PWD##*/}=\"clear;cd ${PWD}\"" | sudo tee -a /home/$USER/.bashrc;
fi 

source /home/$USER/.bashrc;
exit 0
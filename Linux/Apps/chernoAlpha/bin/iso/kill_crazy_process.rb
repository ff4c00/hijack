# 查杀占用cpu过高进程
require 'rubygems'
require 'binding_of_caller'

# TODO 针对资源占用过高 设置时效标准

# 获取系统原始数据
def get_metadata
  res = `ps auxw|head -1;ps auxw|sort -rn -k3|head -5`

  process_arrays_ = res.split(/\n/)
  process_head = process_arrays_[0].split(' ')
  process_arrays = process_arrays_[1..-1]

  [true, process_arrays]
end

# 获取进程处理标准
def get_process_handle_stand(process: )
  judge_stands = [
    {name: 'chrome', condition: /chrome/, cpu_limit: 100},
  ]

  judge_stands.each do |stand|
    reg_res = stand[:condition].match(process[:command])
    return [true, stand] unless reg_res.nil?
  end

  [false, "暂无命令:#{process[:command]},处理标准"]
end

# 元数据处理
def handle_metadata(data: )
  # ["USER", "PID", "%CPU", "%MEM", "VSZ", "RSS", "TTY", "STAT", "START", "TIME", "COMMAND"]
  data_array = data.split(' ')
  [true, {pid: data_array[1].to_i, cpu: data_array[2].to_i, mem: data_array[3].to_i, command: data_array[10]}]
end

def need_kill_process?(process:, stand: )
  common = "[#{stand[:name]}] PID:#{process[:pid]}"
  return [true, "#{common}, CPU占用:#{process[:cpu]}%, 高于标准:#{stand[:cpu_limit]}%, 建议结束进程."] if process[:cpu] > stand[:cpu_limit]

  [false, '尚未达到标准任意参数值, 不建议结束进程.']
end

def check_result(res:)
  unless res[0]
    print "#{res[1]} \n"
  end
  res[0]
end


def main

  # 获取原始进程数据
  res = get_metadata
  exit unless check_result(res: res)

  res[1].each do |metadata|

    # 针对具体原始数据进行处理
    res = handle_metadata(data: metadata)
    next unless check_result(res: res)
    process = res[1]

    next unless process[:cpu] > 30

    # 获取进程处理标准
    res = get_process_handle_stand(process: res[1])
    next unless check_result(res: res)

    # 判断进程是否需要杀掉
    res = need_kill_process?(process: process, stand: res[1])
    next unless res[0]

    print res[1]

    `kill -9 #{process[:pid]}`
  end

end

loop do
  main()
  sleep(10);
end

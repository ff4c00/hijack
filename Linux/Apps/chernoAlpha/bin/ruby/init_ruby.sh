#!/bin/bash

echo '执行ruby初始化脚本'
echo '切换到普通用户并执行Ruby环境搭建等相关操作'
su - ${OPERATOR_USER} <<EOF

  # echo '创建项目目录'
  # sudo mkdir -p ${WORK_DIR}
  # sudo chown -R ${OPERATOR_UID}:${OPERATOR_GID} ${WORK_DIR}

  echo 'ruby基本环境依赖'
  sudo apt install -y curl g++ gcc autoconf automake bison libc6-dev libffi-dev libgdbm-dev libncurses5-dev libsqlite3-dev libtool libyaml-dev make pkg-config sqlite3 zlib1g-dev libgmp-dev libreadline-dev libssl-dev nodejs 
  
  echo '项目依赖安装'
  sudo apt install -y imagemagick ghostscript

  echo 'Mysql数据库相关软件'
  # 这个必须要安装,不然镜像会大300多MB 
  # TODO 具体什么原因待查找
  sudo apt install -y mysql-client libmysqlclient-dev 

  echo '安装RVM'
  curl -sSL https://rvm.io/mpapis.asc | gpg --import -
  curl -sSL https://rvm.io/pkuczynski.asc | gpg --import -
  curl -sSL https://get.rvm.io | bash -s stable
  source ~/.rvm/scripts/rvm

  echo '设置RVM最大超时时间'
  echo "export rvm_max_time_flag=20" >> ~/.rvmrc
  source ~/.rvmrc


  echo '安装RVM相关依赖'
  rvm requirements

  echo '修改更新源'
  echo "ruby_url=https://cache.ruby-china.org/pub/ruby" > ~/.rvm/user/db

  echo '安装Ruby(${RUBY_VERSION})'
  rvm install ${RUBY_VERSION}

  echo '解决: You need to change your terminal emulator preferences to allow login shell.'
  echo '[[ -s "/home/${OPERATOR_USER}/.rvm/scripts/rvm" ]] && . "/home/${OPERATOR_USER}/.rvm/scripts/rvm"' >> /home/${OPERATOR_USER}/.bashrc
  source /home/${OPERATOR_USER}/.bashrc

  echo '指定Ruby默认版本(${RUBY_VERSION})'
  rvm use ${RUBY_VERSION} --default

  echo '设置Gem更新源'
  gem sources --add https://gems.ruby-china.com/ --remove https://rubygems.org/

  echo '安装Bundler'
  # 注意版本 已知1.16.2版本会引发: Traceback (most recent call last)
  # 可安装指定版本: gem install bundler 1.16.6
  gem install bundler

EOF
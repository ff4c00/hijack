
<!-- TOC -->

- [构建步骤](#构建步骤)
  - [创建镜像](#创建镜像)
  - [提交镜像](#提交镜像)
- [更新日志](#更新日志)
  - [x86_64](#x86_64)
    - [u18.04](#u1804)
      - [j8](#j8)
      - [构建时间](#构建时间)
- [请多指教](#请多指教)
  - [联系方式](#联系方式)

<!-- /TOC -->

# 构建步骤

## 创建镜像

```bash
function build_image() {
  java_version=8
  platform_version=$(uname -i)
  system_version='u18.04'
  image_tag="$platform_version-$system_version-j$java_version-$(date +%Y%m%d)"
  depository_name="ff4c00/java"
  docker_file_path='./app/docker/java/Dockerfile'
  docker build -t $depository_name:$image_tag -f $docker_file_path --squash .
}
build_image
```

## 提交镜像

```bash
docker push $depository_name:$image_tag
```

# 更新日志

&emsp;|&emsp;
-|-
基础镜像|ff4c00/linux
默认时区|上海
包含软件<sup>*</sup>|vim<br>sudo<br>git<br>openssh-server<br>java<br>maven<br>cron

*: 在基础镜像上增加

## x86_64

> 适用x86_64硬件架构

### u18.04

> 镜像系统版本为Ubuntu 18.04

#### j8

> java 8(jdk 1.8)

#### 构建时间

> 所有镜像基于同一Dockerfile,如无特殊说明,某一版本新增/修改项将应用于后续所有版本.

构建时间|基于镜像|描述|
-|-|-
20190925|首次初始化

# 请多指教

## 联系方式

个人邮箱: ff4c00@gmail.com


<!-- TOC -->

- [构建步骤](#构建步骤)
  - [创建镜像](#创建镜像)
  - [提交镜像](#提交镜像)
- [更新日志](#更新日志)
  - [x86_64](#x86_64)
    - [u18.04](#u1804)
      - [r2.3.8](#r238)
      - [构建时间](#构建时间)
- [请多指教](#请多指教)
  - [存在问题](#存在问题)
  - [联系方式](#联系方式)

<!-- /TOC -->

# 构建步骤

## 创建镜像

```bash
ruby_version='2.3.8'
platform_version=$(uname -i)
system_version='u18.04'
image_tag="$platform_version-$system_version-r$ruby_version-$(date +%Y%m%d)"
depository_name="ff4c00/ruby"
docker_file_path='./app/docker/ruby/Dockerfile'
docker build -t $depository_name:$image_tag -f $docker_file_path --build-arg RUBY_VERSION=$ruby_version --squash .
```

## 提交镜像

```bash
docker push $depository_name:$image_tag
```

# 更新日志

&emsp;|&emsp;
-|-
基础镜像|ff4c00/linux
默认时区|上海
包含软件<sup>*</sup>|vim<br>sudo<br>git<br>openssh-server<br>ruby<br>imagemagick<br>ghostscript<br>cron

*: 在基础镜像上增加

## x86_64

> 适用x86_64硬件架构

### u18.04

> 镜像系统版本为Ubuntu 18.04

#### r2.3.8

> Ruby 2.3.8版本

#### 构建时间

> 所有镜像基于同一Dockerfile,如无特殊说明,某一版本新增/修改项将应用于后续所有版本.

构建时间|基于镜像|描述|
-|-|-
20190703|ff4c00/linux:x86_64-u18.04-ali-20190703|首次初始化
20190712|ff4c00/linux:x86_64-u18.04-ali-20190712|压缩镜像层,以精简镜像大小(1.87GB -> 1.18GB)<br>新增imagemagick及ghostscript软件
20190913|ff4c00/linux:x86_64-u18.04-ali-20190913|新增执行定时任务软件cron

# 请多指教

## 存在问题

> 用于描述当前镜像存在的已知问题.<br>所有镜像基于同一Dockerfile,解决时间节点之后(含该节点)的所有版本理论上该问题都将得以解决.

问题|描述|状态|备注
-|-|-|-|
镜像过大|近期生成的镜像大小几乎是之前的一倍|已解决(20190712)|通过在构建时指定--squash指令得以解决.<br>该指令用于压缩镜像层,目前(2019-07-12)尚不稳定,需开启实验模式.

## 联系方式

个人邮箱: ff4c00@gmail.com
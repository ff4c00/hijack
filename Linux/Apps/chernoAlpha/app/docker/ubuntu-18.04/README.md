
<!-- TOC -->

- [说明](#说明)
- [构建步骤](#构建步骤)
  - [创建镜像](#创建镜像)
  - [提交镜像](#提交镜像)
  - [进入镜像](#进入镜像)
    - [commit](#commit)
- [更新日志](#更新日志)
  - [x86_64](#x86_64)
    - [u18.04](#u1804)
      - [ali](#ali)
      - [构建时间](#构建时间)
  - [armv7l](#armv7l)
    - [u18.04](#u1804-1)
      - [ali](#ali-1)
      - [构建时间](#构建时间-1)
- [请多指教](#请多指教)
  - [存在问题](#存在问题)
  - [联系方式](#联系方式)

<!-- /TOC -->

# 说明

该镜像仅用于在官方镜像的基础上:

0. 设置合适的系统更新源
1. 安装常用基础软件

# 构建步骤

## 创建镜像

```bash
platform_version=$(uname -i)
system_version='u18.04'
source_from='ali'
image_tag="$platform_version-$system_version-$source_from-$(date +%Y%m%d)"
depository_name="ff4c00/linux"
docker_file_path='./app/docker/ubuntu-18.04/Dockerfile'
tzdata_localtime_file_time='/usr/share/zoneinfo/Asia/Shanghai'
docker build -t $depository_name:$image_tag -f $docker_file_path --build-arg TZDATA_LOCALTIME_FILE_TIME=$tzdata_localtime_file_time --squash .
```

## 提交镜像

```bash
docker push $depository_name:$image_tag
```


## 进入镜像

> 目前有些配置项只能等进入镜像后进行修改然后提交

```bash
docker run -it --privileged=true $depository_name:$image_tag
```

```bash
echo '修改文件描述符数量'
sudo tee -a /etc/sysctl.conf <<-'EOF'
vm.max_map_count=262144
EOF
sudo sysctl -p
```

### commit

```bash
docker commit  -m="提交的描述信息"  -a="ff4c00"   容器id   $depository_name:$image_tag
```

# 更新日志

```
armv7l-u18.04-ali-20190913
[架构]-[系统版本]-[更新源]-[制作时间]
```

&emsp;|&emsp;
-|-
基础镜像|ubuntu:18.04(官方)
默认时区|上海
包含软件<sup>*</sup>|vim<br>sudo<br>git<br>openssh-server<br>cron

*: 在基础镜像上增加

## x86_64

> 适用x86_64硬件架构

### u18.04

> 镜像系统版本为Ubuntu 18.04

#### ali

> 采用阿里更新源

#### 构建时间

> 所有镜像基于同一Dockerfile,如无特殊说明,某一版本新增/修改项将应用于后续所有版本.

构建时间|描述|
-|-
20190703|首次初始化
20190711|文件描述符数量修改<br>(vm.max_map_count=262144)
20190712|压缩镜像层,以精简镜像大小(775MB -> 296MB)
20190913|新增执行定时任务软件cron

## armv7l

> 适用armv7l硬件架构

### u18.04

> 镜像系统版本为Ubuntu 18.04

#### ali

> 采用阿里更新源

#### 构建时间

如无特殊说明,于X_86同一构建时间所生成镜像更新内容一致.


# 请多指教

## 存在问题

> 用于描述当前镜像存在的已知问题.<br>所有镜像基于同一Dockerfile,解决时间节点之后(含该节点)的所有版本理论上该问题都将得以解决.

问题|描述|状态|备注
-|-|-|-|
镜像过大|近期生成的镜像大小几乎是之前的一倍|已解决(20190712)|通过在构建时指定--squash指令得以解决.<br>该指令用于压缩镜像层,目前(2019-07-12)尚不稳定,需开启实验模式.

关于上面问题如果你有相关方面的排查经验以及更好的解决方法并且愿意分享,请多多指教.

## 联系方式

个人邮箱: <ff4c00@gmail.com>
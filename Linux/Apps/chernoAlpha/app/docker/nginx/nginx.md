
##  启动命令

```bash
docker run -d -p 80:80 \
--name nginx-latest \
-v ${NGINX_CNF_PATH}/www:/usr/share/nginx/html \
-v ${NGINX_CNF_PATH}/conf/nginx.conf:/etc/nginx/nginx.conf \
-v ${NGINX_CNF_PATH}/logs:/var/log/nginx nginx
```


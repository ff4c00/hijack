> Elasticsearch

Elasticsearch可用于搜索各种文档.它提供可扩展的搜索,近实时搜索,并支持多租户.

> 多租户

*软件多租户* 指的是软件架构,<br>
其中一个实例的软件在服务器上运行,并提供多租户.<br>
租户是一组用户,他们共享具有软件实例特定权限的公共访问权限.<br>
通过多租户架构,软件应用程序旨在为每个租户提供实例的专用共享,<br>
包括其数据,配置,用户管理,租户个人功能和非功能属性.<br>
多租户与多实例架构形成对比,其中 **单独的软件实例代表不同的租户运行**.

<!-- TOC -->

- [使用](#使用)
- [常见问题](#常见问题)
  - [Could not register mbeans](#could-not-register-mbeans)
  - [vm.max_map_count [65530] is too low](#vmmax_map_count-65530-is-too-low)
- [参考资料](#参考资料)

<!-- /TOC -->


# 使用

# 常见问题

## Could not register mbeans

> main ERROR Could not register mbeans java.security.AccessControlException: access denied ("javax.management.MBeanTrustPermission" "register")

检查文件夹是否为当前为root用户所用.

切换命令:

```bash
chown [-R] 账号名称:用户组名称 文件或目录
```

## vm.max_map_count [65530] is too low

> max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]

**如果是容器需要在启动时指定为特权容器(可修改环境变量):--privileged=true**<sup>1</sup>

```bash
echo '修改文件描述符数量'
sudo tee -a /etc/sysctl.conf <<-'EOF'
vm.max_map_count=262144
EOF

sudo sysctl -p
```

1: 关于容器内修改

# 参考资料 

> [Elasticsearch | 权威指南](https://www.elastic.co/guide/cn/elasticsearch/guide/cn/index.html)

> [Github | polyfractal](https://github.com/polyfractal)

> [CSDN | elasticsearch 安装过程中可遇到的问题](https://blog.csdn.net/zhang89xiao/article/details/68925294)
> Secure Shell(SSH)是一种加密 网络协议,用于在不安全的网络上安全地运行网络服务.<br>典型应用包括远程命令行 登录和远程命令执行,但可以使用SSH保护任何网络服务.<br>SSH 在客户端-服务器体系结构中通过不安全的网络提供安全通道,<br>将SSH客户端应用程序与SSH服务器连接.<br> --摘自[维基百科](https://en.wikipedia.org/wiki/Secure_Shell)

<!-- TOC -->

- [环境配置](#环境配置)
  - [检查服务器ssh是否可用](#检查服务器ssh是否可用)
    - [检查是否安装了ssh-server服务](#检查是否安装了ssh-server服务)
    - [安装ssh-server](#安装ssh-server)
    - [确认ssh-server是否启动](#确认ssh-server是否启动)
  - [配置openssh-server开机自动启动](#配置openssh-server开机自动启动)
  - [root用户开启ssh权限](#root用户开启ssh权限)
- [常用操作](#常用操作)
  - [连接服务器](#连接服务器)
  - [启动/停止/重启](#启动停止重启)
  - [传输文件](#传输文件)
    - [常用参数](#常用参数)
    - [从服务器下载文件](#从服务器下载文件)
    - [上传文件到服务器](#上传文件到服务器)
- [ssh免密登录](#ssh免密登录)
  - [hostA](#hosta)
    - [生成rsa公私钥](#生成rsa公私钥)
  - [hostB](#hostb)
    - [创建~/.ssh目录](#创建ssh目录)
  - [hostA](#hosta-1)
  - [拷贝公钥](#拷贝公钥)
- [Mosh](#mosh)
  - [安装](#安装)
    - [Ubuntu](#ubuntu)
    - [MacOS](#macos)
  - [使用](#使用)
    - [链接服务器](#链接服务器)
    - [删除之前会话](#删除之前会话)
  - [相比ssh的优点](#相比ssh的优点)
- [常见问题](#常见问题)
  - [Permission denied](#permission-denied)
  - [mosh: Did not find remote IP address](#mosh-did-not-find-remote-ip-address)
- [参考资料](#参考资料)

<!-- /TOC -->

# 环境配置

## 检查服务器ssh是否可用

### 检查是否安装了ssh-server服务

*默认只安装ssh-client服务* 

```bash
dpkg -l | grep ssh
```

### 安装ssh-server

如果上一步骤结果中没有 `openssh-server` 关键字,需要:

```bash
sudo apt-get install openssh-server
```

然后重复上一步骤

### 确认ssh-server是否启动

> 进程ssh-agent是客户端,sshd为服务器端.

```bash
ps -e | grep ssh
```

看到 *sshd* 说明ssh-server已经启动了.

如果没有则可以使用下面任意一种方式启动:

0. sudo /etc/init.d/ssh start
0. sudo service ssh start 

## 配置openssh-server开机自动启动

打开/etc/rc.local文件,在exit 0语句前加入:

```bash
/etc/init.d/ssh start
```

## root用户开启ssh权限

```bash
sudo vim /etc/ssh/sshd_config
# 注释掉 PermitRootLogin without-password
# 添加 PermitRootLogin yes
```

# 常用操作

## 连接服务器

```
ssh [-p 端口号(非默认22端口情况下)] user@hostname
```


## 启动/停止/重启

```bash
/etc/init.d/ssh start
/etc/init.d/ssh stop
/etc/init.d/ssh restart
```

## 传输文件

### 常用参数

参数|作用
-|-
r|批量上传/下载文件夹内文件
v|和大多数linux命令中的-v意思一样,用来显示进度.可以用来查看连接,认证,或是配置错误. 
C|使能压缩选项. 
P|选择端口.注意-p已经被rcp使用. 
4|强行使用IPV4地址. 
6|强行使用IPV6地址. 

### 从服务器下载文件

```bash
scp [-r 递归下载整个文件夹内容] <用户名>@<ssh服务器地址>:<文件路径> <本地文件名>
```

### 上传文件到服务器

```
scp [-P 端口] <本地文件名> <用户名>@<ssh服务器地址>:<上传保存路径即文件名>

scp -P 3256 /Users/xxx/Downloads/navugation_ul.png xxx@123.45.124.41:/home/xxx/images/navugation_ul.png
```

# ssh免密登录

> 什么是ssh免密登录

假设hostA上的一个用户aliceA,以用户aliceB的身份ssh到hostB上,在这一过程中无需输入密码.


## hostA

### 生成rsa公私钥

以用户aliceA的身份登录到hostA上,<br>使用ssh-keygen生成一对rsa公私钥,生成的密钥对会存放在~/.ssh目录下.

```bash
ssh-keygen -t rsa -b 4096
```

## hostB

### 创建~/.ssh目录

在目标主机hostB上的aliceB用户目录下创建~/.ssh目录.

如果在aliceB@hostB上已经存在.ssh目录,这一步会被略过.

```bash
mkdir ~/.ssh
```

## hostA

## 拷贝公钥

将hostA上用户"aliceA"的公钥拷贝到aliceB@hostB上,来实现无密码ssh.

```bash
cat ~/.ssh/id_rsa.pub | ssh aliceB@hostB 'cat >> ~/.ssh/authorized_keys'
```

# Mosh

> Mosh表示移动Shell(Mobile Shell),是一个用于从客户端跨互联网连接远程服务器的命令行工具.<br>
它能用于SSH连接,但是比Secure Shell功能更多.<br>
它是一个类似于SSH而带有更多功能的应用.<br>
程序最初由Keith Winstein编写,用于类Unix的操作系统中,发布于GNU GPL V3协议下.<br>
其***在SSH的基础上***采用了一个基于UDP的State Synchronization Protocol新协议来专门解决网络延迟的问题,<br>并且在交互视觉上Mosh也做了调整,采用了与SSH相反的方式「先显示,后传输执行」,<br>默认的SSH是每次敲击都要与服务器进行通信,但Mosh会先显示缓存输入然后再一并提交,<br>二者带来的体验差别 是显而易见的.

## 安装

### Ubuntu

```
apt-get update;apt-get install mosh
```

### MacOS

```
brew install mosh
```

## 使用

### 链接服务器

```
mosh 用户名@地址
```

### 删除之前会话

```bash
# Mosh: You have a detached Mosh session on this server (mosh [16994]).
# kill pid

kill 16994
```

## 相比ssh的优点

0. 它是一个支持漫游的远程终端程序.
0. 在所有主流的类 Unix 版本中可用,如 Linux、FreeBSD、Solaris、Mac OS X和Android
0. 支持不稳定连接
0. 支持智能的本地回显
0. 支持用户输入的行编辑
0. 响应式设计及在 wifi、3G、长距离连接下的鲁棒性
0. 在IP改变后保持连接.它使用UDP代替TCP(在SSH中使用),当连接被重置或者获得新的IP后TCP会超时,但是UDP仍然保持连接
0. 在很长的时候之后恢复会话时仍然保持连接
0. 没有网络延迟.立即显示用户输入和删除而没有延迟
0. 像SSH那样支持一些旧的方式登录
0. 包丢失处理机制
0. 会话的中断不会导致当前正在前端执行的命令中断,相当于所有的操作都是在screen命令中一样在后台执行.
0. 会话在中断过后,不会立刻退出,而是启用一个计时器,当网络恢复后会自动重新连接,同时会延续之前的会话,不会重新开启一个.


# 常见问题

## Permission denied

> Permission denied, please try again.

> 问题源

***在输入密码正确的前提下***,如果出现该问题,原因在于被远程用户没有ssh权限.

> 解决方案1

当前用户的ssh权限,即修改 /etc/ssh/sshd_config 文件中:

```bash
PermitRootLogin without-password
 改为
PermitRootLogin yes
```

> 解决方案2

修改 /etc/ssh/sshd_config 文件:

```bash
 Change to no to disable tunnelled clear text passwords 
 把PasswordAuthentication的#号去掉就行了
PasswordAuthentication yes 
```

我的问题解决在于将密码0写成了o...

## mosh: Did not find remote IP address

> mosh: Did not find remote IP address (is SSH ProxyCommand disabled?)

当远程主机端口非22时,不能使用 --prot 指定端口.

```
--ssh="ssh -p 远程主机ssh端口"
```

```
mosh ff4c00@ruby_server_yggc_public_network --ssh="ssh -p 34256"
```
# 参考资料

> [Linux 中国 | Linux 下 SSH 命令实例指南](https://linux.cn/article-3858-1.html)

> [CSDN | 树莓派普通用户ssh出现Permission denied, please try again的解决方法](https://blog.csdn.net/hnlyzxy123/article/details/54375694)

> [CSDN | 远程登录 Linux 服务器报错:Permission denied, please try again.](https://blog.csdn.net/xsj_blog/article/details/72802837)

> [CSDN | 使用SSH传输文件/文件夹](https://blog.csdn.net/bedisdover/article/details/51622133)

> [Linux中国 | 如何在 Linux 中创建 SSH 别名](https://zhuanlan.zhihu.com/p/65655637?utm_source=wechat_session&utm_medium=social&utm_oi=638272867729674240)

> [Linux中国 | 如何实现 ssh 无密码登录](https://mp.weixin.qq.com/s?__biz=MjM5NjQ4MjYwMQ==&mid=205858279&idx=1&sn=c28e8dec2c01c4cde374cb6b5a3109bc&mpshare=1&scene=1&srcid=011603EvxEzFqbRyJ27kmUV9&pass_ticket=TghIXOUy4wDPvEmr3laK5VNeWliWR5%2BJypfUDMda7qmtLe%2FhuxgFxvMj8OsTMLFX#rd)

> [简书 | 使用mosh连接Linux服务器](https://www.jianshu.com/p/c207fb08411e)

> [程序园 | 使用Mosh来改善高延迟网络下的SSH体验](http://www.voidcn.com/article/p-dryugkbt-bmv.html)

> [Github | Error while connecting to remote server in ubuntu 16.04](https://github.com/mobile-shell/mosh/issues/890)

> [CSDN | scp传文件指定端口](https://blog.csdn.net/qq_29307291/article/details/72819802)


> [stackoverflow | How do I reattach to a detached mosh session?](https://stackoverflow.com/questions/17857733/how-do-i-reattach-to-a-detached-mosh-session)

> [CSDN | scp拷贝文件及文件夹](https://blog.csdn.net/imzkz/article/details/5414546)
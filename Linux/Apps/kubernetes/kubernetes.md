<!-- TOC -->

- [Wiki](#wiki)
  - [Kubernetes](#kubernetes)
  - [Minikube](#minikube)
  - [Microk8s](#microk8s)
  - [Kubectl](#kubectl)
  - [Master](#master)
  - [Node](#node)
  - [Master和Node关系](#master和node关系)
  - [Pod](#pod)
  - [Pod和Node的关系](#pod和node的关系)
  - [Service](#service)
  - [Service和Pod的关系](#service和pod的关系)
  - [Deployment](#deployment)
  - [ReplicaSets](#replicasets)
  - [Deployment和Service的关系](#deployment和service的关系)
  - [Deployment和Pod的关系](#deployment和pod的关系)
  - [Deployment和ReplicaSet的关系](#deployment和replicaset的关系)
  - [Label](#label)
  - [Scaling](#scaling)
  - [Rolling updates](#rolling-updates)
  - [Heapster](#heapster)
  - [CoreDNS](#coredns)
  - [Grafana](#grafana)
  - [InfluxDB](#influxdb)
- [集群搭建基本流程](#集群搭建基本流程)
  - [虚拟化支持检查](#虚拟化支持检查)
  - [启动docker](#启动docker)
  - [创建 Kubernetes 集群](#创建-kubernetes-集群)
  - [部署应用](#部署应用)
  - [使用Kubectl进行故障排除](#使用kubectl进行故障排除)
  - [使用服务(service)](#使用服务service)
  - [实例的伸缩](#实例的伸缩)
  - [更新应用](#更新应用)
- [Minikube](#minikube-1)
  - [安装](#安装)
    - [下载](#下载)
    - [添加到PATH中](#添加到path中)
  - [查看版本](#查看版本)
  - [启动集群](#启动集群)
  - [停止集群](#停止集群)
  - [删除集群](#删除集群)
- [Microk8s](#microk8s-1)
  - [安装](#安装-1)
  - [开启标准服务](#开启标准服务)
  - [检查](#检查)
  - [为microk8s.kubectl建立别名](#为microk8skubectl建立别名)
- [kubectl](#kubectl)
  - [安装kubectl](#安装kubectl)
    - [下载](#下载-1)
    - [制作二进制可执行文件](#制作二进制可执行文件)
    - [将二进制文件移动到PATH中](#将二进制文件移动到path中)
    - [检查版本](#检查版本)
  - [命令式命令管理](#命令式命令管理)
    - [检查版本](#检查版本-1)
    - [查看集群的详细信息](#查看集群的详细信息)
    - [要查看集群中的节点](#要查看集群中的节点)
    - [部署应用](#部署应用-1)
    - [查看所有已部署应用](#查看所有已部署应用)
    - [创建代理](#创建代理)
      - [访问所部署应用](#访问所部署应用)
    - [查看Pod](#查看pod)
      - [查看Pod名称](#查看pod名称)
      - [查看Pod的容器信息](#查看pod的容器信息)
    - [查看Pod日志](#查看pod日志)
    - [在容器(Pod中的某一应用程序实例)内执行命令](#在容器pod中的某一应用程序实例内执行命令)
      - [列出实例中的环境变量](#列出实例中的环境变量)
      - [进入实例内部](#进入实例内部)
    - [使用服务](#使用服务)
      - [服务类型](#服务类型)
      - [查看当前所使用的服务](#查看当前所使用的服务)
      - [将应用暴露为服务](#将应用暴露为服务)
      - [查询应用暴露为公开服务情况](#查询应用暴露为公开服务情况)
      - [获取应用暴露为公开服务后的访问URL](#获取应用暴露为公开服务后的访问url)
      - [使用标签(Lable)](#使用标签lable)
        - [通过标签查询Pod](#通过标签查询pod)
        - [通过标签查询服务(services)](#通过标签查询服务services)
        - [添加标签](#添加标签)
      - [删除服务](#删除服务)
    - [应用实例的伸缩](#应用实例的伸缩)
      - [查看当前实例伸缩情况](#查看当前实例伸缩情况)
      - [实例伸缩](#实例伸缩)
    - [更新应用](#更新应用-1)
      - [滚动更新](#滚动更新)
      - [回退](#回退)
    - [删除应用](#删除应用)
  - [配置文件命令式管理](#配置文件命令式管理)
    - [建立对象](#建立对象)
    - [更新对象](#更新对象)
    - [删除对象](#删除对象)
    - [查看对象](#查看对象)
    - [从命令式命令迁移到命令式对象配置](#从命令式命令迁移到命令式对象配置)
      - [将活动对象导出到本地对象配置文件](#将活动对象导出到本地对象配置文件)
      - [从对象配置文件中手动删除状态字段](#从对象配置文件中手动删除状态字段)
      - [使用配置文件更新对象](#使用配置文件更新对象)
- [PWK](#pwk)
  - [节点初始化](#节点初始化)
  - [节点网络安装](#节点网络安装)
  - [查看节点状态](#查看节点状态)
  - [其他节点创建](#其他节点创建)
  - [加入主节点](#加入主节点)
  - [部署服务](#部署服务)
  - [查看节点情况](#查看节点情况)
  - [查看服务状态](#查看服务状态)
- [参考资料](#参考资料)

<!-- /TOC -->

# Wiki

## Kubernetes

> Kubernetes协调连接在一起作为单个单元工作的高可用性计算机集群.<br>
Kubernetes中的抽象允许您将容器化的应用程序部署到集群,而无需将它们专门绑定到单个机器.<br>
为了利用这种新的部署模型,需要以一种将应用程序与各个主机分离的方式打包应用程序：它们需要进行容器化.<br>
容器化的应用程序比过去的部署模型更加灵活和可用,在过去的部署模型中,将应用程序直接安装在特定的计算机上,而程序包已与主机深度集成.<br>
Kubernetes以更有效的方式自动在整个集群中分配和调度应用程序容器.<br>
Kubernetes是一个开放源代码平台,可以投入生产.

> Kubectl通过使用Kubernetes API与集群进行交互,创建和管理应用.

## Minikube

> Minikube是一种轻量级Kubernetes实现,可在本地计算机上创建虚拟机并**部署仅包含一个节点的简单集群**.<br>
Minikube CLI提供了与集群一起使用的基本引导操作,包括启动,停止,状态和删除.<br>
minikube的主要目标是成为本地Kubernetes应用程序开发的最佳工具,并支持所有合适的Kubernetes功能

## Microk8s

> microk8s是Ubuntu的Kubernetes快照软件包<br>
microk8s是独立的,不会覆盖现有的Kubernetes安装.<br>
通过snap将所有依赖环境打包在一起,可有效避免软件依赖问题,以及快速完成软件安装.


所有使用microk8运行的Kubernetes命令都以开头microk8s.<br>
例如,要kubectl使用microk8s 运行该工具,运行microk8s.kubectl.

## Kubectl

> Kubernetes提供的客户端工具,该工具内部就是对Kubernetes API的调用,通过利用kubectl的各种命令可以实现各种功能,是在使用kubernetes中非常常用的工具.

## Master

> 主节点负责管理群集.<br>
主服务器协调集群中的所有活动,例如调度应用程序,维护应用程序的所需状态,扩展应用程序以及推出新的更新.

## Node

> 节点是充当Kubernetes集群中的辅助计算机的VM或物理计算机.<br>
每个节点都有一个Kubelet,它是用于管理节点并与Kubernetes主节点通信的代理.<br>
该节点还应该具有用于​​处理容器操作的工具,例如Docker或rkt.<br>
处理生产流量的Kubernetes集群至少应具有三个节点.

## Master和Node关系

> 主节点管理集群,节点用于托管正在运行的应用程序.

![](https://d33wubrfki0l68.cloudfront.net/99d9808dcbf2880a996ed50d308a186b5900cec9/40b94/docs/tutorials/kubernetes-basics/public/images/module_01_cluster.svg)


## Pod 

> Kubernetes将创建一个Pod来承载应用程序实例.

> Pod 是容器的集合,它是 Kubernetes 调度的最小单位,同一个 Pod 中的容器始终被一起调度.<br>
通常会将紧密相关的容器放到同一个 Pod 中,<br>
同一个 Pod 中的所有容器共享 IP 地址和 Port 空间,<br>
也就是说它们在一个 network namespace 中.

> 如果容器紧密耦合并且需要共享磁盘之类的资源,则仅应在单个Pod中创建所有关联容器.

![](https://d33wubrfki0l68.cloudfront.net/fe03f68d8ede9815184852ca2a4fd30325e5d15a/98064/docs/tutorials/kubernetes-basics/public/images/module_03_pods.svg)

## Pod和Node的关系

> Pod始终在Node上运行.<br>
节点(Node)是Kubernetes中的工作机,并且可以是虚拟机或物理机,具体取决于集群.每个节点由主节点管理.<br>
一个节点可以有多个Pod,Kubernetes主节点会自动处理跨集群中所有Node调度Pod的过程.<br>
主节点的自动调度考虑了每个节点上的可用资源.

每个Kubernetes节点(Node)至少运行：

0. Kubelet,一个负责Kubernetes Master与Node之间通信的过程,它管理Pods和在机器上运行的容器.
0. 容器运行时(例如Docker,rkt)负责从注册表中提取容器映像,解压缩容器并运行应用程序.

![](https://d33wubrfki0l68.cloudfront.net/5cb72d407cbe2755e581b6de757e0d81760d5b86/a9df9/docs/tutorials/kubernetes-basics/public/images/module_03_nodes.svg)

## Service

> Kubernetes 中Pod实际上有生命周期.<br>
当工作节点(Node)死亡时,在该节点上运行的Pod也将丢失.<br>
然后,ReplicaSet可能会通过创建新Pod来动态地将群集驱动回所需的状态,以保持应用程序运行.

> Kubernetes中的服务(Service)是一种抽象,定义了Pod的逻辑集和访问Pod的策略.<br>
服务(Service)使从属Pod之间的松散耦合成为可能.<br>
像所有Kubernetes对象一样,使用YAML(首选)或JSON 定义服务.<br>
服务所针对的Pod集合通常由LabelSelector决定.

## Service和Pod的关系

> Kubernetes集群中的每个Pod都有一个唯一的IP地址,即使是同一节点上的Pod也是如此,因此需要一种自动协调Pod之间的更改的方法(Service),以便应用程序继续运行.<br>
尽管每个Pod都有一个唯一的IP地址,但是如果没有服务(Service),这些IP不会暴露在群集外部.<br>
服务允许应用程序接收外部流量.

> 服务(Service)在一组Pod之间路由流量.<br>
服务(Service)是允许Pod在Kubernetes中死亡和复制而又不影响应用程序的抽象.<br>
Kubernetes Services处理在依赖的Pod(例如应用程序中的前端和后端组件)之间的发现和路由.

## Deployment

> Deployment为Pod和Replica Set(下一代Replication Controller)提供声明式更新.

> 在Deployment中描述想要的目标状态是什么,Deployment controller就会将Pod和Replica Set的实际状态改变到目标状态.<br>
可以定义一个全新的Deployment,也可以创建一个新的替换旧的Deployment.

## ReplicaSets

> 维护在任何给定时间运行的稳定的副本Pod集.<br>
因此,它通常用于保证指定数量的相同Pod的可用性.

## Deployment和Service的关系

> Deployment来创建服务、滚动升级一个服务(Service).

## Deployment和Pod的关系

> Deployment通过操作RS伸缩Pod集群.

## Deployment和ReplicaSet的关系

> 一般不需要直接操纵ReplicaSet对象, 而是由Deployment控制ReplicaSet来完成对pod的伸缩.

## Label

> 标签类似于为应用实例添加别名,方便对服务以及pod的查询.

> 服务(Service)使用标签和选择器来匹配一组Pod,这是一个分组原语,允许对Kubernetes中的对象进行逻辑操作.<br>
标签是附加在对象上的键/值对,可以以多种方式使用：

0. 指定用于开发,测试和生产的对象
0. 嵌入版本标签
0. 使用标签对对象进行分类

![](https://d33wubrfki0l68.cloudfront.net/cc38b0f3c0fd94e66495e3a4198f2096cdecd3d5/ace10/docs/tutorials/kubernetes-basics/public/images/module_04_services.svg)

![](https://d33wubrfki0l68.cloudfront.net/b964c59cdc1979dd4e1904c25f43745564ef6bee/f3351/docs/tutorials/kubernetes-basics/public/images/module_04_labels.svg)

## Scaling

> 扩展部署将确保创建新Pod并将其调度到具有可用资源的节点上.<br>
缩放会将Pod的数量增加到新的所需状态.<br>
Kubernetes还支持Pods的自动缩放.<br>
缩放到零也是可能的,它将终止指定Deployment的所有Pod.

![](https://d33wubrfki0l68.cloudfront.net/043eb67914e9474e30a303553d5a4c6c7301f378/0d8f6/docs/tutorials/kubernetes-basics/public/images/module_05_scaling1.svg)

![](https://d33wubrfki0l68.cloudfront.net/30f75140a581110443397192d70a4cdb37df7bfc/b5f56/docs/tutorials/kubernetes-basics/public/images/module_05_scaling2.svg)

运行一个应用程序的多个实例将需要一种将流量分配给所有实例的方法.<br>
服务(Service)具有集成的负载均衡器,可以将网络流量分发到公开部署的所有Pod.<br>
服务(Service)将使用端点连续监视正在运行的Pod,以确保流量仅发送到可用Pod.

一旦运行了一个应用程序的多个实例,就可以在不停机的情况下进行滚动更新.

## Rolling updates

> 滚动更新允许通过用新的Pod实例增量更新Pod实例,从而在零停机时间内进行Deployment的更新<br>
新的Pod将在具有可用资源的节点上安排<br>
如果服务是公开的,则该服务将在更新期间仅将流量负载平衡到可用的Pod.

滚动更新允许执行以下操作：

0. 将应用程序从一种环境升级到另一种环境(通过容器映像更新)
0. 回滚到以前的版本
0. 持续集成和持续交付应用程序,停机时间为零

## Heapster

> Kubernetes(版本1.0.6和更高版本)以及包含该平台的平台的容器集群监视和性能分析.**现已弃用不再维护.**

## CoreDNS

> 作为服务的发现机制的基本功能,在集群内需要能够通过服务名对服务进行访问,这就需要一个集群范围内的 DNS 服务来完成从服务名到 ClusterIP 的解析.

> 从 Kubernetes 1.11 版本开始,Kubernetes 集群的 DNS 服务由 CoreDNS 提供<br>
CoreDNS 是 CNCF 基金会的一个项目,用 GO 语言实现的高性能、插件式、易扩展的 DNS 服务端.

## Grafana

> 开源指标分析和可视化套件.

## InfluxDB

> 由InfluxData开发的开源时序型数据库.<br>
它由Go写成,着力于高性能地查询与存储时序型数据<br>
InfluxDB被广泛应用于存储系统的监控数据,IoT行业的实时数据等场景.

# 集群搭建基本流程

## 虚拟化支持检查

```bash
# 确保下面命令输出不为空
grep -E --color 'vmx|svm' /proc/cpuinfo
```

## 启动docker 

```bash 
systemctl enable docker.service
systemctl enable kubelet.service
sudo kubeadm init phase certs all --config /var/lib/kubeadm.yaml
```

## 创建 Kubernetes 集群

0. 检查集群部署工具(minikube等)是否正常(minikube version)
0. 启动集群(如: minikube start)
0. 检查Kubernetes控制工具(kubectl)是否正常(kubectl version)
0. 查看集群节点情况(kubectl get nodes)
0. 查看集群的详细信息(kubectl cluster-info)

## 部署应用

0. 部署应用(kubectl create deployment)
0. 查看所有已部署应用(kubectl get deployments)
0. 创建代理(部署应用默认仅集群内部可见)(kubectl proxy)
0. 访问应用.

## 使用Kubectl进行故障排除

命令|作用
-|-
kubectl get pods|列出所有pod
kubectl describe|显示有关资源的详细信息
kubectl logs|从容器中的容器中打印日志
kubectl exec|在Pod中的容器上执行命令

## 使用服务(service)

0. 将应用暴露为服务(kubectl expose deployment/服deployment名称--type=暴露类型 --port 暴露端口)
0. 查看集群可访问服务情况(kubectl get services)
0. 查询应用暴露为公开服务情况(kubectl describe services/服务名称)
0. 查询服务公开访问方式(kubectl get svc)
0. 通过公开服务访问应用
0. 为实例添加标签(kubectl label pod pod名称 标签键=标签值)
0. 删除服务

## 实例的伸缩

0. 实例伸缩(kubectl scale deployments/服务名称)
0. 查看伸缩后的实例部署情况(kubectl get deployments)
0. 查看pod情况(kubectl get pods)

## 更新应用

0. 更新版本(kubectl set image deployments/部署名称)
0. 回退版本(kubectl rollout status deployments/部署名称)

# Minikube

> Minikube用于创建k8s集群.

Minikube支持以下Kubernetes功能:

0. 域名解析
0. 节点端口
0. ConfigMap和秘密
0. 仪表板
0. 容器运行时:Docker,CRI-O和容器化
0. 启用CNI(容器网络接口)
0. 入口

## 安装


### 下载 

```bash
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && chmod +x minikube
``` 

### 添加到PATH中

```bash
sudo install minikube /usr/local/bin
``` 

## 查看版本

通过运行minikube version命令,检查是否已正确安装：

```bash
minikube version
# minikube version: v1.6.2
# commit: 54f28ac5d3a815d1196cd5d57d707439ee4bb392
```

## 启动集群

通过运行minikube start命令来启动集群：

```
minikube start
* minikube v1.6.2 on Ubuntu 18.04
* Selecting 'none' driver from user configuration (alternates: [])
* Running on localhost (CPUs=2, Memory=2461MB, Disk=47990MB) ...
* OS release is Ubuntu 18.04.3 LTS
* Preparing Kubernetes v1.17.0 on Docker '18.09.7' ...
  - kubelet.resolv-conf=/run/systemd/resolve/resolv.conf
* Pulling images ...
* Launching Kubernetes ...
* Configuring local host environment ...
* Waiting for cluster to come online ...
* Done! kubectl is now configured to use "minikube"
```

此过程中可能会多次遇到超时导致失败,重复尝试即可.

如果Downloading VM boot image一直下载失败,<br>
***Linux*** 下可以添加参数启动:

```bash
# linux下独有,不依赖虚拟机启动
sudo minikube start --vm-driver=none
```


## 停止集群

```
minikube stop
```

## 删除集群

```
minikube delete
```

# Microk8s

## 安装

```
sudo snap install microk8s --classic --stable
```

## 开启标准服务

```
sudo microk8s.enable dns dashboard registry
```

microk8s.disable 即用来关闭服务.

## 检查

通过下面命令可检查主要插件的运行状况.

```
sudo microk8s.status --wait-ready
```


## 为microk8s.kubectl建立别名

为了避免与已安装的kubectl发生冲突并避免覆盖任何现有的Kubernetes配置文件,microk8s添加了microk8s.kubectl命令,可通过建立别名进行默认:

```bash
snap alias microk8s.kubectl kubectl
```

# kubectl

## 安装kubectl

### 下载

使用以下命令下载最新版本:

```bash 
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
```

### 制作二进制可执行文件

```bash 
chmod +x ./kubectl
```

### 将二进制文件移动到PATH中

```bash
sudo mv ./kubectl /usr/local/bin/kubectl
```

### 检查版本

```bash
kubectl version
```

## 命令式命令管理

> 内置的命令用于直接快速创建,更新和删除Kubernetes对象.

kubectl工具支持三种对象管理:

0. 命令式命令
0. 命令式对象配置
0. 声明式对象配置

### 检查版本

要检查是否安装了kubectl,可以运行kubectl version命令：

```bash
kubectl version
# Client Version: version.Info{Major:"1", Minor:"17", GitVersion:"v1.17.0", GitCommit:"70132b0f130acc0bed193d9ba59dd186f0e634cf", GitTreeState:"clean", BuildDate:"2019-12-07T21:20:10Z", GoVersion:"go1.13.4", Compiler:"gc", Platform:"linux/amd64"}
# Server Version: version.Info{Major:"1", Minor:"17", GitVersion:"v1.17.0", GitCommit:"70132b0f130acc0bed193d9ba59dd186f0e634cf", GitTreeState:"clean", BuildDate:"2019-12-07T21:12:17Z", GoVersion:"go1.13.4", Compiler:"gc", Platform:"linux/amd64"}
```

可以看到客户端和服务器的版本.<br>
客户端版本是kubectl版本.<br>
服务器版本是在主服务器上安装的Kubernetes版本.<br>
还可以查看有关构建的详细信息.

### 查看集群的详细信息

通过运行kubectl cluster-info来查看集群的详细信息:

```bash
kubectl cluster-info
# Kubernetes master is running at https://172.17.0.39:8443
# KubeDNS is running at https://172.17.0.39:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

# To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

### 要查看集群中的节点

运行kubectl get nodes命令：

```
$ kubectl get nodes
NAME       STATUS   ROLES    AGE     VERSION
minikube   Ready    master   6m42s   v1.17.0
```

### 部署应用

使用kubectl create deployment命令在Kubernetes上部署应用程序(需要提供部署名称和应用程序映像位置,包括Docker Hub外部托管的映像的完整存储库URL):

```
kubectl create deployment 应用名称 --image=容器镜像路径:镜像版本号
```

```bash
kubectl create deployment kubernetes-bootcamp --image=gcr.io/google-samples/kubernetes-bootcamp:v1
# deployment.apps/kubernetes-bootcamp created
```

上面命令实际上做了以下操作:

0. 搜索可以在其中运行应用程序实例的合适节点
0. 安排应用程序在该节点上运行
0. 配置集群以在需要时在新节点上重新安排实例

### 查看所有已部署应用

```bash
kubectl get deployments
# NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
# kubernetes-bootcamp   1/1     1            1           9m6s
```

可以看到有1个正在运行应用程序的单个实例.<br>
该实例在节点(Node)上的Docker容器中运行.

### 创建代理

> Kubernetes内部运行的Pod在**私有的隔离网络**上运行.<br>
默认情况下,它们在同一kubernetes集群中的其他Pod和服务中可见,但在该网络外部不可见.<br>
当使用kubectl时,通过API端点进行交互以与应用程序进行通信.<br>
**kubectl命令可以创建一个代理,该代理会将通信转发到群集范围的专用网络中.**

```bash
# 通过按Ctrl-C终止代理
kubectl proxy
# Starting to serve on 127.0.0.1:8001
# 运行时不显示任何输出
```

现在,已经和Kubernetes集群之间已建立连接.<br>
代理允许从这些终端直接访问Kuburnetes的API.<br>
可以看到所有通过代理端点托管的Kuburnetes的API:

```bash
curl http://localhost:8001/version
# {
#   "major": "1",
#   "minor": "15",
#   "gitVersion": "v1.15.0",
#   "gitCommit": "e8462b5b5dc2584fdcd18e6bcfe9f1e4d970a529",
#   "gitTreeState": "clean",
#   "buildDate": "2019-06-19T16:32:14Z",
#   "goVersion": "go1.12.5",
#   "compiler": "gc",
#   "platform": "linux/amd64"
# }
```

#### 访问所部署应用

要查看应用程序的输出,通过API进行请求:

```bash
export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
# 网址指向Pod API的路由
curl http://localhost:8001/api/v1/namespaces/default/pods/$POD_NAME/proxy/
# Hello Kubernetes bootcamp! | Running on: kubernetes-bootcamp-5b48cfdcbd-qtjpk | v=1
```

### 查看Pod

查找现有的Pod:

```bash
kubectl get pods
# NAME                                   READY     STATUS    RESTARTS   AGE
# kubernetes-bootcamp-5c69669756-hx7xp   1/1       Running   0          3m
```

#### 查看Pod名称

查看集群中所有正在运行的pod名称:

```bash
echo $(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
# kubernetes-bootcamp-5c69669756-hx7xp
```

#### 查看Pod的容器信息

要查看该Pod中有哪些容器以及用于构建这些容器的镜像,运行describe pods命令：

```bash
kubectl describe pods
# Name:           kubernetes-bootcamp-5b48cfdcbd-qtjpk
# Namespace:      default
# Priority:       0
# Node:           minikube/172.17.0.22
# Start Time:     Wed, 25 Dec 2019 15:42:17 +0000
# Labels:         pod-template-hash=5b48cfdcbd
#                 run=kubernetes-bootcamp
# Annotations:    <none>
# Status:         Running
# IP:             172.18.0.4
# Controlled By:  ReplicaSet/kubernetes-bootcamp-5b48cfdcbd
# Containers:
#   kubernetes-bootcamp:
#     Container ID:   docker://00d008ffb744622b6b38ad2bd082b3d14bbb0fee2a0cb1d00d7455efdaea9a1f
#     Image:          gcr.io/google-samples/kubernetes-bootcamp:v1
#     Image ID:       docker-pullable://jocatalin/kubernetes-bootcamp@sha256:0d6b8ee63bb57c5f5b6156f446b3bc3b3c143d233037f3a2f00e279c8fcc64af
#     Port:           8080/TCP
#     Host Port:      0/TCP
#     State:          Running
#       Started:      Wed, 25 Dec 2019 15:42:20 +0000
#     Ready:          True
#     Restart Count:  0
#     Environment:    <none>
#     Mounts:
#       /var/run/secrets/kubernetes.io/serviceaccount from default-token-p6g6l (ro)
# Conditions:
#   Type              Status
#   Initialized       True
#   Ready             True
#   ContainersReady   True
#   PodScheduled      True
# Volumes:
#   default-token-p6g6l:
#     Type:        Secret (a volume populated by a Secret)
#     SecretName:  default-token-p6g6l
#     Optional:    false
# QoS Class:       BestEffort
# Node-Selectors:  <none>
# Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
#                  node.kubernetes.io/unreachable:NoExecute for 300s
# Events:
#   Type    Reason     Age   From               Message
#   ----    ------     ----  ----               -------
#   Normal  Scheduled  6s    default-scheduler  Successfully assigned default/kubernetes-bootcamp-5b48cfdcbd-qtjpk to minikube
#   Normal  Pulled     4s    kubelet, minikube  Container image "gcr.io/google-samples/kubernetes-bootcamp:v1" already present on machine
#   Normal  Created    3s    kubelet, minikube  Created container kubernetes-bootcamp
#   Normal  Started    3s    kubelet, minikube  Started container kubernetes-bootcamp
```

可以从输出信息中看到有关Pod容器的详细信息：<br>
IP地址,使用的端口以及与Pod生命周期相关的事件列表.<br>
注意：describe命令可用于获取有关大多数kubernetes原语的详细信息：node,pod,deployment.

### 查看Pod日志

应用程序通常会发送到标准输出的所有内容都将成为Pod中容器的日志.<br>
可以使用kubectl logs命令检索这些日志：

```bash
export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
kubectl logs $POD_NAME
# Kubernetes Bootcamp App Started At: 2019-12-25T15:42:20.598Z | Running On:  kubernetes-bootcamp-5b48cfdcbd-qtjpk

# Running On: kubernetes-bootcamp-5b48cfdcbd-qtjpk | Total Requests: 1 | App Uptime: 358.639 seconds | Log Time: 2019-12-25T15:48:19.237Z
```

### 在容器(Pod中的某一应用程序实例)内执行命令

Pod启动并运行后,可以直接在应用实例中执行命令.<br>
需要使用exec命令并将Pod的名称用作参数.

#### 列出实例中的环境变量

```bash
kubectl exec $POD_NAME env
# PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# HOSTNAME=kubernetes-bootcamp-5b48cfdcbd-qtjpk
# KUBERNETES_SERVICE_PORT=443
# KUBERNETES_SERVICE_PORT_HTTPS=443
# KUBERNETES_PORT=tcp://10.96.0.1:443
# KUBERNETES_PORT_443_TCP=tcp://10.96.0.1:443
# KUBERNETES_PORT_443_TCP_PROTO=tcp
# KUBERNETES_PORT_443_TCP_PORT=443
# KUBERNETES_PORT_443_TCP_ADDR=10.96.0.1
# KUBERNETES_SERVICE_HOST=10.96.0.1
# NPM_CONFIG_LOGLEVEL=info
# NODE_VERSION=6.3.1
# HOME=/root
```

#### 进入实例内部

```bash
kubectl exec -ti $POD_NAME bash
# root@kubernetes-bootcamp-5b48cfdcbd-qtjpk:/#
```

### 使用服务

#### 服务类型


通过type在ServiceSpec中指定可以以不同的方式公开服务：

名称|作用
-|-
ClusterIP(默认)|在群集的内部IP上公开服务.<br>这种类型使得只能从群集内访问服务.
NodePort|使用NAT在群集中每个选定节点的相同端口上公开服务.<br>使用可以从群集外部访问服务\<NodeIP>:\<NodePort>.<br>ClusterIP的超集.
LoadBalancer|在当前云中创建一个外部负载平衡器(如果支持),并为该服务分配一个固定的外部IP.<br>NodePort的超集.
ExternalName|externalName通过返回具有该名称的CNAME记录,使用任意名称(在规范中指定)公开服务.<br>不使用代理.<br>此类型需要v1.7或更高版本kube-dns.

#### 查看当前所使用的服务

默认有一个名为kubernetes的服务,它是在minikube启动集群时创建的:

```bash
kubectl get services
# NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
# kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   4m
```

#### 将应用暴露为服务

要创建新服务并将其公开给外部流量,将使用带有NodePort作为参数的暴露命令:

```bash
kubectl expose deployment/deployment名称 --type=暴露类型 --port 暴露端口

kubectl expose deployment/kubernetes-bootcamp --type="NodePort" --port 8080
# service/kubernetes-bootcamp exposed
```

```bash
kubectl get services
# NAME                  TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
# kubernetes            ClusterIP   10.96.0.1        <none>        443/TCP          8m
# kubernetes-bootcamp   NodePort    10.102.249.253   <none>        8080:32209/TCP   48s
```

重新查询集群现有服务发现,有一个正在运行的服务,称为kubernetes-bootcamp.<br>
可以看到该服务收到了一个唯一的群集IP,一个内部端口和一个外部IP(节点(Node)的IP).

#### 查询应用暴露为公开服务情况

```bash
kubectl describe services/kubernetes-bootcamp
# Name:                     kubernetes-bootcamp
# Namespace:                default
# Labels:                   run=kubernetes-bootcamp
# Annotations:              <none>
# Selector:                 run=kubernetes-bootcamp
# Type:                     NodePort
# IP:                       10.102.249.253
# Port:                     <unset>  8080/TCP
# TargetPort:               8080/TCP
# NodePort:                 <unset>  32209/TCP
# Endpoints:                172.18.0.4:8080
# Session Affinity:         None
# External Traffic Policy:  Cluster
# Events:                   <none>
```

其中NodePort为外部暴露端口:

```bash
# NodePort:                 <unset>  32209/TCP
```

较为直接的查询暴露端口命令:

```bash
export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')
echo NODE_PORT=$NODE_PORT
# NODE_PORT=32209
```

#### 获取应用暴露为公开服务后的访问URL

kubectl get svc 或 kubectl get services 查询服务访问链接.

```bash
kubectl get svc
# NAME                  TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
# kubernetes            ClusterIP   10.152.183.1     <none>        443/TCP          14m
# kubernetes-bootcamp   NodePort    10.152.183.113   <none>        8080:31424/TCP   2m6s
curl 10.152.183.113:8080
# Hello Kubernetes bootcamp! | Running on: kubernetes-bootcamp-69fbc6f4cf-rsj6m | v=1
```

#### 使用标签(Lable)

Deployment自动为Pod创建一个标签.<br>
使用 kubectl describe deployment 命令,可以看到标签的名称：

```bash
kubectl describe deployment
# Name:                   kubernetes-bootcamp
# Namespace:              default
# CreationTimestamp:      Thu, 26 Dec 2019 14:09:17 +0000
# Labels:                 run=kubernetes-bootcamp
# Annotations:            deployment.kubernetes.io/revision: 1
# Selector:               run=kubernetes-bootcamp
# Replicas:               1 desired | 1 updated | 1 total | 1 available | 0 unavailable
# StrategyType:           RollingUpdate
# MinReadySeconds:        0
# RollingUpdateStrategy:  25% max unavailable, 25% max surge
# Pod Template:
#   Labels:  run=kubernetes-bootcamp
#   Containers:
#    kubernetes-bootcamp:
#     Image:        gcr.io/google-samples/kubernetes-bootcamp:v1
#     Port:         8080/TCP
#     Host Port:    0/TCP
#     Environment:  <none>
#     Mounts:       <none>
#   Volumes:        <none>
# Conditions:
#   Type           Status  Reason
#   ----           ------  ------
#   Available      True    MinimumReplicasAvailable
#   Progressing    True    NewReplicaSetAvailable
# OldReplicaSets:  <none>
# NewReplicaSet:   kubernetes-bootcamp-5b48cfdcbd (1/1 replicas created)
# Events:
#   Type    Reason             Age    From                   Message
#   ----    ------             ----   ----                   -------
#   Normal  ScalingReplicaSet  5m36s  deployment-controller  Scaled up replica set kubernetes-bootcamp-5b48cfdcbd to 1
```

其中 run=kubernetes-bootcamp 即为默认标签:

```bash
# Labels:  run=kubernetes-bootcamp
```

##### 通过标签查询Pod

```bash
kubectl get pods -l 标签键=标签值
kubectl get pods -l run=kubernetes-bootcamp
# NAME                                   READY   STATUS    RESTARTS   AGE
# kubernetes-bootcamp-5b48cfdcbd-p2tgv   1/1     Running   0          10m
```

##### 通过标签查询服务(services)

```bash
kubectl get services -l 标签键=标签值
kubectl get services -l run=kubernetes-bootcamp
# NAME                  TYPE       CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
# kubernetes-bootcamp   NodePort   10.104.166.107   <none>        8080:32010/TCP   9m53s
```

##### 添加标签

可以为pod, service, deloyment等任何实例范围应用添加 label.

```bash
kubectl label pod pod名称 标签键=标签值
kubectl label service service名称 标签键=标签值
kubectl label deloyment deloyment名称 标签键=标签值

export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
echo Name of the Pod: $POD_NAME
# Name of the Pod: kubernetes-bootcamp-5b48cfdcbd-p2tgv

# 使用label命令,后跟对象类型,对象名称和新标签
kubectl label pod $POD_NAME app=v1
# pod/kubernetes-bootcamp-5b48cfdcbd-p2tgv labeled
```

#### 删除服务

```bash
kubectl delete services service_name
kubectl delete services -l lable_name

kubectl delete service -l run=kubernetes-bootcamp
# service "kubernetes-bootcamp" deleted

# 验证服务是否存在
kubectl get services
# NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
# kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   43m
curl $(minikube ip):$NODE_PORT
# curl: (7) Failed to connect to 172.17.0.52 port 32010: Connection refused
```

服务删除后运行实例的pod依然存在,可通过下面命令进行验证:

```bash
kubectl exec -ti $POD_NAME curl localhost:8080
# Hello Kubernetes bootcamp! | Running on: kubernetes-bootcamp-5b48cfdcbd-p2tgv | v=1

kubectl get pods
# NAME                                   READY   STATUS    RESTARTS   AGE
# kubernetes-bootcamp-5b48cfdcbd-p2tgv   1/1     Running   0          48m
```

### 应用实例的伸缩

#### 查看当前实例伸缩情况

```bash
kubectl get deployments
# NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
# kubernetes-bootcamp   1/1     1            1           53s
```

列名|作用
-|-
READY|列显示当前副本与所需副本的比率
CURRENT|是现在正在运行的副本数
DESIRED|是已配置的副本数
UP-TO-DATE|是已更新以匹配所需(配置)状态的副本数
AVAILABLE|状态显示用户实际上有多少个副本

#### 实例伸缩

使用kubectl scale命令,然后使用部署类型,名称和所需的实例数,将Deployment扩展到4个副本：

```bash
kubectl scale deployments/kubernetes-bootcamp --replicas=4
# deployment.extensions/kubernetes-bootcamp scaled
```

再次查看实例情况:

```bash
kubectl get deployments
# NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
# kubernetes-bootcamp   4/4     4            4           9m30s
```

随着参数 replicas 的变化, 实例数量会对应进行伸缩:

```bash
kubectl scale deployments/kubernetes-bootcamp --replicas=2
# deployment.extensions/kubernetes-bootcamp scaled
kubectl get deployments
# NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
# kubernetes-bootcamp   2/2     2            2           17m
```

### 更新应用

#### 滚动更新

当前使用的 image 版本是 v1,执行如下命令将其升级到 v2:

```bash
kubectl set image deployments/kubernetes-bootcamp kubernetes-bootcamp=jocatalin/kubernetes-bootcamp:v2
# deployment.extensions/kubernetes-bootcamp image updated
```

#### 回退

如果想要回退到 v1 版本,执行下面命令进行回滚即可.

```bash
kubectl rollout status deployments/kubernetes-bootcamp
# deployment "kubernetes-bootcamp" successfully rolled out
```

### 删除应用

```
kubectl delete deployment deploy_name
```

## 配置文件命令式管理

### 建立对象

kubectl create -f 从配置文件创建对象

```
kubectl create -f <filename|url>
```

### 更新对象

kubectl replace -f 根据配置文件更新活动对象

```
kubectl replace -f <filename|url>
```

### 删除对象

kubectl delete -f 用来删除配置文件中描述的对象

```
kubectl delete -f <filename|url>

```

### 查看对象

kubectl get -f 用来查看有关配置文件中描述的对象的信息.

```bash
# -o yaml标志指定将打印完整的对象配置
# 使用kubectl get -h 查看选项列表
kubectl get -f <filename|url> -o yaml

```

### 从命令式命令迁移到命令式对象配置


#### 将活动对象导出到本地对象配置文件

```
kubectl get <kind>/<name> -o yaml > <kind>_<name>.yaml
```

#### 从对象配置文件中手动删除状态字段

从对象配置文件中手动删除状态字段

#### 使用配置文件更新对象

```
kubectl replace -f <kind>_<name>.yaml
```

# PWK

> Play with Kubernetes 一个提供了在浏览器中使用免费 CentOS Linux 虚拟机的体验平台,其内部实际上是 Docker-in-Docker(DinD)技术模拟了多虚拟机/PC 的效果.

Play with Kubernetes 平台有如下几个特色:

0. 允许使用 github 或 dockerhub 账号登录
0. 在登录后会开始倒计时,有 4 小时的时间去实践
0. K8s 环境使用 kubeadm 来部署(使用用 weave 网络)
0. 平台共提供 5 台 centos7 设备可供使用(docker 版本为 17.09.0-ce)

## 节点初始化

> 单击左侧的“Add New Instance” 来创建第一个 Kubernetes 集群节点.它会自动将其命名为“node1”,这个将作为群集的主节点.

由于刚创建的主节点 IP 是 192.168.0.13,因此执行如下命令进行初始化：

```bash
# kubeadm init --apiserver-advertise-address $(hostname -i)
kubeadm init --apiserver-advertise-address 192.168.0.13 --pod-network-cidr=10.244.0.0/16
```

初始化完毕完成之后,界面上会显示 kubeadm join 命令,这个用于后续 node 节点加入集群使用,需要牢记.

## 节点网络安装

接着还需要执行如下命令安装 Pod 网络(这里使用 flannel),否则 Pod 之间无法通信.

```bash
#  kubectl apply -n kube-system -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 |tr -d '\n')"
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```

## 查看节点状态

查看节点状态,可以看到目前只有一个 Master 节点

```
kubectl get nodes
```

## 其他节点创建

单击左侧的“Add New Instance”按钮继续创建 4 个节点作为 node 节点

## 加入主节点

这 4 个节点都执行类似如下的 kubeadm join 命令加入集群(即之前 master 节点初始化完成后红框部分内容)

在主节点执行 kubectl get nodes 查看节点状态,可以看到一个包含有 5 个节点集群已经部署成功了.

## 部署服务

执行如下命令通过 yaml 文件部署运行 nginx 服务(该 yaml 文件中指明了副本数量为 3)

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/application/nginx-app.yaml
```

## 查看节点情况

执行如下命令查看副本(pod)情况,可以看到确实有三个副本分别部署在三个 node 节点上.

```
kubectl get pods -o wide
```

## 查看服务状态

执行如下命令则可以查看 services 状态.

```
kubectl get service
```

当部署完毕后,页面上方会出现一个带有数字的蓝色按钮,按钮数字即为 Services 随机配置的外部 port.点击蓝色按钮,即可看到 Nginx 服务的欢迎页面.(但我测试时一直没有出现,不清楚是什么原因)



# 参考资料

> [Github | minikube](https://github.com/kubernetes/minikube)

> [Microk8s | 首页](https://microk8s.io/)

> [Github | microk8s](https://github.com/ubuntu/microk8s)

> [kubernetes | 学习Kubernetes基础](https://kubernetes.io/docs/tutorials/kubernetes-basics/)

> [kubernetes | 在Linux上安装kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-linux)

> [宋净超 | 使用minikube在本机搭建kubernetes集群](https://johng.cn/minikube-installation/)

> [Jones Shi | Kubernetes DNS](https://wiki.shileizcc.com/confluence/display/KUB/Kubernetes+DNS)

> [WikiPedia | InfluxDB](https://zh.wikipedia.org/wiki/InfluxDB)

> [宋净超 | Kubernete概念解析之Deployment](https://jimmysong.io/posts/kubernetes-concept-deployment/)
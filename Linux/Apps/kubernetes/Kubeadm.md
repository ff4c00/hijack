> Kubernetes是一个生产级的开源平台,用于协调计算机集群内和跨计算机集群的应用程序容器的放置(调度)和执行.

<!-- TOC -->

- [kubeadm安装](#kubeadm安装)
  - [关闭防火墙](#关闭防火墙)
  - [禁用SELINUX](#禁用selinux)
  - [查看状态](#查看状态)
  - [临时禁用(重启后失效)](#临时禁用重启后失效)
  - [永久禁用](#永久禁用)
  - [开启数据包转发](#开启数据包转发)
    - [内核开启ipv4转发](#内核开启ipv4转发)
    - [防火墙修改FORWARD链默认策略](#防火墙修改forward链默认策略)
      - [临时生效](#临时生效)
      - [永久生效](#永久生效)
  - [禁用swap](#禁用swap)
  - [配置iptables参数](#配置iptables参数)
  - [docker启动参数配置](#docker启动参数配置)
  - [安装kubeadm、kubelet、kubectl](#安装kubeadmkubeletkubectl)
    - [配置更新源](#配置更新源)
    - [查看可用软件版本](#查看可用软件版本)
    - [安装指定版本](#安装指定版本)
      - [可能遇到的问题](#可能遇到的问题)
    - [标记软件包不被自动更新](#标记软件包不被自动更新)
- [Kubernetes集群安装](#kubernetes集群安装)
  - [master节点](#master节点)
    - [下载所需镜像](#下载所需镜像)
    - [kubeadm init初始化集群](#kubeadm-init初始化集群)
      - [使用命令行初始化](#使用命令行初始化)
      - [使用配置文件初始化](#使用配置文件初始化)
      - [重新初始化](#重新初始化)
    - [检查kubelet使用的cgroup driver](#检查kubelet使用的cgroup-driver)
      - [查看 Docker 使用的 cgroup driver](#查看-docker-使用的-cgroup-driver)
      - [查看kubelet指定的cgroup driver](#查看kubelet指定的cgroup-driver)
    - [创建kubectl使用的kubeconfig文件](#创建kubectl使用的kubeconfig文件)
    - [设置master参与工作负载](#设置master参与工作负载)
- [网络部署](#网络部署)
  - [calico部署](#calico部署)
    - [etcd as datastore](#etcd-as-datastore)
      - [获取安装包](#获取安装包)
      - [load](#load)
    - [创建rbac](#创建rbac)
    - [创建calico相关pod](#创建calico相关pod)
    - [为calico-node创建clusterrolebinding](#为calico-node创建clusterrolebinding)
    - [启动](#启动)
      - [mapping values are not allowed in this context](#mapping-values-are-not-allowed-in-this-context)
- [参考资料](#参考资料)

<!-- /TOC -->

# kubeadm安装

## 关闭防火墙

关闭ufw防火墙,Ubuntu默认未启用,无需设置.

```bash
sudo ufw disable
```

## 禁用SELINUX

ubuntu默认不安装selinux,假如安装了的话,按如下步骤禁用selinux.

## 查看状态

```bash
/usr/sbin/sestatus -v # 如果SELinux status参数为enabled即为开启状态
SELinux status:                 enabled
```

## 临时禁用(重启后失效)

```bash
sudo setenforce 0 # 0代表permissive 1代表enforcing
```

## 永久禁用

```bash
sudo vi /etc/selinux/config
SELINUX=permissive
```

## 开启数据包转发

### 内核开启ipv4转发

修改/etc/sysctl.conf,开启ipv4转发:

```bash
sudo vim /etc/sysctl.conf
net.ipv4.ip_forward = 1 #开启ipv4转发,允许内置路由
```

写入后执行如下命令生效:

```bash
sudo sysctl -p
```

### 防火墙修改FORWARD链默认策略

数据包经过路由后,假如不是发往本机的流量,下一步会走iptables的FORWARD链,<br>
而docker从1.13版本开始,将FORWARD链的默认策略设置为DROP,<br>
会导致出现一些例如跨主机的两个pod使用podIP互访失败等问题.

解决方案有2个:

0. 在所有节点上开机启动时执行iptables -P FORWARD ACCEPT(临时生效)
0. 让docker不操作iptables

#### 临时生效

```bash
sudo iptables -P FORWARD ACCEPT
```

iptables的配置重启后会丢失,可以将配置写进/etc/rc.local中,重启后自动执行:

```bash
/usr/sbin/iptables -P FORWARD ACCEPT
```

#### 永久生效

设置docker启动参数添加--iptables=false选项,使docker不再操作iptables,<br>
比如1.10版以上可编辑docker daemon默认配置文件/etc/docker/daemon.json:

```json
{
  "iptables": false
}
```

## 禁用swap

禁掉所有的swap分区

```bash
sudo swapoff -a
```

同时还需要修改/etc/fstab文件,注释掉 SWAP 的自动挂载,防止服务器重启后swap启用.

## 配置iptables参数

使得流经网桥的流量也经过iptables/netfilter防火墙

```bash
sudo tee /etc/sysctl.d/k8s.conf <<-'EOF'
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

sudo sysctl --system
```

## docker启动参数配置

参数说明:

```json
{
  "iptables": false, // 禁用iptables设置
  "ip-masq": false, // 如果想让podIP可路由的话,设置docker不再对podIP做MASQUERADE,否则docker会将podIP这个源地址SNAT成nodeIP
  "storage-driver": "overlay2" // 设置docker存储驱动为overlay2(需要linux kernel版本在4.0以上,docker版本大于1.12)

}
```

```bash
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://registry.docker-cn.com"],
  "experimental": true,
  "iptables": false,
  "ip-masq": false,
  "storage-driver": "overlay2"
}
EOF

sudo systemctl restart docker
```

## 安装kubeadm、kubelet、kubectl

### 配置更新源

```bash
sudo apt-get update && sudo apt-get install -y apt-transport-https curl

sudo curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

sudo tee /etc/apt/sources.list.d/kubernetes.list <<-'EOF'
deb https://mirrors.aliyun.com/kubernetes/apt kubernetes-xenial main
EOF

sudo apt-get update
```

google地址被墙的情况下可以使用阿里云或者中科大的镜像站:

```bash
sudo apt-get update && sudo apt-get install -y apt-transport-https curl

sudo curl -s https://mirrors.aliyun.com/kubernetes/apt/doc/apt-key.gpg | sudo apt-key add -

sudo tee /etc/apt/sources.list.d/kubernetes.list <<-'EOF'
deb https://mirrors.aliyun.com/kubernetes/apt kubernetes-xenial main
EOF

sudo apt-get update
```

### 查看可用软件版本

```bash
apt-cache madison kubeadm
```

### 安装指定版本

```bash
sudo apt-get install -y kubelet=1.12.0-00 kubeadm=1.12.0-00 kubectl=1.12.0-00
```

#### 可能遇到的问题

```bash
ff4c00@Yongdu:~$ sudo apt-get install -y kubelet=1.12.0-00 kubeadm=1.12.0-00 kubectl=1.12.0-00
Reading package lists... Done
Building dependency tree
Reading state information... Done
Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming.
The following information may help to resolve the situation:

The following packages have unmet dependencies:
 kubeadm : Depends: kubernetes-cni (= 0.6.0) but 0.7.5-00 is to be installed
 kubelet : Depends: kubernetes-cni (= 0.6.0) but 0.7.5-00 is to be installed
E: Unable to correct problems, you have held broken packages.
```

解决办法:先指定0.6.0版本安装kubernetes-cni再安装kubeadm

```bash
sudo apt-get install kubernetes-cni=0.6.0-00
```

### 标记软件包不被自动更新

```bash
sudo apt-mark hold kubelet=1.12.0-00 kubeadm=1.12.0-00 kubectl=1.12.0-00
```

# Kubernetes集群安装

## master节点

### 下载所需镜像

看一下kubernetes v1.12.0需要哪些镜像:

```bash
kubeadm config images list --kubernetes-version=v1.12.0

k8s.gcr.io/kube-apiserver:v1.12.0
k8s.gcr.io/kube-controller-manager:v1.12.0
k8s.gcr.io/kube-scheduler:v1.12.0
k8s.gcr.io/kube-proxy:v1.12.0
k8s.gcr.io/pause:3.1
k8s.gcr.io/etcd:3.2.24
k8s.gcr.io/coredns:1.2.2
```

由于gcr.io被墙,从anjia0532镜像地址下载:

```bash
docker pull anjia0532/google-containers.kube-apiserver:v1.12.0
docker pull anjia0532/google-containers.kube-controller-manager:v1.12.0
docker pull anjia0532/google-containers.kube-scheduler:v1.12.0
docker pull anjia0532/google-containers.kube-proxy:v1.12.0
docker pull anjia0532/google-containers.pause:3.1
docker pull anjia0532/google-containers.etcd:3.2.24
docker pull anjia0532/google-containers.coredns:1.2.2
```

重新打回k8s.gcr.io的镜像tag:

```bash
docker tag anjia0532/google-containers.kube-apiserver:v1.12.0 k8s.gcr.io/kube-apiserver:v1.12.0
docker tag anjia0532/google-containers.kube-controller-manager:v1.12.0 k8s.gcr.io/kube-controller-manager:v1.12.0
docker tag anjia0532/google-containers.kube-scheduler:v1.12.0 k8s.gcr.io/kube-scheduler:v1.12.0
docker tag anjia0532/google-containers.kube-proxy:v1.12.0 k8s.gcr.io/kube-proxy:v1.12.0
docker tag anjia0532/google-containers.pause:3.1 k8s.gcr.io/pause:3.1
docker tag anjia0532/google-containers.etcd:3.2.24 k8s.gcr.io/etcd:3.2.24
docker tag anjia0532/google-containers.coredns:1.2.2 k8s.gcr.io/coredns:1.2.2
```

以上两步也可以直接通过以下脚本image-process.sh完成:

```bash
#!/bin/bash
images=(kube-proxy:v1.12.0 kube-scheduler:v1.12.0 kube-controller-manager:v1.12.0 kube-apiserver:v1.12.0 etcd:3.2.24 pause:3.1 coredns:1.2.2)
for imageName in ${images[@]} ; do
  docker pull anjia0532/google-containers.$imageName
  docker tag anjia0532/google-containers.$imageName k8s.gcr.io/$imageName
  docker rmi anjia0532/google-containers.$imageName
done
```

### kubeadm init初始化集群

#### 使用命令行初始化

`!` ***pod-network-cidr配置的ip地址在下面CALICO_IPV4POOL_CIDR也有用到***

```bash
sudo kubeadm init --apiserver-advertise-address=192.168.31.96 --pod-network-cidr=172.16.0.0/16 --service-cidr=10.233.0.0/16 --kubernetes-version=v1.12.0
```

参数|含义
-|-
--apiserver-advertise-address|指明用 Master 的哪个 interface 与 Cluster 的其他节点通信.<br>如果 Master 有多个 interface,建议明确指定,如果不指定,kubeadm 会自动选择有默认网关的 interface.
--pod-network-cidr|指定 Pod 网络的范围.Kubernetes 支持多种网络方案,而且不同网络方案对 --pod-network-cidr 有自己的要求

#### 使用配置文件初始化

更多的个性化配置可以使用配置文件.使用配置文件的好处是可以固化启动配置,使得启动参数有迹可循.目前使用配置文件初始化的方式只是实验性的.

配置文件kubeadm.yaml如下:

```yaml
apiVersion: kubeadm.k8s.io/v1alpha2
kind: MasterConfiguration
api:
  advertiseAddress: 10.142.232.155
  bindPort: 8443
controllerManagerExtraArgs:
  bind-address: 10.142.232.155
  address: 10.142.232.155
schedulerExtraArgs:
  address: 10.142.232.155
kubernetesVersion: v1.12.0
networking:
  podSubnet: 192.168.0.0/16
  serviceSubnet: 10.233.0.0/16
---
apiVersion: kubeproxy.config.k8s.io/v1alpha1
kind: KubeProxyConfiguration
metricsBindAddress: 0.0.0.0
---
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
address: 0.0.0.0
```

配置文件写法参考:
* [unofficial-kubernetes](https://unofficial-kubernetes.readthedocs.io/en/latest/admin/kubeadm/)
* [pwittrock](http://pwittrock.github.io/docs/admin/kubeadm/)

#### 重新初始化

集群初始化如果遇到问题,可以使用下面的命令进行清理再重新初始化:

```bash
sudo kubeadm reset
```

### 检查kubelet使用的cgroup driver

kubelet启动时指定的cgroup driver需要和docker所使用的保持一致.

#### 查看 Docker 使用的 cgroup driver

```bash
docker info | grep -i cgroup
Cgroup Driver: cgroupfs
```

#### 查看kubelet指定的cgroup driver

```bash
cat /var/lib/kubelet/kubeadm-flags.env
KUBELET_KUBEADM_ARGS=--cgroup-driver=cgroupfs --network-plugin=cni --resolv-conf=/run/systemd/resolve/resolv.conf

# 如果需要修改, 修改后执行:
sudo systemctl daemon-reload
sudo systemctl restart kubelet
```

### 创建kubectl使用的kubeconfig文件

```bash
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

创建完成即可使用kubectl操作集群.

### 设置master参与工作负载

使用kubeadm初始化的集群,将master节点做了taint(污点),使得默认情况下(不设置容忍)Pod不会被调度到master上.<br>
可以使用下面的命令去掉master的taint,使master参与工作负载:

```bash
kubectl taint nodes --all  node-role.kubernetes.io/master-
```

# 网络部署

## calico部署

calico的数据存储可以有两种方式:

0. calico直接与etcd进行交互,使用etcd作为datastore的方式
0. calico和其他k8s组件一样通过kube-apiserver与etcd进行交互,将数据存储在etcd中(通过CRD实现)

### etcd as datastore

#### 获取安装包

到release页面获取安装包,这里用的是v3.2.3版本

```bash
wget https://github.com/projectcalico/calico/releases/download/v3.2.3/release-v3.2.3.tgz
```

#### load

解压后load release-v3.2.3/images下的镜像

```bash
docker load -i calico-cni.tar
docker load -i calico-kube-controllers.tar
docker load -i calico-node.tar
docker load -i calico-typha.tar
```

可以合成一条命令吧?

load后发现镜像都是calico为前缀,而yaml文件里配置的镜像前缀是quay.io/calico/,所以需要重新打一下tag或改一下yaml里的前缀.

更改前缀:

```bash
docker tag calico/node:v3.2.3 quay.io/calico/node:v3.2.3
docker tag calico/cni:v3.2.3 quay.io/calico/cni:v3.2.3
docker tag calico/kube-controllers:v3.2.3 quay.io/calico/kube-controllers:v3.2.3
docker tag calico/typha:v3.2.3 quay.io/calico/typha:v3.2.3
```

### 创建rbac

```bash
kubectl apply -f release-v3.2.3/k8s-manifests/rbac.yaml
```

### 创建calico相关pod

修改release-v3.2.3/k8s-manifests/hosted/calico.yaml:

```yaml
  # 配置etcd地址
  etcd_endpoints: "https://localhost:2379"
  # 配置etcd相关证书和key
  etcd_ca: "/calico-secrets/ca.crt"   # "/calico-secrets/etcd-ca"
  etcd_cert: "/calico-secrets/server.crt" # "/calico-secrets/etcd-cert"
  etcd_key: "/calico-secrets/server.key"  # "/calico-secrets/etcd-key"
          # 配置etcd的相关证书和key在主机上的位置
          "etcd_key_file": "/etc/kubernetes/pki/etcd/server.key",
          "etcd_cert_file": "/etc/kubernetes/pki/etcd/server.crt",
          "etcd_ca_cert_file": "/etc/kubernetes/pki/etcd/ca.crt",
            # 配置集群内pod的地址范围,要和kubeadm启动时指定的一致
            - name: CALICO_IPV4POOL_CIDR
              value: "172.16.0.0/16"
        # 配置calico-node使用主机上kubeadm生成的etcd相关证书和key
        - name: etcd-certs
          hostPath:
            path: /etc/kubernetes/pki/etcd
#          secret:
#            secretName: calico-etcd-secrets
#            defaultMode: 0400
        # 配置calico-kube-controllers使用主机上kubeadm生成的etcd相关证书和key
        - name: etcd-certs
          hostPath:
            path: /etc/kubernetes/pki/etcd
#          secret:
#            secretName: calico-etcd-secrets
#            defaultMode: 0400
```

### 为calico-node创建clusterrolebinding

```bash
kubectl create clusterrolebinding kube-system-default-role-binding --clusterrole=cluster-admin --serviceaccount=kube-system:calico-node
```
### 启动

```bash
kubectl apply -f release-v3.2.3/k8s-manifests/hosted/calico.yaml
```

#### mapping values are not allowed in this context

> error: error parsing release-v3.2.3/k8s-manifests/hosted/calico.yaml: error converting YAML to JSON: yaml: line 210: mapping values are not allowed in this context

```yaml
# 文件格式错误
- name: etcd-certs
    hostPath:
    path: /etc/kubernetes/pki/etcd

# 应为:
- name: etcd-certs
  hostPath:
    path: /etc/kubernetes/pki/etcd
```

# 参考资料

> [kubernetes | hello-minikube](https://kubernetes.io/docs/tutorials/hello-minikube/)

> [Github | kubernetes](https://github.com/kubernetes/kubernetes)

> [CSDN | Ubuntu-18.04使用kubeadm安装kubernetes-1.12.0](https://blog.csdn.net/liukuan73/article/details/83116271)

> [简书 | ubuntu搭建k8s环境](https://www.jianshu.com/p/f368e8fa80ec)

> [博客园 | Kubeadm部署安装kubernetes1.12.1](https://www.cnblogs.com/zhaojingyu/p/11661151.html)
Nginx主配置文件nginx.conf是一个纯文本类型的文件(其他配置文件大多也是如此),<br>
位于Nginx安装目录下的conf目录中,整个配置文件是以**区块的形式组织**的.<br>
一般,每个区块以一个大括号`{}`来表示,<br>
区块可以分为几个层次:<br>


```
.main(全局设置)
├── events
├── http
│   ├── server(主机设置)
│   ├── upstream(上游服务器设置,主要为反向代理负载均衡相关配置)
│   ├── location(URL匹配特定位置后的设置)

```

<!-- TOC -->

- [main](#main)
  - [events](#events)
  - [http](#http)
    - [server](#server)
    - [location](#location)
- [upstream](#upstream)
- [核心内容说明](#核心内容说明)
- [参考资料](#参考资料)

<!-- /TOC -->

# main

> main部分设置的指令将影响其它所有部分的设置.

```cnf
worker_processes  1;
error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;
```

## events

```cnf
events {
    worker_connections  1024;
}
```

## http

### server

> server部分的指令主要用于指定虚拟主机域名、IP和端口.



### location

> 用于匹配网页位置(比如,根目录“/”,“/images”,等等)

# upstream

>用于设置一系列的后端服务器,设置反向代理及后端服务器的负载均衡

# 核心内容说明

```bash
# 主区块
worker_processes 1; # worker进程的数量

# 事件区块
events {
    worker_connections  1024; # 每个worker进程的最大链接数
}

# http区块
http {
    include       /etc/nginx/mime.types; # 支持的媒体类型库文件
    default_type  application/octet-stream; # 默认的媒体类型
    sendfile        on; # 开启高效传输模式
    keepalive_timeout  65; # 连接超时时长

    # 第一个server区块
    # 表示一个独立的虚拟主机站点
    server {
      listen 80; # 监听端口 提供服务的端口
      server_name localhost; # 提供服务的域名主机名

      # location区块
      location {
        root html; # 站点的根目录 相当于nginx的安装目录
        index index.html index.htm; # 默认的首页文件,多个用空格分隔
      }

      error_page 500 502 503 504 /50x.html; # 出现对应http状态,使用50x.html回应
    }

}
```

# 参考资料

> [SegmentFault | nginx服务器安装及配置文件详解](https://segmentfault.com/a/1190000002797601)

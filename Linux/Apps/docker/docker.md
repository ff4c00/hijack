
<!-- TOC -->

- [说明](#说明)
  - [关于镜像](#关于镜像)
  - [关于容器](#关于容器)
    - [容器与虚拟机](#容器与虚拟机)
- [安装](#安装)
  - [Ubuntu](#ubuntu)
    - [docker.io](#dockerio)
    - [docker-ce](#docker-ce)
      - [安装依赖](#安装依赖)
      - [添加docker的GPG key](#添加docker的gpg-key)
      - [设置docker镜像源](#设置docker镜像源)
      - [安装指定版本docker-ce](#安装指定版本docker-ce)
  - [脚本安装](#脚本安装)
  - [以非root用户身份管理Docker](#以非root用户身份管理docker)
    - [创建docker组](#创建docker组)
    - [向docker组中添加用户](#向docker组中添加用户)
    - [注销并重新登录](#注销并重新登录)
  - [启用实验模式](#启用实验模式)
    - [/etc/docker/daemon.json](#etcdockerdaemonjson)
    - [重启后台服务](#重启后台服务)
    - [验证是否开启](#验证是否开启)
  - [设置开机自启动](#设置开机自启动)
- [卸载](#卸载)
  - [ubuntu](#ubuntu)
- [基础](#基础)
  - [容器与镜像的关系](#容器与镜像的关系)
- [常用命令](#常用命令)
  - [启动后台服务](#启动后台服务)
  - [docker run](#docker-run)
  - [docker build](#docker-build)
  - [其他](#其他)
- [Dockerfile](#dockerfile)
  - [文件格式示例](#文件格式示例)
  - [镜像操作指令](#镜像操作指令)
    - [LABEL](#label)
    - [ENV](#env)
    - [ARG](#arg)
    - [COPY](#copy)
    - [USER](#user)
    - [WORKDIR](#workdir)
    - [VOLUME](#volume)
      - [关于文件权限](#关于文件权限)
    - [EXPOSE](#expose)
    - [CMD](#cmd)
  - [构建镜像](#构建镜像)
    - [docker build](#docker-build-1)
    - [守护进程](#守护进程)
    - [.dockerignore](#dockerignore)
    - [参数:-t](#参数-t)
    - [缓存](#缓存)
      - [寻找缓存的逻辑](#寻找缓存的逻辑)
- [docker-compost](#docker-compost)
  - [基础](#基础-1)
    - [简介](#简介)
    - [常用命令](#常用命令-1)
      - [启动服务](#启动服务)
  - [文件格式](#文件格式)
  - [services](#services)
    - [image](#image)
    - [build](#build)
      - [dockerfile](#dockerfile)
      - [arg](#arg)
    - [command](#command)
      - [关于覆盖默认命令后容器自动退出问题](#关于覆盖默认命令后容器自动退出问题)
    - [container_name](#container_name)
    - [depends_on](#depends_on)
    - [dns](#dns)
    - [dns_search](#dns_search)
    - [tmpfs](#tmpfs)
    - [entrypoint](#entrypoint)
    - [env_file](#env_file)
    - [environment](#environment)
    - [expose](#expose)
    - [external_links](#external_links)
    - [extra_hosts](#extra_hosts)
    - [labels](#labels)
    - [links](#links)
    - [logging](#logging)
    - [pid](#pid)
    - [ports](#ports)
    - [security_opt](#security_opt)
    - [stop_signal](#stop_signal)
    - [volumes](#volumes)
    - [cap_add, cap_drop](#cap_add-cap_drop)
    - [cgroup_parent](#cgroup_parent)
    - [devices](#devices)
    - [extends](#extends)
    - [network_mode](#network_mode)
    - [networks](#networks)
      - [aliases](#aliases)
    - [其它](#其它)
- [镜像使用](#镜像使用)
  - [镜像加速](#镜像加速)
  - [列出镜像列表](#列出镜像列表)
  - [获取一个新的镜像](#获取一个新的镜像)
  - [查找镜像](#查找镜像)
  - [拖取镜像](#拖取镜像)
  - [创建镜像](#创建镜像)
- [提交镜像](#提交镜像)
  - [登录帐号](#登录帐号)
    - [Error saving credentials](#error-saving-credentials)
  - [push 提交](#push-提交)
- [常见问题](#常见问题)
  - [映射后宿主机无法访问容器内服务](#映射后宿主机无法访问容器内服务)
    - [确定容器ip地址](#确定容器ip地址)
    - [宿主机访问容器端口](#宿主机访问容器端口)
    - [容器中查看端口详情](#容器中查看端口详情)
    - [容器中项目启动时指定ip地址](#容器中项目启动时指定ip地址)
  - [Read-only file system](#read-only-file-system)
  - [Error calling StartServiceByName](#error-calling-startservicebyname)
- [调优](#调优)
  - [--squash 精简镜像大小](#--squash-精简镜像大小)
  - [占用空间优化](#占用空间优化)
    - [查看docker占用空间](#查看docker占用空间)
    - [清理镜像](#清理镜像)
    - [清理volumes僵尸文件](#清理volumes僵尸文件)
- [管理工具](#管理工具)
  - [dockly](#dockly)
    - [安装&启动](#安装启动)
    - [常用快捷键](#常用快捷键)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

## 关于镜像

镜像是一种轻量级、可执行的独立软件包,它包含运行某个软件所需的所有内容,包括代码、运行时、库、环境变量和配置文件.

## 关于容器

容器是镜像的运行时实例.

容器可以共享单个内核,并且需要存在于容器镜像中的唯一信息是可执行文件及其软件包依赖项,这些都不需要在主机系统上安装.<br>
这些进程的运行方式类似于原生进程,并且可以通过运行 docker ps 等命令来逐一管理它们.<br>
如同在 Linux 上运行 ps 以查看活动进程一样.<br>
最后,由于它们包含所有依赖项,因此不存在配置关联.<br>
容器化应用"可以随处运行".

### 容器与虚拟机

与仅通过管理程序对主机资源进行虚拟访问的虚拟机相比,它们具有更好的性能特征.<br>
容器可以获取本机访问,每个容器都在独立进程中运行,占用的内存不超过任何其他可执行文件.

将可扩展单位做成单个可移植的可执行文件,具有广泛的意义.<br>
它表示,CI/CD 可以将更新推送到分布式应用程序的任何部分,无需担心系统依赖项.<br>
编排扩展动作将启动新的可执行文件而不是新的虚拟主机.

# 安装

## Ubuntu

### docker.io

```bash
sudo apt-get update
sudo apt-get install -y docker.io
```

### docker-ce

#### 安装依赖

```bash
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
```

#### 添加docker的GPG key

```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

国内访问download.docker.com不稳定的话可以使用阿里云的

```bash
curl -fsSL https://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg | sudo apt-key add -
```

#### 设置docker镜像源

```bash
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

国内访问download.docker.com不稳定的话可以使用阿里云镜像源

```bash
sudo add-apt-repository \
   "deb [arch=amd64] https://mirrors.aliyun.com/docker-ce/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

#### 安装指定版本docker-ce

查看源中都有哪些版本:

```bash
apt-cache madison docker-ce
 docker-ce | 18.06.1~ce~3-0~ubuntu | https://mirrors.aliyun.com/docker-ce/linux/ubuntu bionic/stable amd64 Packages
 docker-ce | 18.06.0~ce~3-0~ubuntu | https://mirrors.aliyun.com/docker-ce/linux/ubuntu bionic/stable amd64 Packages
 docker-ce | 18.03.1~ce~3-0~ubuntu | https://mirrors.aliyun.com/docker-ce/linux/ubuntu bionic/stable amd64 Packages
```

例如安装18.03.1版:

```
$ sudo apt-get install -y docker-ce=18.03.1~ce~3-0~ubuntu
```

## 脚本安装

按照官网教程安装未成功,可以尝试下面脚本:

```bash
sudo apt-get update
# 获取安装脚本
curl -fsSL get.docker.com -o get-docker.sh
# 执行安装脚本
sudo sh get-docker.sh
```

## 以非root用户身份管理Docker

### 创建docker组

```bash
sudo groupadd docker
```

### 向docker组中添加用户

```bash
sudo usermod -aG docker $USER
```

### 注销并重新登录

如果在虚拟机上进行测试,可能必须重启此虚拟机才能使更改生效.

在桌面 Linux 环境(例如,X Windows)中,需要彻底从会话中注销,然后重新登录.

##  启用实验模式

> "--squash" is only supported on a Docker daemon with experimental features enabled<br>
当时构建镜像时加入--squash参数会返回上面提示,<br>
即有些功能 ***尚不稳定*** 需启用实验模式方能使用.

### /etc/docker/daemon.json

文件中加入:

```json
{
  "experimental": true
}
```

### 重启后台服务

```bash
sudo service docker restart
```

### 验证是否开启

下面命令返回true即可:

```bash
docker version -f '{{.Server.Experimental}}'
```

## 设置开机自启动

```bash
sudo systemctl enable docker && sudo systemctl start docker

```

# 卸载

## ubuntu

```bash
sudo apt-get remove docker docker-engine docker.io docker-ce
```

# 基础

## 容器与镜像的关系

容器与镜像的关系类似于面向对象编程中的对象与类:

Docker|面向对象
-|-
容器|对象
镜像|类


# 常用命令

## 启动后台服务

```bash
sudo service docker start
```

## docker run

```bash
docker run [OPTIONS] IMAGE [COMMAND] [ARG...]
```

选项|作用
-|-
-a, --attach=[]|登录容器(以docker run -d启动的容器)
-c, --cpu-shares=0|设置容器CPU权重,在CPU共享场景使用
--cap-add=[]| 添加权限,[权限清单](http://linux.die.net/man/7/capabilities)
--cap-drop=[]| 删除权限,[权限清单](http://linux.die.net/man/7/capabilities)
--cidfile=""|运行容器后,在指定文件中写入容器PID值,一种典型的监控系统用法
--cpuset=""|设置容器可以使用哪些CPU,此参数可以用来容器独占CPU
-d, --detach=false|指定容器运行于前台还是后台
--device=[]|添加主机设备给容器,相当于设备直通
--dns=[]|指定容器的dns服务器
--dns-search=[]|指定容器的dns搜索域名,写入到容器的/etc/resolv.conf文件
-e, --env=[]|指定环境变量,容器中可以使用该环境变量
--entrypoint=""|覆盖image的入口点
--env-file=[]|指定环境变量文件,文件格式为每行一个环境变量
--expose=[]|指定容器暴露的端口,即修改镜像的暴露端口
-h, --hostname=""|指定容器的主机名
-i, --interactive=false|打开STDIN,用于控制台交互
--link=[]|指定容器间的关联,使用其他容器的IP、env等信息
--lxc-conf=[]|指定容器的配置文件,只有在指定--exec-driver=lxc时使用
-m, --memory=""|指定容器的内存上限
--name=""|指定容器名字,后续可以通过名字进行容器管理,links特性需要使用名字
--net="bridge"|容器网络设置,待详述
-P, --publish-all=false|指定容器暴露的端口,待详述
-p, --publish=[]|指定容器暴露的端口,待详述
--privileged=false|指定容器是否为特权容器,特权容器拥有所有的capabilities
--restart=""|指定容器停止后的重启策略,待详述
--rm=false|指定容器停止后自动删除容器(不支持以docker run -d启动的容器)
--sig-proxy=true|设置由代理接受并处理信号,但是SIGCHLD、SIGSTOP和SIGKILL不能被代理
-t, --tty=false|分配tty设备,该可以支持终端登录
-u, --user=""|指定容器的用户
-v, --volume=[]|给容器挂载存储卷,挂载到容器的某个目录
--volumes-from=[]|给容器挂载其他容器上的卷,挂载到容器的某个目录
-w, --workdir=""|指定容器的工作目录

## docker build

命令|作用
-|-
-t|镜像的名字及标签,通常 name:tag 或者 name 格式
--build-arg=|改变镜像创建时通过ENV声明的变量值<br>docker build --build-arg HTTP_PROXY=http://10.20.30.2:1234 --build-arg FTP_PROXY=http://40.50.60.5:4567 .
-f|指定dockerfile文件位置
## 其他

命令|作用
-|-
docker stop 容器ID/容器名称|关闭运行中的容器
docker start 容器ID/容器名称|启动一个已经停止的容器
docker restart 容器ID/容器名称|重启一个容器
docker attach 容器ID/容器名称|进入一个运行中的容器
docker ps -a|显示全部容器
docker ps|显示当前运行的容器
docker images|查看本地
docker rmi $(docker images \| grep -v RESPOSITORY \| awk '{print $3}')|删除所有镜像
docker rm 容器ID/容器名称|删除容器
docker rm $(docker ps -a)|删除所有容器
docker rmi $(docker images \| grep none \| grep -v RESPOSITORY \| awk '{print $3}')|删除仓库或标签为none的镜像
docker history 镜像ID|查看历史
docker export 容器ID/容器名称 > xxx.tar|导出容器
docker save myimage \| bzip2 -9 -c> /home/save.tar.bz2|把 mynewimage 镜像保存成 tar 文件
sudo bzip2 -d -c < /home/save.tar.bz2 \| docker load|加载 myimage 镜像
Ctrl+P+Q|退出但在后台运行容器

# Dockerfile

> Dockerfile 分为四部分:基础镜像信息、维护者信息、镜像操作指令、容器启动执行指令

## 文件格式示例

```docker
# This dockerfile uses the ubuntu image
# VERSION 2 - EDITION 1
# Author: docker_user
# Command format: Instruction [arguments / command] ..
 
# 1、第一行必须指定 基础镜像信息
FROM ubuntu
 
# 2、维护者信息
MAINTAINER docker_user docker_user@email.com
 
# 3、镜像操作指令
RUN echo "deb http://archive.ubuntu.com/ubuntu/ raring main universe" >> /etc/apt/sources.list
RUN apt-get update && apt-get install -y nginx
RUN echo "\ndaemon off;" >> /etc/nginx/nginx.conf
 
# 4、容器启动执行指令
CMD /usr/sbin/nginx
```

## 镜像操作指令

### LABEL

> 为镜像指定标签,目前只有用来编写维护者信息.

```docker
LABEL \
author="mike" \
email="ff4c00@gmail.com"
```

### ENV

功能为设置环境变量

语法有两种
```dockerfile
ENV <key> <value>
ENV <key>=<value>
```

两者的区别就是:<br>
第一种是一次设置一个<br>
第二种是一次设置多个

```dockerfile
ENV \
### 设置系统基本信息
LANG=en_US.UTF-8 LANGUAGE=en_US:en \
SCRIPT_PATH=/tmp/script
```

### ARG

设置变量命令,ARG命令定义了一个变量,在docker build创建镜像的时候,使用 --build-arg \<varname>=\<value>来指定参数.

```dockerfile
ARG <name>[=<default value>]
```

可以定义一个或多个参数,如下:

```dockerfile
ARG user1
ARG buildno

# 也可以给参数一个默认值
# 如果给了ARG定义的参数默认值,那么当build镜像时没有指定参数值,将会使用这个默认值
FROM busybox
ARG user1=someuser
ARG buildno=1
```

### COPY

复制宿主机文件夹到镜像内.

```dockerfile
COPY bin/ $SCRIPT_PATH
```

### USER 

设置启动容器的用户,可以是用户名或UID.

```dockerfile
USER user_name
USER UID
```

```dockerfile
# 设定进入容器后所使用用户
USER ${OPERATOR_UID}:${OPERATOR_GROUP}
```

注意:如果设置了容器以user_name用户去运行,<br>
那么RUN, CMD 和 ENTRYPOINT 都会以这个用户去运行


### WORKDIR 

设置工作目录,对RUN,CMD,ENTRYPOINT,COPY,ADD生效.<br>
如果不存在则会创建,也可以设置多次.

```dockerfile
## 设置进入docker以后的默认目录
WORKDIR ${WORK_DIR}
```



```dockerfile

```


```dockerfile

```


```dockerfile

```


```dockerfile

```


```dockerfile

```


```dockerfile

```


```dockerfile

```


```dockerfile

```


```dockerfile

```


```dockerfile

```

### VOLUME

> VOLUME用于创建挂载点,即向基于所构建镜像创始的容器添加卷.

一个卷可以存在于一个或多个容器的指定目录,该目录可以绕过联合文件系统,并具有以下功能:

0. 卷可以容器间共享和重用
0. 容器并不一定要和其它容器共享卷
0. 修改卷后会立即生效
0. 对卷的修改不会对镜像产生影响
0. 卷会一直存在,直到没有任何容器在使用它

VOLUME可以将源代码、数据或其它内容添加到镜像中,而又不并提交到镜像中,并且可以多个容器间共享这些内容.

如,通过VOLUME创建一个挂载点:

```dockerfile
ENV ITBILU_PATH=/home/itbilu/
VOLUME [$ITBILU_PATH]
```

构建的镜像,并指定镜像名为itbilu/test.<br>
构建镜像后,使用新构建的运行一个容器.<br>
运行容器时,需-v参将能本地目录绑定到容器的卷(挂载点)上,以使容器可以访问宿主机的数据.

```bash
sudo docker run -i -t -v ~/code/itbilu:/home/itbilu/  itbilu/test 
root@31b0fac536c4:/# cd /home/itbilu/
root@31b0fac536c4:/home/itbilu# ls
README.md  app.js  bin  config.js  controller  db  demo  document  lib  minify.js  node_modules  package.json  public  routes  test  views
```

已经可以容器的/home/itbilu/目录下访问到宿主机~/code/itbilu目录下的数据了.

#### 关于文件权限

当容器内挂载外面目录或文件后,在容器内该挂载点默认为uid为1000的用户所有.

所以创建容器用户时将其uid赋为1000即可拥有对挂在目录的写入权限.

### EXPOSE

> EXPOSE用于指定容器在运行时监听的端口

```bash
EXPOSE <port> [<port>...]
```

EXPOSE并不会让容器的端口访问到主机.<br>
要使其可访问,需要在docker run运行容器时通过-p来发布这些端口,<br>
或通过-P参数来发布EXPOSE导出的所有端口.

### CMD

> 指令用于指定默认的容器主进程的启动命令.

在指令格式上,一般推荐使用 exec 格式,<br>这类格式在解析时会被解析为 JSON 数组,因此一定要使用双引号 ",而不要使用单引号.<br>
如果使用 shell 格式的话,<br>实际的命令会被包装为 sh -c 的参数的形式进行执行.比如:<br>
CMD echo $HOME<br>
在实际执行中,会将其变更为:

```docker
CMD [ "sh", "-c", "echo $HOME" ]
```

```docker
# 启动 elasticsearch
CMD ["sh", "-c", "/home/elasticsearch/elasticsearch-5.6.3/bin/elasticsearch"]
```

## 构建镜像

### docker build

> docker build 命令会根据 Dockerfile 文件及上下文构建新 Docker 镜像.<br>
构建上下文是指 Dockerfile 所在的本地路径或一个URL(Git仓库地址).<br>
构建上下文环境会被递归处理,所以构建所指定的路径还包括了子目录,而URL还包括了其中指定的子模块.

将当前目录做为构建上下文时,可以像下面这样使用docker build命令构建镜像:

```bash
docker build .
Sending build context to Docker daemon  6.51 MB
...
```

### 守护进程

> 构建会在 Docker 后台守护进程(daemon)中执行,而不是CLI中.<br>
构建前,构建进程会将全部内容(递归)发送到守护进程.<br>
大多情况下,应该将一个空目录作为构建上下文环境,并将 Dockerfile 文件放在该目录下.

### .dockerignore

> 为了提高构建性能,可以通过.dockerignore文件排除上下文目录下不需要的文件和目录.<br>
在 Docker 构建镜像的第一步,docker CLI 会先在上下文目录中寻找.dockerignore文件,根据.dockerignore 文件排除上下文目录中的部分文件和目录,然后把剩下的文件和目录传递给 Docker 服务.

### 参数:-t

> 用于设置镜像标签.

```bash
docker build -t nginx/v3:1.0.2 -t nginx/v3:latest .
```

### 缓存

> Docker 会重用已生成的中间镜像,以加速docker build的构建速度.<br>
Docker 守护进程会一条一条的执行 Dockerfile 中的指令,而且会在每一步提交并生成一个新镜像,最后会输出最终镜像的ID.<br>
生成完成后,Docker 守护进程会自动清理你发送的上下文.<br>
Dockerfile文件中的每条指令会被独立执行,并会创建一个新镜像,RUN cd /tmp等命令不会对下条指令产生影响.

构建缓存仅会使用本地库生成链上的镜像,如果不想使用本地缓存的镜像,也可以通过--cache-from指定缓存.<br>
指定后将不再使用本地生成的镜像链,而是从镜像仓库中下载.

#### 寻找缓存的逻辑

```
   FROM base_image:version           Dockerfile:
           +----------+                FROM base_image:version
           |base image|                RUN cmd1  --> use cache because we found base image
           +-----X----+                RUN cmd11 --> use cache because we found cmd1
                / \
               /   \
       RUN cmd1     RUN cmd2           Dockerfile:
       +------+     +------+           FROM base_image:version
       |image1|     |image2|           RUN cmd2  --> use cache because we found base image
       +---X--+     +------+           RUN cmd21 --> not use cache because there's no child node
          / \                                        running cmd21, so we build a new image here
         /   \
RUN cmd11     RUN cmd12
+-------+     +-------+
|image11|     |image12|
+-------+     +-------+
```

大部分指令可以根据上述逻辑去寻找缓存,除了 ADD 和 COPY .<br>
这两个指令会复制文件内容到镜像内,除了指令相同以外,Docker 还会检查每个文件内容校验(不包括最后修改时间和最后访问时间),如果校验不一致,则不会使用缓存.

除了这两个命令,Docker 并不会去检查容器内的文件内容,比如 RUN apt-get -y update,每次执行时文件可能都不一样,但是 Docker 认为命令一致,会继续使用缓存.<br>
这样一来,以后构建时都不会再重新运行apt-get -y update.

如果 Docker 没有找到当前指令的缓存,则会构建一个新的镜像,并且之后的所有指令都不会再去寻找缓存.



# docker-compost

> Compose 项目是 [Docker 官方的开源项目](https://github.com/docker/compose),负责实现对 Docker 容器集群的快速编排.<br>
从功能上看,跟 OpenStack 中的 Heat 十分类似.<br>
Compose 定位是 「定义和运行多个 Docker 容器的应用(Defining and running multi-container Docker applications)」,其前身是开源项目 Fig.


## 基础

### 简介

Dockerfile 模板文件,可以让用户很方便的定义一个单独的应用容器.<br>
然而,在日常工作中,经常会碰到需要多个容器相互配合来完成某项任务的情况.<br>
例如要实现一个 Web 项目,除了 Web 服务容器本身,往往还需要再加上后端的数据库服务容器,甚至还包括负载均衡容器等.

Compose 恰好满足了这样的需求.<br>
它允许用户通过一个单独的 docker-compose.yml 模板文件(YAML 格式)来定义一组相关联的应用容器为一个项目(project).

Compose 中有两个重要的概念:

0. 服务 (service):一个应用的容器,实际上可以包括若干运行相同镜像的容器实例.
0. 项目 (project):由一组关联的应用容器组成的一个完整业务单元,在 docker-compose.yml 文件中定义.

可见,一个项目可以由多个服务(容器)关联而成,Compose 面向项目进行管理.<br>


Compose 的默认管理对象是项目,通过子命令对项目中的一组容器进行便捷地生命周期管理.

Compose 项目由 Python 编写,实现上调用了 Docker 服务提供的 API 来对容器进行管理.<br>
因此,只要所操作的平台支持 Docker API,就可以在其上利用 Compose 来进行编排管理.

其最常见的应用场景就是web网站.

### 常用命令

#### 启动服务

```bash
docker-compose [-f 文件路径] up [-d (后台运行)] [--force-recreate(在配置文件没有更新情况下重新构建)] 服务名(文件中services:下的二级服务名)
```

## 文件格式

```yml
version: '2'
services:
  web:
    image: docker_test:v1
    build: /path/to/build/dir
      args:
        - buildno=1
        - password=secret
```

## services

在 services 标签下的第二级标签为自定义,它是服务名称.

### image

image用于指定服务的镜像名称或镜像 ID.<br>
如果镜像在本地不存在,Compose 将会尝试拉取这个镜像.

可指定格式有:

```yml
image: redis
image: ubuntu:14.04
image: tutum/influxdb
image: example-registry.com:4000/postgresql
image: a4bc65fd
```

### build

服务除了可以基于指定的镜像,还可以基于一份 Dockerfile,在使用 up 启动之时执行构建任务,这个构建标签就是 build,它可以指定 Dockerfile 所在文件夹的路径.<br>
Compose 将会利用它自动构建这个镜像,然后使用这个镜像启动服务容器.

```yml
build: /path/to/build/dir

## 也可以是相对路径,只要上下文确定就可以读取到 Dockerfile
build: ./dir

```

#### dockerfile

build 都是一个目录,如果要指定Dockerfile文件需要在build标签的子级标签中使用dockerfile标签指定.

```yml
## 设定上下文根目录,然后以该目录为准指定 Dockerfile
build:
  context: ../
  dockerfile: path/of/Dockerfile
```

**如果同时指定了image和build两个标签,那么Compose会构建镜像并且把镜像命名为image的值**.

#### arg

> 在构建过程中指定环境变量,但是在构建成功后取消

YAML 的布尔值(true, false, yes, no, on, off)必须要使用引号引起来(单引号、双引号均可),否则会当成字符串解析.

```yml
args:
  - buildno=1
  - password=secret
```

### command

> 覆盖容器启动后默认执行的命令

```yml
command: bundle exec thin -p 3000

## 也可以写成类似 Dockerfile 中的格式:
command: [bundle, exec, thin, -p, 3000]
```

#### 关于覆盖默认命令后容器自动退出问题

在命令末尾追加默认命令即可,如:

```yml
command: ['sh', '-c', "sudo service cron start && /bin/bash"]
```

### container_name

> 用于指定容器的名字

```yml
container_name: app
```

### depends_on

> 指定项目容器启动的顺序

下面容器会先启动 redis 和 db 两个服务,最后才启动 web 服务:

```yml
version: '2'
services:
  web:
    build: .
    depends_on:
      - db
      - redis
  redis:
    image: redis
  db:
    image: postgres
```

默认情况下使用 

```
docker-compose up web 
```

这样的方式启动web服务时,也会启动redis和db两个服务,因为在配置文件中定义了依赖关系.

### dns

和 --dns 参数一样用途,格式如下:

```yml
dns: 8.8.8.8

## 或

dns:
  - 8.8.8.8
  - 9.9.9.9
```

### dns_search

```yml
dns_search: example.com

## 或

dns_search:
  - dc1.example.com
  - dc2.example.com
```

### tmpfs

> 挂载临时目录到容器内部

```yml
tmpfs: /run

## 或

tmpfs:
  - /run
  - /tmp
```

### entrypoint

> 用于指定接入点

在 docker-compose.yml 中可以定义接入点,覆盖 Dockerfile 中的定义:

```yml
entrypoint: /code/entrypoint.sh
```

### env_file

> 而在 docker-compose.yml 中可以定义一个专门存放变量的文件

通过 docker-compose -f FILE 指定了配置文件,则 env_file 中路径会使用配置文件路径.

如果有变量名称与 environment 指令冲突,则以后者为准.

```yml
env_file:
  - ./common.env
  - ./apps/web.env
  - /opt/secrets.env
```

这里所说的环境变量是对宿主机的 Compose 而言的,如果在配置文件中有 build 操作,这些变量并不会进入构建过程中,如果要在构建中使用变量还是首选前面刚讲的 arg 标签.

### environment

> 与env_file标签完全不同,反而和 arg 有几分类似.<br>
这个标签的作用是设置镜像变量,它可以保存变量到镜像里面.<br>
也就是说**启动的容器也会包含这些变量设置,这是与 arg 最大的不同**.

一般 arg 标签的变量仅用在构建过程中.<br>
而 environment 和 Dockerfile 中的 ENV 指令一样会把变量一直保存在镜像、容器中.

```yml
environment:
  RACK_ENV: development
  SHOW: 'true'
  SESSION_SECRET:

environment:
  - RACK_ENV=development
  - SHOW=true
  - SESSION_SECRET
```

### expose

> 用于指定暴露的端口,但是只是作为一种参考,<br>
实际上docker-compose.yml的端口映射还得ports这样的标签.

```yml
expose:
 - "3000"
 - "8000"
```

### external_links

> 在使用Docker过程中,会有许多单独使用docker run启动的容器.<br>
为了使Compose**能够连接这些不在docker-compose.yml中定义的容器**,需要external_links标签.<br>
它可以让Compose项目里面的容器连接到那些项目配置外部的容器(前提是外部容器中必须至少有一个容器是连接到与项目内的服务的同一个网络里面)

```yml
external_links:
 - redis_1
 - project_db_1:mysql
 - project_db_1:postgresql

```

### extra_hosts

> 添加主机名的标签,就是往/etc/hosts文件中添加一些记录,与Docker client的--add-host类似.

```yml
extra_hosts:
 - "somehost:162.242.195.82"
 - "otherhost:50.31.209.229"
```

### labels

> 向容器添加元数据,和Dockerfile的LABEL指令一个意思

```yml
labels:
  com.example.description: "Accounting webapp"
  com.example.department: "Finance"
  com.example.label-with-empty-value: ""
labels:
  - "com.example.description=Accounting webapp"
  - "com.example.department=Finance"
  - "com.example.label-with-empty-value"
```

### links 

> 解决的是容器连接问题.<br>
与Docker client的--link一样效果,会连接到其它服务中的容器.

```yml
links:
 - db
 - db:database
 - redis

## 使用的别名将会自动在服务容器中的/etc/hosts里创建,相应的环境变量也将被创建.
### 例如:

172.12.2.186  db
172.12.2.186  database
172.12.2.187  redis
```

### logging

> 配置日志服务

```yml
logging:
  driver: syslog
  options:
    syslog-address: "tcp://192.168.0.42:123"
```

默认的driver是json-file.<br>
只有json-file和journald可以通过docker-compose logs显示日志.<br>
其他方式有其他日志查看方式,但目前Compose不支持.<br>
对于可选值可以使用options指定.

### pid

> 将PID模式设置为主机PID模式,跟主机系统共享进程命名空间.<br>
容器使用这个标签将能够访问和操纵其他容器和宿主机的名称空间.

```yml
pid: "host"
```

### ports

> 映射端口的标签.<br>
使用HOST:CONTAINER格式或者只是指定容器的端口,宿主机会随机映射端口.

当使用HOST:CONTAINER格式来映射端口时,如果你使用的容器端口小于60你可能会得到错误得结果,因为YAML将会解析xx:yy这种数字格式为60进制.<br>
所以建议采用字符串格式.

格式|说明|举例
-|-|-
ip:hostPort:containerPort|映射指定地址的指定端口到虚拟机的指定端口(不常用)|127.0.0.1:3306:3306,映射本机的3306端口到虚拟机的3306端口.
ip::containerPort|映射指定地址的任意端口到虚拟机的指定端口.(不常用)|127.0.0.1::3306,映射本机的3306端口到虚拟机的3306端口.
hostPort:containerPort|映射本机的指定端口到虚拟机的指定端口.(常用)|3306:3306,映射本机的3306端口到虚拟机的3306端口.

```yml
ports:
 - "3000"
 - "8000:8000"
 - "49100:22"
 - "127.0.0.1:8001:8001"
```

### security_opt

> 为每个容器覆盖默认的标签.<br>
简单说来就是管理全部服务的标签.

比如设置全部服务的user标签值为USER:

```yml
security_opt:
  - label:user:USER
  - label:role:ROLE
```

### stop_signal

> 设置另一个信号来停止容器.<br>
在默认情况下使用的是SIGTERM停止容器.<br>
可以使用stop_signal标签设置另一个信号.

```yml
stop_signal: SIGUSR1
```

### volumes

> 挂载一个目录或者一个已存在的数据卷容器,可以直接使用 [HOST:CONTAINER] 这样的格式.<br>
或者使用 [HOST:CONTAINER:ro] 这样的格式.<br>
后者对于容器来说,数据卷是只读的,这样可以有效保护宿主机的文件系统

Compose的数据卷指定路径可以是相对路径,使用 . 或者 .. 来指定相对目录.<br>

数据卷的格式可以是下面多种形式:

```yml
volumes:
  ## 只是指定一个路径,Docker 会自动在创建一个数据卷(这个路径是容器内部的)
  - /var/lib/mysql

  ## 使用绝对路径挂载数据卷
  - /opt/data:/var/lib/mysql

  ## 以 Compose 配置文件为中心的相对路径作为数据卷挂载到容器
  - ./cache:/tmp/cache

  ## 使用用户的相对路径(~/ 表示的目录是 /home/<用户目录>/ 或者 /root/)
  - ~/configs:/etc/configs/:ro

  ## 已经存在的命名的数据卷
  - datavolume:/var/lib/mysql
```

如果不使用宿主机的路径,可以指定一个volume_driver:

```yml
volume_driver: mydriver
```

### cap_add, cap_drop

> 添加或删除容器的内核功能.

```yml
cap_add:
  - ALL

cap_drop:
  - NET_ADMIN
  - SYS_ADMIN
```

### cgroup_parent

> 指定一个容器的父级cgroup.

```yml
cgroup_parent: m-executor-abcd
```

### devices

> 设备映射列表.<br>
与Docker client的--device参数类似.

```yml
devices:
  - "/dev/ttyUSB0:/dev/ttyUSB0"
```

### extends

> 这个标签可以扩展另一个服务,扩展内容可以是来自在当前文件,也可以是来自其他文件,相同服务的情况下,后来者会有选择地覆盖原有配置

```yml
extends:
  file: common.yml
  service: webapp
```

可以在任何地方使用这个标签,只要标签内容包含file和service两个值就可以了.<br>
file的值可以是相对或者绝对路径,如果不指定file的值,那么Compose会读取当前YML文件的信息.


### network_mode

> 网络模式,与Docker client的--net参数类似,只是相对多了一个service:[service name] 的格式.<br>
可以指定使用服务或者容器的网络.

```yml
network_mode: "bridge"
network_mode: "host"
network_mode: "none"
network_mode: "service:[service name]"
network_mode: "container:[container name/id]"
```

### networks

> 加入指定网络.

```yml
services:
  some-service:
    networks:
     - some-network
     - other-network
```

#### aliases

关于这个标签还有一个特别的子标签aliases,这是一个用来设置服务别名的标签,例如:

```yml
services:
  some-service:
    networks:
      some-network:
        aliases:
         - alias1
         - alias3
      other-network:
        aliases:
         - alias2
```

相同的服务可以在不同的网络有不同的别名.

### 其它

> 类似于使用docker run效果的标签.

名称|作用
-|-
cpu_shares|
cpu_quota|
cpuset|
domainname|
hostname|
ipc|
mac_address|
mem_limit|
memswap_limit|
privileged|
read_only|
restart|
shm_size|
stdin_open|
tty|
user|
working_dir|

```yml
cpu_shares: 73
cpu_quota: 50000
cpuset: 0,1

user: postgresql
working_dir: /code

domainname: foo.com
hostname: foo
ipc: host
mac_address: 02:42:ac:11:65:43

mem_limit: 1000000000
memswap_limit: 2000000000
privileged: true

restart: always

read_only: true
shm_size: 64M
stdin_open: true
tty: true
```

# 镜像使用

当运行容器时,使用的镜像如果在本地中不存在,docker 就会自动从 docker 镜像仓库中下载,默认是从 [Docker Hub](https://hub.docker.com/) 公共镜像源下载.

## 镜像加速

编辑或创建:/etc/docker/daemon.json文件

```bash
sudo vim /etc/docker/daemon.json
# 加入并重启服务:
{
  "registry-mirrors": ["https://registry.docker-cn.com"]
}
```

配置存在问题会出现类似下述问题:

```bash
sudo service docker start
Job for docker.service failed because the control process exited with error code.
See "systemctl status docker.service" and "journalctl -xe" for details.

systemctl status docker.service
● docker.service - Docker Application Container Engine
   Loaded: loaded (/lib/systemd/system/docker.service; disabled; vendor preset: enabled)
   Active: failed (Result: exit-code) since Tue 2019-04-16 18:29:51 CST; 38s ago
     Docs: https://docs.docker.com
  Process: 24826 ExecStart=/usr/bin/dockerd -H fd:// (code=exited, status=1/FAILURE)
 Main PID: 24826 (code=exited, status=1/FAILURE)

 4月 16 18:29:51 Yongdu systemd[1]: Failed to start Docker Application Container Engine.
```


## 列出镜像列表

```bash
docker images 
# => REPOSITORY       TAG     IMAGE ID      CREATED      SIZE
# => ubuntu           15.10   9b9cb95443b5  2 years ago  137MB
# => training/webapp  latest  6fae60ef3446  3 years ago  349MB

```

各参数解析:

参数名|作用
-|-
REPOSTITORY|表示镜像的仓库源
TAG|镜像的标签
IMAGE ID|镜像ID
CREATED|镜像创建时间
SIZE|镜像大小

同一仓库源可以有多个 TAG,代表这个仓库源的不同个版本,如ubuntu仓库源里,有15.10、14.04等多个不同的版本,我们使用 REPOSTITORY:TAG 来定义不同的镜像.

所以,如果要使用版本为15.10的ubuntu系统镜像来运行容器时,命令如下:

```bash
docker run -t -i ubuntu:15.10 /bin/bash
```

如果不指定一个镜像的版本标签,例如只使用 ubuntu,docker 将默认使用 ubuntu:latest 镜像.

## 获取一个新的镜像

当在本地主机上使用一个不存在的镜像时 Docker 就会自动下载这个镜像.<br>
如果想预先下载这个镜像,可以使用 docker pull 命令来下载它.

```bash
docker pull ubuntu:13.10
```

## 查找镜像

```bash
docker search 容器名称
```

```bash
docker search httpd
# => NAME                       DESCRIPTION                                     STARS  OFFICIAL  AUTOMATED
# => httpd                      The Apache HTTP Server Project                  2394   [OK]
# => hypriot/rpi-busybox-httpd  Raspberry Pi compatible Docker Image with a …   46
```

各列解析:

列名|作用
-|-
NAME|镜像仓库源的名称
DESCRIPTION|镜像的描述
OFFICIAL|是否docker官方发布

## 拖取镜像

```bash
docker pull httpd
```

下载完成后,就可以使用这个镜像了:

```bash
docker run httpd
```

## 创建镜像

当从docker镜像仓库中下载的镜像不能满足需求时,可以通过以下两种方式对镜像进行更改:

0. 从已经创建的容器中更新镜像,并且提交这个镜像
0. 使用 Dockerfile 指令来创建一个新的镜像


# 提交镜像

## 登录帐号

```bash
docker login
```

登录成功后会提示:

Login Succeeded

### Error saving credentials

> Error saving credentials: error storing credentials - err: exit status 1, out: \`Cannot autolaunch D-Bus without X11 $DISPLAY\`<br>
登录过程中输入帐号密码后提示上述内容.

系统默认安装了golang-docker-credential-helpers,卸载以后就好了:

```bash
sudo apt purge -y golang-docker-credential-helpers
```

## push 提交

```bash
docker push 构建镜像名称:版本号
```

# 常见问题

## 映射后宿主机无法访问容器内服务

> 以rails项目为例,下面是排查过程

### 确定容器ip地址

```bash
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' 容器id
```

### 宿主机访问容器端口

```bash
telnet 容器ip 端口
```

如果不通进入容器

### 容器中查看端口详情


```bash
netstat -lntup | grep 端口号

# 如:
netstat -lntup | grep 3020
# => tcp        0      0 127.0.0.1:3020          0.0.0.0:*               LISTEN      257/puma 3.12.1 (tc 
```

这时发现监听地址是:127.0.0.1 本地回环地址,所以外面的主机访问不到容器内的服务.

这个监听地址在rails项目启动后也会打印出来,主要是把流程记录下来供以后参考.

### 容器中项目启动时指定ip地址

```bash
# 或指定为容器ip 下面方法更为通用
rails s -p 3020 -b 0.0.0.0
```

## Read-only file system

> 当镜像构建过程当涉及到系统环境变量修改的内容,会提示:<br>
*Read-only file system*


相关脚本如下:

```bash
echo '修改文件描述符数量'
sudo tee -a /etc/sysctl.conf <<-'EOF'
vm.max_map_count=262144
EOF
sudo sysctl -p
```

构建过程如下:

```bash
# 修改文件描述符数量
vm.max_map_count=262144
sudo sysctl -p
sysctl: setting key "vm.max_map_count": Read-only file system
...
```

解决方法如下:

0. 在容器启动时添加privileged参数,如:<br>docker run -it -privileged "centos:centos6" /bin/bash
0. 在构建时相关修改内容写在CMD语句内.<br>这个方法没有尝试,目前通过修改基础系统镜像然后提交来暂时维持.

## Error calling StartServiceByName

> output: Error calling StartServiceByName for org.freedesktop.secrets: Timeout was reached

将当前用户加入docker组.


# 调优

## --squash 精简镜像大小

> Docker 在 Docker 1.13 中引入了 --squash 参数,<br>
可以在构建过程中实现导出容器的内容并重新导入成一个单层的镜像.

***目前(2019-07-12)该功能并不稳定,需[启用实验模式](#启用实验模式).***

```bash
docker build --squash .
```

## 占用空间优化

### 查看docker占用空间

```bash
docker system df
```

### 清理镜像

参数|说明
-|-
before|指定哪个TAG之前的所有镜像
reference|过滤某个标识的镜像

```bash
function clean_docker_history_images () {
  echo "请输入需要清除历史镜像的库名及标签:"
  read -e repository_tag
  echo repository=${repository_tag//:*/''}
  echo format_repository=${repository_tag//'/'/'\/'}

  images=$(
    docker images \
    -f before=$repository_tag \
    -f reference=$repository \
    | awk "/?*$format_repository*/{print $1 \":\" $2}"
  )
  
  if [ ! $images == ':' ];then
    docker rmi -f $images
  else
    echo '暂无可清理镜像'
  fi
}
```

### 清理volumes僵尸文件

```bash
docker volume rm $(docker volume ls -qf dangling=true)
```
# 管理工具

## dockly

> 终端管理工具<br>
依赖:NodeJS.

### 安装&启动

```bash
# 安装
sudo npm install -g dockly

# 启动
dockly
```

### 常用快捷键




# 参考资料

> [官网 | debian docker 安装教程](https://docs.docker.com/install/linux/docker-ce/debian/)

> [CSDN | 树莓派安装docker](https://blog.csdn.net/u014569612/article/details/78336476)

> [W3Cschool | Docker教程](https://www.w3cschool.cn/docker/)

> [docker中文 | 在容器中安装新的程序](http://www.docker.org.cn/book/docker/docker-install-package-9.html)

> [Docker 中文文档 | 适用于 Linux 的安装后步骤](https://docs.docker-cn.com/engine/installation/linux/linux-postinstall/)

> [Docker 中文文档 | docker入门 ](https://docs.docker-cn.com/get-started/)

> [CloudMan | 调试 Dockerfile - 每天5分钟玩转 Docker 容器技术](http://www.cnblogs.com/CloudMan6/p/6853329.html)

> [CSND | Docker 容器与镜像清理汇总](https://blog.csdn.net/littlebrain4solving/article/details/77524731)

> [悟能博客 | Docker长时间运行后的volumes目录清理](https://blog.lutty.me/linux/2017-07/docker-clean-volumes-dir.html)

> [Dockerfile 使用介绍](https://www.cnblogs.com/ityouknow/p/8588725.html)

> [IT笔录 | Docker镜像构建文件Dockerfile及相关命令介绍](https://itbilu.com/linux/docker/VyhM5wPuz.html)

> [docker-compose生成的容器立刻退出,exited with code 0](http://www.sail.name/2017/10/09/container-exited-with-code-0-created-by-docker-compose/)

> [Compose 简介](https://yeasy.gitbooks.io/docker_practice/content/compose/introduction.html)

> [简书 | Docker Compose 配置文件详解](https://www.jianshu.com/p/2217cfed29d7)

> [CSDN | Docker运维笔记-Docker端口映射](https://blog.csdn.net/qq_29994609/article/details/51730640)

> [博客园 | Dockerfile命令详解(超全版本)](https://www.cnblogs.com/dazhoushuoceshi/p/7066041.html)

> [Linux中国 | Dockly:从终端管理 Docker 容器](https://linux.cn/article-10925-1.html)

> [博客园 | 探讨Docker容器中修改系统变量的方法](https://www.cnblogs.com/junneyang/p/5278482.html)

> [Linux中国 | 如何打造更小巧的容器镜像](https://zhuanlan.zhihu.com/p/60639076)

> [CSDN | About docker login in Ubuntu 18.04](https://blog.csdn.net/qwfys200/article/details/84105355)

> [stackoverflow | docker login timeout on ubuntu \`Error calling StartServiceByName for org.freedesktop.secrets: Timeout was reached\`](https://stackoverflow.com/questions/50696747/docker-login-timeout-on-ubuntu-error-calling-startservicebyname-for-org-freedes)

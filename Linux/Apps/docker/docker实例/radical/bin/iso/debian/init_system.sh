#!/bin/bash

bash -x $RADICAL_BIN_PATH/iso/edit_sources/edit_sources.sh &&\
bash -x $RADICAL_BIN_PATH/iso/debian/update_system.sh &&\
echo '系统必要软件安装' &&\
apt-get install -fy vim sudo git openssh-server \
tmux htop net-tools apt-utils &&\
bash -x $RADICAL_BIN_PATH/iso/setting_tmux.sh

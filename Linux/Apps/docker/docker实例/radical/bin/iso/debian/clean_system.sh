#!/bin/bash

echo '清理所有软件缓存'
sudo apt-get autoclean
sudo apt-get clean
echo '删除系统不再使用的孤立软件'
sudo apt autoremove
echo '删除所有apt/lists下的文件'
# 就此镜像而言,这条命令在添加后可以使镜像减小423MB
rm -rf /var/lib/apt/lists/*
echo '清理孤立的库文件'
sudo deborphan | xargs sudo apt-get -y remove --purge
# echo '清除构建脚本文件'
# rm -rf /tmp/script/*
#!/bin/bash

echo '创建用户及用户组'
groupadd -g ${OPERATOR_GID} ${OPERATOR_GROUP}
useradd --create-home -s /bin/bash -u ${OPERATOR_UID} -g ${OPERATOR_GID} ${OPERATOR_USER}
echo '为soduers文件添加写权限'
chmod u+w /etc/sudoers
echo '将新生成的用户组添加到sudoers中并且设置为在执行的时候不输入密码'
echo "%${OPERATOR_GROUP}          ALL=(ALL)                NOPASSWD: ALL" >> /etc/sudoers
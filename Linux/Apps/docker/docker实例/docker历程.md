> 记录了docker应用历程中的主要迭代过程以及过程中的一些感悟.

<!-- TOC -->

- [前言](#前言)
  - [关于Dockerfile文件格式](#关于dockerfile文件格式)
- [ubuntu(2019-04)](#ubuntu2019-04)
  - [目录结构](#目录结构)
  - [Dockerfile](#dockerfile)
  - [scripts](#scripts)
    - [db](#db)
      - [oracle/init_oracle_support.sh](#oracleinit_oracle_supportsh)
    - [iso](#iso)
      - [clean_system.sh](#clean_systemsh)
      - [create_user.sh](#create_usersh)
      - [setting_tmux.sh](#setting_tmuxsh)
      - [sources_18.09_ali.sh](#sources_1809_alish)
      - [used_alias.sh](#used_aliassh)
    - [ruby](#ruby)
      - [init_ruby.sh](#init_rubysh)
      - [load_ruby.sh](#load_rubysh)
- [radical(2019-05-01)](#radical2019-05-01)
  - [目录结构](#目录结构-1)
  - [app](#app)
    - [ruby](#ruby-1)
      - [Dockerfile](#dockerfile-1)
    - [ubuntu-18.04](#ubuntu-1804)
      - [Dockerfile](#dockerfile-2)
      - [init_host_system.sh](#init_host_systemsh)
  - [bin](#bin)
    - [iso](#iso-1)
      - [create_alias_for_this_folder.sh](#create_alias_for_this_foldersh)
      - [create_user.sh](#create_usersh-1)
      - [debian](#debian)
        - [clean_system.sh](#clean_systemsh-1)
        - [init_system.sh](#init_systemsh)
        - [update_system.sh](#update_systemsh)
      - [edit_sources](#edit_sources)
        - [edit_sources.sh](#edit_sourcessh)
        - [sources_list](#sources_list)
          - [ubuntu_18.04.sh](#ubuntu_1804sh)
        - [sources_ubuntu.sh](#sources_ubuntush)
      - [setting_tmux.sh](#setting_tmuxsh-1)
      - [used_alias.sh](#used_aliassh-1)
    - [ruby](#ruby-2)
      - [init_ruby.sh](#init_rubysh-1)
  - [hodor.yml](#hodoryml)
  - [ubuntu-18.04.yml](#ubuntu-1804yml)
- [chernoAlpha(2019-05-20)](#chernoalpha2019-05-20)

<!-- /TOC -->

# 前言

## 关于Dockerfile文件格式

0. 每一个文件中一个命令只使用一次,<br>这样在保证代码整洁的同时确保了镜像最小化.

0. 文件中不做具体业务逻辑处理,通过引入脚本进行具体处理,<br>虽然这样做并不为docker推荐,但是在保持文件代码整洁以及代码复用方面确实很有优势.

迭代过程按时间排序主要有以下内容:

# ubuntu(2019-04)

这一版本为接触docker后最原始的应用版本,<br>周期内迭代了近70个小版本,最终得以下面内容定版.

这一版本的核心目的在于在docker内构建ruby开发环境.

主要内容包括:

0. ubuntu系统环境构建(更新源替换、常用软件安装、常用别名配置) 
0. 普通用户构建
0. ruby环境构建
0. ruby连接oracle数据库(把相关依赖全部拷入到镜像内部)

## 目录结构

```
.
├── Dockerfile
└── scripts
    ├── db
    │   └── oracle
    │       └── init_oracle_support.sh
    ├── iso
    │   ├── clean_system.sh
    │   ├── create_user.sh
    │   ├── setting_tmux.sh
    │   ├── sources_18.09_ali.sh
    │   └── used_alias.sh
    └── ruby
        ├── init_ruby.sh
        └── load_ruby.sh
```

## Dockerfile

```dockerfile
# 指定基础镜像信息
FROM ubuntu:18.04

# 编写维护者信息
MAINTAINER ff4c00 ff4c00@gmail.com

# 镜像操作指令

## 设置环境变量
ENV \
## 设置系统基本信息
LANG=en_US.UTF-8 LANGUAGE=en_US:en \
## 设置普通用户信息
OPERATOR_USER=web OPERATOR_GROUP=web OPERATOR_UID=500 OPERATOR_GID=500 \ 
## 设置ruby版本
RUBY_VERSION=2.5.1 \
## oracle数据库连接相关
ORACLE_SUPPORT_DIR_NAME=instantclient_12_1 \
## 设置工作目录
WORK_DIR=/home/web \
## 项目名称
PROJECT_NAME=project \
## 项目目录挂载点
PROJECT_PATH=/home/${OPERATOR_USER}/${PROJECT_NAME} \
## 项目使用端口
WEB_PORT=3000

## 复制脚本文件目录
COPY scripts /tmp/scripts

## 系统初始化相关操作
RUN \
## 修改软件更新源
bash -x /tmp/scripts/iso/sources_18.09_ali.sh &&\
## 系统依赖更新
apt-get update && apt-get upgrade -y &&\
## 系统必要软件安装
apt-get install -fy vim sudo git openssh-server \
tmux htop &&\
## 创建普通用户
# bash -x /tmp/scripts/iso/create_user.sh &&\
## 设置tmux 
bash -x /tmp/scripts/iso/setting_tmux.sh &&\
## 为用户写入常用别名
bash -x /tmp/scripts/iso/used_alias.sh &&\
## 执行ruby初始化脚本
bash -x /tmp/scripts/ruby/init_ruby.sh &&\
## 安装oracle数据库连接依赖
bash -x /tmp/scripts/db/oracle/init_oracle_support.sh &&\
## 清理系统
bash -x /tmp/scripts/iso/clean_system.sh

## 设定进入容器后所使用用户
# USER ${OPERATOR_UID}:${OPERATOR_GROUP}

## 指定开放端口
## TODO: ssh相关内容搭建
EXPOSE 22 ${WEB_PORT}

## 设置进入docker以后的默认目录
WORKDIR ${WORK_DIR}

VOLUME ${PROJECT_PATH}

# CMD \
## 启动容器自动启动ssh
# /usr/sbin/sshd -D &
```

## scripts

### db

#### oracle/init_oracle_support.sh

```bash
# 下载并解压依赖
# tar -xf  $PWD/${ORACLE_SUPPORT_DIR_NAME}.tar.gz 
# 创建目录
sudo mkdir -p /opt/oracle
# 复制文件
# sudo cp -rf ${ORACLE_SUPPORT_DIR_NAME}/ /opt/oracle/${ORACLE_SUPPORT_DIR_NAME}
sudo cp -rf /tmp/scripts/db/oracle/instantclient_12_1/ /opt/oracle/${ORACLE_SUPPORT_DIR_NAME}
# 创建链接
sudo ln -s /opt/oracle/${ORACLE_SUPPORT_DIR_NAME}/libclntsh.so.12.1 /opt/oracle/${ORACLE_SUPPORT_DIR_NAME}/libclntsh.so

# 写入环境变量
tee -a /home/${OPERATOR_USER}/.bashrc <<-'EOF'
export LD_LIBRARY_PATH=/opt/oracle/${ORACLE_SUPPORT_DIR_NAME} 
export NLS_LANG=AMERICAN_AMERICA.UTF8 # 汉字显示乱码
EOF

echo "127.0.0.1 $(hostname)" | sudo tee -a /etc/hosts

sudo apt-get install libaio1
```

### iso

#### clean_system.sh

```bash
#!/bin/bash

# 清理所有软件缓存
sudo apt-get clean
# 删除系统不再使用的孤立软件
sudo apt autoremove
# 删除所有apt/lists下的文件
## 就此镜像而言,这条命令在添加后可以使镜像减小423MB
rm -rf /var/lib/apt/lists/*
# 清除构建脚本文件
rm -rf /tmp/script/*
```

#### create_user.sh

```bash
#!/bin/bash

# 创建普通用户
## 创建用户及用户组
groupadd -g ${OPERATOR_GID} ${OPERATOR_GROUP}
useradd --create-home -s /bin/bash -u ${OPERATOR_UID} -g ${OPERATOR_GID} ${OPERATOR_USER}

## 为soduers文件添加写权限
chmod u+w /etc/sudoers
## 将新生成的用户组添加到sudoers中并且设置为在执行的时候不输入密码
echo "%${OPERATOR_GROUP}          ALL=(ALL)                NOPASSWD: ALL" >> /etc/sudoers
```

#### setting_tmux.sh

```bash
#!/bin/bash

touch /home/${OPERATOR_USER}/.tmux.conf

tee /home/${OPERATOR_USER}/.tmux.conf <<-'EOF'
#将r 设置为加载配置文件,并显示提示信息
bind r source-file ~/.tmux.conf \; display "配置文件已更新"

#复制模式中的默认键盘布局设置为vi
#开启后可以鼠标选中(不要松开)+enter完成复制,前缀+]进行粘贴
setw -g mode-keys vi

#开启窗口的UTF-8支持
set-window-option -g utf8 on

#设置前缀为Ctrl + v
set -g prefix C-v

#解除Ctrl+b 与前缀的对应关系
unbind C-b

# 分割窗口开始
#弃用 前缀+" 作为横向窗口切分
unbind '"'
#启用 前缀+- 作为横向窗口切分
bind - splitw -v
#弃用 前缀+% 作为纵向窗口切分
unbind %
#启用 前缀+| 作为纵向窗口切分
bind | splitw -h
#分割窗口结束

# 设置鼠标支持(2.1版本后)
set -g mouse on

# 设置默认终端模式为 256color
set -g default-terminal "screen-256color"

# 复制到系统剪切板
#首先安装 xclip
#sudo apt-get install xclip
#bind-key -t vi-copy ‘v’ begin-selection 
#bind-key -t vi-copy y copy-pipe ‘xclip -selection clipboard >/dev/null’

EOF

```

#### sources_18.09_ali.sh

```bash
#!/bin/bash

tee /etc/apt/sources.list <<-'EOF'
deb http://mirrors.aliyun.com/ubuntu/ bionic main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ bionic-security main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-security main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ bionic-updates main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-updates main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ bionic-proposed main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-proposed main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ bionic-backports main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-backports main restricted universe multiverse

EOF
```

#### used_alias.sh

```bash
#!/bin/bash

tee -a /home/${OPERATOR_USER}/.bashrc <<-'EOF'
alias start_sshd="sudo /etc/init.d/ssh start"
# su: must be run from a terminal解决:
## /bin/bash --login command
alias init_ruby='/bin/bash --login /tmp/scripts/ruby/load_ruby.sh'
alias git_cache='git config --global credential.helper '\''cache --timeout 36000000000'\'''
alias dir_alias=echo "alias ${PWD##*/}=\"cd ${PWD}\"" | sudo tee -a /home/$USER/.bashrc;source /home/$USER/.bashrc;
EOF
```


### ruby


#### init_ruby.sh

```bash
#!/bin/bash

# 切换到普通用户并执行Ruby环境搭建等相关操作
su - ${OPERATOR_USER} <<EOF

  ## 创建项目目录
  sudo mkdir -p ${WORK_DIR}
  sudo chown -R ${OPERATOR_UID}:${OPERATOR_GID} ${WORK_DIR}

  ## ruby基本环境依赖
  sudo apt install -y curl g++ gcc autoconf automake bison libc6-dev libffi-dev libgdbm-dev libncurses5-dev libsqlite3-dev libtool libyaml-dev make pkg-config sqlite3 zlib1g-dev libgmp-dev libreadline-dev libssl-dev nodejs

  ## 使用mysql需要安装 
  #sudo apt install -y mysql-client libmysqlclient-dev 

  ## 安装RVM
  curl -sSL https://rvm.io/mpapis.asc | gpg --import -
  curl -sSL https://rvm.io/pkuczynski.asc | gpg --import -
  curl -sSL https://get.rvm.io | bash -s stable
  source ~/.rvm/scripts/rvm


  ## 安装RVM相关依赖
  rvm requirements

  ## 修改更新源
  echo "ruby_url=https://cache.ruby-china.org/pub/ruby" > ~/.rvm/user/db

  ## 安装Ruby
  rvm install ${RUBY_VERSION}

  ## 解决: You need to change your terminal emulator preferences to allow login shell.
  echo '[[ -s "/home/${OPERATOR_USER}/.rvm/scripts/rvm" ]] && . "/home/${OPERATOR_USER}/.rvm/scripts/rvm"' >>~/home/${OPERATOR_USER}/.bashrc
  source ~/.bashrc

  ## 指定Ruby默认版本
  rvm use ${RUBY_VERSION} --default

  ## 设置Gem更新源
  gem sources --add https://gems.ruby-china.com/ --remove https://rubygems.org/

  ## 安装Bundler
  ## 注意版本 已知1.16.2版本会引发: Traceback (most recent call last) 
  gem install bundler 1.16.6

EOF
```

#### load_ruby.sh

```bash
#!/bin/bash

# 加载ruby
## /bin/bash --login /tmp/scripts/ruby/load_ruby.sh
# echo '[[ -s "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm"' >> ~/.bashrc
source ~/.bashrc
rvm use ${RUBY_VERSION} --default

```

# radical(2019-05-01)

项目名称来源于变形金刚5中女主前往军事基地解救大黄蜂,面对美军的追赶,继父驾车进行阻挠,车停后女主弟弟只是表示radical然后就吐了.

在这版中实现功能和上版中几乎一致.

这一版中的主要变化有:

0. 引入了docker-composer对容器进行编排
0. 对系统基础镜像和ruby环境镜像进行拆分
0. 更新源方面可根据硬件架构,系统版本等信息选取合适更新源
0. 构建过程中输出关键信息以了解构建进度

## 目录结构


```
.
├── app
│   ├── ruby
│   │   └── Dockerfile
│   └── ubuntu-18.04
│       ├── Dockerfile
│       └── init_host_system.sh
├── bin
│   ├── iso
│   │   ├── create_alias_for_this_folder.sh
│   │   ├── create_user.sh
│   │   ├── debian
│   │   │   ├── clean_system.sh
│   │   │   ├── init_system.sh
│   │   │   └── update_system.sh
│   │   ├── edit_sources
│   │   │   ├── edit_sources.sh
│   │   │   ├── sources_list
│   │   │   │   └── ubuntu_18.04.sh
│   │   │   └── sources_ubuntu.sh
│   │   ├── setting_tmux.sh
│   │   └── used_alias.sh
│   ├── ruby
│   │   └── init_ruby.sh
├── hodor.yml
├── mysql-5.7.yml
├── README.md
└── ubuntu-18.04.yml

```

## app

### ruby

#### Dockerfile

```dockerfile
# 指定基础镜像信息
FROM ff4c00/ubuntu-18.04:production

# 编写维护者信息
LABEL \
author="mike" \
email="ff4c00@gmail.com"

# 镜像操作指令

## 设置环境变量
ENV \
### 设置普通用户信息
#### 用户和组id设置为1000,因为挂载目录时文件默认权限为1000所有
OPERATOR_USER=web OPERATOR_GROUP=web OPERATOR_UID=1000 OPERATOR_GID=1000 \ 
### 设置ruby版本
RUBY_VERSION=2.3.8 \
### 设置工作目录
WORK_DIR=/home/web \
### 项目名称
PROJECT_NAME=project

## 复制脚本文件目录
COPY bin/ /tmp/script

## 系统初始化相关操作
RUN \
bash -x /tmp/script/iso/debian/update_system.sh &&\
bash -x /tmp/script/iso/create_user.sh &&\
bash -x /tmp/script/iso/used_alias.sh &&\
bash -x /tmp/script/ruby/init_ruby.sh &&\
bash -x /tmp/script/iso/debian/clean_system.sh

## 设定进入容器后所使用用户
USER ${OPERATOR_UID}:${OPERATOR_GROUP}

## 设置进入docker以后的默认目录
WORKDIR ${WORK_DIR}

```

### ubuntu-18.04

#### Dockerfile

```dockerfile
# 此镜像为项目其他服务的基础系统镜像
# 仅用于在官方系统镜像的基础上:
#   0. 设置合适的系统更新源
#   1. 安装常用基础软件

# 指定基础镜像信息
FROM ubuntu:18.04

# 编写维护者信息
LABEL \
author="mike" \
email="ff4c00@gmail.com"

# 镜像操作指令

## 设置环境变量
ENV \
### 设置系统基本信息
LANG=en_US.UTF-8 LANGUAGE=en_US:en \
RADICAL_BIN_PATH=/tmp/script

## 复制脚本文件目录
COPY bin/ $RADICAL_BIN_PATH

## 系统初始化相关操作
RUN \
bash -x $RADICAL_BIN_PATH/iso/debian/init_system.sh &&\
bash -x $RADICAL_BIN_PATH/iso/debian/clean_system.sh
```

#### init_host_system.sh

```bash
#!/bin/bash

bash -x $RADICAL_BIN_PATH/iso/edit_sources/edit_sources.sh &&\
bash -x $RADICAL_BIN_PATH/iso/debian/update_system.sh &&\
echo '系统必要软件安装' &&\
apt-get install -fy vim sudo git openssh-server \
tmux htop net-tools apt-utils &&\
bash -x $RADICAL_BIN_PATH/iso/setting_tmux.sh

```

## bin

### iso

#### create_alias_for_this_folder.sh

```bash
#!/bin/bash

dir_name=${PWD##*/}
relevant_alias=$(cat /home/$USER/.bashrc | grep "alias $dir_name")

# 判断bash.rc里面是否已存在别名
if [ -n "$relevant_alias" ]; then
  used_alias=$(alias | grep $dir_name)
  # 判断正在使用的别名中是否存在
  if [ -n "$used_alias"]; then
    echo "已存在现目录别名,无需再次创建."
    exit 0
  fi
else
  echo "alias ${PWD##*/}=\"clear;cd ${PWD}\"" | sudo tee -a /home/$USER/.bashrc;
fi 

source /home/$USER/.bashrc;
exit 0
```

#### create_user.sh

```bash
#!/bin/bash

echo '创建用户及用户组'
groupadd -g ${OPERATOR_GID} ${OPERATOR_GROUP}
useradd --create-home -s /bin/bash -u ${OPERATOR_UID} -g ${OPERATOR_GID} ${OPERATOR_USER}
echo '为soduers文件添加写权限'
chmod u+w /etc/sudoers
echo '将新生成的用户组添加到sudoers中并且设置为在执行的时候不输入密码'
echo "%${OPERATOR_GROUP}          ALL=(ALL)                NOPASSWD: ALL" >> /etc/sudoers
```

#### debian

##### clean_system.sh

```bash
#!/bin/bash

echo '清理所有软件缓存'
sudo apt-get autoclean
sudo apt-get clean
echo '删除系统不再使用的孤立软件'
sudo apt autoremove
echo '删除所有apt/lists下的文件'
# 就此镜像而言,这条命令在添加后可以使镜像减小423MB
rm -rf /var/lib/apt/lists/*
echo '清理孤立的库文件'
sudo deborphan | xargs sudo apt-get -y remove --purge
# echo '清除构建脚本文件'
# rm -rf /tmp/script/*
```

##### init_system.sh

```bash
#!/bin/bash

bash -x $RADICAL_BIN_PATH/iso/edit_sources/edit_sources.sh &&\
bash -x $RADICAL_BIN_PATH/iso/debian/update_system.sh &&\
echo '系统必要软件安装' &&\
apt-get install -fy vim sudo git openssh-server \
tmux htop net-tools apt-utils &&\
bash -x $RADICAL_BIN_PATH/iso/setting_tmux.sh
```

##### update_system.sh

```bash
#!/bin/bash

echo '系统依赖更新'
apt-get update -y
apt-get upgrade -y
```

#### edit_sources

##### edit_sources.sh

```bash
#!/bin/bash

source $RADICAL_BIN_PATH/iso/edit_sources/sources_ubuntu.sh

# 获取系统名称
# ubuntu镜像没有安装lsb-choose_ubuntu_sources
# 如果必须使用需要:
# 0. 修改默认更新源(18.04为例),添加: deb http://security.ubuntu.com/ubuntu/ bionic-security main 
# 1. 更新系统: apt update
# 2. apt-get install -y lsb-core
# sys_name=$(lsb_release -i | awk '{print $3}')
sys_name=$(cat /etc/issue | awk '{print $1}')

case $sys_name in 
'Ubuntu')
  choose_ubuntu_sources
;;
*)
  echo '未找到相关系统更新源文件！'
  exit 1
;;
esac
```

##### sources_list

###### ubuntu_18.04.sh

```bash
#!/bin/bash

function write_sources_18.04_x86_64 () {
echo '修改ubuntu 18.04更新源为阿里源(x86_64)'
tee /etc/apt/sources.list <<-'EOF'
deb http://mirrors.aliyun.com/ubuntu/ bionic main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ bionic-security main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-security main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ bionic-updates main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-updates main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ bionic-proposed main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-proposed main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ bionic-backports main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-backports main restricted universe multiverse
EOF
}

function write_sources_18.04_armv7l () {
echo '修改ubuntu 18.04更新源为科大源(armv7l)'
tee /etc/apt/sources.list<<-'EOF'
deb http://mirrors.ustc.edu.cn/ubuntu-ports/ bionic main restricted universe multiverse
deb-src http://mirrors.ustc.edu.cn/ubuntu-ports/ bionic main restricted universe multiverse

deb http://mirrors.ustc.edu.cn/ubuntu-ports/ bionic-security main restricted universe multiverse
deb-src http://mirrors.ustc.edu.cn/ubuntu-ports/ bionic-security main restricted universe multiverse

deb http://mirrors.ustc.edu.cn/ubuntu-ports/ bionic-updates main restricted universe multiverse
deb-src http://mirrors.ustc.edu.cn/ubuntu-ports/ bionic-updates main restricted universe multiverse

deb http://mirrors.ustc.edu.cn/ubuntu-ports/ bionic-proposed main restricted universe multiverse
deb-src http://mirrors.ustc.edu.cn/ubuntu-ports/ bionic-proposed main restricted universe multiverse

deb http://mirrors.ustc.edu.cn/ubuntu-ports/ bionic-backports main restricted universe multiverse
deb-src http://mirrors.ustc.edu.cn/ubuntu-ports/ bionic-backports main restricted universe multiverse
EOF
}
```

##### sources_ubuntu.sh

```bash
#!/bin/bash

source $RADICAL_BIN_PATH/iso/edit_sources/sources_list/ubuntu_18.04.sh
# export SYS_VERSION=$(lsb_release -r | awk '{print $2}')
export SYS_VERSION=$(cat /etc/issue | awk '{print $2}' | cut -d \. -f 1,2)

# 判断系统具体版本
function judge_model_x86_64 () {
  case $SYS_VERSION in 
  '18.04')
    write_sources_18.04_x86_64
  ;;
  *)
    echo "没有找到和系统具体版本($SYS_VERSION)相匹配的更新源！"
    exit 1
  ;;
  esac
}

function judge_model_armv7l () {
  case $SYS_VERSION in 
  '18.04')
    write_sources_18.04_armv7l
  ;;
  *)
    echo "没有找到和系统具体版本($SYS_VERSION)相匹配的更新源！"
    exit 1
  ;;
  esac
}

# 判断硬件架构
function choose_ubuntu_sources () {
  hardware_platform=$(uname -i)
  case $hardware_platform in
  'x86_64')
    judge_model_x86_64
  ;;
  'armv7l')
    judge_model_armv7l
  ;;
  *)
    echo "没有找到和硬件架构($hardware_platform)相匹配的更新源！"
    exit 1
  ;;
  esac
}
```

#### setting_tmux.sh

```bash
#!/bin/bash

echo '设置tmux配置文件' 
touch /home/${OPERATOR_USER}/.tmux.conf

tee /home/${OPERATOR_USER}/.tmux.conf <<-'EOF'
#将r 设置为加载配置文件,并显示提示信息
bind r source-file ~/.tmux.conf \; display "配置文件已更新"

#复制模式中的默认键盘布局设置为vi
#开启后可以鼠标选中(不要松开)+enter完成复制,前缀+]进行粘贴
setw -g mode-keys vi

#开启窗口的UTF-8支持
set-window-option -g utf8 on

#设置前缀为Ctrl + v
set -g prefix C-v

#解除Ctrl+b 与前缀的对应关系
unbind C-b

# 分割窗口开始
#弃用 前缀+" 作为横向窗口切分
unbind '"'
#启用 前缀+- 作为横向窗口切分
bind - splitw -v
#弃用 前缀+% 作为纵向窗口切分
unbind %
#启用 前缀+| 作为纵向窗口切分
bind | splitw -h
#分割窗口结束

# 设置鼠标支持(2.1版本后)
set -g mouse on

# 设置默认终端模式为 256color
set -g default-terminal "screen-256color"

# 复制到系统剪切板
#首先安装 xclip
#sudo apt-get install xclip
#bind-key -t vi-copy ‘v’ begin-selection 
#bind-key -t vi-copy y copy-pipe ‘xclip -selection clipboard >/dev/null’

EOF
```

#### used_alias.sh

```bash
#!/bin/bash
tee -a /home/${OPERATOR_USER}/.bashrc <<-'EOF'
echo '为用户写入常用别名'
alias start_sshd="sudo /etc/init.d/ssh start"
alias git_cache='git config --global credential.helper '\''cache --timeout 36000000000'\'''
alias create_alias_for_this_folder="bash + x $RADICAL_BIN_PATH/iso/create_alias_for_this_folder.sh"
EOF
```

### ruby

#### init_ruby.sh

```bash
#!/bin/bash

echo '执行ruby初始化脚本'
echo '切换到普通用户并执行Ruby环境搭建等相关操作'
su - ${OPERATOR_USER} <<EOF

  echo '创建项目目录'
  sudo mkdir -p ${WORK_DIR}
  sudo chown -R ${OPERATOR_UID}:${OPERATOR_GID} ${WORK_DIR}

  echo 'ruby基本环境依赖'
  sudo apt install -y curl g++ gcc autoconf automake bison libc6-dev libffi-dev libgdbm-dev libncurses5-dev libsqlite3-dev libtool libyaml-dev make pkg-config sqlite3 zlib1g-dev libgmp-dev libreadline-dev libssl-dev nodejs

  echo 'Mysql数据库相关软件'
  # 这个必须要安装,不然镜像会大300多MB 
  # TODO 具体什么原因待查找
  sudo apt install -y mysql-client libmysqlclient-dev 

  echo '安装RVM'
  curl -sSL https://rvm.io/mpapis.asc | gpg --import -
  curl -sSL https://rvm.io/pkuczynski.asc | gpg --import -
  curl -sSL https://get.rvm.io | bash -s stable
  source ~/.rvm/scripts/rvm

  echo '设置RVM最大超时时间'
  echo "export rvm_max_time_flag=20" >> ~/.rvmrc
  source ~/.rvmrc


  echo '安装RVM相关依赖'
  rvm requirements

  echo '修改更新源'
  echo "ruby_url=https://cache.ruby-china.org/pub/ruby" > ~/.rvm/user/db

  echo '安装Ruby(${RUBY_VERSION})'
  rvm install ${RUBY_VERSION}

  echo '解决: You need to change your terminal emulator preferences to allow login shell.'
  echo '[[ -s "/home/${OPERATOR_USER}/.rvm/scripts/rvm" ]] && . "/home/${OPERATOR_USER}/.rvm/scripts/rvm"' >> /home/${OPERATOR_USER}/.bashrc
  source /home/${OPERATOR_USER}/.bashrc

  echo '指定Ruby默认版本(${RUBY_VERSION})'
  rvm use ${RUBY_VERSION} --default

  echo '设置Gem更新源'
  gem sources --add https://gems.ruby-china.com/ --remove https://rubygems.org/

  echo '安装Bundler'
  # 注意版本 已知1.16.2版本会引发: Traceback (most recent call last)
  # 可安装指定版本: gem install bundler 1.16.6
  gem install bundler

EOF
```

## hodor.yml

```yml
version: '3'
services:
  web:
    image: ff4c00/ruby:0.5
    build: 
      # TypeError: You must specify a directory to build in path
      ## 没有指定context
      context: .
      dockerfile: ./app/ruby/Dockerfile
    container_name: hordor
    ports:
      - '3020:3020'
    environment:
      PROJECT_NAME: 'hordor'
      RUBY_VERSION: '2.3.7'
    # command: "bash"
    stdin_open: true
    tty: true
    volumes:
      - /home/ff4c00/space/code/hodor:/home/web/hodor

```

## ubuntu-18.04.yml

```yml
version: '3'
services:
  system:
    image: ff4c00/ubuntu-18.04:production
    build: 
      context: .
      dockerfile: ./app/ubuntu-18.04/Dockerfile
```

# chernoAlpha(2019-05-20)

前面两版的核心思想主要是:以dockerfile为主,shell脚本为辅.

在本版中shell脚本为主,dockerfile为辅.dockerfile通过借助shell脚本来达到目的.

这一观点的转变主要来源于上一版本中的init_host_system.sh文件,这些原本是为docker编写的脚本同样适用于宿主机环境初始化,容器软件是变化的,不变的是shell脚本,这一观点的转变使得以后切换为其他容器时可以快速应用部署.

命名来源于环太平洋中俄罗斯一代机甲切尔诺阿尔法.


<!-- TOC -->

- [准备](#准备)
  - [安装](#安装)
  - [tmux.conf](#tmuxconf)
  - [启动](#启动)
- [使用](#使用)
  - [分屏](#分屏)
    - [横向](#横向)
    - [纵向](#纵向)
  - [全屏/退出全屏](#全屏退出全屏)
  - [复制内容](#复制内容)
    - [窗口内粘贴](#窗口内粘贴)
  - [会话](#会话)
    - [列出会话](#列出会话)
    - [退出(非关闭)](#退出非关闭)
    - [进入已有会话](#进入已有会话)
    - [删除指定session](#删除指定session)
- [参考资料](#参考资料)

<!-- /TOC -->

# 准备

## 安装
```
sudo apt-get install tmux
```

## tmux.conf

用户根目录下(~/)新建.tmux.conf隐藏文件.<br>
将tmux.conf中的内容复制进去.

## 启动

在终端下输入下面命令即可启动:

```bash
tmux
```

# 使用

> **!** 以下部分命令为根据个人喜好配置(.tmux.conf)而来,并非全部通用.

## 分屏

### 横向

```
C-v -
```

### 纵向

```
C-v |
```

## 全屏/退出全屏

```
C-v z
```

## 复制内容

### 窗口内粘贴

0. 鼠标选中(不要松开)+enter完成复制
0. `C-v ]` 进行粘贴

## 会话

### 列出会话

```bash
tmux ls
```

### 退出(非关闭)

```
tmux detach
```

快捷键: `C-v d`

### 进入已有会话

```
tmux a -t session_index
```

### 删除指定session

```
tmux kill-session -t <name-of-my-session>
```

# 参考资料

> [CSDN|终端分屏软件 tmux简单教程](https://blog.csdn.net/longxibendi/article/details/38541005)

> [小土刀 | tmux 指南](https://gitlab.com/ff4c00/hijack/blob/80230c25d59ddfe5e035d31d7aa7fc474a679538/Linux/Apps/Tmux/tmux.md#L94)

> [segmentfault | Tmux常用功能总结](https://segmentfault.com/a/1190000007427965)

<!-- TOC -->

- [快捷键](#快捷键)
  - [桌面常用快捷键](#桌面常用快捷键)
- [默认软件](#默认软件)
  - [卸载自带不常用软件](#卸载自带不常用软件)
- [参考资料](#参考资料)

<!-- /TOC -->

# 快捷键

## 桌面常用快捷键

快捷键|作用
-|-
Super+D|可以最小化所有正在运行的应用程序窗口并显示桌面
Super+A|打开应用程序菜单
Super+Tab <br> Alt+Tab|在应用程序之间切换
Super+Shift+Tab|逆向在应用程序之间切换
Super+M <br> Super+V|打开通知栏
Ctrl+Alt+上箭头 <br> Ctrl+Alt+下箭头|在工作区之间切换
Ctrl+Alt+Del|打开注销菜单
自定义快捷键|"设置->设备->键盘",向下滚动到底部,"自定义快捷方式"选项

# 默认软件

## 卸载自带不常用软件

```
sudo apt-get purge -y libreoffice? unity-webapps-common thunderbird totem rhythmbox empathy brasero simple-scan gnome-mahjongg aisleriot gnome-mines cheese gnome-sudoku transmission-common gnome-orca webbrowser-app landscape-client-ui-install deja-dup;sudo apt autoremove -y
```


# 参考资料

> [Linux公社 | Linux下彻底卸载LibreOffice方法](https://www.linuxidc.com/Linux/2013-02/79239.htm)

> [云梦小站 | Ubuntu 16.04 LTS 删除不常用的软件](https://www.htcp.net/2568.html)
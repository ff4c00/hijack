> Ubuntu MATE是仅有的原生支持树莓派且包含一个完整的桌面环境的发行版.

<!-- TOC -->

- [安装](#安装)
  - [官网下载镜像](#官网下载镜像)
  - [制作镜像](#制作镜像)
    - [工具选择](#工具选择)
    - [镜像解压](#镜像解压)
    - [开始制作](#开始制作)
  - [设置树莓派](#设置树莓派)
- [参考资料](#参考资料)

<!-- /TOC -->

# 安装

## 官网下载镜像

在[官网](https://ubuntu-mate.org/download/)下载对应版本镜像.

## 制作镜像

### 工具选择

如果没有合适的工具,可以选择[Balena Etcher](https://www.balena.io/etcher/)将镜像写入SD卡.

### 镜像解压

```
xz -d ubuntu-mate***.img.xz
```

### 开始制作

在制作工具中选择镜像文件和SD卡开始制作.

## 设置树莓派

插入电源线给它供电,剩下的按照指导一步步走就好了,<br>
这里不再赘述,如果不太清楚可以查看下面给出的参考链接.



# 参考资料

> [知乎 Linux中国 | 在树莓派上安装 Ubuntu MATE](https://zhuanlan.zhihu.com/p/64681595)
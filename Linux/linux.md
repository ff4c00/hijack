<!-- TOC -->

- [目录文件类](#目录文件类)
  - [tree 打印树结构目录](#tree-打印树结构目录)
  - [chmod](#chmod)
  - [chown](#chown)
  - [链接](#链接)
    - [硬链接](#硬链接)
      - [特点](#特点)
    - [软链接](#软链接)
      - [特点](#特点-1)
  - [sed](#sed)
    - [常用选项](#常用选项)
    - [命令示例](#命令示例)
      - [命令类别](#命令类别)
      - [追加行(a \\)](#追加行a-\\)
          - [通过行号追加](#通过行号追加)
          - [通过正则追加](#通过正则追加)
    - [使用sed进行日志分析](#使用sed进行日志分析)
  - [EOF写入文件](#eof写入文件)
    - [delimited by end-of-file (wanted `EOF')](#delimited-by-end-of-file-wanted-eof)
  - [文件切割](#文件切割)
    - [split](#split)
- [用户类](#用户类)
  - [添加](#添加)
    - [添加用户](#添加用户)
    - [新用户设置密码](#新用户设置密码)
  - [修改](#修改)
    - [修改用户名](#修改用户名)
    - [加入用户组](#加入用户组)
    - [修改用户目录](#修改用户目录)
  - [删除](#删除)
- [文件系统类](#文件系统类)
  - [tar 打包命令](#tar-打包命令)
    - [打包](#打包)
    - [解压](#解压)
  - [rsync-数据镜像备份](#rsync-数据镜像备份)
  - [pigz](#pigz)
- [软件包管理类](#软件包管理类)
  - [snap](#snap)
    - [安装](#安装)
    - [常用命令](#常用命令)
    - [常用软件](#常用软件)
    - [Docker和Snap之间的主要区别是什么](#docker和snap之间的主要区别是什么)
  - [更新源](#更新源)
    - [ubuntu-ports和ubuntu区别](#ubuntu-ports和ubuntu区别)
  - [apt](#apt)
    - [更新软件](#更新软件)
    - [删除软件包](#删除软件包)
    - [删除ppa](#删除ppa)
      - [删除源](#删除源)
      - [/etc/apt/sources.list.d](#etcaptsourceslistd)
  - [dpkg](#dpkg)
    - [解决依赖问题](#解决依赖问题)
- [系统管理类](#系统管理类)
  - [locale](#locale)
    - [设置语言环境](#设置语言环境)
    - [设置示例](#设置示例)
    - [locale: Cannot set LC_CTYPE to default locale: No such file or directory](#locale-cannot-set-lc_ctype-to-default-locale-no-such-file-or-directory)
    - [locale-gen: command not found](#locale-gen-command-not-found)
  - [top](#top)
  - [alias 创建别名](#alias-创建别名)
  - [echo](#echo)
  - [终端](#终端)
    - [常用快捷键](#常用快捷键)
  - [uname](#uname)
  - [df](#df)
  - [tzdata](#tzdata)
  - [设置虚拟内存](#设置虚拟内存)
    - [查看虚拟内存大小](#查看虚拟内存大小)
    - [交换文件](#交换文件)
      - [创建/root/swapfile文件](#创建rootswapfile文件)
      - [创建交换文件](#创建交换文件)
      - [格式化交换文件](#格式化交换文件)
      - [启用交换文件](#启用交换文件)
      - [检查虚拟内存](#检查虚拟内存)
      - [开机自动加载虚拟内存](#开机自动加载虚拟内存)
      - [停用交换文件](#停用交换文件)
  - [环境变量文件](#环境变量文件)
  - [定时任务](#定时任务)
    - [cron](#cron)
      - [安装](#安装-1)
      - [常用命令](#常用命令-1)
      - [crontab](#crontab)
        - [常用命令](#常用命令-2)
        - [crontab文件](#crontab文件)
- [进程管理类](#进程管理类)
  - [sleep 延迟指定时间](#sleep-延迟指定时间)
  - [ps](#ps)
    - [查看LINUX进程内存占用情况](#查看linux进程内存占用情况)
- [网络管理类](#网络管理类)
  - [127.0.0.1和0.0.0.0地址](#127001和0000地址)
  - [pingtop](#pingtop)
    - [安装](#安装-2)
    - [使用](#使用)
  - [Netplan](#netplan)
    - [备份原配置文件](#备份原配置文件)
    - [确定网络设备名称](#确定网络设备名称)
    - [确定网关地址](#确定网关地址)
    - [确定DNS地址](#确定dns地址)
    - [配置静态IP地址](#配置静态ip地址)
    - [测试配置](#测试配置)
  - [重启网络](#重启网络)
- [内核管理类](#内核管理类)
- [其他](#其他)
  - [传递自定义参数](#传递自定义参数)
  - [特殊符号](#特殊符号)
  - [拆分变量](#拆分变量)
  - [字符串大小写转换表达式](#字符串大小写转换表达式)
- [参考资料](#参考资料)

<!-- /TOC -->

# 目录文件类

## tree 打印树结构目录

参数|含义
-|-
-a|打印包含隐藏文件在内的所有文件
-L n| 打印n层目录
-N|原样输出(字符按原样而不是转义八进制数字)

## chmod

> 修改文件、文件夹权限.

```
chmod 参数 XXX(权限) 文件/文件夹
```

> 参数

参数|作用
-|-
-R|修改文件夹及子文件夹权限

> 权限

XXX分别代表文件所有者、所有者所在群组以及其他用户的相关权限.

权限用数字表示,角色拥有权限即为读、写、执行三项权限值之和.

权限|权限值|说明
-|-|-
r(Read)|4|读取,对文件而言,具有读取文件内容的权限.<br>对目录来说,具有浏览目 录的权限.
w(Write)|2|写入,对文件而言,具有新增、修改文件内容的权限.<br>对目录来说,具有删除、移动目录内文件的权限.
x(Execute)|1|执行,对文件而言,具有执行文件的权限.<br>对目录了来说该用户具有进入目录的权限.

例如将foo及其子文件等设置为所有用户均可读、写、执行

```
chmod -R 777 foo/
```

## chown

> 更改所属用户

```
chown [-R] 账号名称:用户组名称 文件或目录
```

## 链接

Linux链接分两种,一种被称为硬链接(Hard Link),另一种被称为符号链接(Symbolic Link).默认情况下,ln命令产生硬链接.

### 硬链接

硬链接实际上是为文件建一个别名,链接文件和原文件实际上是同一个文件.

#### 特点

1. 它会在链接文件处创建一个和被链接文件一样大小的文件,类似于国外网站和国内镜像的关系.
2. 硬链接占用的空间和被链接文件一样大(其实就是同一片空间)
3. 修改链接文件和被链接文件中的其中一个,另外一个随之同样发生变化
4. 硬链接的对象不能是目录,也就是说被链接文件不能为目录
5. 硬链接的两个文件是独立的两个引用计数文件,他们共用同一份数据,所以他们- 的inode节点相同.
6. 删除硬链接中的任意一个文件,另外一个文件不会被删除.没有任何影响,链接文件一样可以访问,内容和被链接文件一模一样.

### 软链接

通过软链接建立的链接文件与原文件并不是同一个文件,相当于原文件的快捷方式.

#### 特点

0. 软连接的链接文件就是一个基本单元大小的文件,一般为3B,和被链接文件的大小没有关系.
0. 软链接的链接文件中存储的是被链接文件的元信息,路径或者inode节点.
0. 软连接的连接文件是一个独立的文件,有自己的元信息和inode节点.
0. 删除软链接的链接文件,被链接文件不会受到任何影响.
0. 删除软链接的被链接文件,链接文件会变成红色,这时打开链接文件会报错,报找不到被链接的文件这种错误.
0. 软链接可以链接任何类型的文件,包括目录和设备文件都可以作为被链接的对象.

## sed

> sed是stream editor的简称,也就是流编辑器.<br>它一次处理一行内容,处理时,把当前处理的行存储在 *临时缓冲区中*,称为 *模式空间*(pattern space),<br>接着用sed命令 *处理缓冲区中的内容*,处理完成后,把缓冲区的内容送往屏幕.


```
sed [选项] '命令' 被操作文件
```

### 常用选项

选项|作用
-|-
n|安静模式,只有经过sed特殊处理的那一行(或者动作)才会被列出来.<br>一般所有来自stdin的内容一般都会被列出到屏幕上.
e|直接在指令列模式上进行 sed 的动作编辑.
f|直接将sed的动作写在一个文件内,<br> `-f filename` 则可以执行filename内的sed命令.
r|让sed命令支持扩展的正则表达式(默认是基础正则表达式).
i|直接修改读取的文件内容,而不是由屏幕输出.

### 命令示例

本地创建test.txt文件,并写入下面内容:

```
this is first line
this is second line
this is third line
this is fourth line
this fifth line
happy everyday
end
```
#### 命令类别

命令|区别
-|-
匹配行a \s |在匹配行 *下方* **追加** 指定内容s
匹配行i \s |在匹配行 *上方* **追加** 指定内容s
匹配行c \s |将匹配行内容 **替换** 为指定内容s<br>***整行内容进行替换***
匹配行d|将匹配行进行 **删除** <br>因为是删除所以无需指定内容
匹配行p|将匹配行进行打印输出<br>p命令一般和-n选项一起使用
s/匹配文本/需替换为内容/g|在 *全局范围* 内将所有 `匹配文本` 全部替换为 `需替换为内容`<br> 最后的g是global的意思,也就是全局替换,<br> 如果不加g,则只会替换每行的第一个匹配内容

这几个命令之间有一个共同点就是在内容匹配方面,都有三种匹配方式:

0. 具体行号匹配
0. 行号范围匹配
0. 正则匹配

以命令a进行示例:

#### 追加行(a \\)

a \:追加行(append), <br>a \的后面跟上字符串s(多行字符串可以用\n分隔),<br>则会在当前选择的行的后面都加上字符串s

###### 通过行号追加

```
sed '行号a \追加指定内容' 被操作文件
```

```
sed '1a \add one' test.txt
#=>
this is first line
add one
this is second line
this is third line
this is fourth line
this fifth line
happy everyday
end

```

**这里的行号既可以是具体的数值,<br>也可以表达特定的范围.**

如 `1,3`(第一行到第三行), `2,$`(第二行到最后一行中间所有的行)

###### 通过正则追加

```
sed '/正则表达式/a \追加内容' 被操作文件

```

### 使用sed进行日志分析

需要分析的日志,主要分为:

0. 特定时间范围内的日志
0. 包含具体关键字的日志

```
sed -n '/21\/Mar\/2019:13:24/,/21\/Mar\/2019:13:34/p' access.log >> c.log
```

## EOF写入文件

### delimited by end-of-file (wanted `EOF')

> 原因在于结尾的EOF存在多余空格

## 文件切割

### split

```
split -d -b 切割大小  切割文件  存储目录/文件名
split -d -b 1m  development.log  /home/ff4c00/test.log
```

# 用户类

## 添加

### 添加用户


```
useradd user_name
```

### 新用户设置密码

```
passwd user_name
```

## 修改


### 修改用户名

```
usermod -l new_user_name old_user_name
```

### 加入用户组

```
# 将用户 user1 加入到 users组中
usermod -g users user1
```

### 修改用户目录

```
# 将用户 user1 目录改为/users/us1
usermod -d /users/us1 user1
usermod -d /users/web_server_1 web_server_1
```

## 删除

```
userdel user_name
# -r 同时删除工作目录
```

# 文件系统类

## tar 打包命令
> 必选参数

***必选参数是独立的命令,压缩解压都要用到其中一个,可以和别的命令连用,但一次只能用其中一个.***

参数|含义
-|-
c|建立一个压缩文件的参数指令(create 的意思)
x|解开一个压缩文件的参数指令
t|查看 tarfile 里面的文件
r|向压缩归档文件末尾追加文件
u|更新原压缩包中的文件

>可选参数

下面的参数是根据需要在压缩或解压档案时可选的.

参数|含义
-|-
z|有gzip属性,即需要用 gzip 压缩
j|有bz2属性,即需要用 bzip2 压缩
Z|有compress属性的
v|压缩的过程中显示文件(显示所有过程)！这个常用,但不建议用在背景执行过程！
O|将文件解开到标准输出
f|使用档名,请留意,在f之后要立即接档名!不要再加参数!<br>例如:使用『 tar -zcvfP tfile sfile』就是错误的写法,要写成『 tar -zcvPf tfile sfile』才对喔！
p|使用原文件的原来属性(属性不会依据使用者而变)
P|可以使用绝对路径来压缩！
N|比后面接的日期(yyyy/mm/dd)还要新的才会被打包进新建的文件中<br>--exclude FILE:在压缩的过程中,不要将 FILE 打包！
C|将文件解压缩到指定文件夹

### 打包

```
tar -czf file_name.tar.gz file_name/*
```

### 解压

```
# 将文件解压到~/test目录下
tar -xf $target_file_path -C ~/test
```

## rsync-数据镜像备份
rsync命令是一个远程数据同步工具,可通过LAN/WAN快速同步多台主机间的文件.
rsync使用"rsync算法"来使本地和远程两个主机之间的文件达到同步,这个算法只传送两个文件的不同部分,而不是每次都整份传送,因此速度相当快.
rsync是一个功能非常强大的工具,其命令也有很多功能特色选项.

> [Linux命令大全-rsync命令](http://man.linuxde.net/rsync)

## pigz
pigz是支持并行压缩的gzip,pigz默认用当前逻辑cpu个数来并发压缩,无法检测个数的话,则默认并发8个线程,
也可以使用-p指定线程数.需要注意的是其CPU使用比较高.

# 软件包管理类

## snap

> Snap是Ubuntu母公司Canonical于2016年4月发布Ubuntu16.04时候引入的一种安全的、易于管理的、沙盒化的软件包格式,与传统的dpkg/apt有着很大的区别.<br>


### 安装

```
sudo apt install -y snapd snapcraft
```

### 常用命令

命令|作用
-|-
sudo snap list|列出已经安装的snap包
sudo snap find \<text to search>|搜索要安装的snap包
sudo snap install \<snap name>|安装一个snap包
sudo snap refresh \<snap name>|更新一个snap包,如果你后面不加包的名字的话那就是更新所有的snap包
sudo snap revert \<snap name>|把一个包还原到以前安装的版本
sudo snap remove \<snap name>|删除一个snap包

### 常用软件

名称|作用
-|-

### Docker和Snap之间的主要区别是什么

Docker的特点:

0. 不同类型的容器看起来相同,但用于不同目的.
0. 容器在内核级别并不存在.
0. 我们可以独立地创建关于容器看到的用户,网络,磁盘和进程的虚拟.
0. 不同类型的容器实际上是关于创建的不同类型的虚拟.

Snaps的特点:

0. 不可变,但仍然是基础系统的一部分.
在网络方面集成,因此共享系统IP地址,与Docker不同,每个容器都有自己的IP地址.
0. 快照不会污染系统的其余部分.它在自己的盒子里.但它仍然可以看到(只读)系统的其余部分,这使它能够与系统进行通信和集成.

Docker不能运行桌面应用程序,但这是快照可以做的.<br>
第三方可以使用快照发送桌面应用程序,用户可以轻松地安装和更新它们.<br>
Docker容器不能(轻松地)在屏幕上以图形方式与用户交互,从用户的主目录加载文档,或通过用户的网络摄像头提供视频会议,快照可以(一旦获得许可).

## 更新源

### ubuntu-ports和ubuntu区别

ubuntu-ports和ubuntu是指源地址中的最后层级地址:

```
http://mirrors.aliyun.com/ubuntu-ports/
http://mirrors.aliyun.com/ubuntu/
```

源地址|收录架构|收录版本|
-|-|-
ubuntu-ports|arm64<br>armhf<br>PowerPC<br>ppc64el<br>s390x|所有 Ubuntu 当前对该架构支持的版本,包括开发版
ubuntu|arm64<br>Intel x86|所有 Ubuntu 当前支持的版本,包括开发版

## apt

### 更新软件

```
sudo apt-get update;sudo apt-get upgrade;sudo apt-get dist-upgrade
```

### 删除软件包

参数|区别
-|-
remove|会删除软件包,但会保留配置文件
purge|会将软件包以及配置文件都删除

```
sudo apt-get remove package-name
sudo apt-get purge package-name
```

### 删除ppa

#### 删除源

```bash
# user/ppa-name具体如何查询有待考证
#  http://ppa.launchpad.net/wine/wine-builds/ubuntu
# 之前是通过wine/wine-builds推断出来的
sudo add-apt-repository -r ppa:user/ppa-name
```

#### /etc/apt/sources.list.d

删除相关内容

## dpkg

### 解决依赖问题

```
sudo apt-get -f -y install
sudo dpkg -i XXX.deb
```

# 系统管理类

## locale

> 用于管理系统安装的字符编码.

### 设置语言环境

```
export LC_ALL=zh_CN.UTF-8
```

### 设置示例

```
LC_COLLATE #定义该环境的排序和比较规则
LC_CTYPE #用于字符分类和字符串处理,控制所有字符的处理方式,包括字符编码,字符是单字节还是多字节,如何打印等.<br>
是最重要的一个环境变量.<br>

LC_MONETARY # 货币格式

LC_NUMERIC # 非货币的数字显示格式

LC_TIME # 时间和日期格式

LC_MESSAGES # 提示信息的语言.<br>


# 另外还有一个LANGUAGE参数,它与LC_MESSAGES相似,但如果该参数一旦设置,则LC_MESSAGES参数就会失效.<br>
 LANGUAGE参数可同时设置多种语言信息,如LANGUAGE="zh_CN.GB18030:zh_CN.GB2312:zh_CN".<br>


LANG # LC_*的默认值,是最低级别的设置,如果LC_*没有设置,则使用该值.<br>
类似于 LC_ALL

LC_ALL # 它是一个宏,如果该值设置了,则该值会覆盖所有LC_*的设置值.<br>
注意,LANG的值不受该宏影响
```

### locale: Cannot set LC_CTYPE to default locale: No such file or directory

> 本地没有相关语言包.

```
sudo locale-gen zh_CN.UTF-8
```


### locale-gen: command not found

```
sudo apt-get clean && sudo apt-get update && sudo apt-get install -y locales
```

## top

操作项|作用
-|-
Ctrl+E|切换内存展示单位

## alias 创建别名

> 用于为命令设置别名,生命周期在本次关机前有效.<br>
如果需要永久生效可以加入当前用户的.brachrc文件中.

```
# alias 别名='命令'
alias elasticsearch_start='~/space/software/elasticsearch-5.6.10/bin/elasticsearch -d'
```

## echo

常用参数|含义
-|-
-n|输出末尾禁止换行
-e|启用反斜杠转义
-E|禁用反斜杠转义(默认)


## 终端

### 常用快捷键

快捷键|含义
-|-
C-c|终端进程或命令
C-z|将当前运行程序送到后台执行
C-a|跳转到行首
C-e|跳转到行尾
C-u|清除光标位置到行首内容
C-k|清除光标位置到行尾内容
C-w|清除光标位置到词首内容
C-y|撤销上次清除内容
C-p|查看上一个执行命令
C-n|查看下一个执行命令
C-r|搜索历史命令
C-l|清屏

## uname

> uname用于显示电脑以及操作系统的相关信息.

参数|作用
-|-
-a|显示全部的信息
-m|显示电脑类型
-n|显示在网络上的主机名称
-r|显示操作系统的发行编号
-s|显示操作系统名称
-v|显示操作系统的版本
-p|输出处理器类型
-i|输出硬件平台
-o|输出操作系统名称

## df

> 用于报告剩余空闲磁盘空间.

参数|含义
-|-
h|以人类友好格式显示文件系统硬盘空间使用情况(默认使用1K大小的块为单位来表示使用情况).
m|以MB为单位来显示文件系统磁盘空间使用情况.
i|列出节点而不是块的使用情况.
T|显示文件系统类型.
t|仅显示指定类型的文件系统.<br>只列出ext4文件系统:<br>df -t ext4
x|从结果中去排除指定类型的文件系统.(示例参考`-t`)
hT|显示某个目录的硬盘空间使用情况以及它的挂载点.

## tzdata

> 设置系统时区

普通流程:

```
export DEBIAN_FRONTEND=noninteractive
sudo apt-get install -y tzdata
# 采用上海时区: 6,70

# 建立期望的时区链接(上海)
sudo ln -fs /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

# 重新配置tzdata软件包,使得时区设置生效
sudo dpkg-reconfigure -f noninteractive tzdata

# -----
sudo apt update &&\
sudo ln -fs /usr/share/zoneinfo/Asia/Shanghai /etc/localtime &&\
sudo apt-get install -y tzdata
```

## 设置虚拟内存

> Error occurred during initialization of VM<br>
Could not reserve enough space for 2097152KB object heap

原因在于虚拟内存太小.

### 查看虚拟内存大小

```
free -m
# =>
       total used free shared buff/cache available
Mem:     923  157    8      4        758       747
Swap:      0   0     0
```

<hr>

Linux系统实现虚拟内存有两种方法:

0. 交换分区(swap分区)
0. 交换文件

### 交换文件

#### 创建/root/swapfile文件

```
sudo touch /root/swapfile
```

#### 创建交换文件

```
# if表示input_file输入文件
# of表示output_file输出文件
# bsbs表示block_size块大小,单位为百kb
# count表示计数

# 采用数据块大小为1M,数据块数目为2048,这样分配的空间就是2G大小:
sudo dd if=/dev/zero of=/root/swapfile bs=1M count=2048
```

#### 格式化交换文件

```
sudo mkswap /root/swapfile
```


#### 启用交换文件

```
sudo swapon /root/swapfile
```

#### 检查虚拟内存

```
free -m
# =>
      total used free shared buff/cache available
Mem:    923  133    9      4        781       771
Swap:  2047    0 2047

```

#### 开机自动加载虚拟内存

在/etc/fstab文件中加入如下命令:

```
sudo tee -a /etc/fstab <<-'EOF'
/root/swapfile swap swap defaults 0 0
EOF
```

重启后生效reboot

#### 停用交换文件

0. 先删除/etc/fstab文件中添加的交换文件行
0. 停用交换文件: swapoff /root/swapfile
0. 删除交换文件: rm -fr /root/swapfile

## 环境变量文件

文件名|作用
-|-
/etc/profile|此文件为系统的每个用户设置环境信息,当用户第一次登录时,该文件被执行.<br>并从/etc/profile.d目录的配置文件中搜集shell的设置.
/etc/bashrc|为每一个运行bash shell的用户执行此文件.当bash shell被打开时,该文件被读取.
~/.bash_profile|每个用户都可使用该文件输入专用于自己使用的shell信息,当用户登录时,该文件仅仅执行一次!默认情况下,他设置一些环境变量,执行用户的.bashrc文件.
~/.bashrc|该文件包含专用于你的bash shell的bash信息,当登录时以及每次打开新的shell时,该
该文件被读取.

## 定时任务

### cron

> cron是一个Linux定时执行工具,可以在无需人工干预的情况下运行作业.

#### 安装

```
sudo apt-get install cron
```

#### 常用命令

命令|作用
-|-
service cron start|启动服务
service cron stop|关闭服务
service cron restart|重启服务
service cron reload|重新载入配置
service cron status|检查状态
pgrep cron|查看是否运行

#### crontab

> crontab 命令用于安装、删除或者列出用于驱动cron后台进程的表格<br>
也就是说,用户把需要执行的命令序列放到crontab文件中以获得执行,<br>
每个用户都可以有自己的crontab文件.

##### 常用命令

命令|作用
-|-
crontab -u|设定某个用户的cron服务<br>如果不使用 -u user 的话,就是表示设定自己的时程表.
crontab -l|列出某个用户cron服务的详细内容
crontab -r|删除某个用户的cron服务
crontab -e|编辑某个用户的cron服务

##### crontab文件

基本格式:
```
M H D m d cmd
```

标题|含义
-|-
M|分钟(0-59)
H|小时(0-23)
D|天(1-31)
m|月(1-12)
d|一星期内的天(0~6,0为星期天)
cmd|要运行的程序,程序被送入sh执行,<br>这个shell只有USER,HOME,SHELL这三个环境变量


# 进程管理类

## sleep 延迟指定时间

sleep 参数n(可以为小数)

参数|说明
-|-
n|延迟n秒
nm|延迟n分钟
nh|延迟n小时
nd|延迟n天

## ps

### 查看LINUX进程内存占用情况

```
# 实现按内存排序,由大到小
ps -e -o 'pid,comm,args,pcpu,rsz,vsz,stime,user,uid' | grep oracle |  sort -nrk5
```

&emsp;|&emsp;
-|-
rsz|实际内存
vsz|进程的虚拟大小


# 网络管理类

## 127.0.0.1和0.0.0.0地址

地址|作用
-|-
127.0.0.1|回环地址<br>该地址指电脑本身地址<br>其他主机通过该地址无法访问到目标服务器
0.0.0.0|在IP数据报中只能用作源IP地址<br>在服务器中,0.0.0.0指的是本机上的所有IPV4地址<br>如果一个主机有两个IP地址,192.168.1.1 和 10.1.2.1<br>并且该主机上的一个服务监听的地址是0.0.0.0<br>那么通过两个ip地址都能够访问该服务

## pingtop

> 顾名思义,它会一次ping多台服务器,并在类似top的终端UI中显示结果.

### 安装

```
# 安装pip
sudo apt install -y python-pip
# 使用pip安装pingtop
pip install pingtop
```

### 使用

```
pingtop google.com baidu.com bing.com
```

## Netplan

> Netplan 是一款使用在终端的配置网络工具.

### 备份原配置文件

```
sudo cp  /etc/netplan/01-network-manager-all.yaml /etc/netplan/01-network-manager-all.yaml.bak
```

### 确定网络设备名称

```
ip a
```

### 确定网关地址

```
ip route show
```

### 确定DNS地址

```
cat /etc/resolv.conf | grep 'nameserver'
```

### 配置静态IP地址

```yaml
network:
  version: 2
  renderer: networkd
  ethernets:
    DEVICE_NAME: # 需要配置设备的实际名称
      dhcp4: yes/no # 是否启用 dhcp4
      addresses: [IP/NETMASK] # IP地址/掩码
      gateway4: GATEWAY # 网关的地址
      nameservers:
        Addresses: [NAMESERVER, NAMESERVER] # 由逗号分开的 DNS 服务器列表
```

### 测试配置

```
sudo netplan try
```

如果确信配置文件没有问题,可以跳过测试环节并且直接使用新的配置:

```
sudo netplan apply
```

可以使用 ip a 看看新的地址是否正确.

## 重启网络

```
sudo service networking restart
```

# 内核管理类

# 其他

## 传递自定义参数

```
rails s -p3000
alias ras='rails s -p$1'
```

## 特殊符号
符号|说明|示例
-|-|-
&|任务在后台执行
&&|前一条命令执行成功时,才执行后一条命令|bin/elasticsearch -d && curl localhost:9200
;|不管前一句是否执行成功均会执行后一句
\|\||上一条命令执行失败后,才执行下一条命令
()|括号里面的内容作为一个整体执行<br>一条命令独占一个物理行或用;号隔开
\||表示管道,上一条命令的输出,作为下一条命令参数

## 拆分变量

```
cd /var/log/squid
echo ${PWD##*/}
squid
```

## 字符串大小写转换表达式

```
var=toronto
echo "${var^}"
# => Toronto
echo "${var^[n-z]}"
# => Toronto
echo "${var^^[a-m]}"
# => toronto
echo "${var^^[n-q]}"
# => tOrONtO
echo "${var^^}"
# => TORONTO
```

# 参考资料

> [CSDN | linux 下获得当前目录,上级目录,文件夹名](https://blog.csdn.net/blog_lunatic/article/details/39339581)

> [Bash 4.0 新增的字符串大小写转换表达式](https://www.zybuluo.com/haokuixi/note/77707)

> [Linux中国 | sed 命令详解](https://mp.weixin.qq.com/s?__biz=MjM5NjQ4MjYwMQ==&mid=400341569&idx=3&sn=8febe5e13149dd90f890441a998e136d&mpshare=1&scene=1&srcid=0115ZifbikoWRJptuy8proKL&pass_ticket=IIhQlu%2F%2BGFc2ieUh8wpWDDo2p8dyPmno%2BrdVZWBVQGyuHosZHBALnWZPs5KVEEVz#rd)

> [CSDN | 127.0.0.1和0.0.0.0地址的区别](https://blog.csdn.net/ythunder/article/details/61931080)

> [Linux 公社 | Ubuntu报"xxx is not in the sudoers file.This incident will be reported" 错误解决方法](https://www.linuxidc.com/Linux/2016-07/133066.htm)

> [OSCHINA | EOF 后面的空格](https://my.oschina.net/huayd/blog/137214)

> [Linux 公社 | Ubuntu报"xxx is not in the sudoers file.This incident will be reported" 错误解决方法](https://www.linuxidc.com/Linux/2016-07/133066.htm)

> [Linux中国 | ping 多台服务器并在类似 top 的界面中显示](https://zhuanlan.zhihu.com/p/64915287)

> [CSDN | 在自动化运维中设置apt-get install tzdata的noninteractive方法](https://blog.csdn.net/taiyangdao/article/details/80512997)

> [CSDN | linux ps top 命令 VSZ,RSS,TTY,STAT, VIRT,RES,SHR,DATA的含义](https://blog.csdn.net/zjc156m/article/details/38920321)

> [博客园 | 查看LINUX进程内存占用情况](https://www.cnblogs.com/gaojun/p/3406096.html)

> [简书 | Linux设置虚拟内存](https://www.jianshu.com/p/fae46241ba0c)

> [博客园 | .bash_profile和.bashrc的区别(如何设置生效)](https://www.cnblogs.com/persist/p/5197561.html)

> [云网牛站 | 在Ubuntu 18.04系统中使用Netplan工具配置网络](https://ywnz.com/linuxjc/3280.html)

> [stackoverflow | docker ubuntu /bin/sh: 1: locale-gen: not found](https://stackoverflow.com/questions/39760663/docker-ubuntu-bin-sh-1-locale-gen-not-found)

> [博客园 | Ubuntu下安装Snap](https://www.cnblogs.com/DragonStart/p/10369043.html)

> [Linux公社 | Ubuntu中snap包的安装,删除,更新使用入门教程](https://www.linuxidc.com/Linux/2018-05/152385.htm)

> [智库101 | Docker和Snap之间的主要区别是什么？](http://www.kbase101.com/question/10814.html)

<!-- TOC -->

- [可选择系统](#可选择系统)
  - [Snappy Ubuntu Core](#snappy-ubuntu-core)
- [常见问题](#常见问题)
  - [NoDistroTemplateException: Error: could not find a distribution template for Raspbian/stretch](#nodistrotemplateexception-error-could-not-find-a-distribution-template-for-raspbianstretch)
- [参考资料](#参考资料)

<!-- /TOC -->

# 可选择系统

## Snappy Ubuntu Core

Snappy Ubuntu Core是Ubuntu的新版本,<br>
一个最小的服务器映像,具有与Ubuntu相同的库,但应用程序是通过更简单的机制提供.<br>
可用于Raspberry Pi 2和3.

默认的用户名和密码都是ubuntu.

# 常见问题

## NoDistroTemplateException: Error: could not find a distribution template for Raspbian/stretch

不要添加任何Debian/Ubuntu存储库或软件, 在Pi上自行编译.

# 参考资料

> [raspberrypi | Unable to add repository](https://www.raspberrypi.org/forums/viewtopic.php?t=64734)

> [Ubuntu | Snappy Ubuntu Core](https://wiki.ubuntu.com/ARM/RaspberryPi)
<!-- TOC -->

- [引脚编号](#引脚编号)
- [wiringPi](#wiringpi)
  - [安装](#安装)
  - [引脚供电](#引脚供电)
- [样品](#样品)
  - [继电器控制LED灯](#继电器控制led灯)
- [参考资料](#参考资料)

<!-- /TOC -->

# 引脚编号

![](https://img-blog.csdn.net/20171207093720535?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvcXFfMzg4ODAzODA=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

# wiringPi

> wiringPi是一个树莓派IO控制库,使用C语言开发.

## 安装

```bash
# git clone https://github.com/WiringPi/WiringPi.git
git clone git://git.drogon.net/wiringPi
cd wiringPi && ./build
```

## 引脚供电

> 通过GPIO接口供电树莓派的GPIO接口也可以接受直流电的输入,<br>但是GPIO供电没有保险丝,当电压或电流过高容易将板子直接烧毁.

将5v电流的正极接针脚2,负极接针脚6.

![](https://imgsa.baidu.com/exp/w=480/sign=9598eeb4e2dde711e7d242fe97eecef4/32fa828ba61ea8d3bde52ab5900a304e241f58bf.jpg)


# 样品

## 继电器控制LED灯

# 参考资料

> [CSDN | 树莓派GPIO控制](https://blog.csdn.net/chentuo2000/article/details/81051645)

> [知乎 | 树莓派的GPIO控制](https://zhuanlan.zhihu.com/p/40594358)

> [微雪 | 树莓派引脚对照表](http://www.waveshare.net/study/article-789-1.html)

> [CSDN | 树莓派学习笔记——GPIO功能学习](https://blog.csdn.net/xukai871105/article/details/12684617)

> [Gordons Projects | Functions (API)](https://projects.drogon.net/raspberry-pi/wiringpi/functions/)

> [百度经验 | 树莓派的三种供电方式](https://jingyan.baidu.com/article/414eccf64b92ba6b421f0a67.html)

<!-- TOC -->

- [类型、值和变量](#类型值和变量)
  - [数字](#数字)
    - [算术运算符](#算术运算符)
    - [日期和时间](#日期和时间)
    - [文本](#文本)
    - [匹配模式](#匹配模式)
    - [null和undefined](#null和undefined)
    - [](#)
    - [](#-1)
    - [](#-2)
    - [](#-3)
    - [](#-4)
    - [](#-5)
    - [](#-6)
    - [](#-7)
  - [](#-8)
  - [](#-9)
  - [](#-10)
  - [](#-11)
  - [](#-12)
  - [](#-13)
  - [](#-14)
  - [](#-15)
  - [](#-16)
  - [](#-17)
  - [](#-18)
- [语句](#语句)
  - [条件语句](#条件语句)
    - [if](#if)
    - [switch](#switch)
  - [循环](#循环)
    - [while](#while)
    - [for](#for)
      - [for/in](#forin)
  - [跳转](#跳转)
    - [break](#break)
    - [continue](#continue)
    - [return](#return)
    - [try/catch/finally](#trycatchfinally)
- [数组](#数组)
  - [稀疏数组](#稀疏数组)
  - [in 操作符](#in-操作符)
  - [length属性](#length属性)
    - [设置length属性](#设置length属性)
  - [删除数组元素](#删除数组元素)
    - [delete操作符](#delete操作符)
  - [遍历](#遍历)
    - [排除非法元素](#排除非法元素)
  - [数组方法](#数组方法)
    - [join()](#join)
    - [reverse()](#reverse)
    - [sort()](#sort)
      - [比较函数](#比较函数)
    - [cocat()](#cocat)
    - [slice()](#slice)
    - [splice()](#splice)
    - [pop()和push()](#pop和push)
    - [shift()和unshift()](#shift和unshift)
    - [toString()和toLocaleString()](#tostring和tolocalestring)
    - [ECMAScript5中新增数组方法](#ecmascript5中新增数组方法)
      - [forEach()](#foreach)
      - [map()](#map)
      - [filter()](#filter)
      - [every()和some()](#every和some)
      - [reduce()和reduceRight()](#reduce和reduceright)
      - [indexOf()和lastIndexOf()](#indexof和lastindexof)
- [Window](#window)
  - [把字符串当函数执行的方法](#把字符串当函数执行的方法)
- [参考资料](#参考资料)

<!-- /TOC -->

# 类型、值和变量

> Javascript的数据类型分为两类: 原始类型和对象类型.<br>
原始类型包括数字、字符串和布尔值.<br>
<br>
Javascript中有两个特殊的原始值:null(空)和undefined(未定义),<br>他们不是数字/字符串/布尔值,他们通常代表了各自特殊类型的唯一的成员.


Javascript中除了数字,字符串,布尔值,null和undefined之外的就是对象了.<br>
对象是属性的集合,每个属性都由"名/值对"(值可以是数字,字符串,布尔值,null和undefined以及对象)构成.<br>
其中有一个特殊的对象-全局对象.

Javascipt的类型可以分为原始类型和对象类型,<br>
也可以分为拥有方法的类型和不能拥有方法的类型,<br>
同样可以分为可变类型(对象和数组)和不可变类型.

J_S变量是无类型的,变量可以被赋予任何类型的值,<br>同样一个变量也可以重新赋予不同类型的值.<br>使用var关键字来声明变量.

## 数字

J_s不区分整数值和浮点数值.J_s中所有数值据用浮点数值表示.

### 算术运算符

除了基本运算符外,J_S还支持更加复杂的算数运算,<br>通过Math对象的属性定义的函数和常量来实现.

函数|结果|含义|
-|-|-
Math.pow(2, 53)|9007199254740992|2的53次幂
Math.round(.6)|1.0|四舍五入
Math.ceil(.6)|1.0|向上求整
Math.floor(.6)|0.0|向下求整
Math.abs(-5)|5|求绝对值
Math.max(1, 6, -1)|6|求最大值
Math.min(1, 6, -1)|-1|求最小值
Math.random()||生成一个大于等于0小于1.0的伪随机数.
Math.PI()|3.141592653589793|圆周率
Math.E()||自然对数的底数
Math.sqrt(3)|1.7320508075688772|3的平方根
Math.pow(3, 1/3)|1.4422495703074083|3的立方根
Math.sin(0)<br>Math.cos(0)<br>Math.atan(0)||三角函数
Math.log(10)|2.302585092994046|10的自然对数
Math.log(100)/Math.LN10||以10为底100的对数
Math.log(512)/Math.LN2||以2为底512的对数
Math.exp(3)||e的三次幂

### 日期和时间

函数|结果|含义|
-|-|-
var later = new Date(2019, 06, 07, 09, 50)|Sun Jul 07 2019 09:50:00 GMT+0800 (China Standard Time)|生成具体时间
later.getFullYear()|2019|
later.getMonth()|6|月份从0开始计算
later.getDate()|7|天数从1开始计算
later.getHours()|9|
later.getUTCHours()|1|使用UTC表示小时

### 文本

> 字符串可以看作是字符组成的数组,在Javascript中字符串是不可变的,<br>
可以访问字符串任意位置的文本,<br>
但J_S并未提供修改已知字符串的文本内容的方法.

函数|结果|含义|
-|-|-
var s = 'hello, world'|"hello, world"|定义字符串
s.charAt(0)|h|第一个字符
s.charAt(s.length - 1)|d|最后一个字符
s.substring(1, 4)<br>(不包括4)|ell|获取第2-4个字符
s.slice(1, 4)<br>(不包括4)|ell|获取第2-4个字符
s.slice(-3)|rld|最后三个字符
s.indexOf('l')|2|字符l首次出现位置的下标
s.indexOf('l', 4)|10|字符l在下标4后首次出现位置的下标
s.lastIndexOf('l')|10|字符l最后出现位置的下标
s.split(',')|["hello", " world"]|根据','分割成数组
s.replace('h', 'H')<sup>*</sup>|"Hello, world"|全文字符替换
s.toUpperCase()<sup>*</sup>|"HELLO, WORLD"|全文字符转大写

*: J_S中字符串是固定不变的,这些方法返回新字符串,原字符串不变.

### 匹配模式

> J_S定义了RegExp()函数,用来创建表示文本匹配模式的对象,<br>
这些模式成为: 正则表达式,J_S采用Perl中的正则表达式语法.<br>
String和PegExp对象均定义了利用正则表达式进行模式匹配和查找与替换的函数.

函数|结果|含义|
-|-|-
var text = 'testing: 1, 2, 3'|"testing: 1, 2, 3"|
text.search(/\d+/g)|9|首次匹配成功的位置下标
text.match(/\d+/g)|["1", "2", "3"]|所有匹配组成的数组
text.replace(/\d+/g, '#')|"testing: #, #, #"|
text.split(/\D+/)|["", "1", "2", "3"]|用非数字字符串截取字符串

### null和undefined

null是J_S的关键字,它表示一个特殊值,常用来描述空值.

undefined用来定义更深层次的空值.<br>
它是变量的一种取值,表明变量没有初始化,<br>
如果要查询对象属性或数组元素的值时返回undefined则说明这个属性或元素不存在.<br>
如果函数没有返回任何值也会返回undefined.<br>
引用没有提供实参的函数形参的值也只得到undefined.

undefined是预定义的全局变量,它和null不一样,它不是关键字.

可以认为undefined是系统级的,出乎意料的或类似错误的值的空缺,<br>
而null是表示程序级的,正常或在意料之中的空缺.

### 



```js


```

### 



```js


```

### 



```js


```

### 



```js


```

### 



```js


```

### 



```js


```

### 



```js


```

### 



```js


```

## 

```js


```

## 

```js


```

## 

```js


```

## 

```js


```

## 

```js


```

## 

```js


```

## 

```js


```

## 

```js


```

## 

```js


```

## 

```js


```

## 

```js


```


# 语句

## 条件语句

### if

花括号并非必须,当逻辑较为复杂时,应考虑适当添加以便于理解.

```js
flag = false
a = null
if (flag)
  a = 'hello'
else if (!flag)
  a = 'word'
else
  a = 'hi'
a // => "word"
```

### switch

```js
n = 1;
switch (n){
  case 1:
    a = 1;
    break;
  case 2:
    a = 1;
    break;
  default:
    a = -1;
    break;
}
a; // => 1
```

## 循环

### while

> 条件为真的情况下重复执行.

```js
while (life < end) {
  love++;
}
```

### for

```js
for (变量定义; 判定是否继续执行条件; 进行下次循环前要做的操作) {
  // 执行代码块
}

sum = 0;
for (i=0,j=10; i < j; i++, j--) {
  sum += i*j
}
sum; // => 70

sum = 0;
i=0,j=10;
for (; i < j; i++, j--) {
  sum += i*j
}
sum; // => 70
```

#### for/in 

```js
array = [14, 52, 63, 47, 58, 86, 97]
for (index in array) {
  console.log(index)
}
/* =>
0
1
2
3
4
5
6
*/

for (index in array) {
  console.log(array[index])
}
/* =>
14
52
63
47
58
86
97
*/
```

## 跳转

### break

> 跳出循环或其他语句的结束.

### continue

> 跳过本次循环进入下次循环同其他语言的next.

### return

> 跳出函数体的执行,并提供调用的返回值.

### try/catch/finally 

```js
try {
  // 代码块
}
catch (e) {
  // 仅当try语句中抛出异常后才执行这里的代码
}
finally {
  // 不管是否抛出异常,这里的逻辑总会执行
}
```

# 数组

数组元素可以是任意类型,同一数组中的不同元素也可以是不同类型.

## 稀疏数组

> 从0开始的索引不连续数组.

## in 操作符

> 用于检测索引在数组中是否存在元素.

```js
var a1 = [,,,] // 数组是[undefined, undefined, undefined]
var a2 = new Array(3) // 数组没有任何元素
0 in a1 // => true
0 in a2 // => false
```

## length属性

> 对于稠密数组来说,length属性值代表数组中元素个数即数组长度.

### 设置length属性

> 手动设置length属性为一个小于当前长度的非负整数n时,<br>当前数组中索引值 *大于或等于* n的元素将从中删除.

```js
a = [1, 2, 3, 4, 5]
a.length = 3 // a为[1, 2, 3]
```

## 删除数组元素

### delete操作符

> delete 操作符不会修改数组的length属性,<br>也不会将高索引处移下来补充已删除属性留下的空白,<br>元素删除后数组会变为稀疏数组.

```js
a = [1, 2, 3]
delete a[1]
1 in a[1] // => false
a.length // => 3
```


## 遍历

```js
array = [1, 3, 5, '7']
Object.keys(array) // => ["0", "1", "2", "3"] 返回数组元素的索引
array[100] = 100
Object.keys(array) // => ["0", "1", "2", "3", "100"]
```

### 排除非法元素

```js
for( i=0; i < a.length; i++) {
  // 跳过null, undefined和不存在的元素
  if (!a[i]) continue;

  // 跳过undefined和不存在元素
  if ( a[1 === undefined]) continue;

  // 跳过不存在的元素
  if (!(i in a)) continue;
}

```

## 数组方法

### join()

> 将数组中所有元素转化为字符串连接在一起.

```js
i = 3
a = [1, 2, '3', i+1]; // => [1, 2, "3", 4]
a.join();             // => "1,2,3,4"
a.join('');           // => "1234"
a.join(' ');          // => "1 2 3 4"
a.join('-');          // => "1-2-3-4"

a = [1, 2, '3', [4 ,5]]; // => [1, 2, "3", [4, 5]]
a.join('-');             // => "1-2-3-4,5"
```

### reverse()

> 数组元素进行颠倒顺序,返回逆序数组.

```js
a = [1, 2, '3', 4]; // => [1, 2, "3", 4]
a.reverse();        // => [4, "3", 2, 1]
a;                  // => [4, "3", 2, 1]
```

### sort()

> 返回数组排序后的数组,不带参数时默认以字母排序.

```js
a = ['c', 'b', 'r', 't', 'w', 'a', 'z']; // => ["c", "b", "r", "t", "w", "a", "z"]
a.sort();                                // => ["a", "b", "c", "r", "t", "w", "z"]
a;                                       // => ["a", "b", "c", "r", "t", "w", "z"]

```

#### 比较函数

> 函数返回值决定两参数在数组中的先后顺序,<br>如果第一个参数应该在前,函数返回值应小于0,<br>若第一个参数应在后,返回值应大于0.

```js
a = [72, 41, 57, 39, 32];           // => [72, 41, 57, 39, 32]
a.sort();                           // => [32, 39, 41, 57, 72]
a.sort(function(a, b){return a-b}); // => [32, 39, 41, 57, 72]
a.sort(function(a, b){return b-a}); // => [72, 57, 41, 39, 32]
```

### cocat()

> 返回包含调用方法数组以及参数在内的 *新数组*.

```js
a = [1, 2, 3, 4];         // => [1, 2, 3, 4]
a.concat(5, 6);           // => [1, 2, 3, 4, 5, 6]
a.concat([5, 6]);         // => [1, 2, 3, 4, 5, 6]
a.concat([5, 6], [7, 8]); // => [1, 2, 3, 4, 5, 6, 7, 8]
a.concat(5, [6, [7, 8]]); // => [1, 2, 3, 4, 5, 6, [7, 8]]
a;                        // => [1, 2, 3, 4]
```

### slice()

> 返回指定数组的一个片段或子数组.<br>参数1应小于参数2.

```js
a = [1, 2, 3, 4]; // => [1, 2, 3, 4]
a.slice(0, 3);    // => [1, 2, 3]
a.slice(1, -1);   // => [2, 3]
a.slice(-1, 1);   // => []
a.slice(-3, -2);  // => [2]
a;                // => [1, 2, 3, 4]
```

### splice()

> 数组中插入或删除元素的通用方法,<br>不同与concat()和slice(),splice()会 *修改调用数组* .<br>并返回删除的元素数组.

```js
// splice() 前两个参数制定需要删除的参数

a = [1, 2, 3, 4]; // => [1, 2, 3, 4]
// 如果省略第二个参数,将删除从起始点到结尾的所有元素
a.splice(2);      // => [3, 4]
a;                // => [1, 2]

a = [1, 2, 3, 4]; // => [1, 2, 3, 4]
a.splice(1, 2);   // => [2, 3]
a;                // => [1, 4]

a = [1, 2, 3, 4]; // => [1, 2, 3, 4]
a.splice(1, 1);   // => [2]
a;                // => [1, 3, 4]

// 前两个参数后任意个数参数指定了需要插入的元素,插入位置从地一个参数指定的位置开始插入.

a = [1, 2, 3, 4];         // => [1, 2, 3, 4]
a.splice(2, 0, 'a', 'b'); // => []
a;                        // => [1, 2, "a", "b", 3, 4]

a = [1, 2, 3, 4];         // => [1, 2, 3, 4]
a.splice(2, 2, [1, 2],3); // => [3, 4]
a;                        // => [1, 2, [1, 2], 3]
```

### pop()和push() 

> 从数组 *尾部* 一次减少一个元素,并返回删除的元素.

```js
a = [1, 3, 5, 8, 9]; // => [1, 3, 5, 8, 9]
a.pop();             // => 9 返回删除元素
a;                   // => [1, 3, 5, 8]

// 与此相对的是push()方法
a.push(10) // => 5 返回数组长度, 数组为 [1, 3, 5, 8, 10]
```

### shift()和unshift()

```js
a = [1, 3, 5, 8, 9]; // => [1, 3, 5, 8, 9]
a.shift();           // => 1 返回删除元素
a;                   // => [3, 5, 8, 9]

// 与此相对的是unshift() 方法
a.unshift(10);       // => 5 返回数组长度, 
a;                   // => [10, 3, 5, 8, 9]
```

### toString()和toLocaleString()

> 不带任何参数调用toString()方法和调用join()方法的返回值一致.

```js
a = [1, 2, 3, 4]; // => [1, 2, 3, 4]
a.toString();     // => "1,2,3,4"
a;                // => [1, 2, 3, 4]
a.toString('-');  // => "1,2,3,4"
```

### ECMAScript5中新增数组方法

共同特点是这些方法的第一个参数接收一个函数并对每一个元素调用一次该函数.

#### forEach()

> 从头到尾便利数组,为每个元素执行指定函数.<br>该方法无法提前终止遍历,即break语句在循环中不好使,需抛出异常来提前终止.

```js
a = [1, 2, 3, 4];                         // => [1, 2, 3, 4]
sum = 0;
a.forEach(function(value) {sum += value}); // => undefined
sum;                                      // => 10
a;                                        // => [1, 2, 3, 4]
a.forEach(function(value, index, array) {array[index] = value+1}); // => undefined
a;                                        // => [2, 3, 4, 5]
```

#### map()

> 将每个元素传递给指定函数,并返回函数的返回值.<br>这就要求指定函数 *必须存在返回值* . 

```js
a = [1, 2, 3, 4];                              // => [1, 2, 3, 4]
a.map(function(value) {return value * value}); // => [1, 4, 9, 16]
a;                                             // => [1, 2, 3, 4]
```

#### filter()

> 指定函数的返回值必须为布尔类型,<br>从而由filter()判断返回结果中是否应包含该元素.

```js
a = [1, 2, 3, 4];                             // => [1, 2, 3, 4]
a.filter(function(value) {return value > 2}); // => [3, 4]
a;                                            // => [1, 2, 3, 4]
```

#### every()和some()

> 对指定函数进行判定,返回布尔值.<br>一旦确定返回值将停止遍历,类似逻辑&&运算.

```js
a = [1, 2, 3, 4];                             // => [1, 2, 3, 4]
a.every(function(value) {return value < 10}); // => true
a.every(function(value) {return value > 10}); // => false
```

#### reduce()和reduceRight()

> 

```js


```

#### indexOf()和lastIndexOf()

> 搜索整个数组中符合给定值的元素,并返回找到的 *第一个元素* 的索引值,<br>如果没有找到则返回-1.

```js
a = [10, 32, 13, 54, 13, 58]; // => [10, 32, 13, 54, 13, 58]
a.indexOf(9);                 // => -1
a.indexOf(13);                // => 2
a.lastIndexOf(13);            // => 4
```

# Window

## 把字符串当函数执行的方法 

> 全局函数声明会变成全局对象的属性

```js
function test(str) { 
  alert(str); 
} 
window['test']('param'); //直接执行

window['test'].call(this,'param');//如果需要修改函数运行时的this
```

```js


```



```js


```



```js


```



```js


```



```js


```


# 参考资料

> [JavaScript 权威指南]()

> [简书 | js 把字符串当函数执行的方法](https://www.jianshu.com/p/636000befcf3)
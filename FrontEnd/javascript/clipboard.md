> 提供一件复制功能

# 示例

```html
<input type="text" id="copyVal" readonly value="被复制内容" />
<button class="copyBtn" data-clipboard-target="#copyVal">点击复制</button>
```

```js 
//实例化 ClipboardJS对象;
var copyBtn = new ClipboardJS('.copyBtn');

copyBtn.on("success",function(e){
  // 复制成功
  alert(e.text);
  e.clearSelection();
});
copyBtn.on("error",function(e){
  //复制失败;
  console.log( e.action )
});
```

# 参考资料

> [CSDN | Clipboard.js 实现点击复制](https://blog.csdn.net/fly_wugui/article/details/80327385)

> [GitHub | cnd地址列表](https://github.com/zenorocha/clipboard.js/wiki/CDN-Providers)

> [Github | 下载地址](https://github.com/zenorocha/clipboard.js/archive/master.zip)
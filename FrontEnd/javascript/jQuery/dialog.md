# 表单

## 新建,更新通用方法

```js
/*
  说明:
    通过dialog展示更新/新建页面表单内容,通过ajax提交表单内容,并使用dialog提示表单提交结果.
    并且可针对表单提交结果决定下一步操作.

    参数说明:
      form_mark:
        表单标识,id或唯一class均可.
      call_back:
        call_back参数为回调参数,调用方法中有定义则会调用,无需回调不传參即可
        回调中接收一个布尔值参数,后端返回的success具体值用于标识操作是否成功
        可根据该标识针对不同情况进行处理.
        
  示例:   
    调用示例:
      function add_address() {
        common_dialog_update('<%= main_app.new_address_addresses_path %>', '新增地址', '#address_form', function(flag){
          alert(flag);
        });
      }
    后端返回值示例:
      return render :json => {"success" => true, "msg" => "保存成功"}
*/
function common_dialog_update(url, origin_title, form_mark, call_back) {
  $.ajax({
    type: 'get',
    url: url,
    success: function (data) {
      var d = dialog({
        title: origin_title,
        content: data,
        okValue: '确定',
        ok: function () {
          form_data = $(form_mark).closest('form').serialize();
          $.ajax({
            type: 'post',
            url: $(form_mark).closest('form')[0].action,
            data: form_data,
            success: function (data) {
              dialog_msg (data['msg']);
              if (typeof(call_back) == 'function'){
                call_back(data['success']);
              }
            },
            error: function() {
              alert('系统错误,请刷新页面重试');
            }
          });
        },
        cancelValue: '取消',
        cancel: function () {}
      });
      d.show();
    },
    error: function() {
      alert('系统错误,请刷新页面重试');
    }
  })
};
```
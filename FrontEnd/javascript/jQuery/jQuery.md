<!-- TOC -->

- [获取页面内容](#获取页面内容)
  - [input](#input)
    - [获取input选中/输入内容](#获取input选中输入内容)
- [基础](#基础)
  - [字符串](#字符串)
    - [拆分](#拆分)
      - [split](#split)
      - [substring](#substring)
- [延时执行](#延时执行)
- [参考资料](#参考资料)

<!-- /TOC -->
# 获取页面内容

## input

### 获取input选中/输入内容

```javascript
$('input[name="input_name"]:checked').val()
```

# 基础

## 字符串

### 拆分

#### split

```js
str = "abd,sda,dsad"
// => "abd,sda,dsad"
str.split(",")
// => (3) ["abd", "sda", "dsad"]
```

#### substring

```js
str ="abdsdafewe"
// => "abdsdafewe"
str.substring(0,4)
// => "abds"
str.substring(4,str.length)
// => "dafewe"
```

# 延时执行

> 单位:毫秒(1秒=1000毫秒)

```js
setTimeout(function () { 
  test(); 
}, 2000);
```

# 参考资料

> [CSDN | JS拆分字符串](https://blog.csdn.net/guochanof/article/details/80823828)

> [简书 | jquery 延时执行](https://www.jianshu.com/p/ec208a92b1e2)

> XML Schema 描述了 XML文档的结构.<br>
XML Schema 的作用是定义 XML 文档的合法构建模块,类似 DTD.

<!-- TOC -->

- [简介](#简介)
- [什么是XML Schema?](#什么是xml-schema)
  - [相对DTD的优点](#相对dtd的优点)
  - [可保护数据通信](#可保护数据通信)
- [XSD如何使用?](#xsd如何使用)
  - [XML 文档](#xml-文档)
  - [DTD 文件](#dtd-文件)
  - [XML Schema](#xml-schema)
  - [对DTD的引用](#对dtd的引用)
  - [对XML Schema的引用](#对xml-schema的引用)
- [XSD - \<schema> 元素](#xsd---\schema-元素)
  - [在XML文档中引用Schema](#在xml文档中引用schema)
- [XSD简易元素](#xsd简易元素)
  - [定义简易元素](#定义简易元素)
  - [内建数据类型](#内建数据类型)
    - [字符串数据类型](#字符串数据类型)
      - [规格化字符串数据类型(NormalizedString Data Type)](#规格化字符串数据类型normalizedstring-data-type)
      - [Token 数据类型(Token Data Type)](#token-数据类型token-data-type)
      - [字符串数据类型](#字符串数据类型-1)
      - [对字符串数据类型的限定(Restriction)](#对字符串数据类型的限定restriction)
    - [日期数据类型](#日期数据类型)
      - [时区](#时区)
      - [时间数据类型(Time Data Type)](#时间数据类型time-data-type)
        - [时区](#时区-1)
      - [日期时间数据类型(DateTime Data Type)](#日期时间数据类型datetime-data-type)
        - [时区](#时区-2)
      - [持续时间数据类型(Duration Data Type)](#持续时间数据类型duration-data-type)
        - [负的持续时间](#负的持续时间)
        - [对日期数据类型的限定(Restriction)](#对日期数据类型的限定restriction)
    - [数值数据类型](#数值数据类型)
      - [十进制数据类型](#十进制数据类型)
      - [整数数据类型](#整数数据类型)
      - [数值数据类型](#数值数据类型-1)
      - [对数值数据类型的限定(Restriction)](#对数值数据类型的限定restriction)
    - [杂项 数据类型](#杂项-数据类型)
      - [布尔数据类型(Boolean Data Type)](#布尔数据类型boolean-data-type)
      - [二进制数据类型(Binary Data Types)](#二进制数据类型binary-data-types)
      - [AnyURI 数据类型(AnyURI Data Type)](#anyuri-数据类型anyuri-data-type)
      - [杂项数据类型](#杂项数据类型)
      - [对杂项数据类型的限定(Restriction)](#对杂项数据类型的限定restriction)
  - [简易元素的默认值和固定值](#简易元素的默认值和固定值)
- [XSD属性](#xsd属性)
  - [内建数据类型](#内建数据类型-1)
  - [实例](#实例)
  - [属性的默认值和固定值](#属性的默认值和固定值)
  - [可选的和必需的属性](#可选的和必需的属性)
  - [对内容的限定](#对内容的限定)
- [XSD Facets](#xsd-facets)
  - [对值的限定](#对值的限定)
  - [对一组值的限定](#对一组值的限定)
  - [对一系列值的限定](#对一系列值的限定)
  - [对一系列值的其他限定](#对一系列值的其他限定)
  - [对空白字符的限定](#对空白字符的限定)
  - [对长度的限定](#对长度的限定)
  - [数据类型的限定](#数据类型的限定)
- [XSD复合元素](#xsd复合元素)
  - [复合元素类型](#复合元素类型)
    - [复合空元素](#复合空元素)
    - [仅含元素](#仅含元素)
    - [仅含文本](#仅含文本)
    - [包含元素和文本的元素](#包含元素和文本的元素)
- [XSD指示器](#xsd指示器)
  - [指示器种类](#指示器种类)
    - [Order指示器](#order指示器)
      - [All指示器](#all指示器)
      - [Choice指示器](#choice指示器)
      - [Sequence指示器](#sequence指示器)
    - [Occurrence指示器](#occurrence指示器)
      - [maxOccurs指示器](#maxoccurs指示器)
      - [minOccurs 指示器](#minoccurs-指示器)
        - [实例](#实例-1)
    - [Group指示器](#group指示器)
      - [元素组](#元素组)
      - [属性组](#属性组)
- [XSD\<any>元素](#xsd\any元素)
- [XSD\<anyAttribute>元素](#xsd\anyattribute元素)
- [XSD元素替换(Element Substitution)](#xsd元素替换element-substitution)
- [参考资料](#参考资料)

<!-- /TOC -->

# 简介

# 什么是XML Schema?

XML Schema作用|
-|
定义可出现在文档中的元素|
定义可出现在文档中的属性|
定义哪个元素是子元素|
定义子元素的次序|
定义子元素的数目|
定义元素是否为空,或者是否可包含文本|
定义元素和属性的数据类型|
定义元素和属性的默认值以及固定值|

## 相对DTD的优点

0. XML Schema 可针对未来的需求进行扩展
0. XML Schema 更完善,功能更强大
0. XML Schema 基于 XML 编写
0. XML Schema 支持数据类型
0. XML Schema 支持命名空间

## 可保护数据通信

当数据从发送方被发送到接受方时,其要点是双方应有关于内容的相同的"期望值".

通过XML Schema,发送方可以用一种接受方能够明白的方式来描述数据.

一种数据,比如 "03-11-2004",在某些国家被解释为11月3日,而在另一些国家为当作3月11日.

但是一个带有数据类型的 XML 元素,比如:\<date type="date">2004-03-11</date>,可确保对内容一致的理解,这是因为 XML 的数据类型 "date" 要求的格式是 "YYYY-MM-DD".

# XSD如何使用?

XML 文档可对 DTD 或 XML Schema 进行引用.

## XML 文档

名为 "note.xml" 的 XML 文档:

```xml
<?xml version="1.0"?>
<note>
  <to>Tove</to>
  <from>Jani</from>
  <heading>Reminder</heading>
  <body>Don't forget me this weekend!</body>
</note>
```

## DTD 文件

名为 "note.dtd" 的 DTD 文件,它对上面那个 XML 文档( "note.xml" )的元素进行了定义:

```xml
<!-- 定义note元素有四个子元素:"to, from, heading, body" -->
<!ELEMENT note (to, from, heading, body)>
<!-- 2-5 行定义了 to, from, heading, body 元素的类型是 "#PCDATA" -->
<!ELEMENT to (#PCDATA)>
<!ELEMENT from (#PCDATA)>
<!ELEMENT heading (#PCDATA)>
<!ELEMENT body (#PCDATA)>
```

## XML Schema

下面这个例子是一个名为 "note.xsd" 的 XML Schema 文件,它定义了上面那个 XML 文档(note.xml)的元素:

```xml
<?xml version="1.0"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
targetNamespace="http://www.w3schools.com"
xmlns="http://www.w3schools.com"
elementFormDefault="qualified">

<!-- note 元素是一个复合类型,因为它包含其他的子元素 -->
<xs:element name="note">
  <xs:complexType>
    <xs:sequence>
    <!-- to, from, heading, body是简易类型,因为它们没有包含其他元素 -->
      <xs:element name="to" type="xs:string"/>
      <xs:element name="from" type="xs:string"/>
      <xs:element name="heading" type="xs:string"/>
      <xs:element name="body" type="xs:string"/>
    </xs:sequence>
  </xs:complexType>
</xs:element>

</xs:schema>
```

## 对DTD的引用

note.xml包含对 DTD 的引用:

```xml
<?xml version="1.0"?>

<!DOCTYPE note SYSTEM
"http://www.w3schools.com/dtd/note.dtd">

<note>
  <to>Tove</to>
  <from>Jani</from>
  <heading>Reminder</heading>
  <body>Don't forget me this weekend!</body>
</note>
```

## 对XML Schema的引用

note.xml包含对XML Schema的引用:

```xml
<?xml version="1.0"?>

<!-- 指定XML Schema文件名称:note.xsd -->
<note
xmlns="http://www.w3schools.com"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.w3schools.com note.xsd">
  <to>Tove</to>
  <from>Jani</from>
  <heading>Reminder</heading>
  <body>Don't forget me this weekend!</body>
</note>
```

# XSD - \<schema> 元素

\<schema> 元素是每一个XML Schema的根元素:

```xml
<?xml version="1.0"?>

<xs:schema>
...
...
</xs:schema>
```

\<schema> 元素可包含属性.

一个 schema 声明往往看上去类似这样:

```xml
<?xml version="1.0"?>

<!-- 

xmlns:xs="http://www.w3.org/2001/XMLSchema"定义了:
schema中用到的元素和数据类型来自命名空间 "http://www.w3.org/2001/XMLSchema" 
同时它还规定了来自命名空间的元素和数据类型应该使用前缀 xs:

targetNamespace="http://www.runoob.com"定义了:
此schema中定义的元素 (note, to, from, heading, body) 来自命名空间: "http://www.runoob.com"

xmlns="http://www.runoob.com"定义了:
默认的命名空间是 "http://www.runoob.com"

elementFormDefault="qualified"定义了:
任何XML实例文档所使用的且在此schema中声明过的元素必须被命名空间限定
-->

<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
targetNamespace="http://www.runoob.com"
xmlns="http://www.runoob.com"
elementFormDefault="qualified">
...
...
</xs:schema>
```

## 在XML文档中引用Schema

XML 文档对XML Schema的引用:

```xml
<?xml version="1.0"?>

<!-- 
xmlns="http://www.runoob.com"定义了:
默认命名空间的声明.<br>
此声明会告知schema验证器,在此 XML 文档中使用的所有元素都被声明于 "http://www.runoob.com" 这个命名空间 

xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"定义了:
需要使用的命名空间

xsi:schemaLocation="http://www.runoob.com note.xsd"定义了:
供命名空间使用的XML schema的位置
-->

<note xmlns="http://www.runoob.com"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.runoob.com note.xsd">

<to>Tove</to>
<from>Jani</from>
<heading>Reminder</heading>
<body>Don't forget me this weekend!</body>
</note>
```

# XSD简易元素

XML Schema 可定义 XML 文件的元素.

简易元素指那些只包含文本的元素.<br>
它不会包含任何其他的元素或属性.

不过,"仅包含文本"这个限定却很容易造成误解.<br>
文本有很多类型.<br>
它可以是 XML Schema 定义中包括的类型中的一种(布尔、字符串、数据等等),或者它也可以是行定义的定制类型.

可向数据类型添加限定(即 facets),以此来限制它的内容,或者可以要求数据匹配某种特定的模式.

## 定义简易元素

element用于定义元素.

定义简易元素的语法:

```xml
<!-- xxx 指元素的名称,yyy 指元素的数据类型 -->
<xs:element name="xxx" type="yyy"/>
```

## 内建数据类型

常用类型|
-|
xs:string|
xs:decimal|
xs:integer|
xs:boolean|
xs:date|
xs:time|

### 字符串数据类型

字符串数据类型用于可包含字符串的值.

字符串数据类型可包含字符、换行、回车以及制表符.

下面是一个关于某个scheme中字符串声明的例子:

```xml
<xs:element name="customer" type="xs:string"/>
```

文档中的元素看上去应该类似这样:

```xml
<customer>John Smith</customer>
```

如果使用字符串数据类型,XML处理器就不会更改其中的值.

#### 规格化字符串数据类型(NormalizedString Data Type)

规格化字符串数据类型源自于字符串数据类型.

规格化字符串数据类型同样可包含字符,但是XML处理器会移除折行,回车以及制表符.

下面是一个关于在某个schema中规格化字符串数据类型的例子:

```xml
<xs:element name="customer" type="xs:normalizedString"/>
```

文档中的元素看上去应该类似这样:

```xml
<customer>John Smith</customer>
<customer>     John Smith     </customer>
```

在上面的例子中,XML 处理器会使用空格替换所有的制表符.

#### Token 数据类型(Token Data Type)

Token 数据类型同样源自于字符串数据类型.

Token 数据类型同样可包含字符,但是 XML 处理器会移除换行符、回车、制表符、开头和结尾的空格以及(连续的)空格.

下面是在 schema 中一个有关 token 声明的例子:

```xml
<xs:element name="customer" type="xs:token"/>
```

文档中的元素看上去应该类似这样:

```xml
<customer>John Smith</customer>
<customer>     John Smith     </customer>
```

在上面这个例子中,XML 解析器会移除制表符.

#### 字符串数据类型

所有以下的数据类型均衍生于字符串数据类型(除了字符串数据类型本身)！

名称|描述
-|-
ENTITIES| 
ENTITY| 
ID|在XML中提交ID属性的字符串(仅与schema属性一同使用)
IDREF|在XML中提交IDREF属性的字符串(仅与 schema 属性一同使用)
IDREFS language|包含合法的语言id的字符串
Name|包含合法XML名称的字符串
NCName|
NMTOKEN|在XML中提交NMTOKEN属性的字符串(仅与schema属性一同使用)
NMTOKENS|
normalizedString|不包含换行符、回车或制表符的字符串
QName|
string|字符串
token|不包含换行符、回车或制表符、开头或结尾空格或者多个连续空格的字符串

#### 对字符串数据类型的限定(Restriction)

可与字符串数据类型一同使用的限定:

0. enumeration
0. length
0. maxLength
0. minLength
0. pattern(NMTOKENS、IDREFS 以及 ENTITIES 无法使用此约束)
0. whiteSpace

### 日期数据类型

日期数据类型用于定义日期.

日期使用此格式进行定义:"YYYY-MM-DD",其中:

0. YYYY 表示年份
0. MM 表示月份
0. DD 表示天数

所有的成分都是必需的

下面是一个有关schema中日期声明的例子:

```xml
<xs:element name="start" type="xs:date"/>
```

文档中的元素看上去应该类似这样:

```xml
<start>2002-09-24</start>
```

#### 时区

```xml
<!-- 如需规定一个时区,可以通过在日期后加一个 "Z" 的方式,使用世界调整时间(UTC time)来输入一个日期 -->
<start>2002-09-24Z</start>
<!-- 也可以通过在日期后添加一个正的或负时间的方法,来规定以世界调整时间为准的偏移量 -->
<start>2002-09-24-06:00</start>
<start>2002-09-24+06:00</start>
```

#### 时间数据类型(Time Data Type)

时间数据类型用于定义时间.

时间使用下面的格式来定义:"hh:mm:ss",其中

0. hh 表示小时
0. mm 表示分钟
0. ss 表示秒

所有的成分都是必需的.

下面是一个有关schema中时间声明的例子:

```xml
<xs:element name="start" type="xs:time"/>
```

文档中的元素看上去应该类似这样:

```xml
<start>09:00:00</start>
<start>09:30:10.5</start>
```

##### 时区

如需规定一个时区,也可以通过在时间后加一个 "Z" 的方式,使用世界调整时间(UTC time)来输入一个时间 - 比如这样:

```xml
<start>09:30:10Z</start>
```

或者也可以通过在时间后添加一个正的或负时间的方法,来规定以世界调整时间为准的偏移量 - 比如这样:

```xml
<start>09:30:10-06:00</start>
```

```xml
<start>09:30:10+06:00</start>
```

#### 日期时间数据类型(DateTime Data Type)

日期时间数据类型用于定义日期和时间.

日期时间使用下面的格式进行定义:"YYYY-MM-DDThh:mm:ss",其中:

0. YYYY 表示年份
0. MM 表示月份
0. DD 表示日
0. T 表示必需的时间部分的起始
0. hh 表示小时
0. mm 表示分钟
0. ss 表示秒

所有的成分都是必需的.

下面是一个有关schema中日期时间声明的例子:

```xml
<xs:element name="startdate" type="xs:dateTime"/>
```

文档中的元素看上去应该类似这样:

```xml
<startdate>2002-05-30T09:00:00</startdate>
<startdate>2002-05-30T09:30:10.5</startdate>
```

##### 时区

```xml
<!-- 如需规定一个时区,也可以通过在日期时间后加一个 "Z" 的方式,使用世界调整时间(UTC time)来输入一个日期时间 -->
<startdate>2002-05-30T09:30:10Z</startdate>
<!-- 也可以通过在时间后添加一个正的或负时间的方法,来规定以世界调整时间为准的偏移量 -->
<startdate>2002-05-30T09:30:10-06:00</startdate>
<startdate>2002-05-30T09:30:10+06:00</startdate>
```

#### 持续时间数据类型(Duration Data Type)

持续时间数据类型用于规定时间间隔.

时间间隔使用下面的格式来规定:"PnYnMnDTnHnMnS",其中:

0. P 表示周期(必需)
0. nY 表示年数
0. nM 表示月数
0. nD 表示天数
0. T 表示时间部分的起始 (如果您打算规定小时、分钟和秒,则此选项为必需)
0. nH 表示小时数
0. nM 表示分钟数
0. nS 表示秒数

下面是一个有关schema中持续时间声明的例子:

```xml
<xs:element name="period" type="xs:duration"/>
```

文档中的元素看上去应该类似这样:

```xml
<!-- 表示一个 5 年的周期 -->
<period>P5Y</period>
<!-- 表示一个 5 年、2 个月及 10 天的周期 -->
<period>P5Y2M10D</period>
<!-- 表示一个 5 年、2 个月、10 天及 15 小时的周期 -->
<period>P5Y2M10DT15H</period>
<!-- 表示一个 15 小时的周期 -->
<period>PT15H</period>
```

##### 负的持续时间

如需规定一个负的持续时间,请在 P 之前输入减号:

```xml
<period>-P10D</period>
```

上面的例子表示一个负 10 天的周期.

日期和时间数据类型
名称|描述
-|-
date|定义一个日期值
dateTime|定义一个日期和时间值
duration|定义一个时间间隔
gDay|定义日期的一个部分 - 天 (DD)
gMonth|定义日期的一个部分 - 月 (MM)
gMonthDay|定义日期的一个部分 - 月和天 (MM-DD)
gYear|定义日期的一个部分 - 年 (YYYY)
gYearMonth|定义日期的一个部分 - 年和月 (YYYY-MM)
time|定义一个时间值

##### 对日期数据类型的限定(Restriction)

可与日期数据类型一同使用的限定:

0. enumeration
0. maxExclusive
0. maxInclusive
0. minExclusive
0. minInclusive
0. pattern
0. whiteSpace

### 数值数据类型

#### 十进制数据类型

十进制数据类型用于规定一个数值.

下面是一个关于某个scheme中十进制数声明的例子.

```xml
<xs:element name="prize" type="xs:decimal"/>
```

文档中的元素看上去应该类似这样:

```xml
<prize>999.50</prize>
<prize>+999.5450</prize>
<prize>-999.5230</prize>
<prize>0</prize>
<prize>14</prize>
```

可规定的十进制数字的最大位数是18位.

#### 整数数据类型

整数数据类型用于规定无小数成分的数值.

下面是一个关于某个scheme中整数声明的例子.

```xml
<xs:element name="prize" type="xs:integer"/>
```

文档中的元素看上去应该类似这样:

```xml
<prize>999</prize>
<prize>+999</prize>
<prize>-999</prize>
<prize>0</prize>
```

#### 数值数据类型

下面所有的数据类型均源自于十进制数据类型(除 decimal 本身以外)！

名字|秒数
-|-
byte|有正负的 8 位整数
decimal|十进制数
int|有正负的 32 位整数
integer|整数值
long|有正负的 64 位整数
negativeInteger|仅包含负值的整数 ( .., -2, -1.)
nonNegativeInteger|仅包含非负值的整数 (0, 1, 2, ..)
nonPositiveInteger|仅包含非正值的整数 (.., -2, -1, 0)
positiveInteger|仅包含正值的整数 (1, 2, ..)
short|有正负的 16 位整数
unsignedLong|无正负的 64 位整数
unsignedInt|无正负的 32 位整数
unsignedShort|无正负的 16 位整数
unsignedByte|无正负的 8 位整数

#### 对数值数据类型的限定(Restriction)

可与数值数据类型一同使用的限定:

0. enumeration
0. fractionDigits
0. maxExclusive
0. maxInclusive
0. minExclusive
0. minInclusive
0. pattern
0. totalDigits
0. whiteSpace

### 杂项 数据类型

其他杂项数据类型包括布尔、base64Binary、十六进制、浮点、双精度、anyURI、anyURI 以及 NOTATION.

#### 布尔数据类型(Boolean Data Type)

布尔数据性用于规定 true 或 false 值.

下面是一个关于某个scheme中逻辑声明的例子:

```xml
<xs:attribute name="disabled" type="xs:boolean"/>
```

文档中的元素看上去应该类似这样:

```xml
<prize disabled="true">999</prize>
```

合法的布尔值是 true、false、1(表示 true) 以及 0(表示 false).

#### 二进制数据类型(Binary Data Types)

二进制数据类型用于表达二进制形式的数据.

可使用两种二进制数据类型:

0. base64Binary (Base64 编码的二进制数据)
0. hexBinary (十六进制编码的二进制数据)

下面是一个关于某个scheme中hexBinary声明的例子:

```xml
<xs:element name="blobsrc" type="xs:hexBinary"/>
```

#### AnyURI 数据类型(AnyURI Data Type)

anyURI 数据类型用于规定 URI.

下面是一个关于某个 scheme 中 anyURI 声明的例子:

```xml
<xs:attribute name="src" type="xs:anyURI"/>
```

文档中的元素看上去应该类似这样:

```xml
<pic src="http://www.w3schools.com/images/smiley.gif" />
```

如果某个 URI 含有空格,请用%20替换它们.

#### 杂项数据类型

名称|描述
-|-
anyURI| 
base64Binary| 
boolean| 
double| 
float| 
hexBinary| 
NOTATION| 
QName| 

#### 对杂项数据类型的限定(Restriction)

可与杂项数据类型一同使用的限定:

0. enumeration (布尔数据类型无法使用此约束*)
0. length (布尔数据类型无法使用此约束)
0. maxLength (布尔数据类型无法使用此约束)
0. minLength (布尔数据类型无法使用此约束)
0. pattern
0. whiteSpace

约束指 constraint

## 简易元素的默认值和固定值

简易元素可拥有指定的默认值或固定值.

当没有其他的值被规定时,默认值就会自动分配给元素.

在中,缺省值是 "red":

```xml
<xs:element name="color" type="xs:string" default="red"/>
```

固定值同样会自动分配给元素,并且无法规定另外一个值.

在中,固定值是 "red":

```xml
<xs:element name="color" type="xs:string" fixed="red"/>
```

# XSD属性

所有的属性均作为简易类型来声明.<br>
简易元素无法拥有属性.<br>
假如某个元素拥有属性,它就会被当作某种复合类型.<br>
但是属性本身总是作为简易类型被声明的.

attribute用于定义属性.

## 内建数据类型

和元素类型一致.

## 实例

这是带有属性的 XML 元素:

```xml
<lastname lang="EN">Smith</lastname>
```

这是对应的属性定义:

```xml
<xs:attribute name="lang" type="xs:string"/>
```

## 属性的默认值和固定值

属性可拥有指定的默认值或固定值.

当没有其他的值被规定时,默认值就会自动分配给元素.

在中,缺省值是 "EN":

```xml
<xs:attribute name="lang" type="xs:string" default="EN"/>
```

固定值同样会自动分配给元素,并且无法规定另外的值.

在中,固定值是 "EN":

```xml
<xs:attribute name="lang" type="xs:string" fixed="EN"/>
```

## 可选的和必需的属性

在缺省的情况下,属性是可选的.<br>
如需规定属性为必选,请使用"use"属性:

```xml
<xs:attribute name="lang" type="xs:string" use="required"/>
```

## 对内容的限定

当 XML 元素或属性拥有被定义的数据类型时,就会向元素或属性的内容添加限定.

假如 XML 元素的类型是 "xs:date",而其包含的内容是类似 "Hello World" 的字符串,元素将不会(通过)验证.

通过 XML schema,也可向XML元素及属性添加自己的限定.<br>
这些限定被称为 facet(编者注:意为(多面体的)面,可译为限定面).

# XSD Facets

限定(restriction)用于为 XML 元素或者属性定义可接受的值.<br>
对 XML 元素的限定被称为 facet.

## 对值的限定

定义了带有一个限定且名为 "age" 的元素.<br>
age 的值不能低于 0 或者高于 120:

```xml
<xs:element name="age">
  <xs:simpleType>
    <xs:restriction base="xs:integer">
      <xs:minInclusive value="0"/>
      <xs:maxInclusive value="120"/>
    </xs:restriction>
  </xs:simpleType>
</xs:element>
```

## 对一组值的限定

如需把 XML 元素的内容限制为一组可接受的值,要使用枚举约束(enumeration constraint).

定义了带有一个限定的名为 "car" 的元素.<br>
可接受的值只有:Audi, Golf, BMW:

```xml
<xs:element name="car">
  <xs:simpleType>
    <xs:restriction base="xs:string">
      <xs:enumeration value="Audi"/>
      <xs:enumeration value="Golf"/>
      <xs:enumeration value="BMW"/>
    </xs:restriction>
  </xs:simpleType>
</xs:element>
```

上面的例子也可以被写为:

```xml
<xs:element name="car" type="carType"/>

<xs:simpleType name="carType">
  <xs:restriction base="xs:string">
    <xs:enumeration value="Audi"/>
    <xs:enumeration value="Golf"/>
    <xs:enumeration value="BMW"/>
  </xs:restriction>
</xs:simpleType>
```

在这种情况下,类型 "carType" 可被其他元素使用,因为它不是 "car" 元素的组成部分.

## 对一系列值的限定

如需把 XML 元素的内容限制定义为一系列可使用的数字或字母,要使用模式约束(pattern constraint).

定义了带有一个限定的名为 "letter" 的元素.<br>
可接受的值只有小写字母 a - z 其中的一个:

```xml
<xs:element name="letter">
  <xs:simpleType>
    <xs:restriction base="xs:string">
      <xs:pattern value="[a-z]"/>
    </xs:restriction>
  </xs:simpleType>
</xs:element>
```

定义了带有一个限定的名为 "initials" 的元素.<br>
可接受的值是大写字母 A - Z 其中的三个:

```xml
<xs:element name="initials">
  <xs:simpleType>
    <xs:restriction base="xs:string">
      <xs:pattern value="[A-Z][A-Z][A-Z]"/>
    </xs:restriction>
  </xs:simpleType>
</xs:element>
```

定义了带有一个限定的名为 "initials" 的元素.<br>
可接受的值是大写或小写字母 a - z 其中的三个:

```xml
<xs:element name="initials">
  <xs:simpleType>
    <xs:restriction base="xs:string">
      <xs:pattern value="[a-zA-Z][a-zA-Z][a-zA-Z]"/>
    </xs:restriction>
  </xs:simpleType>
</xs:element>
```

定义了带有一个限定的名为 "choice 的元素.<br>
可接受的值是字母 x, y 或 z 中的一个:

```xml
<xs:element name="choice">
  <xs:simpleType>
    <xs:restriction base="xs:string">
      <xs:pattern value="[xyz]"/>
    </xs:restriction>
  </xs:simpleType>
</xs:element>
```

定义了带有一个限定的名为 "prodid" 的元素.<br>
可接受的值是五个阿拉伯数字的一个序列,且每个数字的范围是 0-9:

```xml
<xs:element name="prodid">
  <xs:simpleType>
    <xs:restriction base="xs:integer">
      <xs:pattern value="[0-9][0-9][0-9][0-9][0-9]"/>
    </xs:restriction>
  </xs:simpleType>
</xs:element>
```

## 对一系列值的其他限定

定义了带有一个限定的名为 "letter" 的元素.<br>
可接受的值是 a - z 中零个或多个字母:

```xml
<xs:element name="letter">
  <xs:simpleType>
    <xs:restriction base="xs:string">
      <xs:pattern value="([a-z])*"/>
    </xs:restriction>
  </xs:simpleType>
</xs:element>
```

定义了带有一个限定的名为 "letter" 的元素.<br>
可接受的值是一对或多对字母,每对字母由一个小写字母后跟一个大写字母组成.<br>
举个例子,"sToP"将会通过这种模式的验证,但是 "Stop"、"STOP" 或者 "stop" 无法通过验证:

```xml
<xs:element name="letter">
  <xs:simpleType>
    <xs:restriction base="xs:string">
      <xs:pattern value="([a-z][A-Z])+"/>
    </xs:restriction>
  </xs:simpleType>
</xs:element>
```

定义了带有一个限定的名为 "gender" 的元素.<br>
可接受的值是 male 或者 female:

```xml
<xs:element name="gender">
  <xs:simpleType>
    <xs:restriction base="xs:string">
      <xs:pattern value="male|female"/>
    </xs:restriction>
  </xs:simpleType>
</xs:element>
```

定义了带有一个限定的名为 "password" 的元素.<br>
可接受的值是由 8 个字符组成的一行字符,这些字符必须是大写或小写字母 a - z 亦或数字 0 - 9:

```xml
<xs:element name="password">
  <xs:simpleType>
    <xs:restriction base="xs:string">
      <xs:pattern value="[a-zA-Z0-9]{8}"/>
    </xs:restriction>
  </xs:simpleType>
</xs:element>
```

## 对空白字符的限定

如需规定对空白字符(whitespace characters)的处理方式,需要使用 whiteSpace 限定.

定义了带有一个限定的名为 "address" 的元素.<br>
这个 whiteSpace 限定被设置为 "preserve",这意味着 XML 处理器不会移除任何空白字符:

```xml
<xs:element name="address">
  <xs:simpleType>
    <xs:restriction base="xs:string">
      <xs:whiteSpace value="preserve"/>
    </xs:restriction>
  </xs:simpleType>
</xs:element>
```

定义了带有一个限定的名为 "address" 的元素.<br>
这个 whiteSpace 限定被设置为 "replace",这意味着 XML 处理器将移除所有空白字符(换行、回车、空格以及制表符):

```xml
<xs:element name="address">
  <xs:simpleType>
    <xs:restriction base="xs:string">
      <xs:whiteSpace value="replace"/>
    </xs:restriction>
  </xs:simpleType>
</xs:element>
```

定义了带有一个限定的名为 "address" 的元素.<br>
这个 whiteSpace 限定被设置为 "collapse",这意味着 XML 处理器将移除所有空白字符(换行、回车、空格以及制表符会被替换为空格,开头和结尾的空格会被移除,而多个连续的空格会被缩减为一个单一的空格):

```xml
<xs:element name="address">
  <xs:simpleType>
    <xs:restriction base="xs:string">
      <xs:whiteSpace value="collapse"/>
    </xs:restriction>
  </xs:simpleType>
</xs:element>
```

## 对长度的限定

如需限制元素中值的长度,需要使用 length、maxLength 以及 minLength 限定.

本例定义了带有一个限定且名为 "password" 的元素.<br>
其值必须精确到 8 个字符:

```xml
<xs:element name="password">
  <xs:simpleType>
    <xs:restriction base="xs:string">
      <xs:length value="8"/>
    </xs:restriction>
  </xs:simpleType>
</xs:element>
```

定义了带有一个限定的名为 "password" 的元素.<br>
其值最小为 5 个字符,最大为 8 个字符:

```xml
<xs:element name="password">
  <xs:simpleType>
    <xs:restriction base="xs:string">
      <xs:minLength value="5"/>
      <xs:maxLength value="8"/>
    </xs:restriction>
  </xs:simpleType>
</xs:element>
```

## 数据类型的限定

限定|描述
-|-
enumeration|定义可接受值的一个列表.
fractionDigits|定义所允许的最大的小数位数.<br>必须大于等于0.
length|定义所允许的字符或者列表项目的精确数目.<br>必须大于或等于0.
maxExclusive|定义数值的上限.<br>所允许的值必须小于此值.
maxInclusive|定义数值的上限.<br>所允许的值必须小于或等于此值.
maxLength|定义所允许的字符或者列表项目的最大数目.<br>必须大于或等于0.
minExclusive|定义数值的下限.<br>所允许的值必需大于此值.
minInclusive|定义数值的下限.<br>所允许的值必需大于或等于此值.
minLength|定义所允许的字符或者列表项目的最小数目.<br>必须大于或等于0.
pattern|定义可接受的字符的精确序列.
totalDigits|定义所允许的阿拉伯数字的精确位数.<br>必须大于0.
whiteSpace|定义空白字符(换行、回车、空格以及制表符)的处理方式.

# XSD复合元素

复合元素包含了其他的元素及/或属性.

## 复合元素类型

类型|
-|
空元素|
包含其他元素的元素|
仅包含文本的元素|
包含元素和文本的元素|

上述元素均可包含属性

### 复合空元素

空的复合元素不能包含内容,只能含有属性.

一个空的 XML 元素:

```xml
<product prodid="1345" />
```

上面的 "product" 元素根本没有内容.<br>
为了定义无内容的类型,就必须声明一个在其内容中只能包含元素的类型,但是实际上并不会声明任何元素,比如这样:

```xml
<xs:element name="product">
  <xs:complexType>
    <xs:complexContent>
      <xs:restriction base="xs:integer">
        <xs:attribute name="prodid" type="xs:positiveInteger"/>
      </xs:restriction>
    </xs:complexContent>
  </xs:complexType>
</xs:element>
```

在上面的例子中,定义了一个带有复合内容的复合类型.<br>
complexContent 元素给出的信号是,打算限定或者拓展某个复合类型的内容模型,而 integer 限定则声明了一个属性但不会引入任何的元素内容.

但是,也可以更加紧凑地声明此 "product" 元素:

```xml
<xs:element name="product">
  <xs:complexType>
    <xs:attribute name="prodid" type="xs:positiveInteger"/>
  </xs:complexType>
</xs:element>
```

或者可以为一个complexType元素起一个名字,然后为"product"元素设置一个type属性并引用这个complexType名称(通过使用此方法,若干个元素均可引用相同的复合类型):

```xml
<xs:element name="product" type="prodtype"/>

<xs:complexType name="prodtype">
  <xs:attribute name="prodid" type="xs:positiveInteger"/>
</xs:complexType>
```

### 仅含元素

"仅含元素"的复合类型元素是只能包含其他元素的元素.

XML 元素,"person",仅包含其他的元素:

```xml
<person>
  <firstname>John</firstname>
  <lastname>Smith</lastname>
</person>
```

可在schema中这样定义"person"元素:

```xml
<xs:element name="person">
  <xs:complexType>
    <xs:sequence>
      <xs:element name="firstname" type="xs:string"/>
      <xs:element name="lastname" type="xs:string"/>
    </xs:sequence>
  </xs:complexType>
</xs:element>
```

它意味着被定义的元素必须按上面的次序出现在"person"元素中.

或者可以为complexType元素设定一个名称,并让"person"元素的type属性来引用此名称(如使用此方法,若干元素均可引用相同的复合类型):

```xml
<xs:element name="person" type="persontype"/>

<xs:complexType name="persontype">
  <xs:sequence>
    <xs:element name="firstname" type="xs:string"/>
    <xs:element name="lastname" type="xs:string"/>
  </xs:sequence>
</xs:complexType>
```

### 仅含文本

仅含文本的复合元素可包含文本和属性.

此类型仅包含简易的内容(文本和属性),因此要向此内容添加simpleContent元素.<br>
当使用简易内容时,就必须在simpleContent元素内定义扩展或限定,就像这样:

```xml
<xs:element name="somename">
  <xs:complexType>
    <xs:simpleContent>
      <xs:extension base="basetype">
        ....
        ....
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>
</xs:element>
```

或者:

```xml
<xs:element name="somename">
  <xs:complexType>
    <xs:simpleContent>
      <xs:restriction base="basetype">
        ....
        ....
      </xs:restriction>
    </xs:simpleContent>
  </xs:complexType>
</xs:element>
```

请使用 extension 或 restriction 元素来扩展或限制元素的基本简易类型.<br>
 这里有一个 XML 元素的例子,"shoesize",其中仅包含文本:

```xml
<shoesize country="france">35</shoesize>
```

下面这个例子声明了一个复合类型,其内容被定义为整数值,并且 "shoesize" 元素含有名为 "country" 的属性:

```xml
<xs:element name="shoesize">
  <xs:complexType>
    <xs:simpleContent>
      <xs:extension base="xs:integer">
        <xs:attribute name="country" type="xs:string" />
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>
</xs:element>
```

也可为complexType元素设定一个名称,并让"shoesize"元素的type属性来引用此名称(通过使用此方法,若干元素均可引用相同的复合类型):

```xml
<xs:element name="shoesize" type="shoetype"/>

<xs:complexType name="shoetype">
  <xs:simpleContent>
    <xs:extension base="xs:integer">
      <xs:attribute name="country" type="xs:string" />
    </xs:extension>
  </xs:simpleContent>
</xs:complexType>
```

### 包含元素和文本的元素

混合的复合类型可包含属性、元素以及文本.

XML元素,"letter",含有文本以及其他元素:

```xml
<letter>
  Dear Mr.<name>John Smith</name>.
  Your order <orderid>1032</orderid>
  will be shipped on <shipdate>2001-07-13</shipdate>.
</letter>
```

下面这个schema声明了这个"letter"元素:

```xml
<xs:element name="letter">
  <xs:complexType mixed="true">
    <xs:sequence>
      <xs:element name="name" type="xs:string"/>
      <xs:element name="orderid" type="xs:positiveInteger"/>
      <xs:element name="shipdate" type="xs:date"/>
    </xs:sequence>
  </xs:complexType>
</xs:element>
```

为了使字符数据可以出现在"letter"的子元素之间,mixed属性必须被设置为"true".<br>
\<xs:sequence> 标签 (name、orderid 以及 shipdate ) 意味着被定义的元素必须依次出现在 "letter" 元素内部.

也可以为complexType元素起一个名字,并让"letter"元素的type属性引用complexType 的这个名称(通过这个方法,若干元素均可引用同一个复合类型):

```xml
<xs:element name="letter" type="lettertype"/>

<xs:complexType name="lettertype" mixed="true">
  <xs:sequence>
    <xs:element name="name" type="xs:string"/>
    <xs:element name="orderid" type="xs:positiveInteger"/>
    <xs:element name="shipdate" type="xs:date"/>
  </xs:sequence>
</xs:complexType>
```

# XSD指示器

通过指示器,可以控制在文档中使用元素的方式.

## 指示器种类

种类|内容
-|-
Order指示器|All<br>Choice<br>Sequence
Occurrence指示器|maxOccurs<br>minOccurs
Group指示器|Group name<br>attributeGroup name

### Order指示器

Order指示器用于定义元素的顺序.

#### All指示器

\<all> 指示器规定子元素可以按照任意顺序出现,且每个子元素必须只出现一次:

```xml
<xs:element name="person">
  <xs:complexType>
    <xs:all>
      <xs:element name="firstname" type="xs:string"/>
      <xs:element name="lastname" type="xs:string"/>
    </xs:all>
  </xs:complexType>
</xs:element>
```

当使用\<all>指示器时,可以把\<minOccurs> 设置为0或者1,而只能把 \<maxOccurs> 指示器设置为1(稍后将讲解\<minOccurs>以及\<maxOccurs>).

#### Choice指示器

\<choice> 指示器规定可出现某个子元素或者可出现另外一个子元素(非此即彼):

```xml
<xs:element name="person">
  <xs:complexType>
    <xs:choice>
      <xs:element name="employee" type="employee"/>
      <xs:element name="member" type="member"/>
    </xs:choice>
  </xs:complexType>
</xs:element>
```

#### Sequence指示器

\<sequence> 规定子元素必须按照特定的顺序出现:

```xml
<xs:element name="person">
   <xs:complexType>
    <xs:sequence>
      <xs:element name="firstname" type="xs:string"/>
      <xs:element name="lastname" type="xs:string"/>
    </xs:sequence>
  </xs:complexType>
</xs:element>
```

### Occurrence指示器

Occurrence指示器用于定义某个元素出现的频率.

对于所有的"Order"和"Group"指示器(any、all、choice、sequence、group name 以及 group reference),其中的 maxOccurs 以及 minOccurs 的默认值均为 1.

#### maxOccurs指示器

\<maxOccurs>指示器可规定某个元素可出现的最大次数:

```xml
<xs:element name="person">
  <xs:complexType>
    <xs:sequence>
      <xs:element name="full_name" type="xs:string"/>
      <xs:element name="child_name" type="xs:string" maxOccurs="10"/>
    </xs:sequence>
  </xs:complexType>
</xs:element>
```

上面的例子表明,子元素"child_name"可在"person"元素中最少出现一次(其中minOccurs的默认值是1),最多出现10次.

#### minOccurs 指示器

\<minOccurs>指示器可规定某个元素能够出现的最小次数:

```xml
<xs:element name="person">
  <xs:complexType>
    <xs:sequence>
      <xs:element name="full_name" type="xs:string"/>
      <xs:element name="child_name" type="xs:string"
      maxOccurs="10" minOccurs="0"/>
    </xs:sequence>
  </xs:complexType>
</xs:element>
```

上面的例子表明,子元素 "child_name" 可在 "person" 元素中出现最少 0 次,最多出现 10 次.

如需使某个元素的出现次数不受限制,请使用 maxOccurs="unbounded" 这个声明:

##### 实例

名为"Myfamily.xml"的XML文件:

```xml
<?xml version="1.0" encoding="ISO-8859-1"?>

<persons xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:noNamespaceSchemaLocation="family.xsd">

<person>
  <full_name>Hege Refsnes</full_name>
  <child_name>Cecilie</child_name>
</person>

<person>
  <full_name>Tove Refsnes</full_name>
  <child_name>Hege</child_name>
  <child_name>Stale</child_name>
  <child_name>Jim</child_name>
  <child_name>Borge</child_name>
</person>

<person>
  <full_name>Stale Refsnes</full_name>
</person>

</persons>
```

上面这个XML文件含有一个名为"persons"的根元素.<br>
在这个根元素内部,定义了三个 "person"元素.<br>
每个"person"元素必须含有一个"full_name"元素,同时它可以包含多至5个"child_name"元素.

schema文件"family.xsd":

```xml
<?xml version="1.0" encoding="ISO-8859-1"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
elementFormDefault="qualified">

<xs:element name="persons">
  <xs:complexType>
    <xs:sequence>
      <xs:element name="person" maxOccurs="unbounded">
        <xs:complexType>
          <xs:sequence>
            <xs:element name="full_name" type="xs:string"/>
            <xs:element name="child_name" type="xs:string"
            minOccurs="0" maxOccurs="5"/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
</xs:element>

</xs:schema>
```

### Group指示器

Group指示器用于定义相关的数批元素.

#### 元素组

元素组通过 group 声明进行定义:

```xml
<xs:group name="groupname">
...
</xs:group>
```

必须在group声明内部定义一个all、choice或者sequence元素.<br>
下面这个例子定义了名为"persongroup"的group,它定义了必须按照精确的顺序出现的一组元素:

```xml
<xs:group name="persongroup">
  <xs:sequence>
    <xs:element name="firstname" type="xs:string"/>
    <xs:element name="lastname" type="xs:string"/>
    <xs:element name="birthday" type="xs:date"/>
  </xs:sequence>
</xs:group>
```

在把group定义完毕以后,就可以在另一个定义中引用它了:

```xml
<xs:group name="persongroup">
  <xs:sequence>
    <xs:element name="firstname" type="xs:string"/>
    <xs:element name="lastname" type="xs:string"/>
    <xs:element name="birthday" type="xs:date"/>
  </xs:sequence>
</xs:group>

<xs:element name="person" type="personinfo"/>

<xs:complexType name="personinfo">
  <xs:sequence>
    <xs:group ref="persongroup"/>
    <xs:element name="country" type="xs:string"/>
  </xs:sequence>
</xs:complexType>
```

#### 属性组

属性组通过attributeGroup声明来进行定义:

```xml
<xs:attributeGroup name="groupname">
...
</xs:attributeGroup>
```

下面例子中定义了名为"personattrgroup"的一个属性组:

```xml
<xs:attributeGroup name="personattrgroup">
  <xs:attribute name="firstname" type="xs:string"/>
  <xs:attribute name="lastname" type="xs:string"/>
  <xs:attribute name="birthday" type="xs:date"/>
</xs:attributeGroup>
```

在已定义完毕属性组之后,就可以在另一个定义中引用它了,就像这样:

```xml
<xs:attributeGroup name="personattrgroup">
  <xs:attribute name="firstname" type="xs:string"/>
  <xs:attribute name="lastname" type="xs:string"/>
  <xs:attribute name="birthday" type="xs:date"/>
</xs:attributeGroup>

<xs:element name="person">
  <xs:complexType>
    <xs:attributeGroup ref="personattrgroup"/>
  </xs:complexType>
</xs:element>
```

# XSD\<any>元素

# XSD\<anyAttribute>元素

# XSD元素替换(Element Substitution)

# 参考资料

> [菜鸟教程 | XML Schema教程](https://www.runoob.com/schema/schema-tutorial.html)
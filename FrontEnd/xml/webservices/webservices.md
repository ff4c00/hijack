>Web Services<br> 
是应用程序组件<br>
使用开放协议进行通信<br>
是独立的(self-contained)并可自我描述<br>
可通过使用UDDI来发现<br>
可被其他应用程序使用<br>
XML 是 Web Services 的基础

<!-- TOC -->

- [简介](#简介)
  - [它如何工作?](#它如何工作)
  - [Web services平台的元素](#web-services平台的元素)
- [Web Services平台元素](#web-services平台元素)
  - [什么是 SOAP?](#什么是-soap)
  - [什么是 WSDL?](#什么是-wsdl)
  - [什么是UDDI?](#什么是uddi)
- [Web Service实例](#web-service实例)
  - [服务端](#服务端)
  - [客户端](#客户端)
- [参考资料](#参考资料)

<!-- /TOC -->

# 简介

## 它如何工作?

基础的 Web Services 平台是 XML + HTTP.

HTTP 协议是最常用的因特网协议.

XML 提供了一种可用于不同的平台和编程语言之间的语言.

## Web services平台的元素

SOAP (简易对象访问协议)<br>
UDDI (通用描述、发现及整合)<br>
WSDL (Web services 描述语言)<br>

# Web Services平台元素

Web Services拥有三种基本的元素:SOAP、WSDL 以及 UDDI.

## 什么是 SOAP?

基本的 Web services 平台是 XML + HTTP.

SOAP 指简易对象访问协议<br>
SOAP 是一种通信协议<br>
SOAP 用于应用程序之间的通信<br>
SOAP 是一种用于发送消息的格式<br>
SOAP 被设计用来通过因特网进行通信<br>
SOAP 独立于平台<br>
SOAP 独立于语言<br>
SOAP 基于XML<br>
SOAP 很简单并可扩展<br>
SOAP 允许您绕过防火墙<br>
SOAP 将作为W3C标准来发展


## 什么是 WSDL?

WSDL 是基于 XML 的用于描述 Web Services 以及如何访问 Web Services 的语言.

WSDL 指网络服务描述语言<br>
WSDL 使用 XML 编写<br>
WSDL 是一种 XML 文档<br>
WSDL 用于描述网络服务<br>
WSDL 也可用于定位网络服务<br>
WSDL 还不是 W3C 标准

## 什么是UDDI?

UDDI 是一种目录服务,通过它,企业可注册并搜索 Web services.

UDDI 指通用的描述、发现以及整合(Universal Description, Discovery and Integration)<br>
UDDI 是一种用于存储有关web services的信息的目录<br>
UDDI 是一种由WSDL描述的网络服务接口目录<br>
UDDI 经由SOAP进行通迅<br>
UDDI 被构建于Microsoft .NET平台之中

# Web Service实例

任何应用程序都可拥有 Web Service 组件.

Web Service的创建与编程语言的种类无关.

下面内容将使用PHP的SOAP扩展来创建Web Service.

SOAP有两种操作方式:NO-WSDL 与 WSDL.

名称|作用
-|-
NO-WSDL模式|使用参数来传递要使用的信息.
WSDL模式| 使用WSDL文件名作为参数,并从WSDL中提取服务所需的信息.

## 服务端 

Server.php 文件代码如下:

```php
<?php 
// SiteInfo 类用于处理请求
Class SiteInfo
{
    /**
     *    返回网站名称
     *    @return string 
     *
     */
    public function getName(){
        return "菜鸟教程";
    }

    public function getUrl(){
        return "www.runoob.com";
    }
}

// 创建 SoapServer 对象
$s = new SoapServer(null,array("location"=>"http://localhost/soap/Server.php","uri"=>"Server.php"));

// 导出 SiteInfo 类中的全部函数
$s->setClass("SiteInfo");
// 处理一个SOAP请求,调用必要的功能,并发送回一个响应.
$s->handle();
?>
```

## 客户端 


Client.php 文件代码如下:

```php
<?php
try{
  // non-wsdl方式调用web service
  // 创建 SoapClient 对象
  $soap = new SoapClient(null,array('location'=>"http://localhost/soap/Server.php",'uri'=>'Server.php'));
  // 调用函数 
  $result1 = $soap->getName();
  $result2 = $soap->__soapCall("getUrl",array());
  echo $result1."<br/>";
  echo $result2;
} catch(SoapFault $e){
  echo $e->getMessage();
}catch(Exception $e){
  echo $e->getMessage();
}
```

# 参考资料

> [菜鸟教程 | Web Services教程](https://www.runoob.com/webservices/ws-intro.html)
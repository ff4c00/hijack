> 关于chrome浏览器调试工具用法记录

<!-- TOC -->

- [常用快捷键](#常用快捷键)
- [使用 Elements 调试 DOM](#使用-elements-调试-dom)
  - [查看与选择 DOM 节点](#查看与选择-dom-节点)
  - [实时编辑 HTML 和 DOM](#实时编辑-html-和-dom)
  - [在 Console 中访问节点](#在-console-中访问节点)
  - [在 DOM 中断点调试](#在-dom-中断点调试)
- [调试样式及 CSS](#调试样式及-css)
  - [查看与编辑 CSS](#查看与编辑-css)
  - [在元素中动态增加类与伪类](#在元素中动态增加类与伪类)
  - [快速调试 CSS 数值及颜色图形动画等](#快速调试-css-数值及颜色图形动画等)
- [使用 Console 和 Sources 调试 JavaScript](#使用-console-和-sources-调试-javascript)
  - [Console 面板简介与交互式命令](#console-面板简介与交互式命令)
  - [在 Console 中调试 Log 日志](#在-console-中调试-log-日志)
  - [调试 JavaScript 的基本流程以及如何使用断点调试](#调试-javascript-的基本流程以及如何使用断点调试)
  - [Sources 面板简介](#sources-面板简介)
  - [使用 Snippets 来辅助 Debugging](#使用-snippets-来辅助-debugging)
  - [使用 DevTools 作为代码编辑器](#使用-devtools-作为代码编辑器)
- [调试网络](#调试网络)
  - [Network 面板简介](#network-面板简介)
  - [使用 Network 详细分析请求](#使用-network-详细分析请求)
  - [使用 Network Waterfall 分析页面载入性能](#使用-network-waterfall-分析页面载入性能)
- [客户端存储 Application 面板](#客户端存储-application-面板)
  - [查看与调试 Cookie](#查看与调试-cookie)
  - [查看与调试 LocalStorage 与 SessionStorage](#查看与调试-localstorage-与-sessionstorage)
- [调试移动端、H5 页面及远程调试](#调试移动端h5-页面及远程调试)
  - [模拟移动设备](#模拟移动设备)
  - [使用 Chrome DevTools 进行 H5 页面开发](#使用-chrome-devtools-进行-h5-页面开发)
- [在 DevTools 中集成 React 和 Vue 插件](#在-devtools-中集成-react-和-vue-插件)
  - [集成 React 插件](#集成-react-插件)
  - [集成 Vue 插件](#集成-vue-插件)
- [参考资料](#参考资料)

<!-- /TOC -->

# 常用快捷键

快捷键|作用
-|-
Ctrl+Shift+I|打开最近关闭的状态
Ctrl+Shift+C|查看DOM或样式
Ctrl+Shift+J|进入Console查看log或运行JS
F12|直接打开

# 使用 Elements 调试 DOM

> 如何使用元素面板来查看、编辑和调试HTML和DOM

浏览器拿到返回html页面结构的时候会将其解析为DOMTree.

## 查看与选择 DOM 节点



## 实时编辑 HTML 和 DOM



## 在 Console 中访问节点

在Elements中:

0. 选中元素 
0. 右键 
0. 复制 
0. 选中js路径

可以复制控制该元素的js方法.


## 在 DOM 中断点调试



# 调试样式及 CSS

> 开发网页样式中常用的的基本方法

## 查看与编辑 CSS

在样式模块中优先级由上到下递减.

## 在元素中动态增加类与伪类



## 快速调试 CSS 数值及颜色图形动画等

选中元素后在element.style的右侧有一个按钮,鼠标放上去可以对文字阴影等进行编辑.

调试动画:

0. 菜单栏
0. More tools
0. animations

animations.css网站可快速调试动画效果.

# 使用 Console 和 Sources 调试 JavaScript

> 本章是课程最核心内容之一,你将掌握调试页面JavaScript脚本最常用的方法,包括调试Log,断点调试,在源代码中调试等等.学完本章内容将辅助你更快速地在日常开发工作中调试脚本,让你事半功倍

## Console 面板简介与交互式命令



## 在 Console 中调试 Log 日志

vscode的 live server插件可以实时将文件变化反映到浏览器中

## 调试 JavaScript 的基本流程以及如何使用断点调试



## Sources 面板简介

点击右下角的 `{}`可以格式化压缩后的代码.

## 使用 Snippets 来辅助 Debugging



## 使用 DevTools 作为代码编辑器



# 调试网络

> 本章也是课程核心内容之一,使用Network面板将让你非常容易的对页面请求进行抓包,分析以及做性能优化

## Network 面板简介



## 使用 Network 详细分析请求



## 使用 Network Waterfall 分析页面载入性能



# 客户端存储 Application 面板

> 本章介绍如何调试客户端存储,包括学会如何对Cookie,LocalStorage等调试

## 查看与调试 Cookie



## 查看与调试 LocalStorage 与 SessionStorage



# 调试移动端、H5 页面及远程调试

> 本章介绍使用Chrome开发这工具针对移动端浏览器进行页面的调试

## 模拟移动设备



## 使用 Chrome DevTools 进行 H5 页面开发



# 在 DevTools 中集成 React 和 Vue 插件

> 本章介绍如何安装React和Vue插件,结合现代流程框架的插件调试会上你更容易的针对构建前的组件及状态进行跟踪

## 集成 React 插件



## 集成 Vue 插件




# 参考资料

> [慕课 | Chrome DevTools开发者工具调试指南](https://www.imooc.com/learn/1164)

<!-- TOC -->

- [说明](#说明)
  - [关于供电顺序](#关于供电顺序)
  - [示意图](#示意图)
  - [添加依赖库](#添加依赖库)
  - [文件类型](#文件类型)
    - [.ino](#ino)
  - [串口权限问题](#串口权限问题)
- [语法](#语法)
  - [C字符串](#c字符串)
  - [逗号分隔文本拆成数组](#逗号分隔文本拆成数组)
  - [字符串转化为数字](#字符串转化为数字)
  - [代码划分为多个功能块](#代码划分为多个功能块)
  - [函数返回多个值](#函数返回多个值)
  - [根据条件采取行动](#根据条件采取行动)
  - [重复一个语句序列](#重复一个语句序列)
  - [使用计数器重复执行语句](#使用计数器重复执行语句)
  - [跳出循环](#跳出循环)
  - [基于单个变量进行不同操作](#基于单个变量进行不同操作)
  - [字符和数量值的比较](#字符和数量值的比较)
  - [字符串的比较](#字符串的比较)
  - [逻辑比较操作](#逻辑比较操作)
  - [执行位运算](#执行位运算)
  - [复合运算和赋值](#复合运算和赋值)
- [数据类型](#数据类型)
  - [浮点数](#浮点数)
    - [检测两个浮点数是否足够接近](#检测两个浮点数是否足够接近)
  - [数组](#数组)
  - [String](#string)
    - [常用方法](#常用方法)
- [数学运算符](#数学运算符)
- [引脚](#引脚)
  - [模拟输入引脚](#模拟输入引脚)
  - [模拟输出引脚](#模拟输出引脚)
- [常用函数](#常用函数)
  - [函数应用示例](#函数应用示例)
    - [示例1-控制LED灯开启/关闭](#示例1-控制led灯开启关闭)
- [串口通信](#串口通信)
  - [打印输出](#打印输出)
    - [F()函数](#f函数)
    - [print和println](#print和println)
  - [控制台查看输出信息](#控制台查看输出信息)
- [模拟输入](#模拟输入)
  - [检测开关闭合(去抖)](#检测开关闭合去抖)
  - [读取模拟值](#读取模拟值)
- [传感器输入](#传感器输入)
  - [温度测量](#温度测量)
    - [LM35](#lm35)
    - [DHT11](#dht11)
      - [引脚说明](#引脚说明)
      - [相关依赖](#相关依赖)
      - [示例代码](#示例代码)
- [可视输出](#可视输出)
- [物理输出](#物理输出)
  - [控制电磁铁和继电器](#控制电磁铁和继电器)
- [音频输出](#音频输出)
- [遥控外部设备](#遥控外部设备)
- [显示屏的使用](#显示屏的使用)
  - [1602](#1602)
    - [中文乱码](#中文乱码)
- [时间和日期的使用](#时间和日期的使用)
- [I2C和SPI通信](#i2c和spi通信)
- [无线通信](#无线通信)
- [以太网和网络](#以太网和网络)
- [库的使用,修改和创建](#库的使用修改和创建)
- [高级编程和内存操作](#高级编程和内存操作)
- [使用控制器芯片的硬件](#使用控制器芯片的硬件)
- [常见问题](#常见问题)
  - ["/dev/ttyUSB0": Permission denied](#devttyusb0-permission-denied)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

## 关于供电顺序

当电路板同时使用USB以及外部9V直流变压器供电时,优先以外部外接电源供电.

## 示意图

![](https://img-blog.csdnimg.cn/20190424154621367.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2NoZW5jaGVuMjM2MDA2MA==,size_16,color_FFFFFF,t_70)

## 添加依赖库

ArduinoIDE -> Sketch -> Include Library -> Add .Zip Library

## 文件类型

### .ino

> INO file is an Arduino Sketch. Arduino is an open-source electronic prototyping platform allowing to create interactive electronic objects. Software written using Arduino are called sketches. These sketches are written in the text editor. Sketches are saved with the file extension .ino.

> ino文件是一个arduino草图.<br>
Arduino是一个开源的电子原型平台,允许创建交互式电子对象.<br>
使用Arduino编写的软件称为草图.<br>
这些草图是在文本编辑器中编写的.<br>
草图以文件扩展名.ino保存.

## 串口权限问题

```bash
# 查看设备所属用户组
ls -l /dev/ttyUSB0
# => crw-rw---- 1 root dialout 188, 0 10月 28 21:57 /dev/ttyUSB0

# 将当前用户加入到组中
# sudo usermod -a -G 组名 用户名
sudo usermod -a -G dialout ff4c00
# =>
```

# 语法

## C字符串

```c

```

## 逗号分隔文本拆成数组

```c

```

## 字符串转化为数字

```c

```

## 代码划分为多个功能块

```c

```

## 函数返回多个值

通过更改全局变量实现.

## 根据条件采取行动

```c

```

## 重复一个语句序列

```c

```

## 使用计数器重复执行语句

```c

```

## 跳出循环

```c

```

## 基于单个变量进行不同操作

```c

```

## 字符和数量值的比较

```c

```

## 字符串的比较

```c

```

## 逻辑比较操作

```c

```

## 执行位运算

```c

```

## 复合运算和赋值

```c

```

# 数据类型

数据类型|说明
-|-
int|表示正和负整数值
unsigned int|仅表示正的值,否则和int相同
long|代表一个非常大范围的正和负值
Unsigned long|大范围正值
float|带小数部分的数字
double|float的别名
boolean|代表true和false
char|代表*单个字符*,也可以代表-128~127之间的有符号值
Byte|类似char但代表0~255之间的无符号值
String|代表char阵列,用于包含文本
void|仅用在无返回值的函数声明

## 浮点数

```c
// 声明浮点型变量
float value = 0.1;

// 打印到7位小数
Serial.print(value, 7);
```

### 检测两个浮点数是否足够接近

```c
boolean almostEqual(folat a, folat b){
  // 视作几乎相等的最大差值
  const float DELTA = .00001
  // fabs用于取绝对值
  if (a == 0) return fabs(b) <= DELTA;
  if (b == 0) return fabs(a) <= DELTA;
  return fabs((a - b) / max(fabs(a), fabs(b))) <= DELTA;
}
```

## 数组

```c
// 声明4个元素的数组
int arrays[4];

// 声明数组并赋值
int arrays[] = {1,2,3,4,5};
```

## String

```c
// 声明字符串变量并赋值
String text = 'text';
```

### 常用方法

方法名|作用
-|-
charAt(n)|返回String的第n个字符
compareTo(S2)|比较给定的String和S2
concat(S2)|返回一个新的String作为String和S2的组合
endsWith(S2)|判断String是否以S2结尾
equals(S2)|判断String和S2是否精确匹配(区分大小写)
equalsignoreCase(S2)|与equals类似但不区分大小写
getBytes(buffer, len)|复制len字符到提供的字节缓冲区
indexOf(S)|返回所提供字符的索引,未找到则返回-1
lastIndexOf(S)|与indexOf类似但从末尾开始
length()|返回String的字符数
replace(A,B)|替换String中的A为B


# 数学运算符



# 引脚

标准板引脚共14个引脚,编号0~13.

## 模拟输入引脚

> 用于读取各种模拟输入信号,如感应器的电压,是否有电流经过等,<br>
并在程序中将其转换成0~1023之间的数值.

## 模拟输出引脚

> 可以理解为电源+极,用于输出电流.

# 常用函数

名称|作用|备注
-|-|-
setup()|初始化时执行一次|
loop()|代码块中的代码会不断重复执行|Arduino程序不允许同时执行多个函数,<br>也没有退出或关闭程序功能,<br>整个程序的开始和停止取决于电路板电源的开启或关闭.
\#define|将一个数值定义常数|将13定义为LED:<br>#definde LED 13
const|用于定义常数|
OUTPUT<br>INPUT<br>HIGH<br>LOW|预定义常数|程序中不可以有其他同名变量或常数,<br>而且这些常数的值也是不会改变的.
pinMode(引脚号, 输入/输出)|为引脚模式进行定义|
digitalWrite(引脚号, 开/关)|为引脚状态进行赋值(引脚通电/断电)|
delay(毫秒数)|设定程序休眠时间

## 函数应用示例

### 示例1-控制LED灯开启/关闭

```ino
// 设定常数LED的值为数字13
#define LED 13

// 除#define函数外,其他函数均以;结尾,表示该行代码结束.

void setup(){
  // 设定13号引脚为输出模式
  pinMode(LED, OUTPUT);
}

void loop(){
  // 13号引脚进行通电
  digitalWrite(LED, HIGH);

  // 程序休眠1秒钟(1秒等于1000毫秒)
  delay(1000);

  // 13号引脚断开电流
  digitalWrite(LED, LOW)

  // 程序休眠1秒钟
  delay(1000);

}
```

# 串口通信

## 打印输出

```arduino
void setup(){
  // 设定发送和接收的波特率
  // 程序必须调用Serial.begin()函数才可以使用串口输入或输出
  // 发送方和接收方必须使用相同的通信速度,否则屏幕上将显示乱码或什么都没有
  Serial.begin(9600);
  // ...
}

int number = 0;

void loop (){
  // Serial.print()函数显示文本,字符串将被原样打印
  Serial.print("The number is ");
  // Serial.println()用于打印数字类型的变量值
  Serial.println(number);
  delay(500);
  number ++;
}
```

### F()函数

> 在打印输出时如果加上 F(), 当中的string会放到flash memory 中, 而不会占用SRAM.<br>
Arduino中的SRAM非常珍貴, 相对而言flash memory比较充足.

```c
Serial.print(F("湿度: "));
```

### print和println

print输出后不会进行换行, println则会.

## 控制台查看输出信息

Arduino -> Tools -> Serial Monitor<br>
&emsp;&emsp;&emsp;&emsp;-> 快捷键: Ctrl+Shift+M

# 模拟输入

## 检测开关闭合(去抖)

> 在开关触点闭合或打开的瞬间,触点反弹产生杂散信号,消除杂散信号的过程称为去抖.

![](https://imgsa.baidu.com/exp/w=480/sign=7150003c62380cd7e61ea3e59145ad14/fcfaaf51f3deb48f4d7a5e52f71f3a292df5782f.jpg)

```arduino
/*
 * 连接到引脚2的开关点亮引脚13上的LED
 * 通过去抖逻辑防止开关状态的误读
 * 在去抖周期内,每隔固定时间检测当前开关状态
 * 如果发生变化,跳出循环返回当前状态
 * 否则跳出循环后返回当前状态值
 */

// 设定输入引脚号
const int inputPin = 2;
// 设定led引脚号
const int ledPin = 13;
// 稳定之前的等待毫秒数
const int debounceDelay = 10;

// 如果给定的引脚上的开关是闭合且稳定状态, debounce返回true
boolean debounce(int pin){
  boolean state;
  boolean previousState;

  // 保存开关状态
  previousState = digitalRead(pin);

  for(int counter = 0; counter < debounceDelay; counter++){
    // 等待1毫秒
    delay(1);
    // 读取引脚状态
    state = digitalRead(pin);
    if (state != previousState){
      // 如果状态变化,复位计数器
      counter = 0;
      // 保存当前状态
      previousState = state;
    }
  }

  // 此开关状态长于去抖周期的时间内一直稳定
  return state;
}

void setup(){
  pinMode(inputPin, INPUT);
  // 一定要设置引脚模式,否则读取到的永远是0
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
}

int state = 0;

void loop(){
  if (debounce(inputPin)){
    state += 1;
    Serial.print("The state is ");
    Serial.println(state);
    delay(200);
  };

  if ((state %= 2) == 0){
    digitalWrite(ledPin, LOW);
  }else{
    digitalWrite(ledPin, HIGH);
  };
}
```

## 读取模拟值

> 用于读取模拟引脚上提供0-5V之间电压的电位器或设备或传感器的电压.

```arduino
void loop(){
  // 读取13号引脚上的电压
  analogRead(13);
}
```

# 传感器输入

## 温度测量

### LM35

```arduino
// 设定inPin常数值为0
const int inPin = 0;

void setup(){
  Serial.begin(9600);
}

void loop(){
  // 读取inPin对应引脚号上引脚的电压
  int value = analogRead(inPin);
  // 将值转换为毫伏
  float millivolts = (value / 1024.0) * 5000;
  // 传感器输出为10 mv/℃
  float celsius = millvolts / 10;
  Serial.print("当前温度为:");
  Serial.print(celsius);
}
```

### DHT11

#### 引脚说明
![](http://c.51hei.com/d/forum/201706/16/025346qjoymrr7pw7ss5op.png)


#### 相关依赖

[Github | Adafruit_Sensor](https://github.com/adafruit/Adafruit_Sensor)

[Github | DHT-sensor-library](https://github.com/adafruit/DHT-sensor-library)

[Github | DHT-sensor-library示例](https://github.com/adafruit/DHT-sensor-library/blob/master/examples/DHT_Unified_Sensor/DHT_Unified_Sensor.ino)


#### 示例代码

```arduino
#include "DHT.h"
// DATA引脚连接2号数字输入引脚
#define DHTPIN 2
// Feather HUZZAH ESP8266 注意: 使用引脚3、4、5、12、13或14
// 引脚15可以工作,但是在程序上传过程中必须断开DHT.

// Uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11
// #define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
// #define DHTTYPE DHT21   // DHT 21 (AM2301)

// 将传感器的引脚1(左侧)连接到+ 5V
// 注意：如果使用具有3.3V逻辑的板(例如Arduino Due),请连接引脚1
// 至3.3V而不是5V！
// 将传感器的引脚2连接到您的DHTPIN上
// 将传感器的第4针(右侧)连接到GROUND
// 在传感器的引脚2(数据)和引脚1(电源)之间连接一个10K电阻

// 初始化DHT传感器.
// 请注意,该库的旧版本采用了可选的第三个参数
// 调整时间以使用更快的处理器.不再需要此参数
// 因为当前的DHT读取算法会自行调整以适应更快的处理速度.
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  Serial.println(F("DHT11 test!"));

  dht.begin();
}

void loop() {
  // Wait a few seconds between measurements.
  delay(2000);

  // 读取温度或湿度大约需要250毫秒
  // 传感器速度非常慢的话可能需要2秒
  float h = dht.readHumidity();
  // 将温度读取为摄氏温度(默认值)
  float t = dht.readTemperature();
  // 将温度读取为华氏度(isFahrenheit = true)
  float f = dht.readTemperature(true);

  // 检查是否有任何读取失败并提早退出(重试)
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }

  // 以华氏度计算热量指数(默认值)
  float hif = dht.computeHeatIndex(f, h);
  // 计算摄氏温度指数(isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.print(F("%  Temperature: "));
  Serial.print(t);
  Serial.print(F("°C "));
  Serial.print(f);
  Serial.print(F("°F  Heat index: "));
  Serial.print(hic);
  Serial.print(F("°C "));
  Serial.print(hif);
  Serial.println(F("°F"));
}
```

# 可视输出



# 物理输出

## 控制电磁铁和继电器


![](https://pic2.zhimg.com/80/v2-cbf35540f6c23f9af2dbc869b1101701_hd.jpg)

![](http://www.soxitoday.com/wp-content/uploads/2018/10/beepress7-1539668285.jpeg)

```arduino

```

# 音频输出



# 遥控外部设备



# 显示屏的使用

## 1602

![](https://upload-images.jianshu.io/upload_images/1670644-a789cad0adc5a131.png?imageMogr2/auto-orient/strip|imageView2/2/w/1012/format/webp)

**背光有极性,引脚15要接+5V,16接地.**

示例代码:

File -> Examples -> LiquidCrystal -> HelloWorld

```c
#include <LiquidCrystal.h>

// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

void setup() {
  // 设置LCD的列数和行数
  lcd.begin(16, 2);
  // 打印输出
  lcd.print("hello, world!");
}

void loop() {
  // 将光标设置为第0列第1行(换行打印)
  lcd.setCursor(0, 1);
  lcd.print(millis() / 1000);
}
```

![](https://4c3q4z2euxqn29mk0e34ayce-wpengine.netdna-ssl.com/wp-content/uploads/2014/05/LCD-indexing_31-e1400082055535.png)

### 中文乱码

[TODO](https://blog.csdn.net/weixin_33805743/article/details/93243021)

# 时间和日期的使用



# I2C和SPI通信



# 无线通信



# 以太网和网络



# 库的使用,修改和创建



# 高级编程和内存操作



# 使用控制器芯片的硬件

# 常见问题

## "/dev/ttyUSB0": Permission denied

> avrdude: ser_open(): can't open device "/dev/ttyUSB0": Permission denied

```bash
sudo chmod 777 /dev/ttyUSB0
sudo usermod -aG　dialout 用户名
```

# 参考资料

> [Arduino权威指南]()

> [Arduino教程 | DHT11数字温湿度传感器(Ⅱ)](https://www.arduino.cn/forum.php?mod=viewthread&tid=3604&highlight=DHT11)

> [CSDN | arduino学习系列——DHT11温湿度传感器的使用](https://blog.csdn.net/wgj99991111/article/details/53749144)

>[51黑 | 温湿度传感器DHT11引脚图](http://www.51hei.com/bbs/dpj-87978-1.html)

> [51黑 | 【Arduino】108种传感器模块系列实验(资料+代码+图形+仿真](http://www.51hei.com/bbs/dpj-159232-1.html)

> [Arduino公开课](http://ardui.co/?page_id=2)

> [filedesc | 详细的文件扩展名 .ino](https://www.filedesc.com/zh/file/ino)
/*
通过温度控制风扇状态
  读取风扇引脚当前状态
  获取目标当前温度
  目标温度是否大于等于设定温度
    否>
      当前风扇状态
        开启> 关闭风扇
        关闭> 无操作
      记录当前时间,目标温度,风扇状态等关键日志
      休眠1分钟
    是>
      当前风扇状态
        开启> 无操作
        关闭> 打开风扇
      记录当前时间,目标温度,风扇状态等关键日志
      休眠2分钟
 */

#include "DHT.h"
#include <LiquidCrystal.h>

#define DHTPIN 6
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

// 定义常量
// 风扇停转的最高温度(摄氏度)
const float maxTeC = 28.0;
// 风扇引脚
const int fanPin = 13;

// 定义全局变量
// 目标当前湿度
float hum;
// 目标当前摄氏温度
float teC;
// 风扇状态
int fanState;

void clean_row(int row){
  lcd.setCursor(0, row);
  lcd.print(F("                "));
}

void clean_screen(){
  // 显示屏初始化
  lcd.begin(16, 2);
  // clean_row(0);
  // clean_row(1);
}

// 记录日志
void writeLog(){
  // 读取风扇引脚当前状态
  // 应对风扇状态改变后变量存储的还是旧状态情况
  // fanState = digitalRead(fanPin);
  clean_screen();
  lcd.setCursor(0, 0);
  lcd.print(F("Hum:"));
  lcd.print(hum);
  lcd.print(F("%"));
  lcd.setCursor(0, 1);
  lcd.print(F("Tem:"));
  lcd.print(teC);
  lcd.print(F("C"));
}

// 获取温度 返回是否读取成功
boolean getTeC(){
  // 获取目标当前温度
  hum = dht.readHumidity();
  // 将温度读取为摄氏温度(默认值)
  teC = dht.readTemperature();

  // 检查是否有任何读取失败并提早退出(重试)
  if (isnan(hum) || isnan(teC)) {
    return false;
  }
  return true;
}

// 控制风扇状态
void controlFanState(){
  // 读取风扇引脚当前状态
  fanState = digitalRead(fanPin);

  if ( teC >= maxTeC ){
    if (fanState == LOW) {
      digitalWrite(fanPin, HIGH);
    }
    // 记录日志
    writeLog();
    delay(120000);
  }else{
    if (fanState == HIGH) {
      digitalWrite(fanPin, LOW);
    }
    // 记录日志
    writeLog();
    delay(60000);
  };
}

void setup() {
  pinMode(fanPin, OUTPUT);
  dht.begin();

  // 显示屏初始化
  lcd.begin(16, 2);
}

void loop() {
  delay(2000);

  // 获取温度
  if (!getTeC()){
    // 获取温度失败,打开风扇并重新执行代码
    clean_screen();
    lcd.setCursor(0, 0);
    lcd.print(F("Failed read DHT!"));
    digitalWrite(fanPin, HIGH);
    return;
  }

  // 控制风扇状态
  controlFanState();

}

> 自动绘图语言.

<!-- TOC -->

- [基础](#基础)
  - [文件扩展名](#文件扩展名)
  - [主要对象](#主要对象)
- [参考资料](#参考资料)

<!-- /TOC -->

# 基础

## 文件扩展名

\*.dot后缀.

## 主要对象

主要有三种对象: 图 graph, 边 edge, 节点 node.<br>
其中graph 又包括有向图(directed graph) digraph 和 无向图graph.<br>
在图内,{} 大括号中定义的内容(节点和边)叫做 subgraph.

属性直接写出来 --> 为图设置属性
节点和边的属性定义在方括号中

# 参考资料

> [Github | DOT语言学习笔记 ](https://github.com/uolcano/blog/issues/13)
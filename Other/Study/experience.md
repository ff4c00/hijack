<!-- TOC -->

- [一二三四学习方法](#一二三四学习方法)
- [关于新知识入门](#关于新知识入门)
- [关于公众号及微博等学习平台的利用](#关于公众号及微博等学习平台的利用)
- [什么是有效问题](#什么是有效问题)
- [应对不良问题](#应对不良问题)
- [提高元认知能力](#提高元认知能力)
- [如何阅读](#如何阅读)
  - [检视阅读](#检视阅读)
  - [分析阅读](#分析阅读)
  - [注意事项](#注意事项)
  - [元认知](#元认知)
  - [努力做到](#努力做到)

<!-- /TOC -->
# 一二三四学习方法

一二三四指的是:

> 一个前提

学会提问

> 两组概念

0. 知识的分类 
0. 认知的层次

> 三个技能

0. 思维过滤
0. 个性笔记
0. 高效记忆

> 四步提升

0. 知识体系
0. 学习迁移
0. 元认知能力
0. 知觉学习

# 关于新知识入门

学习一门新的语言可以先找一本业内权威的书籍,<br>
不需要从头看到尾,而是把目录结构及每章标题整理汇总下来.<br>
书籍尤其是权威的知识体系会比较全面而且逻辑清晰,这样关于这门知识有哪些分类,涉及到哪些知识要做到大致清楚.<br>
这样在需要用到或者解决实际问题时会知道去哪里找答案,<br>
而且也会知道这块知识点和哪里有联系.<br>
如果找不到具体书可以到京东等平台的商品详情里面也会有目录介绍.

很多在线课程会提供课程大纲,可以参考其大纲与自己的知识体系进行对比,查漏补缺.

# 关于公众号及微博等学习平台的利用

秉承学以致用,每天推送内容可以不看,当需要了解某些方面内容可以去搜索历史文章看看有没有所需的内容.

# 什么是有效问题

0. 目标: 想要达到的最终状态是什么
0. 现况: 现已经知道的信息都有什么
0. 策略: 接近目标所需的步骤和行动

一个好的问题的解决策略,其实就是根据已有信息达成目标的可行操作.

# 应对不良问题

0. 分析问题的结构
0. 筛选并挖掘重要的信息
0. 界定问题,寻找方法

# 提高元认知能力

0. 学会发现并界定问题
0. 注重分析相似知识的差异
0. 学习一般的问题解决策略


# 如何阅读

## 检视阅读

0. 有系统地略读或粗读<br>序/目录/后记/附录/随意翻阅
0. 初读: 囫囵吞枣,不求甚解即可

## 分析阅读

0. 这本书在谈什么?(WHAT)<br>明确图书的分类<br>叙述整本书的大意<br>列出整本书的大纲<br>发现作者的提问
0. 作者是怎么谈的?(HOW)<br>明确关键词定义<br>抓住重要主旨<br>找出重要论述<br>找出作者解答
0. 作者这么讲对吗?(WHY)
0. 这本书和我有什么关系?

## 注意事项

0. 确立目标,采取适宜的阅读策略
0. 明晰重点,集中注意并付出努力
0. 利用已有知识理解新知识的意义
0. 利用图表和其他的视觉材料理解
0. 精细化: 推断,分析,应用...
0. 向自己提问,谨防"知识错觉"
0. 保持开放: 知识和概念总会变化
0. 批判性的评价和总结所读的内容

## 元认知

0. 这一主题我知道些什么?
0. 我需要多少时间学习这些内容?
0. 解决这一问题的最佳计划是什么?
0. 我预测的结果是什么?评价结果的标准是什么?
0. 我怎么调整程序?
0. 怎么发现犯的错误?
0. 我理解自己阅读的东西吗?

## 努力做到

0. 建立目标
0. 做好计划
0. 自我驱动
0. 控制注意
0. 利用有效学习策略
0. 自我监控
0. 自我评价
0. 自我反省
0. 适当寻求帮助



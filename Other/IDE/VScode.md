<!-- TOC -->

- [常用插件](#常用插件)
- [常用快捷键](#常用快捷键)
- [常用配置](#常用配置)
  - [格式化空格数量](#格式化空格数量)
- [常用命令](#常用命令)
- [Java开发环境搭建](#java开发环境搭建)
  - [安装JDK](#安装jdk)
  - [Java Extension Pack](#java-extension-pack)
  - [CodeLens](#codelens)
  - [Spring Boot](#spring-boot)
    - [maven](#maven)
    - [Spring Initializr Java Support](#spring-initializr-java-support)
      - [编辑pom.xml](#编辑pomxml)
    - [Spring Boot Dashboard](#spring-boot-dashboard)
    - [Spring Boot Extension Pack](#spring-boot-extension-pack)
    - [配置相关信息](#配置相关信息)
- [Arduino 开发环境](#arduino-开发环境)
  - [安装](#安装)
  - [Arduino IDE](#arduino-ide)
    - [Arduino插件](#arduino插件)
      - [vscode-cpptools](#vscode-cpptools)
  - [快捷键](#快捷键)
  - [命令行选项](#命令行选项)
  - [选项](#选项)
  - [调试Arduino代码](#调试arduino代码)
- [远程开发](#远程开发)
  - [环境准备](#环境准备)
  - [使用](#使用)
    - [连接远程目标](#连接远程目标)
- [同步插件及配置](#同步插件及配置)
  - [Settings Sync](#settings-sync)
    - [创建Gist](#创建gist)
    - [插件安装](#插件安装)
- [常见问题](#常见问题)
  - [markdown-toc](#markdown-toc)
    - [目录展示异常](#目录展示异常)
- [参考资料](#参考资料)

<!-- /TOC -->

# 常用插件

名称|作用
-|-
Beautify|格式化代码插件
Chinese (Simplified) Language|VS Code 的中文(简体)语言包
Git History|git 提交记录查看工具
GitLens|显示当前行的commit信息
Markdown Preview Mermaid Support|Markdown流程图工具
Markdown TOC|为Markdown生成目录
HTML CSS Support<sup>*</sup>|html 代码补全

> HTML CSS Support

用户设置中添加如下代码:
```json
"editor.parameterHints": true,
"editor.quickSuggestions": { "other": true, "comments": true, "strings": true }
```

# 常用快捷键

快捷键|作用
-|-
Ctrl+Shift+I|格式化代码
Ctrl+J|开启/关闭终端面板

# 常用配置

名称|作用
-|-
files.trimTrailingWhitespace|文件保存后自动格式化多余空格

## 格式化空格数量

右下角的Spaces展示的是当前使用的空格数量,点击可以进行修改.

# 常用命令

将当前文件夹用vscode打开:

```
code .
```

# Java开发环境搭建

## 安装JDK

[OpenJDK安装参考](../../Linux/Apps/jdk.md)

## Java Extension Pack

> vscode java 扩展包.

打开命令选项板(Ctrl+Shift+P),可输入命令:

```bash
Java: Getting Started # 快速入门指南
Java: Configure Java Runtime # 配置运行时环境
Java: Create Java Project # 创建 Eclipse样式项目
```

[Java Extension Pack](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack)


## CodeLens

> 查看方法定义

## Spring Boot

### maven

[maven.md](../../Linux/Apps/maven.md)

### Spring Initializr Java Support

> 基于Spring Initializr的轻量级扩展,用于生成快速启动Spring Boot Java项目.

命令板命令:

```bash
Spring Initializr # 开始生成Maven或Gradle项目
```

[Spring Initializr Java Support](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-spring-initializr)

#### 编辑pom.xml

右键单击以选择Edit starters<br>
在命令面板会显示已经有开始的依赖关系√.<br>
可以搜索要添加到项目中的其他依赖项.<br>
或者单击现有依赖项以删除它们.

### Spring Boot Dashboard

[Spring Boot Dashboard](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-spring-boot-dashboard)

### Spring Boot Extension Pack

### 配置相关信息

File->Preferences->settings->Settings->Commonly Used->Edit in settings.json<br>
或Ctrl+,->Commonly Used->Edit in settings.json<br>
或直接打开: ~/.config/Code/User/settings.json

```json
{
  "java.home": "/usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java",
  "java.configuration.maven.userSettings": "/home/ff4c00/space/software/apache-maven-3.6.2/conf/settings.xml",
  "maven.executable.path": "/home/ff4c00/space/software/apache-maven-3.6.2/bin/mvn"
}
```

# Arduino 开发环境

## 安装

## Arduino IDE

Arduino IDE是必需的, 可以从[官网](https://www.arduino.cc/en/main/software#download)下载.

### Arduino插件

[Arduino插件](https://github.com/Microsoft/vscode-arduino)

#### vscode-cpptools

> If you work in an offline environment or repeatedly see this error, try downloading a version of the extension with all the dependencies pre-included from https://github.com/Microsoft/vscode-cpptools/releases, then use the "Install from VSIX" command in VS Code to install it.

安装Arduino插件后命令行输出以上信息,根据提示:

0. 在[Github](https://github.com/Microsoft/vscode-cpptools/releases)下载页面下载相应版本如: cpptools-linux.vsix.
0. Ctrl+Shift+P, 输入Install from VSIX,选择下载文件.
0. 重启VSCode即可.

## 快捷键

快捷键|作用
-|-
Alt+Ctrl+U|上传代码到硬件
Alt+Ctrl+R|验证代码

## 命令行选项

> Ctrl+Shift+P

命令|作用
-|-
Arduino: Board Manager|管理板子上的packages.<br>可以通过Additional Board Manager URLs在板管理器中进行配置来添加第三方Arduino板.
Arduino: Change Baud Rate|更改所选串口的波特率.
Arduino: Change Board Type|更改电路板类型或平台.
Arduino: Close Serial Monitor|停止串行监视器并释放串行端口.
Arduino: Examples|显示示例列表.
Arduino: Initialize|初始化带有Arduino草图的VS Code项目.
Arduino: Library Manager|探索和管理图书馆.
Arduino: Open Serial Monitor|在集成输出窗口中打开串行监视器.
Arduino: Select Serial Port|更改当前串口.
Arduino: Send Text to Serial Port|通过当前串行端口发送一行文本.
Arduino: Upload|构建sketch并上传到Arduino板.
Arduino: Upload Using Programmer|使用外部程序上传.
Arduino: Verify|构建sketch.

## 选项

设置选项方式(按优先级降序排序):

0. 工作区的.vscode/settings.json文件
0. Ctrl+,

选项|描述
-|-
arduino.path|Arduino IDE安装路径自动检测默认值. <br>Arduino的路径,可以通过修改此设置以包含完整路径来使用自定义版本的Arduino.<br>示例:/home/<username>/Downloads/arduino-1.8.1<br>更改后需要重新启动.
arduino.commandPath|可执行文件(或脚本)的路径(相对于arduino.path的相对路径).<br>bin/run-arduino.sh
arduino.additionalUrls|第三方软件包的其他Boards Manager URL.<br>可以在一个字符串中包含多个URL,并使用逗号(,)作为分隔符,<br>或者写为字符串数组.默认值为空.
arduino.logLevel|CLI输出日志级别.<br>可能是'info'(信息)或'verbose'(详细).<br>默认值为"info".<br>
arduino.allowPDEFiletype|允许VSCode Arduino扩展打开1.0.0版本Ardiuno的.pde文件.<br>注意,这将破坏源代码.<br>默认值是false.
arduino.enableUSBDetection|从VSCode Arduino扩展启用/禁用USB检测.<br>默认值为true.<br>当设备插入计算机时,它会弹出一条消息“ Detected board ****, Would you like to switch to this board type”.<br>单击该Yes按钮后,它将自动检测哪个串行端口(COM)连接了USB设备.
arduino.disableTestingOpen|启用/禁用将测试消息自动发送到串行端口以检查打开状态.<br>默认值为false(将发送测试消息).
arduino.skipHeaderProvider|启用/禁用, the extension providing completion items for headers.<br>此功能包含在较新版本的C ++扩展中.默认值为false.
arduino.defaultBaudRate|串口监视器的默认波特率.<br>默认值为115200.<br>支持的值为:300,1200,2400,4800,9600,19200,38400,57600,74880,115200,230400和250000

示例:

```json
{
    "arduino.path": "C:/Program Files (x86)/Arduino",
    "arduino.commandPath": "arduino_debug.exe",
    "arduino.logLevel": "info",
    "arduino.allowPDEFiletype": false,
    "arduino.enableUSBDetection": true,
    "arduino.disableTestingOpen": false,
    "arduino.skipHeaderProvider": false,
    "arduino.additionalUrls": [
        "https://raw.githubusercontent.com/VSChina/azureiotdevkit_tools/master/package_azureboard_index.json",
        "http://arduino.esp8266.com/stable/package_esp8266com_index.json"
    ],
    "arduino.defaultBaudRate": 115200
}
```

以下设置与Arduino扩展的sketch设置相同.可以.vscode/arduino.json在工作区下找到它们:

```json
{
    "sketch": "example.ino",
    "port": "COM5",
    "board": "adafruit:samd:adafruit_feather_m0",
    "output": "../build",
    "debugger": "jlink",
    "prebuild": "bash prebuild.sh"
}
```

配置名|作用
-|-
sketch|Arduino的主草图文件名.
port|连接到设备的串行端口的名称.<br>可以通过Arduino: Select Serial Port命令设置.
board|目前选择的Arduino板别名.<br>可以通过Arduino: Change Board Type命令设置.<br>此外,还可以在那里找到board list.
output|Arduino生成输出路径.<br>如果没有设置,Arduino每次都会创建一个新的临时输出文件夹,这意味着它不能重用先前生成的中间结果,从而导致验证/上载时间过长,因此建议设置该字段.<br>Arduino要求输出路径不应是工作区本身或工作区的子文件夹,否则,它可能无法正常工作.<br>默认情况下,未设置此选项.<br>值得注意的是,此文件的内容可能在生成过程中被删除,因此请选择(或创建)一个目录,该目录将不会存储您要保留的文件.
debugger|当电路板本身没有调试器且有多个调试器可用时将使用的调试器的简称.<br>可以在[此处](https://github.com/Microsoft/vscode-arduino/blob/master/misc/debuggerUsbMapping.json)找到调试器列表.<br>默认情况下,未设置此选项.
prebuild|构建草图文件之前的外部命令.<br>应该只设置一个prebuild命令.<br>command1 && command2格式执行不起作用.<br>如果需要在构建之前运行多个命令,则创建一个脚本.

## 调试Arduino代码

确保现有Arduino板可以与STLink,Jlink或EDBG一起使用.

0. 将电路板正确插入开发机器.对于那些没有板载调试芯片的电路板,需要使用STLink或JLink连接器.
0. 转到调试视图(Ctrl+ Shift+ D).并在源文件中设置断点.
0. 按F5选择调试环境.
0. 当运行到断点时,可以看到变量并可以添加要在Debug Side Bar上观察的表达式.

# 远程开发

## 环境准备

0. 本地机器与目标机器设置ssh免密登录,参考[Linux/Apps/ssh.md](../../Linux/Apps/ssh.md)
0. 安装[Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)插件
0. 本地命令行免密登录,目前需要首次同意连接才可以.

## 使用

### 连接远程目标

0. Ctrl+Shift+P
0. Remote-SSH: Connect to Host
0. Add New SSH Host

# 同步插件及配置

## Settings Sync

> ***目前因[gist](https://gist.github.com/)国内无法访问,需翻墙访问未实际应用, 流程尚未走通.***


### 创建Gist

点击Github右上角头像旁边+号 -> New gist

### 插件安装

0. 安装同步插件Settings Sync.
0. 进入github -> Settings 左侧 -> Developer settings -> Personal access tokens -> Generate a personal access token.
0. 填写Note, 勾选下面的gist, 提交.
0. 复制生成的token.
0. vscode中 Ctrl+Shift+P 输入:Sync: Update/Uplaod Settings,或Shift+Alt+U, <br>在弹窗里输入token, 回车后会生成syncSummary.txt文件.

# 常见问题

## markdown-toc

### 目录展示异常

> The files.eol end of line setting now has a new default value auto. When set to auto, the end of line character for new files is specific to the operating system. It is \r\n on Windows and \n on macOS and Linux. You can also still explicitly set file.eol to \n or \r\n.

# 参考资料

> [Vscode | Writing Java with Visual Studio Code](https://code.visualstudio.com/docs/java/java-tutorial)

> [Vscode | Spring Boot in Visual Studio Code](https://code.visualstudio.com/docs/java/java-spring-boot)

> [Vscode | Running and Debugging Java](https://code.visualstudio.com/docs/java/java-debugging)

> [简书 | VSCode开发SpringBoot](https://www.jianshu.com/p/619567d31311)

> [Vscode | Remote Development using SSH](https://code.visualstudio.com/docs/remote/ssh#_remembering-hosts-you-connect-to-frequently)

> [CNBLOGS | vscode 同步设置及扩展插件](https://www.cnblogs.com/xzqyun/p/10829181.html)

> [CSDN | VS code保存文件后自动删除多余空格](https://blog.csdn.net/Crazy_Sakura/article/details/88707414)

[markdown-toc | 目录展示异常修复参考](https://github.com/AlanWalk/markdown-toc/issues/65)

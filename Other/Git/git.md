<!-- TOC -->

- [说明](#说明)
  - [WIP](#wip)
- [常用操作](#常用操作)
  - [查看信息](#查看信息)
  - [添加到暂存区](#添加到暂存区)
  - [代码提交](#代码提交)
  - [远程同步](#远程同步)
  - [撤销](#撤销)
  - [版本](#版本)
    - [回退](#回退)
  - [分支](#分支)
  - [头指针分离](#头指针分离)
  - [帐号缓存](#帐号缓存)
  - [查看提交内容](#查看提交内容)
- [本地项目初始化并提交远程仓库](#本地项目初始化并提交远程仓库)
  - [初始化本地仓库](#初始化本地仓库)
  - [本地仓库添加远程仓库地址](#本地仓库添加远程仓库地址)
  - [同步远程仓库代码](#同步远程仓库代码)
  - [添加文件到本地仓库并提交](#添加文件到本地仓库并提交)
- [常见问题](#常见问题)
  - [git status显示中文文件名乱码问题](#git-status显示中文文件名乱码问题)
- [参考资料](#参考资料)

<!-- /TOC -->

# 说明

## WIP

> WIP全称：Work in progress,正在工作过程中,引申含义为*目前工作区中的代码正在编写中,这部分代码不能独立运行,是半成品*.


WIP其实代表的就是WIP版本里面的代码是 *正在工作并编写的代码*,意思便是 *代码工作只开发了一半,不能独立的运行*.<br>
直接把这种半成品代码提交commit是极其不合适的,但是我们可以把这部分代码git stash储藏起来,并用WIP给他们做标记.<br>

在GitLab中如果申请合并的title以WIP:开头的合并请求会被认为是暂存:

> Start the title with WIP: to prevent a Work In Progress merge request from being merged before it's ready.

```
WIP: Test
```

# 常用操作

## 查看信息

操作|含义
-|-
git status|显示有变更的文件
git log|显示当前分支的版本历史
git diff|显示暂存区和工作区的代码差异

## 添加到暂存区

```bash
git add options
```
options|含义
-|-
file1 file2 ...|添加指定文件到暂存区
dir1 dir2...|添加指定目录到暂存区,包括子目录
.|添加当前目录及其子目录的所有文件到暂存区

## 代码提交

```bash
git commit options
```

options|含义
-|-
-m message<sup>[1]</sup>|提交暂存区内容到仓库区
-a message<sup>[2]</sup>|提交工作区自上次commit之后的变化,直接到仓库区
-v|提交时显示所有diff信息

[1] :message指对本次提交的一个简要概述,如:<br>
git commit -m "订单完成条件修改"

[2] 通常会使用 git -am message 来提交代码.



## 远程同步

操作|含义
-|-
git push remote branch|上传本地指定分支到远程仓库
git pull remote branch|取回远程仓库的变化,并与本地分支合并

## 撤销

操作|说明
-|-
git checkout .|恢复暂存区的所有文件到工作区

## 版本

### 回退

```
git reset commit_id
```

## 分支

操作|含义
-|-
git branch|列出所有本地分支
git branch -r|列出所有远程分支
git branch -a|列出所有本地分支和远程分支
git branch branch_name|新建一个分支,但依然停留在当前分支
git checkout -b branch_name|新建一个分支,并切换到该分支
git branch -d branch_name|删除分支
git checkout branch_name|切换到指定分支

## 头指针分离
```
# 强制将 master 分支指向当前头指针的位置
$ git branch -f master HEAD
# 检出 master 分支
$ git checkout master
```

## 帐号缓存

在输入帐号密码后:

```
git config --global credential.helper  'cache --timeout 36000000000'
```

36000000000为缓存秒数.

## 查看提交内容

根据 commit id查看该提交修改内容

```
git show commit_id
```

# 本地项目初始化并提交远程仓库

## 初始化本地仓库

```
git init
```

## 本地仓库添加远程仓库地址

```
git remote add 别名 远程地址
```

## 同步远程仓库代码

```
git pull 别名 分支
```

如在github上初始的远程仓库有初始化.gitignore文件,先拉远程代码,可避免提交忽略文件.



## 添加文件到本地仓库并提交

```
git add .
git commit -m "提交说明"
```

# 常见问题

## git status显示中文文件名乱码问题

> core.quotepath 选项为 true 时，会对中文字符进行转义再显示，看起来就像是乱码。设置该选项为false，就会正常显示中文.

```
git config --global core.quotepath false
```

# 参考资料

> [博客园 | 天才卧龙](https://www.cnblogs.com/chenwolong/p/GIT.html)

> [简书 | 危险!分离头指针](https://www.jianshu.com/p/91a0f8feb45d)

> [CSDN | Git中对于"git stash"中的"WIP"缩写的正确理解](https://blog.csdn.net/wq6ylg08/article/details/88965520)

> [廖雪峰的官方网站 | 版本回退](https://www.liaoxuefeng.com/wiki/896043488029600/897013573512192)

> [segmentfault | 解决git status显示中文文件名乱码问题](https://segmentfault.com/a/1190000020807726)
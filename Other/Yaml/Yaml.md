
<!-- TOC -->

- [基本语法规则](#基本语法规则)
- [数据结构](#数据结构)
  - [对象](#对象)
    - [冒号表示法](#冒号表示法)
    - [行内表示法](#行内表示法)
  - [数组](#数组)
    - [连词线表示法](#连词线表示法)
    - [缩进表示法](#缩进表示法)
    - [行内表示法](#行内表示法-1)
  - [纯量](#纯量)
    - [字符串](#字符串)
    - [多行字符串](#多行字符串)
      - [无换行符](#无换行符)
      - [保留/折叠换行符](#保留折叠换行符)
      - [保留/删除末尾换行符](#保留删除末尾换行符)
    - [HTML标记](#html标记)
    - [数值](#数值)
    - [布尔值](#布尔值)
    - [null](#null)
    - [时间](#时间)
    - [日期](#日期)
    - [强转](#强转)
  - [复合结构](#复合结构)
- [引用](#引用)
- [函数和正则表达式的转换](#函数和正则表达式的转换)
- [参考资料](#参考资料)

<!-- /TOC -->

# 基本语法规则

0. 大小写敏感
0. 使用缩进表示层级关系
0. 缩进时不允许使用Tab键,只允许使用空格.
0. 缩进的空格数目不重要,只要相同层级的元素左侧对齐即可
0. \# 表示注释,从这个字符一直到行尾

# 数据结构

YAML支持的数据结构有三种:

0. 对象:键值对的集合,又称为映射(mapping)/ 哈希(hashes) / 字典(dictionary)
0. 数组:一组按次序排列的值,又称为序列(sequence) / 列表(list)
0. 纯量(scalars):单个的、不可再分的值

## 对象

### 冒号表示法

对象的一组键值对,使用冒号结构表示:

```yaml
animal: pets
```

转为 JavaScript 如下:

```js
{ animal: 'pets' }
```

### 行内表示法

Yaml 也允许另一种写法,将所有键值对写成一个行内对象:

```yaml
hash: { name: Steve, foo: bar } 
```

转为 JavaScript 如下:

```js
{ hash: { name: 'Steve', foo: 'bar' } }
```

## 数组

### 连词线表示法

一组连词线开头的行,构成一个数组.

```yaml
- Cat
- Dog
- Goldfish
```

转为 JavaScript 如下:
```js
[ 'Cat', 'Dog', 'Goldfish' ]
```

### 缩进表示法

数据结构的子成员是一个数组,则可以在该项下面缩进一个空格:

```yaml
-
 - Cat
 - Dog
 - Goldfish
```

转为 JavaScript 如下:

```js
[ [ 'Cat', 'Dog', 'Goldfish' ] ]
```

### 行内表示法

数组也可以采用行内表示法:

```yaml
animal: [Cat, Dog]
```

转为 JavaScript 如下:

```js
{ animal: [ 'Cat', 'Dog' ] }
```

## 纯量

纯量是最基本的、不可再分的值.<br>
以下数据类型都属于 JavaScript 的纯量.

0. 字符串
0. 布尔值
0. 整数
0. 浮点数
0. Null
0. 时间
0. 日期

### 字符串

字符串是最常见,也是最复杂的一种数据类型.<br>
字符串默认不使用引号表示.

```yaml
str: 这是一行字符串
```

转为 JavaScript 如下:

```js
{ str: '这是一行字符串' }
```

如果字符串之中包含空格或特殊字符,需要放在引号之中.

```yaml
str: '内容: 字符串'
```

转为 JavaScript 如下:

```js
{ str: '内容: 字符串' }
```

单引号和双引号都可以使用,双引号不会对特殊字符转义.

```yaml
s1: '内容\n字符串'
s2: "内容\n字符串"
```

转为 JavaScript 如下:

```js
{ s1: '内容\\n字符串', s2: '内容\n字符串' }
```

单引号之中如果还有单引号,必须连续使用两个单引号转义.

```yaml
str: 'labor''s day' 
```

转为 JavaScript 如下:

```js
{ str: 'labor\'s day' }
```
### 多行字符串

字符串可以写成多行,从第二行开始,必须有一个单空格缩进.换行符会被转为空格.

#### 无换行符

```yaml
str: 这是一段
  多行
  字符串
```

转为 JavaScript 如下:

```js
{ str: '这是一段 多行 字符串' }
```

#### 保留/折叠换行符

多行字符串可以使用|保留换行符,也可以使用>折叠换行.

```yaml
this: |
  Foo
  Bar
that: >
  Foo
  Bar
```

转为 JavaScript 代码如下:

```js
{ this: 'Foo\nBar\n', that: 'Foo Bar\n' }
```

#### 保留/删除末尾换行符

+表示保留文字块末尾的换行,-表示删除字符串末尾的换行.

```yaml
s1: |
  Foo

s2: |+
  Foo


s3: |-
  Foo
```

转为 JavaScript 代码如下:

```js
{ s1: 'Foo\n', s2: 'Foo\n\n\n', s3: 'Foo' }
```

### HTML标记

字符串之中可以插入 HTML 标记.

```yaml
message: |

  <p style="color: red">
    段落
  </p>
```

转为 JavaScript 如下:

```js
{ message: '\n<p style="color: red">\n  段落\n</p>\n' }
```

### 数值

数值直接以字面量的形式表示.

```yaml
number: 12.30
```

转为 JavaScript 如下:

```js
{ number: 12.30 }
```

### 布尔值

布尔值用true和false表示.

```yaml
isSet: true
```

转为 JavaScript 如下:

```js
{ isSet: true }
```

### null

null用~表示.

```yaml
parent: ~ 
```

转为 JavaScript 如下:

```js
{ parent: null }
```

### 时间

时间采用 ISO8601 格式.

```yaml
iso8601: 2001-12-14t21:59:43.10-05:00 
```

转为 JavaScript 如下:

```js
{ iso8601: new Date('2001-12-14t21:59:43.10-05:00') }
```

### 日期

日期采用复合 iso8601 格式的年、月、日表示.

```yaml
date: 1976-07-31
```

转为 JavaScript 如下:

```js
{ date: new Date('1976-07-31') }
```

### 强转

YAML 允许使用两个感叹号,强制转换数据类型.

```yaml
e: !!str 123
f: !!str true
```

转为 JavaScript 如下:

```js
{ e: '123', f: 'true' }
```

## 复合结构

对象和数组可以结合使用,形成复合结构:

```yaml
languages:
 - Ruby
 - Perl
 - Python 
websites:
 YAML: yaml.org 
 Ruby: ruby-lang.org 
 Python: python.org 
 Perl: use.perl.org 
```

转为 JavaScript 如下:

```js
{ languages: [ 'Ruby', 'Perl', 'Python' ],
  websites: 
   { YAML: 'yaml.org',
     Ruby: 'ruby-lang.org',
     Python: 'python.org',
     Perl: 'use.perl.org' } }
```

# 引用

锚点&和别名*,可以用来引用.

```yaml
defaults: &defaults
  adapter:  postgres
  host:     localhost

development:
  database: myapp_development
  <<: *defaults

test:
  database: myapp_test
  <<: *defaults
```

等同于下面的代码:

```yaml
defaults:
  adapter:  postgres
  host:     localhost

development:
  database: myapp_development
  adapter:  postgres
  host:     localhost

test:
  database: myapp_test
  adapter:  postgres
  host:     localhost
```

&用来建立锚点(defaults),<<表示合并到当前数据,*用来引用锚点.

下面是另一个例子.

```yaml
- &showell Steve 
- Clark 
- Brian 
- Oren 
- *showell 
```

转为 JavaScript 代码如下:

```yaml
[ 'Steve', 'Clark', 'Brian', 'Oren', 'Steve' ]
```

# 函数和正则表达式的转换

这是 JS-YAML 库特有的功能,可以把函数和正则表达式转为字符串:

```yaml
# example.yml
fn: function () { return 1 }
reg: /test/
```

解析上面的 yml 文件的代码如下:

```js
var yaml = require('js-yaml');
var fs   = require('fs');

try {
  var doc = yaml.load(
    fs.readFileSync('./example.yml', 'utf8')
  );
  console.log(doc);
} catch (e) {
  console.log(e);
}
```

从 JavaScript 对象还原到 yaml 文件的代码如下:

```js
var yaml = require('js-yaml');
var fs   = require('fs');

var obj = {
  fn: function () { return 1 },
  reg: /test/
};

try {
  fs.writeFileSync(
    './example.yml',
    yaml.dump(obj),
    'utf8'
  );
} catch (e) {
  console.log(e);
}
```

# 参考资料

> [阮一峰的网络日志 | YAML 语言教程](http://www.ruanyifeng.com/blog/2016/07/yaml.html)